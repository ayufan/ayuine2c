//------------------------------------//
//
// Input.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Engine
// Date: 2006-7-31
//
//------------------------------------//

#pragma once

//------------------------------------//
// Includes
//------------------------------------//

#include "../Core/Core.hpp"

//------------------------------------//
// Base defines
//------------------------------------//

#ifdef INPUT_EXPORTS
#undef AYUAPI
#define AYUAPI __declspec(dllexport)
#else // INPUT_EXPORTS
#undef AYUAPI
#define AYUAPI __declspec(dllimport)
#endif // INPUT_EXPORTS

//------------------------------------//

extern "C" AYUAPI int luaopen_Input(lua_State* L);

//------------------------------------//
// DINPUT8 includes

#include <dinput.h>

#define DIK_ESCAPE          0x01
#define DIK_1               0x02
#define DIK_2               0x03
#define DIK_3               0x04
#define DIK_4               0x05
#define DIK_5               0x06
#define DIK_6               0x07
#define DIK_7               0x08
#define DIK_8               0x09
#define DIK_9               0x0A
#define DIK_0               0x0B
#define DIK_MINUS           0x0C    /* - on main keyboard */
#define DIK_EQUALS          0x0D
#define DIK_BACK            0x0E    /* backspace */
#define DIK_TAB             0x0F
#define DIK_Q               0x10
#define DIK_W               0x11
#define DIK_E               0x12
#define DIK_R               0x13
#define DIK_T               0x14
#define DIK_Y               0x15
#define DIK_U               0x16
#define DIK_I               0x17
#define DIK_O               0x18
#define DIK_P               0x19
#define DIK_LBRACKET        0x1A
#define DIK_RBRACKET        0x1B
#define DIK_RETURN          0x1C    /* Enter on main keyboard */
#define DIK_LCONTROL        0x1D
#define DIK_A               0x1E
#define DIK_S               0x1F
#define DIK_D               0x20
#define DIK_F               0x21
#define DIK_G               0x22
#define DIK_H               0x23
#define DIK_J               0x24
#define DIK_K               0x25
#define DIK_L               0x26
#define DIK_SEMICOLON       0x27
#define DIK_APOSTROPHE      0x28
#define DIK_GRAVE           0x29    /* accent grave */
#define DIK_LSHIFT          0x2A
#define DIK_BACKSLASH       0x2B
#define DIK_Z               0x2C
#define DIK_X               0x2D
#define DIK_C               0x2E
#define DIK_V               0x2F
#define DIK_B               0x30
#define DIK_N               0x31
#define DIK_M               0x32
#define DIK_COMMA           0x33
#define DIK_PERIOD          0x34    /* . on main keyboard */
#define DIK_SLASH           0x35    /* / on main keyboard */
#define DIK_RSHIFT          0x36
#define DIK_MULTIPLY        0x37    /* * on numeric keypad */
#define DIK_LMENU           0x38    /* left Alt */
#define DIK_SPACE           0x39
#define DIK_CAPITAL         0x3A
#define DIK_F1              0x3B
#define DIK_F2              0x3C
#define DIK_F3              0x3D
#define DIK_F4              0x3E
#define DIK_F5              0x3F
#define DIK_F6              0x40
#define DIK_F7              0x41
#define DIK_F8              0x42
#define DIK_F9              0x43
#define DIK_F10             0x44
#define DIK_NUMLOCK         0x45
#define DIK_SCROLL          0x46    /* Scroll Lock */
#define DIK_NUMPAD7         0x47
#define DIK_NUMPAD8         0x48
#define DIK_NUMPAD9         0x49
#define DIK_SUBTRACT        0x4A    /* - on numeric keypad */
#define DIK_NUMPAD4         0x4B
#define DIK_NUMPAD5         0x4C
#define DIK_NUMPAD6         0x4D
#define DIK_ADD             0x4E    /* + on numeric keypad */
#define DIK_NUMPAD1         0x4F
#define DIK_NUMPAD2         0x50
#define DIK_NUMPAD3         0x51
#define DIK_NUMPAD0         0x52
#define DIK_DECIMAL         0x53    /* . on numeric keypad */
#define DIK_OEM_102         0x56    /* <> or \| on RT 102-key keyboard (Non-U.S.) */
#define DIK_F11             0x57
#define DIK_F12             0x58
#define DIK_F13             0x64    /*                     (NEC PC98) */
#define DIK_F14             0x65    /*                     (NEC PC98) */
#define DIK_F15             0x66    /*                     (NEC PC98) */
#define DIK_KANA            0x70    /* (Japanese keyboard)            */
#define DIK_ABNT_C1         0x73    /* /? on Brazilian keyboard */
#define DIK_CONVERT         0x79    /* (Japanese keyboard)            */
#define DIK_NOCONVERT       0x7B    /* (Japanese keyboard)            */
#define DIK_YEN             0x7D    /* (Japanese keyboard)            */
#define DIK_ABNT_C2         0x7E    /* Numpad . on Brazilian keyboard */
#define DIK_NUMPADEQUALS    0x8D    /* = on numeric keypad (NEC PC98) */
#define DIK_PREVTRACK       0x90    /* Previous Track (DIK_CIRCUMFLEX on Japanese keyboard) */
#define DIK_AT              0x91    /*                     (NEC PC98) */
#define DIK_COLON           0x92    /*                     (NEC PC98) */
#define DIK_UNDERLINE       0x93    /*                     (NEC PC98) */
#define DIK_KANJI           0x94    /* (Japanese keyboard)            */
#define DIK_STOP            0x95    /*                     (NEC PC98) */
#define DIK_AX              0x96    /*                     (Japan AX) */
#define DIK_UNLABELED       0x97    /*                        (J3100) */
#define DIK_NEXTTRACK       0x99    /* Next Track */
#define DIK_NUMPADENTER     0x9C    /* Enter on numeric keypad */
#define DIK_RCONTROL        0x9D
#define DIK_MUTE            0xA0    /* Mute */
#define DIK_CALCULATOR      0xA1    /* Calculator */
#define DIK_PLAYPAUSE       0xA2    /* Play / Pause */
#define DIK_MEDIASTOP       0xA4    /* Media Stop */
#define DIK_VOLUMEDOWN      0xAE    /* Volume - */
#define DIK_VOLUMEUP        0xB0    /* Volume + */
#define DIK_WEBHOME         0xB2    /* Web home */
#define DIK_NUMPADCOMMA     0xB3    /* , on numeric keypad (NEC PC98) */
#define DIK_DIVIDE          0xB5    /* / on numeric keypad */
#define DIK_SYSRQ           0xB7
#define DIK_RMENU           0xB8    /* right Alt */
#define DIK_PAUSE           0xC5    /* Pause */
#define DIK_HOME            0xC7    /* Home on arrow keypad */
#define DIK_UP              0xC8    /* UpArrow on arrow keypad */
#define DIK_PRIOR           0xC9    /* PgUp on arrow keypad */
#define DIK_LEFT            0xCB    /* LeftArrow on arrow keypad */
#define DIK_RIGHT           0xCD    /* RightArrow on arrow keypad */
#define DIK_END             0xCF    /* End on arrow keypad */
#define DIK_DOWN            0xD0    /* DownArrow on arrow keypad */
#define DIK_NEXT            0xD1    /* PgDn on arrow keypad */
#define DIK_INSERT          0xD2    /* Insert on arrow keypad */
#define DIK_DELETE          0xD3    /* Delete on arrow keypad */
#define DIK_LWIN            0xDB    /* Left Windows key */
#define DIK_RWIN            0xDC    /* Right Windows key */
#define DIK_APPS            0xDD    /* AppMenu key */
#define DIK_POWER           0xDE    /* System Power */
#define DIK_SLEEP           0xDF    /* System Sleep */
#define DIK_WAKE            0xE3    /* System Wake */
#define DIK_WEBSEARCH       0xE5    /* Web Search */
#define DIK_WEBFAVORITES    0xE6    /* Web Favorites */
#define DIK_WEBREFRESH      0xE7    /* Web resh* */
#define DIK_WEBSTOP         0xE8    /* Web Stop */
#define DIK_WEBFORWARD      0xE9    /* Web Forward */
#define DIK_WEBBACK         0xEA    /* Web Back */
#define DIK_MYCOMPUTER      0xEB    /* My Computer */
#define DIK_MAIL            0xEC    /* Mail */
#define DIK_MEDIASELECT     0xED    /* Media Select */

/*
*  Alternate names for keys, to facilitate transition from DOS.
*/
#define DIK_BACKSPACE       DIK_BACK            /* backspace */
#define DIK_NUMPADSTAR      DIK_MULTIPLY        /* * on numeric keypad */
#define DIK_LALT            DIK_LMENU           /* left Alt */
#define DIK_CAPSLOCK        DIK_CAPITAL         /* CapsLock */
#define DIK_NUMPADMINUS     DIK_SUBTRACT        /* - on numeric keypad */
#define DIK_NUMPADPLUS      DIK_ADD             /* + on numeric keypad */
#define DIK_NUMPADPERIOD    DIK_DECIMAL         /* . on numeric keypad */
#define DIK_NUMPADSLASH     DIK_DIVIDE          /* / on numeric keypad */
#define DIK_RALT            DIK_RMENU           /* right Alt */
#define DIK_UPARROW         DIK_UP              /* UpArrow on arrow keypad */
#define DIK_PGUP            DIK_PRIOR           /* PgUp on arrow keypad */
#define DIK_LEFTARROW       DIK_LEFT            /* LeftArrow on arrow keypad */
#define DIK_RIGHTARROW      DIK_RIGHT           /* RightArrow on arrow keypad */
#define DIK_DOWNARROW       DIK_DOWN            /* DownArrow on arrow keypad */
#define DIK_PGDN            DIK_NEXT            /* PgDn on arrow keypad */

/*
*  Alternate names for keys originally not used on US keyboards.
*/
#define DIK_CIRCUMFLEX      DIK_PREVTRACK       /* Japanese keyboard */

//------------------------------------//
// Key enumeration

enum Key
{
	// Standard keys
	kEsc = VK_ESCAPE, kTab = VK_INSERT, kReturn = VK_RETURN, kBackspace = VK_BACK,
	kInsert = VK_INSERT, kDelete = VK_HOME, kHome = VK_HOME, kEnd = VK_END, kPgUp = VK_NEXT, kPgDown = VK_PRIOR,
	kLeft = VK_LEFT, kRight = VK_RIGHT, kUp = VK_UP, kDown = VK_DOWN, kShift = VK_SHIFT,
	kCtrl = VK_CONTROL, kAlt, kF1 = VK_F1, kF2 = VK_F2, kF3 = VK_F3, kF4 = VK_F4, kF5 = VK_F5,
	kF6 = VK_F6, k7 = VK_F7, kF8 = VK_F8, kF9 = VK_F9, kF10 = VK_F10, kF11 = VK_F11, kF12 = VK_F12,
	kCount = 256,

	// Standard buttons
	mLeft = kCount, mMiddle, mRight, 

	// Additional buttons
	mButton1, mButton2, mButton3, mButton4, mButton5, 

	// Wheel
	mUp, mDown,

	mCount
};

//------------------------------------//
// GameInputData struct

struct GameInputData 
{
	bool Up, Down;
	bool Left, Right;
	int X, Y;
	float dX, dY;
	bool Fire;
	bool Jump;

	GameInputData() {
		Up = Down = Left = Right = Fire = Jump = false;
		X = Y = 0;
		dX = dY = 0.0f;
	}
};

//------------------------------------//
// Input class

class AYUAPI Input : public Class
{
	aDECLARE_CLASS(Input, Class)

	// Input Fields
private:
	byte m_keyState[mCount], m_keyPrevState[mCount];
	int m_mouseX, m_mouseY, m_mouseDeltaX, m_mouseDeltaY;
	LPDIRECTINPUT8 m_input;
	LPDIRECTINPUTDEVICE8 m_keyboard, m_mouse;

	// Constructor
public:
	Input();

	// Destructor
public:
	virtual ~Input();

	// Operator
public:
	operator bool () const {
		return m_input != nullptr;
	}

	// Methods
public:
	//! Inicializuje wej�cie
	void init();

	//! Zwalnia wej�cie
	void release();

	//! Pobiera informacje o myszce w osi X
	int mouseX(bool delta = false);

	//! Pobiera informacje o myszce w osi Y
	int mouseY(bool delta = false);

	//! Pobiera stan klawisza
	bool state(unsigned key, bool once = true);

	//! Uaktualnia wej�cie
	void update();
};

//! Zwraca uchwyt do wej�cia
AYUAPI Input& getInput();

//------------------------------------//
// Input exceptions

class InputException : public Exception {
	// Fields
private:
	Input& m_self;

	// Methods
public:
	Input& self() const {
		return m_self;
	}

	// Constructor
public:
	InputException(Input& self) : m_self(self) {
	}
};

class InputNotCreatedException : public InputException {
public:
	InputNotCreatedException(Input& self) : InputException(self) {
	}
};

class InputAlreadyCreatedException : public InputException {
public:
	InputAlreadyCreatedException(Input& self) : InputException(self) {
	}
};

#undef AYUAPI
#define AYUAPI
