TEMPLATE = subdirs
CONFIG += ordered

# Moc extensions
SUBDIRS += ExtMoc

# Base modules
SUBDIRS += Core
SUBDIRS += Math
SUBDIRS += Sound
SUBDIRS += Render
SUBDIRS += Physics
SUBDIRS += MaterialSystem
SUBDIRS += Pipeline
SUBDIRS += MeshSystem
SUBDIRS += SceneGraph
SUBDIRS += GameSystem

# Editor modules
SUBDIRS += Editor
SUBDIRS += Render.Editor
SUBDIRS += MeshSystem.Editor
SUBDIRS += MaterialSystem.Editor
SUBDIRS += SceneGraph.Editor

# Extensions
SUBDIRS += NifReader

# Launcher
SUBDIRS += Launcher
SUBDIRS += Test

# Dependencies
Core.depends = ExtMoc
Math.depends = Core
Sound.depends = Math Core
Render.depends = Math Core
Physics.depends = Math
MaterialSystem.depends = Math Core Render
Pipeline.depends = $$MaterialSystem.depends MaterialSystem
MeshSystem.depends = $$Pipeline.depends Physics
SceneGraph.depends = $$MeshSystem.depends MeshSystem
GameSystem.depends = $$SceneGraph.depends SceneGraph

Editor.depends = $$Pipeline.depends
Render.Editor.depends = $$Render.depends Editor
MaterialSystem.Editor.depends = $$MaterialSystem.depends MeshSystem Editor
MeshSystem.Editor.depends = $$MeshSystem.depends Editor
SceneGraph.Editor.depends = $$SceneGraph.depends Editor GameSystem

NifReader.depends = $$SceneGraph.depends

Launcher.depends = $$GameSystem.depends Editor
Test.depends = Math Core
