TARGET = MeshSystem.Editor
TEMPLATE = lib
CONFIG -= precompile_header
CONFIG += editor

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lPipeline -lMaterialSystem -lMeshSystem -lEditor

HEADERS += \
    MeshViewer.hpp

SOURCES += \
    MeshViewer.cpp

FORMS += \
    MeshViewer.ui
