//------------------------------------//
//
// StdAfx.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: Test
// Date: 2011-05-05
//
//------------------------------------//

#pragma once

#include "../Math/Math.hpp"
#include "../Core/Core.hpp"
#include "../Render/Render.hpp"
#include "../MaterialSystem/MaterialSystem.hpp"
#include "../Engine/Engine.hpp"

#include "Test.hpp"

