#include <Math/Vec2.hpp>
#include <Core/ClassInfo.hpp>
#include <windows.h>
#include <QDebug>

int main(int argc, char* argv[])
{
  vec2 test;
  QVariant v = qVariantFromValue(test);
  if(GadgetInfo::staticIsGadget(v))
  {
    qDebug() << v << "is gadget";
    qDebug() << "x" << GadgetInfo::staticProperty(v, "x");
    qDebug() << "y" << GadgetInfo::staticProperty(v, "y");
    GadgetInfo::staticSetProperty(v, "x", 10.0f);
    qDebug() << "x" << GadgetInfo::staticProperty(v, "x");
    qDebug() << "y" << GadgetInfo::staticProperty(v, "y");
    QVariant normalized = GadgetInfo::staticProperty(v, "normalize");
    qDebug() << "normalized" << normalized;
    qDebug() << "x" << GadgetInfo::staticProperty(normalized, "x");
    qDebug() << "y" << GadgetInfo::staticProperty(normalized, "y");
  }
  return 0;
}
