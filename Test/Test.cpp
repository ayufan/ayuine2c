#include "StdAfx.hpp"
#include "../Lua/LuaLib.hpp"

extern int luaopen_Test (lua_State* tolua_S);

const char* vec3::staticClassName() {
	return "vec3";
}

void testToluaType(const char* name, const ToluaType& type) {
	printf("Type %s has%s been found.\n", name, type ? "" : " not");
	if(!type)
		return;

	printf("Type %s is%s enum.\n", name, type.isEnum() ? "" : " not");
	printf("Type %s is%s class.\n", name, type.isClass() ? "" : " not");
	printf("Type %s is%s struct.\n", name, type.isStruct() ? "" : " not");
	printf("Type %s is%s struct.\n", name, type.isStruct() ? "" : " not");
	printf("Type %s has%s base.\n", name, type.getBaseType() ? "" : " not");
	printf("Type %s has%s new method.\n", name, type.getMethod("new") ? "" : " not");
	printf("Type %s has%s new_local method.\n", name, type.getMethod("new_local") ? "" : " not");
	printf("Type %s has%s X property.\n", name, type.getProperty("X") ? "" : " not");
	printf("Property %s::X is%s readonly.\n", name, type.getProperty("X").isReadOnly() ? "" : " not");
	printf("Property %s::X is%s writeonly.\n", name, type.getProperty("X").isWriteOnly() ? "" : " not");
	printf("Property %s::X is%s readwrite.\n", name, type.getProperty("X").isReadWrite() ? "" : " not");

	vector<string> methods;
	type.getMethods(methods, true);
	for(int i = 0; i < methods.size(); ++i)
		printf("Type %s has method %s\n", name, methods[i].c_str());

	vector<string> properties;
	type.getProperties(properties, true);
	for(int i = 0; i < properties.size(); ++i)
		printf("Type %s has property %s\n", name, properties[i].c_str());
}

class TestApp : public wxApp
{
	// Methods
public:
	bool OnInit();
};

DECLARE_APP(TestApp);
IMPLEMENT_APP_CONSOLE(TestApp);

bool TestApp::OnInit() {
	getRender().setWindow(new wxFrame(nullptr, 1, "TestApp"));
	getRender().init();

	ToluaState L(ToluaState::newState());

	L.openLibs(false, false, true);

	luaopen_Math(L);
	luaopen_Core(L);
	luaopen_Test(L);
	luaopen_Render(L);
	luaopen_MaterialSystem(L);
	luaopen_Engine(L);

	if(ToluaEnumerator enumerator = L.getGlobals())
	{
		ToluaGuardStack stack(L, 2);

		while(ToluaKeyValue keyValue = enumerator)
		{
			printf("%s - %s\n", keyValue.key.getString().c_str(), keyValue.value.getString().c_str());
		}
	}

	if(ToluaEnumerator types = L.getTypes())
	{
		while(ToluaType type = types)
		{
			ToluaGuardStack stack(L);
			if(type.isClass() || type.isStruct())
				printf("type: %s\n", type.getName().c_str());
		}
		printf("End of type enumeration.");
	}

 	testToluaType("MaterialShaderBlock", ToluaType(L, "MaterialShaderBlock"));

	char buffer[10000];
	while(!feof(stdin)) 
	{
		printf("> ");
		fgets(buffer, 1000, stdin);
		if(!strlen(buffer))
			continue;

		try 
		{
			ToluaFunction function = L.loadFromString(buffer);
			if(function)
			{
				function();
			}
		}
		catch(std::exception& e) 
		{
			printf("%s\n", e.what());
		}
	}

	// Zwolnij wszystkie zasoby
	Resource::disablePreservedAll();

	// Zwolnij engine
	getRender().release();	

	lua_close(L);
	return false;
}

