TEMPLATE	= app
TARGET		= ExtMoc

CONFIG -= precompile_header

DEFINES	+= QT_MOC

include(../ayuine2c.pri)

INCLUDEPATH += $$PWD

HEADERS += \
    parser.h \
    outputrevision.h \
    moc.h \
    generator.h

SOURCES += \
    parser.cpp \
    moc.cpp \
    main.cpp \
    generator.cpp \
    preprocessor.cpp
