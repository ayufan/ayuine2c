#include <CoreFrames/ResourcePreviewGenerator.hpp>
#include <Render/Texture.hpp>
#include <QPixmap>

class TexturePreviewGenerator : public ResourcePreviewGenerator
{
public:
  bool generate(const QString &fileName, const QMetaObject *metaObject, QPixmap &preview)
  {
    if(metaObject == &Texture::staticMetaObject)
    {
      QPixmap pixmap(fileName);

      if(pixmap.isNull())
      {
        if(QSharedPointer<Texture> texture = Resource::load<Texture>(fileName))
        {
          preview = QPixmap::fromImage(texture->toImage());
        }
      }
      else
      {
        preview = pixmap;
      }

      return true;
    }

    return false;
  }
};

static TexturePreviewGenerator texturePreviewGenerator;
