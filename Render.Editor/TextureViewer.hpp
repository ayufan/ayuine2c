#ifndef TEXTUREVIEWER_HPP
#define TEXTUREVIEWER_HPP

#include <QMainWindow>
#include <CoreFrames/ResourceEditor.hpp>

namespace Ui {
class TextureViewer;
}

class Texture;

class TextureViewer : public ResourceEditor
{
  Q_OBJECT

public:
  Q_INVOKABLE explicit TextureViewer(QWidget *parent = 0);
  ~TextureViewer();

public:
  const QMetaObject* resourceType() const;
  QSharedPointer<Resource> resource() const;
  bool setResource(const QSharedPointer<Resource> &newResource);

private slots:
  void updateScaledContents(bool checked);

private:
  Ui::TextureViewer *ui;
  QSharedPointer<Texture> m_texture;
};

#endif // TEXTUREVIEWER_HPP
