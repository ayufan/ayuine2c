#include "RenderShader.hpp"
#include "RenderShader_p.hpp"
#include "RenderSystem.hpp"
#include <Core/Resource.hpp>
#include <QDebug>
#include <d3dx9.h>

RenderShader::RenderShader(const QByteArray& shaderCompiled) :
  QObject(&getRenderSystem()), d_ptr(new RenderShaderPrivate)
{
  d_ptr->create(shaderCompiled);
}

RenderShader::RenderShader(CompileFromCode, const QByteArray& shaderCode, const QByteArray& shaderTechnique) :
  QObject(&getRenderSystem()), d_ptr(new RenderShaderPrivate)
{
  d_ptr->createFromCode(shaderCode, shaderTechnique);
}

RenderShader::RenderShader(CompileFromFile, const QString& shaderFile, const QByteArray& shaderTechnique) :
  QObject(&getRenderSystem()), d_ptr(new RenderShaderPrivate)
{
  d_ptr->createFromFile(shaderFile, shaderTechnique);
}


RenderShader::~RenderShader()
{
}

struct RenderShaderInclude : public ID3DXInclude
{
  STDMETHOD(Open)(THIS_ D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, unsigned int* pBytes)
  {
    QFile file(Resource::absoluteFilePath(pFileName));

    if(file.open(QFile::ReadOnly))
    {
      QByteArray out = file.readAll();
      void* data = malloc(out.size());
      *pBytes = out.size();
      *ppData = memcpy(data, out.constData(), out.size());
      return S_OK;
    }
    else
    {
      *ppData = "";
      *pBytes = 0;
      return S_FALSE;
    }
  }
  STDMETHOD(Close)(THIS_ LPCVOID pData) {
    free((void*)pData);
    return S_OK;
  }
};

static QByteArray toByteArray(const boost::intrusive_ptr<ID3DXBuffer>& buffer)
{
  if(buffer)
    return QByteArray((const char*)buffer->GetBufferPointer(), buffer->GetBufferSize());
  return QByteArray();
}

static QByteArray compileShader(const QByteArray& shaderCode, const QByteArray& shaderTechnique,
                                const QByteArray& shaderProfile, const QByteArray& shaderEntry)
{
  boost::intrusive_ptr<ID3DXBuffer> shaderOutput, shaderError;

  QByteArray scopeDef = shaderEntry.toUpper();
  QByteArray entryDef = shaderTechnique + shaderEntry;

  D3DXMACRO macros[] =
  {
    {scopeDef.constData(), shaderProfile.constData()},
    {entryDef.constData(), shaderEntry.constData()},
    {NULL, NULL}
  };

  RenderShaderInclude shaderInclude;

  if(FAILED(D3DXCompileShader(shaderCode.constData(), shaderCode.length(),
                              macros, &shaderInclude,
                              shaderEntry.constData(), shaderProfile.constData(),
                              D3DXSHADER_USE_LEGACY_D3DX9_31_DLL/* | D3DXSHADER_PARTIALPRECISION | D3DXSHADER_OPTIMIZATION_LEVEL3*/,
                              (LPD3DXBUFFER*)&shaderOutput, (LPD3DXBUFFER*)&shaderError,
                              NULL)))
  {
    qWarning() << "[SHADER] Compile Error " << toByteArray(shaderError);
    return QByteArray();
  }

  return toByteArray(shaderOutput);
}

inline ShaderCompiled compileShaders(const QByteArray& shaderCode, const QByteArray& shaderTechnique)
{
  ShaderCompiled compiled;
  compiled.m_vertexCode = compileShader(shaderCode, shaderTechnique, D3DXGetVertexShaderProfile(getRenderSystem()), "VertexShader");
  compiled.m_pixelCode = compileShader(shaderCode, shaderTechnique, D3DXGetPixelShaderProfile(getRenderSystem()), "PixelShader");
  return compiled;
}

bool RenderShaderPrivate::create()
{
  if(!m_compiled)
    return false;

  if(dxe(getRenderSystem()->CreateVertexShader((const DWORD*)m_compiled.m_vertexCode.constData(), (IDirect3DVertexShader9**)&m_vertexShader)) &&
      dxe(getRenderSystem()->CreatePixelShader((const DWORD*)m_compiled.m_pixelCode.constData(), (IDirect3DPixelShader9**)&m_pixelShader)))
  {
    m_shaderUsedConsts.resize(256);
    m_shaderUsedSamplers.resize(16);

    if(dxe(D3DXGetShaderConstantTable((const DWORD*)m_compiled.m_vertexCode.constData(),
                               (LPD3DXCONSTANTTABLE*)&m_vertexShaderConsts)))
    {
      process(m_vertexShaderConsts.get(), VertexShader);
    }

    if(dxe(D3DXGetShaderConstantTable((const DWORD*)m_compiled.m_pixelCode.constData(),
                               (LPD3DXCONSTANTTABLE*)&m_pixelShaderConsts)))
    {
      process(m_pixelShaderConsts.get(), PixelShader);
    }
  }

  return false;
}

bool RenderShaderPrivate::create(const QByteArray& shaderCompiled)
{
  QDataStream ds(shaderCompiled);
  ds >> m_compiled;
  return create();
}

bool RenderShaderPrivate::createFromCode(const QByteArray& shaderCode, const QByteArray& shaderTechnique)
{
  m_compiled = compileShaders(shaderCode, shaderTechnique);
  if(!m_compiled)
    return false;
  return create();
}

bool RenderShaderPrivate::createFromFile(const QString &shaderFile, const QByteArray &shaderTechnique)
{
  QFile file(Resource::absoluteFilePath(shaderFile));

  if(file.open(QFile::ReadOnly))
  {
    qDebug() << "[SHADER] Compiling " << shaderFile << " as " << shaderTechnique;
    return createFromCode(file.readAll(), shaderTechnique);
  }
  return false;
}

RenderShaderPrivate::RenderShaderPrivate()
{
}

RenderShaderPrivate::~RenderShaderPrivate()
{
}

int RenderShader::constsCount()
{
  return d_ptr->m_shaderConsts.size();
}

const char * RenderShader::constName(unsigned index)
{
  if(index >= (unsigned)d_ptr->m_shaderConsts.size())
    return NULL;
  return d_ptr->m_shaderConsts[index].name.constData();
}

unsigned RenderShader::constSize(unsigned index)
{
  if(index >= (unsigned)d_ptr->m_shaderConsts.size())
    return NULL;
  return qMax(d_ptr->m_shaderConsts[index].pixelSize, d_ptr->m_shaderConsts[index].vertexSize);
}

bool RenderShader::isValid() const
{
  return d_ptr->m_vertexShader.get() && d_ptr->m_pixelShader.get();
}

void RenderShaderPrivate::processSampler(const D3DXCONSTANT_DESC& desc, Type type)
{
  ShaderSamplerDesc refSampler;
  refSampler.name = desc.Name;

  ShaderSamplerDesc* samplerDesc = NULL;

  QVector<ShaderSamplerDesc>::const_iterator itor = qFind(m_shaderSamplers, refSampler);

  if(itor == m_shaderSamplers.end())
  {
    m_shaderSamplers.append(ShaderSamplerDesc());
    samplerDesc = &m_shaderSamplers.last();
    samplerDesc->name = desc.Name;
  }
  else
  {
    samplerDesc = const_cast<ShaderSamplerDesc*>(&*itor);
  }

  switch(type)
  {
  case VertexShader:
    samplerDesc->vertexIndex = desc.RegisterIndex;
    break;

  case PixelShader:
    samplerDesc->pixelIndex = desc.RegisterIndex;
    if(desc.RegisterIndex < m_shaderUsedSamplers.size())
      m_shaderUsedSamplers.setBit(desc.RegisterIndex);
    break;
  }
}

void RenderShaderPrivate::processConst(const D3DXCONSTANT_DESC& desc, Type type, ShaderConstDesc::Type constType)
{
  ShaderConstDesc refConst;
  refConst.name = desc.Name;

  ShaderConstDesc* shaderConst = NULL;

  QVector<ShaderConstDesc>::const_iterator itor = qFind(m_shaderConsts, refConst);

  if(itor == m_shaderConsts.end())
  {
    m_shaderConsts.append(ShaderConstDesc());
    shaderConst = &m_shaderConsts.last();
    shaderConst->name = desc.Name;
    shaderConst->type = constType;
  }
  else
  {
    shaderConst = const_cast<ShaderConstDesc*>(&*itor);

    if(shaderConst->type != constType)
    {
      // type mismatch
      return;
    }
  }

  switch(type)
  {
  case VertexShader:
    shaderConst->vertexOffset = desc.RegisterIndex;
    shaderConst->vertexCount = desc.RegisterCount;
    shaderConst->vertexSize = desc.Bytes;
    if(desc.RegisterIndex < m_shaderUsedConsts.size())
      m_shaderUsedConsts.setBit(desc.RegisterIndex);
    break;

  case PixelShader:
    shaderConst->pixelOffset = desc.RegisterIndex;
    shaderConst->pixelCount = desc.RegisterCount;
    shaderConst->pixelSize = desc.Bytes;
    if(desc.RegisterIndex < m_shaderUsedConsts.size())
      m_shaderUsedConsts.setBit(desc.RegisterIndex);
    break;
  }
}

void RenderShaderPrivate::process(ID3DXConstantTable *constantTable, Type type)
{
  D3DXCONSTANTTABLE_DESC tableDesc;

  if(FAILED(constantTable->GetDesc(&tableDesc)))
    return;

  for(unsigned i = 0; i < tableDesc.Constants; ++i)
  {
    D3DXHANDLE handle;
    handle = constantTable->GetConstant(NULL, i);
    if(!handle)
      continue;

    D3DXCONSTANT_DESC desc;
    UINT descSize = sizeof(desc);
    if(FAILED(constantTable->GetConstantDesc(handle, &desc, &descSize)))
      continue;

    // qDebug() << "[SHADER] ProcessConst " << desc.Name << desc.RegisterIndex;

    switch(desc.RegisterSet)
    {
    case D3DXRS_FLOAT4:
      processConst(desc, type, ShaderConstDesc::Float4);
      break;

    case D3DXRS_SAMPLER:
      processSampler(desc, type);
      break;

    default:
      // not supported
      continue;
    }
  }

  qSort(m_shaderConsts);
  qSort(m_shaderSamplers);
}

void RenderShaderPrivate::unlink(QObject *object)
{
  for(int i = 0; i < m_shaderConsts.size(); ++i)
  {
  }
  for(int i = 0; i < m_shaderSamplers.size(); ++i)
  {
  }
}

int RenderShader::samplerCount()
{
  return d_ptr->m_shaderSamplers.size();
}

const char * RenderShader::samplerName(unsigned index)
{
  if(index >= (unsigned)d_ptr->m_shaderSamplers.size())
    return NULL;
  return d_ptr->m_shaderSamplers[index].name;
}

bool RenderShader::isConstUsed(unsigned index) const
{
  if(d_ptr->m_shaderUsedConsts.size() <= index)
    return false;
  return d_ptr->m_shaderUsedConsts.testBit(index);
}

bool RenderShader::isSamplerUser(unsigned index) const
{
  if(d_ptr->m_shaderUsedSamplers.size() <= index)
    return false;
  return d_ptr->m_shaderUsedSamplers.testBit(index);
}
