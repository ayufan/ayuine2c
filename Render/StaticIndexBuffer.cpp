#include "StaticIndexBuffer.hpp"
#include "Render.hpp"
#include "RenderSystem.hpp"
#include "IndexBuffer_p.hpp"

StaticIndexBuffer::StaticIndexBuffer(const unsigned short* indicies, unsigned indexCount)
{
  Q_ASSERT(indicies);
  Q_ASSERT(indexCount);
  d_ptr->create(indices, indexCount, Short);
}

StaticIndexBuffer::StaticIndexBuffer(const unsigned* indicies, unsigned indexCount)
{
  Q_ASSERT(indicies);
  Q_ASSERT(indexCount);
  d_ptr->create(indices, indexCount, Long);
}

StaticIndexBuffer::StaticIndexBuffer(const QVector<unsigned short>& indicies)
{
  d_ptr->create(indices.constData(), indicies.size(), Short);
}

StaticIndexBuffer::StaticIndexBuffer(const QVector<unsigned>& indicies)
{
  d_ptr->create(indices.constData(), indicies.size(), Long);
}
