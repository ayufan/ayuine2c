#ifndef RENDER_P_HPP
#define RENDER_P_HPP

#include <windows.h>
#include <boost/intrusive_ptr.hpp>

extern bool dxe(long hr);

inline void intrusive_ptr_add_ref(IUnknown* ptr)
{
  ptr->AddRef();
}

inline void intrusive_ptr_release(IUnknown* ptr)
{
  ptr->Release();
}

#endif // RENDER_P_HPP
