#ifndef TEXTURE_P_HPP
#define TEXTURE_P_HPP

#include "Render_p.hpp"
#include <QScopedPointer>
#include <QImage>
#include <QPointer>

class RenderTexture;

class TexturePrivate
{
public:
  QPointer<RenderTexture> m_texture;
  QByteArray m_data;
  QImage m_image;
  QString m_lazyFileName;

public:
  TexturePrivate();
  ~TexturePrivate();
};

#endif // TEXTURE_P_HPP
