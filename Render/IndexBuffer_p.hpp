#ifndef INDEXBUFFER_P_HPP
#define INDEXBUFFER_P_HPP

#include "Render_p.hpp"
#include "IndexBuffer.hpp"

struct IDirect3DIndexBuffer9;

class IndexBufferPrivate
{
public:
  boost::intrusive_ptr<IDirect3DIndexBuffer9> m_handle;
  unsigned m_count;
  unsigned m_stride;
  IndexBuffer::Format m_format;
  bool m_dynamic;
  bool m_locked;
  unsigned m_offset;

public:
  IndexBufferPrivate();
  ~IndexBufferPrivate();
  bool create(unsigned count, IndexBuffer::Format format, bool dynamic);
  bool create(const void* data, unsigned count, IndexBuffer::Format format);
  void* lock(unsigned offset, unsigned count, bool nooverwrite = false);
  void unlock();
};

#endif // INDEXBUFFER_P_HPP
