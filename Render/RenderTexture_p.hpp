#ifndef RENDERTEXTURE_P_HPP
#define RENDERTEXTURE_P_HPP

#include "Render_p.hpp"
#include "RenderTexture.hpp"

#include <Math/MathLib.hpp>

struct IDirect3DBaseTexture9;
struct IDirect3DSurface9;

class RenderTexturePrivate
{
public:
  boost::intrusive_ptr<IDirect3DBaseTexture9> m_handle;
  unsigned m_width, m_height, m_depth;
  unsigned m_type;
  unsigned m_usage;
  RenderTexture::Format m_format;

public:
  RenderTexturePrivate();
  ~RenderTexturePrivate();
  boost::intrusive_ptr<IDirect3DSurface9> surface(CubeFace::Enum side = CubeFace::PX);
  bool create(const QString& fileName);
  bool create(const QByteArray& data, const QString& source = QString());
};

#endif // RENDERTEXTURE_P_HPP
