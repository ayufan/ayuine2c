#ifndef VERTEXBUFFER_HPP
#define VERTEXBUFFER_HPP

#include <QObject>
#include <QScopedPointer>
#include "Render.hpp"

typedef struct _D3DVERTEXELEMENT9 D3DVERTEXELEMENT9;

class VertexBufferPrivate;

class RENDER_EXPORT VertexBuffer : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(VertexBuffer)
  Q_FLAGS(Type Types)

public:
  enum Type
  {
    Vertex = 0,
    Coords = 1,
    Coords2 = 2,
    Color = 4,
    Normal = 8,
    Tangent = 16,
    Size = 32,
    Instanced = 64,
    InstanceColor = 128
  };
  Q_DECLARE_FLAGS(Types, Type)

  enum { Max = 256, StrideOffset = 16 };

protected:
  explicit VertexBuffer();
  explicit VertexBuffer(const void* vertices, unsigned count, unsigned stride);
  explicit VertexBuffer(unsigned count, unsigned stride);
public:
  ~VertexBuffer();

public slots:
  unsigned stride() const;
  unsigned count() const;
  unsigned size() const;

public:
  void set(const void* data, unsigned count, unsigned offset = 0);

public:
  bool isLocked() const;
  void* lock(unsigned count, unsigned offset = 0);
  void unlock();

public:
  static Types encodeStride(Types type, unsigned stride = 0);
  static Types decodeType(Types type);
  static unsigned decodeStride(Types type);
  static unsigned stride(Types type);
  static unsigned instanceStride(Types type);
  static unsigned decl(Types type, D3DVERTEXELEMENT9* declList);

protected:
  QScopedPointer<VertexBufferPrivate> d_ptr;

  friend class RenderSystem;
};

template<typename T>
class StaticVertexBuffer : public VertexBuffer
{
public:
  explicit StaticVertexBuffer(const T* vertices, unsigned count)
    : VertexBuffer(vertices, count, sizeof(T))
  {
  }
  explicit StaticVertexBuffer(const QVector<T>& vertices)
    : VertexBuffer(vertices.constData(), vertices.size(), sizeof(T))
  {
  }
  explicit StaticVertexBuffer(unsigned count)
    : VertexBuffer(count, sizeof(T))
  {
  }

public:
  void set(const T* data, unsigned count, unsigned offset = 0)
  {
    VertexBuffer::set(data, count, offset);
  }

  T* lock(unsigned count, unsigned offset = 0)
  {
    return (T*)VertexBuffer::lock(count, offset);
  }

  void set(const QVector<T>& data, unsigned offset = 0)
  {
    set(data.constData(), data.size(), offset);
  }
};

class BaseDynamicVertexBuffer : public VertexBuffer
{
  Q_OBJECT

public:
  explicit BaseDynamicVertexBuffer(unsigned count, unsigned stride);

public:
  int append(const void* data, unsigned count);

public:
  void* lockAppend(unsigned count, int& offset);

private slots:
  void deviceLost();
  void deviceReset();
};

template<typename T>
class DynamicVertexBuffer : public BaseDynamicVertexBuffer
{
public:
  explicit DynamicVertexBuffer(unsigned count)
    : BaseDynamicVertexBuffer(count, sizeof(T))
  {
  }

public:
  void set(const T* data, unsigned count, unsigned offset = 0)
  {
    BaseDynamicVertexBuffer::set(data, count, offset);
  }

  void set(const QVector<T>& data, unsigned offset = 0)
  {
    set(data.constData(), data.size(), offset);
  }

  int append(const T* data, unsigned count)
  {
    return BaseDynamicVertexBuffer::append(data, count);
  }

  int append(const QVector<T>& data)
  {
    return append(data.constData(), data.size());
  }

  T* lock(unsigned count, unsigned offset = 0)
  {
    return (T*)BaseDynamicVertexBuffer::lock(count, offset);
  }

  T* lockAppend(unsigned count, unsigned offset = 0)
  {
    return (T*)BaseDynamicVertexBuffer::lock(count, offset);
  }
};

#endif // VERTEXBUFFER_HPP
