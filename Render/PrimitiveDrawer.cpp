#include "PrimitiveDrawer.hpp"

#include <Core/Serializer.hpp>

Q_IMPLEMENT_METATYPE(PrimitiveDrawer)

Q_IMPLEMENT_METATYPE(IndexedPrimitiveDrawer)
