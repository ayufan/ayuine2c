#ifndef RENDERSHADER_HPP
#define RENDERSHADER_HPP

#include <QObject>
#include "Render.hpp"
#include <Core/ObjectRef.hpp>

class RenderShaderPrivate;

class RENDER_EXPORT RenderShader : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(RenderShader)

public:
  enum CompileFromCode
  {
    Code
  };

  enum CompileFromFile
  {
    File
  };

public:
  explicit RenderShader(const QByteArray& shaderCompiled);
  explicit RenderShader(CompileFromCode, const QByteArray& shaderCode, const QByteArray& shaderTechnique = QByteArray());
  explicit RenderShader(CompileFromFile, const QString& shaderFile, const QByteArray& shaderTechnique = QByteArray());
  ~RenderShader();

public:
  int constsCount();
  const char* constName(unsigned index);
  unsigned constSize(unsigned index);

  bool isValid() const;

public:
  int samplerCount();
  const char* samplerName(unsigned index);

public:
  bool isConstUsed(unsigned index) const;
  bool isSamplerUser(unsigned index) const;

private:
  QScopedPointer<RenderShaderPrivate> d_ptr;

  friend class RenderSystem;
};

Q_DECLARE_OBJECTREF(RenderShader)

#endif // RENDERSHADER_HPP
