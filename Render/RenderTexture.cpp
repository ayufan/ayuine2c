#include "RenderTexture.hpp"
#include "RenderTexture_p.hpp"
#include "RenderSystem.hpp"
#include <d3d9.h>
#include <d3dx9.h>
#include <QDebug>
#include <QImage>
#include <QBitmap>

D3DFORMAT toFormat(RenderTexture::Format format)
{
  switch(format)
  {
  case RenderTexture::Alpha:     return D3DFMT_A8;
  case RenderTexture::Rgb:       return D3DFMT_X8R8G8B8;
  case RenderTexture::Rgba:      return D3DFMT_A8R8G8B8;
  case RenderTexture::RFloat:    return D3DFMT_R16F;
  case RenderTexture::RgFloat:   return D3DFMT_G16R16F;
  case RenderTexture::RgbaFloat: return D3DFMT_A16B16G16R16F;
  case RenderTexture::DXT1:      return D3DFMT_DXT1;
  case RenderTexture::DXT2:      return D3DFMT_DXT2;
  case RenderTexture::DXT3:      return D3DFMT_DXT3;
  case RenderTexture::DXT4:      return D3DFMT_DXT4;
  case RenderTexture::DXT5:      return D3DFMT_DXT5;
  case RenderTexture::Rgba10:    return D3DFMT_A2B10G10R10;
  default:                     return D3DFMT_A8R8G8B8;
  }
}

RenderTexture::RenderTexture() :
  QObject(&getRenderSystem()), d_ptr(new RenderTexturePrivate)
{
}

RenderTexture::~RenderTexture()
{
}

unsigned RenderTexture::width() const
{
  return d_ptr->m_width;
}

unsigned RenderTexture::height() const
{
  return d_ptr->m_height;
}

unsigned RenderTexture::depth() const
{
  return d_ptr->m_depth;
}

RenderTexture::Format RenderTexture::format() const
{
  return d_ptr->m_format;
}

bool RenderTexture::isDynamic() const
{
  return (d_ptr->m_usage & D3DUSAGE_DYNAMIC) != 0;
}

bool RenderTexture::isTarget() const
{
  return (d_ptr->m_usage & D3DUSAGE_RENDERTARGET) != 0;
}

bool RenderTexture::isDynamicOrTarget() const
{
  return isDynamic() || isTarget();
}

bool RenderTexture::isCube() const
{
  return d_ptr->m_type == D3DRTYPE_CUBETEXTURE;
}

bool RenderTexture::isPlain() const
{
  return d_ptr->m_type == D3DRTYPE_TEXTURE;
}

bool RenderTexture::isVolume() const
{
  return d_ptr->m_type == D3DRTYPE_VOLUMETEXTURE;
}

boost::intrusive_ptr<IDirect3DSurface9> RenderTexturePrivate::surface(CubeFace::Enum side)
{
  if(!m_handle)
    return NULL;

  HRESULT hr;
  boost::intrusive_ptr<IDirect3DSurface9> surface;

  switch(m_type)
  {
  case D3DRTYPE_TEXTURE:
    hr = boost::static_pointer_cast<IDirect3DTexture9>(m_handle)->GetSurfaceLevel(0, (LPDIRECT3DSURFACE9*)&surface);
    break;

  case D3DRTYPE_CUBETEXTURE:
    hr = boost::static_pointer_cast<IDirect3DCubeTexture9>(m_handle)->GetCubeMapSurface((D3DCUBEMAP_FACES)side, 0, (LPDIRECT3DSURFACE9*)&surface);
    break;

  default:
    return NULL;
  }

  if(SUCCEEDED(hr))
    return surface;
  return NULL;
}

bool RenderTexturePrivate::create(const QString &fileName)
{
  QFile file(Resource::absoluteFilePath(fileName));

  if(file.open(QFile::ReadOnly))
  {
    return create(file.readAll(), fileName);
  }

  qWarning() << "[TEXTURE] File Not Found: " << fileName;
  return false;
}

inline unsigned powerOf2(unsigned x)
{
  unsigned xx;
  for(xx = 1; xx < x; xx <<= 1);
  return xx;
}

bool RenderTexturePrivate::create(const QByteArray& data, const QString& source)
{
  const unsigned TextureQuality = 0;
  const unsigned TextureMinimalSize = 0;
  const bool TextureCompression = false;
  const bool TextureScale = false;

  D3DXIMAGE_INFO info;

  // Wczytaj informacje o teksturze
  if(!dxe(D3DXGetImageInfoFromFileInMemory(data.constData(), data.size(), &info)))
  {
    qWarning() << "[TEXTURE] Load Error: " << source;
    return false;
  }

  // Przeskaluj tekstur�
  info.Width = TextureScale ? qMax(powerOf2(info.Width) >> TextureQuality, TextureMinimalSize) : info.Width;
  info.Height = TextureScale ? qMax(powerOf2(info.Height) >> TextureQuality, TextureMinimalSize) : info.Height;
  info.Depth = TextureScale ? qMax(powerOf2(info.Depth) >> TextureQuality, TextureMinimalSize) : info.Depth;

  // Dodaj kompresj�
  if(TextureCompression)
  {
    switch(info.Format)
    {
    case D3DFMT_DXT1:
    case D3DFMT_DXT2:
    case D3DFMT_DXT3:
    case D3DFMT_DXT4:
    case D3DFMT_DXT5:
      break;

    default:
      info.Format = D3DFMT_DXT5;
      break;
    }
  }

  HRESULT hr;

  switch(info.ResourceType) {
  case D3DRTYPE_TEXTURE:
    hr = D3DXCreateTextureFromFileInMemoryEx(getRenderSystem(), data.constData(), data.size(), info.Width, info.Height, D3DX_DEFAULT, 0, info.Format, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, (LPDIRECT3DTEXTURE9*)(&m_handle));
    break;

  case D3DRTYPE_CUBETEXTURE:
    // Utw�rz tekstur�
    hr = D3DXCreateCubeTextureFromFileInMemoryEx(getRenderSystem(), data.constData(), data.size(), info.Width, D3DX_DEFAULT, 0, (D3DFORMAT)info.Format, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, (LPDIRECT3DCUBETEXTURE9*)(&m_handle));
    break;

  case D3DRTYPE_VOLUMETEXTURE:
    // Utw�rz tekstur�
    hr = D3DXCreateVolumeTextureFromFileInMemoryEx(getRenderSystem(), data.constData(), data.size(), info.Width, info.Height, info.Depth, D3DX_DEFAULT, 0, (D3DFORMAT)info.Format, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, (LPDIRECT3DVOLUMETEXTURE9*)(&m_handle));
    break;

  default:
    qWarning() << "[TEXTURE] Not Supported: " << source;
    return false;
  }

  if(SUCCEEDED(hr))
  {
    m_width = info.Width;
    m_height = info.Height;
    m_depth = info.Depth;
    // TODO: m_format
    m_type = info.ResourceType;
    m_usage = 0;
    return true;
  }
  return false;
}

RenderTexturePrivate::RenderTexturePrivate()
{
  m_type = 0;
  m_usage = 0;
  m_format = RenderTexture::Undefined;
  m_width = 0;
  m_height = 0;
  m_depth = 0;
}

RenderTexturePrivate::~RenderTexturePrivate()
{
}

bool RenderTexture::isValid() const
{
  return d_ptr->m_handle;
}

QImage RenderTexture::toImage() const
{
  if(!isPlain() && !isCube())
      return QImage();

  QImage image(width(), height(), QImage::Format_ARGB32);
  if(image.isNull())
      return QImage();

  boost::intrusive_ptr<IDirect3DSurface9> destSurface;
  if(FAILED(getRenderSystem()->CreateOffscreenPlainSurface(width(), height(), D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, (LPDIRECT3DSURFACE9*)&destSurface, NULL)))
      return QImage();

  if(FAILED(D3DXLoadSurfaceFromSurface(destSurface.get(), NULL, NULL, d_ptr->surface().get(), NULL, NULL, D3DX_DEFAULT, 0)))
      return QImage();

  D3DLOCKED_RECT lockedRect;
  if(FAILED(destSurface->LockRect(&lockedRect, NULL, D3DLOCK_READONLY)))
    return QImage();

  memcpy(image.scanLine(0), lockedRect.pBits, lockedRect.Pitch * image.height());
  destSurface->UnlockRect();
  return image;
}

bool RenderTexture::saveToFile(const QString &fileName)
{
  if(!d_ptr->m_handle)
    return false;

  // Zapisz tekstur� do pliku
  return dxe(D3DXSaveTextureToFileA(qPrintable(fileName), D3DXIFF_DDS, d_ptr->m_handle.get(), NULL));
}

RenderTexture * RenderTexture::fromFile(const QString &fileName)
{
  QScopedPointer<RenderTexture> newTexture(new RenderTexture());

  if(newTexture->d_ptr->create(fileName))
  {
    newTexture->setObjectName(fileName);
    return newTexture.take();
  }
  return NULL;
}

RenderTexture * RenderTexture::fromMemory(const QByteArray &data, const QString &source)
{
  QScopedPointer<RenderTexture> newTexture(new RenderTexture());

  if(newTexture->d_ptr->create(data, source))
  {
    newTexture->setObjectName(source);
    return newTexture.take();
  }
  return NULL;
}

RenderTexture * RenderTexture::fromImage(const QImage &image)
{
  QScopedPointer<RenderTexture> newTexture(new RenderTexture());

  QImage im = image.convertToFormat(QImage::Format_ARGB32);

  if(dxe(getRenderSystem()->CreateTexture(im.width(), im.height(), 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, (LPDIRECT3DTEXTURE9*)&newTexture->d_ptr->m_handle, NULL)))
  {
    LPDIRECT3DTEXTURE9 texture = (LPDIRECT3DTEXTURE9)newTexture->d_ptr->m_handle.get();

    D3DLOCKED_RECT rect;

    if(FAILED(texture->LockRect(0, &rect, 0, 0)))
    {
        qWarning() << "[TEXTURE] Unable to lock texture rect.";
        return NULL;
    }

    DWORD *dst = (DWORD *)rect.pBits;
    DWORD *src = (DWORD *)im.scanLine(0);

    Q_ASSERT((rect.Pitch/4) == (im.bytesPerLine()/4));
    memcpy(dst, src, rect.Pitch*im.height());
    texture->UnlockRect(0);

#if 0
    texture->GenerateMipSubLevels();
#endif

    newTexture->d_ptr->m_width = im.width();
    newTexture->d_ptr->m_height = im.height();
    newTexture->d_ptr->m_format = Rgba;
    newTexture->d_ptr->m_type = D3DRTYPE_TEXTURE;
    newTexture->d_ptr->m_usage = 0;

    return newTexture.take();
  }

  return NULL;
}
