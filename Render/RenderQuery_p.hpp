#ifndef RENDERQUERY_P_HPP
#define RENDERQUERY_P_HPP

#include "Render_p.hpp"
#include "RenderQuery.hpp"

struct IDirect3DQuery9;

class RenderQueryPrivate
{
public:
  boost::intrusive_ptr<IDirect3DQuery9> m_query;
  RenderQuery::Type m_type;
  bool m_running;
  unsigned m_result;

public:
  RenderQueryPrivate();
  bool create(RenderQuery::Type type);
  bool begin();
  void end();
  bool flush();
};

#endif // RENDERQUERY_P_HPP
