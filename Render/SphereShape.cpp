#include "RenderSystem.hpp"
#include "RenderSystem_p.hpp"
#include "PrimitiveDrawer.hpp"

#include <Math/Box.hpp>
#include <Math/Matrix.hpp>

struct Face
{
  ushort a, b, c;

  Face()
  {
  }

  Face(ushort aa, ushort bb, ushort cc)
  {
    a = aa;
    b = bb;
    c = cc;
  }
};

typedef QPair<quint16, quint16> VertexConnection;

static ushort middle(QVector<vec3>& verts, QHash<VertexConnection, ushort> &connections, ushort a, ushort b)
{
  if(connections.contains(VertexConnection(a, b)))
  {
    return connections[VertexConnection(a,b)];
  }
  else if(connections.contains(VertexConnection(b, a)))
  {
    return connections[VertexConnection(b,a)];
  }
  else
  {
    ushort index = verts.size();
    verts.append(vec3::lerp(verts[a], verts[b]).normalize());
    connections[VertexConnection(a,b)] = index;
    return index;
  }
}

void RenderSystem::restoreSphere()
{
  const int MaxLevels = 5;

  QVector<vec3> verts;
  QVector<quint16> indices;
  QHash<VertexConnection, ushort> connections;

  float t = (1.0f + qSqrt(5.0f)) / 2.0f;
  verts.append(vec3(-1,  t,  0).normalize());
  verts.append(vec3( 1,  t,  0).normalize());
  verts.append(vec3(-1, -t,  0).normalize());
  verts.append(vec3( 1, -t,  0).normalize());
  verts.append(vec3( 0, -1,  t).normalize());
  verts.append(vec3( 0,  1,  t).normalize());
  verts.append(vec3( 0, -1, -t).normalize());
  verts.append(vec3( 0,  1, -t).normalize());
  verts.append(vec3( t,  0, -1).normalize());
  verts.append(vec3( t,  0,  1).normalize());
  verts.append(vec3(-t,  0, -1).normalize());
  verts.append(vec3(-t,  0,  1).normalize());

  // 5 faces around point 0
  indices << 0 << 11 << 5;
  indices << 0 << 5 << 1;
  indices << 0 << 1 << 7;
  indices << 0 << 7 << 10;
  indices << 0 << 10 << 11;

  // 5 adjacent faces
  indices << 1 << 5 << 9;
  indices << 5 << 11 << 4;
  indices << 11 << 10 << 2;
  indices << 10 << 7 << 6;
  indices << 7 << 1 << 8;

  // 5 faces around point 3
  indices << 3 << 9 << 4;
  indices << 3 << 4 << 2;
  indices << 3 << 2 << 6;
  indices << 3 << 6 << 8;
  indices << 3 << 8 << 9;

  // 5 adjacent faces
  indices << 4 << 9 << 5;
  indices << 2 << 4 << 11;
  indices << 6 << 2 << 10;
  indices << 8 << 6 << 7;
  indices << 9 << 8 << 1;

  d_ptr->m_sphereLevels.clear();
  d_ptr->m_sphereLevels.append(IndexedPrimitiveDrawer(PrimitiveDrawer::TriList,
                                                      0, verts.size(),
                                                      0, indices.size()));

  unsigned offset = 0;
  unsigned count = indices.size();

  for(int i = 1; i < MaxLevels; i++)
  {
    for(int j = offset; j < count; j += 3)
    {
      quint16* face = &indices[j];

      ushort aa = face[0];
      ushort bb = face[1];
      ushort cc = face[2];
      ushort ab = middle(verts, connections, aa, bb);
      ushort bc = middle(verts, connections, bb, cc);
      ushort ca = middle(verts, connections, cc, aa);

      indices << aa << ab << ca;
      indices << bb << bc << ab;
      indices << cc << ca << bc;
      indices << ab << bc << ca;
    }

    offset = count;
    count = indices.size();

    d_ptr->m_sphereLevels.append(IndexedPrimitiveDrawer(PrimitiveDrawer::TriList,
                                                        0, verts.size(),
                                                        offset, (count - offset) / 3));
  }

  IndexBuffer::stripify(indices, d_ptr->m_sphereLevels);

  d_ptr->m_sphereVertexBuffer.reset(new StaticVertexBuffer<vec3>(verts));
  d_ptr->m_sphereIndexBuffer.reset(new ShortStaticIndexBuffer(indices));
}

void RenderSystem::lostSphere()
{
  d_ptr->m_sphereVertexBuffer.reset();
  d_ptr->m_sphereIndexBuffer.reset();
  d_ptr->m_sphereLevels.clear();
}

bool RenderSystem::drawSphere(const matrix &transform, unsigned level)
{
  if(d_ptr->m_sphereLevels.isEmpty())
    return false;

  setTransform(transform);

  if(setVertexBuffer(VertexBuffer::Vertex, d_ptr->m_sphereVertexBuffer.data()) &&
     setIndexBuffer(d_ptr->m_sphereIndexBuffer.data()))
  {

    draw(d_ptr->m_sphereLevels[qMin<unsigned>(level, d_ptr->m_sphereLevels.size()-1)]);
    return true;
  }

  return false;
}

bool RenderSystem::drawSphere(const sphere& bounds, unsigned level)
{
  return drawSphere(bounds.toTransform(), level);
}

bool RenderSystem::drawSphere(const sphere& bounds, const matrix& transform, unsigned level)
{
  matrix newTransform;
  matrix::multiply(newTransform, transform, bounds.toTransform());
  return drawSphere(newTransform, level);
}
