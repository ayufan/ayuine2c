#ifndef RENDERSHADERCONSTS_HPP
#define RENDERSHADERCONSTS_HPP

#include <QObject>

class VertexShaderConst
{
  Q_GADGET
  Q_ENUMS(Const)

public:
  enum Const
  {
    Frame = 0,        // [1] [pixelSizeX, pixelSizeY, 0, time]
    ProjView = 1,     // [4]
    Object = 5,       // [3] [m11 m21 m31 m41] [m12 m22 m32 m42] [m13 m23 m33 m43]

    ViewAt = 9,       // [1]
    ViewUp = 10,       // [1]
    ViewRight = 11,   // [1]
    ViewOrigin = 12,  // [1]
    ProjViewObject = 13
  };
};

class PixelShaderConst
{
  Q_GADGET
  Q_ENUMS(Const)

public:
  enum Const
  {
    Frame = 0,			// [1] <see>vscFrame</see>
    Color = 1,			// [1]
    InvProjView = 2,    // [4]

    MaterialConst = 20,
    LightConsts = 40
  };

  enum
  {
    MaxMaterialConsts = 20,
    MaxLightConsts = 20
  };

  enum Sampler
  {
    MaterialSampler = 0,
    LightSampler = 4
  };

  enum
  {
    MaxMaterialSamplers = 4,
    MaxLightSamplers = 2
  };
};

#endif // RENDERSHADERCONSTS_HPP
