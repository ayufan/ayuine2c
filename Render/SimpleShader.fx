uniform float4x4 vProjViewObjectMatrix : register(c13);
uniform float4 vColor : register(c1);
uniform sampler diffuseTexture : register(s0);

// FixedConst

float4 FixedConstVertexShader(
    in float3 vertexOrigin : POSITION0) : POSITION0
{
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedConstPixelShader() : COLOR0
{
    return vColor;
}

// FixedDiffuse

float4 FixedDiffuseVertexShader(
    in float3 vertexOrigin : POSITION0,
    in float4 color : COLOR0,
    out float4 l_color : TEXCOORD0) : POSITION0
{
    l_color = color;
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedDiffusePixelShader(in float4 color : TEXCOORD0) : COLOR0
{
    return color;
}

// FixedTexture

float4 FixedTextureVertexShader(
    in float3 vertexOrigin : POSITION0,
    in float2 texCoords : TEXCOORD0,
    out float2 l_texCoords : TEXCOORD0) : POSITION0
{
    l_texCoords = texCoords;
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedTexturePixelShader(in float2 texCoords : TEXCOORD0) : COLOR0
{
    return tex2D(diffuseTexture, texCoords);
}

// FixedTextureConst

float4 FixedTextureConstVertexShader(
    in float3 vertexOrigin : POSITION0,
    in float2 texCoords : TEXCOORD0,
    out float2 l_texCoords : TEXCOORD0) : POSITION0
{
    l_texCoords = texCoords;
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedTextureConstPixelShader(in float2 texCoords : TEXCOORD0) : COLOR0
{
    return tex2D(diffuseTexture, texCoords) * float4(vColor.rgb, 1.0f);
}

// FixedTextureDiffuse

float4 FixedTextureDiffuseVertexShader(
    in float3 vertexOrigin : POSITION0,
    in float2 texCoords : TEXCOORD0,
    out float2 l_texCoords : TEXCOORD0,
    in float4 color : COLOR0,
    out float4 l_color : TEXCOORD1) : POSITION0
{
    l_texCoords = texCoords;
    l_color = color;
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedTextureDiffusePixelShader(
    in float2 texCoords : TEXCOORD0,
    in float4 color : TEXCOORD1) : COLOR0
{
    return tex2D(diffuseTexture, texCoords) * float4(color.rgb, 1.0f);
}

// FixedMaskedColor

float4 FixedMaskedColorVertexShader(
    in float3 vertexOrigin : POSITION0,
    in float2 texCoords : TEXCOORD0,
    out float2 l_texCoords : TEXCOORD0,
    in float4 color : COLOR0,
    out float4 l_color : TEXCOORD1) : POSITION0
{
    l_texCoords = texCoords;
    l_color = color;
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedMaskedColorPixelShader(
    in float2 texCoords : TEXCOORD0,
    in float4 color : TEXCOORD1) : COLOR0
{
    return float4(color.rgb, tex2D(diffuseTexture, texCoords).a);
}

// FixedMaskedConst

float4 FixedMaskedConstVertexShader(
    in float3 vertexOrigin : POSITION0,
    in float2 texCoords : TEXCOORD0,
    out float2 l_texCoords : TEXCOORD0) : POSITION0
{
    l_texCoords = texCoords;
    return mul(float4(vertexOrigin, 1.0f), vProjViewObjectMatrix);
}

float4 FixedMaskedConstPixelShader(
    in float2 texCoords : TEXCOORD0) : COLOR0
{
    return float4(vColor.rgb, tex2D(diffuseTexture, texCoords).a);
}
