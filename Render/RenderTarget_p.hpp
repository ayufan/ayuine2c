#ifndef RENDERTARGET_P_HPP
#define RENDERTARGET_P_HPP

#include "RenderTarget.hpp"

#include <Math/MathLib.hpp>

struct IDirect3DSurface9;

class RenderTargetPrivate
{
public:
  boost::intrusive_ptr<IDirect3DSurface9> m_depthTarget;
  BaseRenderTarget::DepthFormat m_depthFormat;

public:
  RenderTargetPrivate();
  ~RenderTargetPrivate();
};

#endif // RENDERTARGET_P_HPP
