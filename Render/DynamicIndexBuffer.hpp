#ifndef DYNAMICINDEXBUFFER_HPP
#define DYNAMICINDEXBUFFER_HPP

#include "IndexBuffer.hpp"

class DynamicIndexBuffer : public IndexBuffer
{
  Q_OBJECT

public:
  explicit DynamicIndexBuffer(unsigned indexCount, Format format);

public:
  void set(const unsigned void* data, unsigned count, unsigned offset = 0);
  int append(const unsigned void* data, unsigned count);
};

template<typename T>
class DynamicIndexBufferT : public DynamicIndexBuffer
{
public:
  DynamicIndexBufferT(unsigned indexCount)
    : DynamicIndexBuffer(indexCount, (Format)sizeof(T))
  {}

public:
  void set(const T* data, unsigned count, unsigned offset = 0)
  {
    DynamicIndexBuffer::set(data, count, offset);
  }

  int append(const T* data, unsigned count)
  {
    return DynamicIndexBuffer::set(data, count);
  }
};

typedef DynamicIndexBufferT<unsigned short> ShortDynamicIndexBuffer;
typedef DynamicIndexBufferT<unsigned long> LongDynamicIndexBuffer;

#endif // DYNAMICINDEXBUFFER_HPP
