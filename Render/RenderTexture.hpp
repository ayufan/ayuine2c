#ifndef RENDERTEXTURE_HPP
#define RENDERTEXTURE_HPP

#include <QObject>

#include <Core/Resource.hpp>

#include "Render.hpp"

class QImage;
class RenderTexturePrivate;

class RENDER_EXPORT RenderTexture : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(RenderTexture)
  Q_ENUMS(Format)
  Q_PROPERTY(Format format READ format)
  Q_PROPERTY(unsigned width READ width)
  Q_PROPERTY(unsigned height READ height)
  Q_PROPERTY(unsigned depth READ depth)

public:
  enum Format
  {
    Undefined,
    Alpha,
    Rgb,
    Rgba,
    RFloat,
    RgFloat,
    RgbaFloat,
    DXT1,
    DXT2,
    DXT3,
    DXT4,
    DXT5,
    Rgba10
  };

protected:
  explicit RenderTexture();

public:
  ~RenderTexture();

public:
  unsigned width() const;
  unsigned height() const;
  unsigned depth() const;
  Format format() const;
  bool isDynamic() const;
  bool isTarget() const;
  bool isDynamicOrTarget() const;
  bool isCube() const;
  bool isPlain() const;
  bool isVolume() const;
  bool isValid() const;

public:
  virtual QImage toImage() const;
  bool saveToFile(const QString& fileName);

public:
  static RenderTexture* fromFile(const QString& fileName);
  static RenderTexture* fromMemory(const QByteArray& data, const QString& source = QString());
  static RenderTexture* fromImage(const QImage& image);

protected:
  QScopedPointer<RenderTexturePrivate> d_ptr;

public:
  friend class RenderSystem;
};

typedef QSharedPointer<RenderTexture> RenderTextureRef;

Q_DECLARE_METATYPE(RenderTextureRef)

#endif // RENDERTEXTURE_HPP
