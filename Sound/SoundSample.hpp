//------------------------------------//
// 
// SoundSample.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#pragma once

#include <QVector>
#include <Core/Resource.hpp>
#include "Sound.hpp"

//------------------------------------//
// SoundSample class

class SOUND_EXPORT SoundSample : public Resource
{
  Q_OBJECT

  // Fields
private:
  unsigned m_buffer;
  QByteArray m_data;

  // Constructor
public:
  SoundSample();
  SoundSample(const QByteArray& data);

  // Destructor
public:
  virtual ~SoundSample();

public:
  //! Zwraca bufor do kt�rego przypisany jest sampel
  unsigned buffer() const {
    return m_buffer;
  }

  //! Zwraca cz�stotliwo�� sampla
  unsigned frequency();

  //! Zwraca ilo�� bit�w sampla
  unsigned bits();

  //! Zwraca ilo�� kana��w sampla
  unsigned channels();

  //! Zwraca rozmiar sampla
  unsigned size();

  //! Zwraca czas sampla
  float length();
};

