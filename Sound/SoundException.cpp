//------------------------------------//
// 
// SoundException.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#include "StdAfx.hpp"

//------------------------------------//
// SoundException: Constructor

SoundException::SoundException(unsigned errCode) : m_errCode(errCode) {
}

//------------------------------------//
// SoundException: Destructor

SoundException::~SoundException() throw() {
}

//------------------------------------//
// SoundException: Methods

string SoundException::message() const {
	return va("ErrorCode: %i", m_errCode);
}

//------------------------------------//
// SoundException: Functions

