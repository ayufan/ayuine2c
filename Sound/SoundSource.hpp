//------------------------------------//
// 
// SoundSource.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Sound
// Date: 2008-03-08
// 
//------------------------------------//

#pragma once

#include <QObject>
#include <QSharedPointer>
#include <Math/Vec3.hpp>
#include "Sound.hpp"

class SoundSample;

//------------------------------------//
// SoundSource class

class SOUND_EXPORT SoundSource : public QObject
{
  Q_OBJECT

  // Fields
protected:
  //! �r�d�o d�wi�ku
  unsigned m_source;

  // U�ywany sampel
  QSharedPointer<SoundSample> m_sample;

  //! Pozycje
  mutable vec3 m_origin, m_direction, m_velocity;

  // Constructor
public:
  SoundSource(const QSharedPointer<SoundSample>& sample);

  // Destructor
public:
  ~SoundSource();

  // Base methods
public:
  //! Zwraca indeks bufora
  unsigned source() const {
    return m_source;
  }

  virtual void play();
  virtual void pause();
  virtual void stop();
  virtual void rewind();
  virtual bool stopped();
  virtual bool paused();
  virtual float length();

  // Source methods
public:
  float pitch() const;
  float gain() const;
  float gainMin() const;
  float gainMax() const;
  float distance() const;
  float coneOuterGain() const;
  float coneInnerAngle() const;
  float coneOuterAngle() const;
  const vec3& origin() const;
  const vec3& velocity() const;
  const vec3& direction() const;
  bool looping() const;
  bool relative() const;
  void setPitch(float pitch);
  void setGain(float g);
  void setGainMin(float gmin);
  void setGainMax(float gmax);
  void setDistance(float maxDistance);
  void setConeOuterGain(float gain);
  void setConeInnerAngle(float angle);
  void setConeOuterAngle(float angle = 360.0f);
  void setOrigin(const vec3 &pt);
  void setVelocity(const vec3 &vel);
  void setDirection(const vec3 &dir);
  void setLooping(bool loop = false);
  void setRelative(bool relative = false);
};
