TARGET = SceneGraph.Editor
TEMPLATE = lib
CONFIG -= precompile_header
CONFIG += editor

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lPipeline -lMaterialSystem -lMeshSystem -lSceneGraph -lEditor -lGameSystem

HEADERS += \
    SceneEditor.hpp \
    SceneItemModel.hpp

SOURCES += \
    SceneEditor.cpp \
    SceneItemModel.cpp \
    SceneEditorGraphics.cpp \
    SceneEditorDebugView.cpp \
    SceneEditorSelection.cpp \
    SceneEditorMove.cpp \
    SceneEditorRotate.cpp \
    SceneEditorColorPick.cpp

FORMS += \
    SceneEditor.ui

RESOURCES += \
    SceneGraph.qrc

LIBS += -lUser32
