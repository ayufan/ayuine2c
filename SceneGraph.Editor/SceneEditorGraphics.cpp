#include "SceneEditor.hpp"
#include "SceneItemModel.hpp"

#include <CoreFrames/MetaObjectItemModel.hpp>

#include "CameraController/CameraController.hpp"
#include "CameraController/PerspectiveController.hpp"

#include <Math/Vec3.hpp>
#include <Math/mathlib.hpp>

#include <Render/RenderSystem.hpp>
#include <Render/RenderComponentWidget.hpp>

#include <Pipeline/Frame.hpp>
#include <Pipeline/ColorPickFrame.hpp>
#include <Pipeline/Fragment.hpp>
#include <Pipeline/InstancedFragment.hpp>
#include <Pipeline/GridFragment.hpp>
#include <Pipeline/SheetFragment.hpp>
#include <Pipeline/AmbientLight.hpp>

#include <SceneGraph/Scene.hpp>

class SceneDebugView : public DebugView
{
public:
  SceneDebugView(const QSet<Actor*>& selected,
                 const QSet<Actor*>& hidden,
                 const QSet<const QMetaObject*>& types)
    : m_selected(selected), m_hidden(hidden), m_types(types)
  {
  }

public:
  bool isSelected(QObject* object) const
  {
    if(m_selected.contains((Actor*)object))
      return true;

    if(Actor* actor = qobject_cast<Actor*>(object))
    {
      for(ActorManager* manager = actor->parentActor().data(); manager; manager = manager->parentActor().data())
      {
        if(manager->grouped() && m_selected.contains(manager))
        {
          return true;
        }
      }
    }
    return false;
  }

  bool isHidden(QObject* object) const
  {
    if(m_hidden.contains((Actor*)object))
      return true;
    if(m_types.contains(object->metaObject()))
      return false;
    return true;
  }

private:
  QSet<Actor*> m_selected;
  QSet<Actor*> m_hidden;
  QSet<const QMetaObject*> m_types;
};

static void addGridFragment(Frame& frame, Axis::Enum axis, const vec3& origin, float gridSize, float bounds = 50)
{
  QScopedPointer<GridFragment> gridFragment(new GridFragment());

  vec3 up, at, right;
  cubeFaceVectors(axis, at, up, right);

  if(fabsf(origin.X) > (1<<10) || fabsf(origin.Y) > (1<<10) || fabsf(origin.Z) > (1<<10))
    return;

  vec3 originAligned = origin.round(gridSize);

  QPointF origin2(originAligned ^ right, originAligned ^ up);
  QPointF bounds2(bounds, bounds);

  gridFragment->setOrder(-1000);
  gridFragment->setSize(gridSize);
  gridFragment->setOffset(at * (origin ^ at));
  gridFragment->setBounds(QRectF(origin2 - bounds2, origin2 + bounds2));
  gridFragment->setAxis(axis);
  gridFragment->setShowText(false);

  frame.addFragment(gridFragment.take());
}

void SceneEditor::prepareComponents()
{
  FrameEditor::prepareComponents();

  m_frame->clear();

  if(!m_sceneItemModel->scene())
    return;

  SceneDebugView debugView(selected(), m_hidden, m_actorTypesItemModel->checked());
  m_frame->setDebugView(&debugView);

  if(ColorPickFrame* colorPickFrame = qobject_cast<ColorPickFrame*>(m_frame.data()))
    m_sceneItemModel->scene()->generateColorPickFrame(*colorPickFrame);
  else if(actionEditMode->isChecked())
    m_sceneItemModel->scene()->generateDebugFrame(*m_frame);
  else
    m_sceneItemModel->scene()->generateFrame(*m_frame);

  if(actionShowGrid->isChecked())
  {
    if(actionXAxis->isChecked())
      addGridFragment(*m_frame, Axis::X, moveCenter(), 1, 8.0f);
    else if(actionYAxis->isChecked())
      addGridFragment(*m_frame, Axis::Y, moveCenter(), 1, 8.0f);
    else if(actionZAxis->isChecked())
      addGridFragment(*m_frame, Axis::Z, moveCenter(), 1, 8.0f);
    else
      addGridFragment(*m_frame, Axis::Z, moveCenter(), 1, 8.0f);
  }

  if(actionPivotMode->isChecked())
  {
    QScopedPointer<SheetFragment> pivotFragment(new SheetFragment());
    pivotFragment->setFixedState(RenderSystem::FixedConst);
    pivotFragment->setFillMode(RenderSystem::Solid);
    pivotFragment->setDepthStencilState(RenderSystem::DepthRead);
    pivotFragment->setOrder(1005);
    pivotFragment->setOrigin(m_pivotOffset);
    pivotFragment->setSize(0.2f);
    pivotFragment->setCamera(m_frame->camera());
    m_frame->addFragment(pivotFragment.take());
  }
  m_frame->setDebugView(NULL);
}

void SceneEditor::finalizeComponents()
{
  QStringList lines;
  lines << renderComponents()->toStringList();
  lines << "";
  lines << m_cameraController->toStringList();
  lines << "";
  getRenderSystem().drawText(vec2(0,0), lines.join("\n"));
}
