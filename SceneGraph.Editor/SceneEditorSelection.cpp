#include "SceneEditor.hpp"
#include "SceneItemModel.hpp"

#include "CameraController/CameraController.hpp"

#include <Math/Ray.hpp>
#include <Render/RenderComponentWidget.hpp>

#include <Physics/CollideInfo.hpp>

#include <SceneGraph/Actor.hpp>
#include <SceneGraph/Scene.hpp>

#include <QMouseEvent>
#include <QDebug>

bool SceneEditor::selectMousePress(QMouseEvent* mouseEvent)
{
  if(!m_sceneItemModel->scene())
    return false;

  if (!m_selectBand)
      m_selectBand = new QRubberBand(QRubberBand::Line, renderComponents());
  m_selectBand->setGeometry(QRect(mouseEvent->pos(), QSize()));
  m_selectBand->show();

  m_selectOffset = mouseEvent->pos();

  renderComponents()->grabMouse();
  return true;
}

bool SceneEditor::selectMouseMove(QMouseEvent* mouseEvent)
{
  if(m_selectBand)
    m_selectBand->setGeometry(QRect(m_selectOffset, mouseEvent->pos()).normalized());

  return true;
}

bool SceneEditor::selectMouseRelease(QMouseEvent* mouseEvent)
{
  if(!m_selectBand || m_selectBand->isHidden())
    return false;

  m_selectBand->hide();

  renderComponents()->releaseMouse();

  QRect area(m_selectOffset, QSize(1,1));

  bool singleSelect = false;

  if(!m_selectBand->geometry().isEmpty())
  {
    area = m_selectBand->geometry();
  }
  else
  {
    singleSelect = true;
  }

  QItemSelection selection;
  QObject* selectedObject = NULL;
  QModelIndex selectedIndex;

  foreach(const QObject* object, colorPick(area))
  {
    Actor* actor = qobject_cast<Actor*>(const_cast<QObject*>(object));
    if(!actor)
      continue;

    QModelIndex index = m_sceneItemModel->index(actor);

    if(index.isValid())
    {
      selection.select(index, index);
      selectedObject = actor;
      selectedIndex = index;
    }
  }

  QItemSelectionModel::SelectionFlags selectionFlags = QItemSelectionModel::ClearAndSelect;

  if(mouseEvent->modifiers() == (Qt::ControlModifier | Qt::ShiftModifier))
    selectionFlags = QItemSelectionModel::Select;
  if(mouseEvent->modifiers() == Qt::ControlModifier)
    selectionFlags = QItemSelectionModel::Toggle;

  sceneTreeView->selectionModel()->select(selection, selectionFlags);
  setPropertyObject(selectedObject);

  if(singleSelect)
  {
    sceneTreeView->scrollTo(selectedIndex);
    sceneTreeView->setCurrentIndex(selectedIndex);
  }
  return true;
}
