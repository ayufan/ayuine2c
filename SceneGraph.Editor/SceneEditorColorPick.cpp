#include "SceneEditor.hpp"
#include "SceneItemModel.hpp"

#include <CoreFrames/MetaObjectItemModel.hpp>

#include "CameraController/CameraController.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/RenderTarget.hpp>

#include <Pipeline/ColorPickFrame.hpp>

#include <SceneGraph/Scene.hpp>

#include <QSet>

class ScenePickDebugView : public DebugView
{
public:
  ScenePickDebugView(const QSet<Actor*>& hidden,
                     const QSet<const QMetaObject*>& types)
    : m_hidden(hidden), m_types(types)
  {
  }

public:
  bool isSelected(QObject* object) const
  {
    return false;
  }

  bool isHidden(QObject* object) const
  {
    if(m_hidden.contains((Actor*)object))
      return true;
    if(m_types.contains(object->metaObject()))
      return false;
    return true;
  }

private:
  QSet<Actor*> m_hidden;
  QSet<const QMetaObject*> m_types;
};

QList<QObject *> SceneEditor::colorPick(const QRect &area) const
{
  if(!m_sceneItemModel->scene())
    return QList<QObject *>();

  // setup frame
  ColorPickFrame renderFrame;
  ScenePickDebugView debugView(m_hidden, m_actorTypesItemModel->checked());
  renderFrame.setDebugView(&debugView);

  if(!m_cameraController->updateFrame(renderFrame))
    return QList<QObject *>();

  // generate frame
  m_sceneItemModel->scene()->generateColorPickFrame(renderFrame);

  // render scene to buffer
  QSet<QObject *> objects;
  QImage image = renderFrame.toImage();

  for(unsigned y = area.top(); y <= area.bottom(); ++y)
  {
    for(unsigned x = area.left(); x <= area.right(); ++x)
    {
      QRgb pixel = image.pixel(x, y);
      QObject* object = renderFrame.colorToObject(QColor::fromRgba(pixel).rgb());
      if(!object)
        continue;

      // get parent grouped object
      if(Actor* actor = qobject_cast<Actor*>(object))
      {
        for(ActorManager* manager = actor->parentActor().data(); manager; manager = manager->parentActor().data())
        {
          if(manager->grouped())
          {
            object = manager;
          }
        }
      }

      objects.insert(object);
    }
  }

  return objects.toList();
}
