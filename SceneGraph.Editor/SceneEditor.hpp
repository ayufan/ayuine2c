#ifndef SCENEEDITOR_HPP
#define SCENEEDITOR_HPP

#include <QMainWindow>

#include <CoreFrames/FrameEditor.hpp>
#include <Math/Euler.hpp>

#include <Pipeline/Frame.hpp>

#include <QSet>
#include <QClipboard>
#include <QRubberBand>

#include "ui_SceneEditor.h"

class Actor;
class Scene;
class SceneItemModel;
class SceneSelectionModel;
class MetaObjectItemModel;

struct euler;

class SceneEditor : public FrameEditor, protected Ui::SceneEditor
{
  Q_OBJECT

public:
  Q_INVOKABLE explicit SceneEditor(QWidget *parent = 0);
  ~SceneEditor();

public:
  void prepareComponents();
  void finalizeComponents();

public:
  const QMetaObject* resourceType() const;
  QSharedPointer<Resource> resource() const;
  bool setResource(const QSharedPointer<Resource> &newResource);
  void updateUi();

protected:
  bool eventFilter(QObject *widget, QEvent *event);

private:
  vec3 newOrigin() const;
  vec3 moveCenter() const;
  vec3 moveAxis() const;
  vec4 movePlane() const;

  QList<QObject*> colorPick(const QRect& area) const;

private:
  bool selectMousePress(QMouseEvent* mouseEvent);
  bool selectMouseMove(QMouseEvent* mouseEvent);
  bool selectMouseRelease(QMouseEvent* mouseEvent);
  bool moveMousePress(QMouseEvent* mouseEvent);
  bool moveMouseMove(QMouseEvent* mouseEvent);
  bool moveMouseRelease(QMouseEvent* mouseEvent);
  bool rotateMousePress(QMouseEvent* mouseEvent);
  bool rotateMouseMove(QMouseEvent* mouseEvent);
  bool rotateMouseRelease(QMouseEvent* mouseEvent);
  bool pivotMousePress(QMouseEvent* mouseEvent);

private slots:
  void actorVisibilityChanged();  
  void currentChanged(const QModelIndex& index);
  void clipboardChanged(QClipboard::Mode mode);

private slots:
  void on_actionShowSelected_triggered();
  void on_actionHideSelected_triggered();
  void on_actionInvertSelection_triggered();
  void on_actionShowAll_triggered();
  void on_actionDeselectActors_triggered();
  void on_sceneTreeView_doubleClicked(const QModelIndex &index);
  void on_actionDeleteActors_triggered(bool force = false);
  void on_actionShowGrid_triggered();
  void on_actionSnapGrid_triggered();
  void on_actionXAxis_triggered();
  void on_actionYAxis_triggered();
  void on_actionZAxis_triggered();
  void on_actionCloneSelected_triggered();
  void on_actionUngroupSelected_triggered();
  void on_actionGroupSelected_triggered();
  void on_actionCut_triggered();
  void on_actionCopy_triggered();
  void on_actionPaste_triggered();
  void on_actionReplaceActor_triggered();
  void on_actionShowGrouped_toggled(bool arg1);
  void on_actionEditMode_toggled(bool arg1);
  void on_actionBuild_triggered();
  void on_actionPlay_triggered();
  void on_actionGoCamera_triggered();
  void on_actorTypesView_doubleClicked(const QModelIndex &index);

private:
  QSet<Actor*> selected() const;

private:
  QScopedPointer<SceneItemModel> m_sceneItemModel;
  QScopedPointer<MetaObjectItemModel> m_actorTypesItemModel;
  QSet<Actor*> m_hidden;

private:
  QPoint m_selectOffset;
  QRubberBand* m_selectBand;

private:
  QPoint m_mouseOffset;
  vec3 m_moveOffset;
  vec4 m_movePlane;

private:
  euler m_rotateOffset;
  vec3 m_rotateCenter;
  vec4 m_rotatePlane;

private:
  vec3 m_pivotOffset;
};

#endif // SCENEEDITOR_HPP
