#include "SceneEditor.hpp"
#include "SceneItemModel.hpp"
#include "CameraController/CameraController.hpp"

#include <Math/Vec3.hpp>

#include <SceneGraph/Actor.hpp>
#include <Render/RenderComponentWidget.hpp>

#include <QMouseEvent>

vec3 SceneEditor::moveAxis() const
{
  vec3 normal(0,0,0);

  if(actionXAxis->isChecked())
    normal.X = 1.0f;
  else if(actionYAxis->isChecked())
    normal.Y = 1.0f;
  else if(actionZAxis->isChecked())
    normal.Z = 1.0f;

  return normal;
}

vec3 SceneEditor::moveCenter() const
{
  vec3 origin = m_cameraController->origin();
  origin.Z = 0.0f;

  if(sceneTreeView->selectionModel()->selectedIndexes().size() > 0)
  {
    if(Actor* actor = (Actor*)sceneTreeView->selectionModel()->selectedIndexes()[0].internalPointer())
    {
      box bounds = actor->worldBoxBounds();
      if(bounds.empty())
        origin = actor->worldOrigin();
      else
        origin = bounds.center();
    }
  }

  return origin;
}

vec3 SceneEditor::newOrigin() const
{
  vec3 origin = m_cameraController->origin();
  if(actionSnapGrid->isChecked())
    origin = origin.round(0.5f);
  return origin;
}

vec4 SceneEditor::movePlane() const
{
  return vec4(moveAxis(), moveCenter());
}

bool SceneEditor::pivotMousePress(QMouseEvent *mouseEvent)
{
  if(mouseEvent->modifiers() == Qt::ShiftModifier)
  {
    ray dir = m_cameraController->direction(mouseEvent->pos());

    m_pivotOffset = dir.hit(movePlane().collide(dir));

    if(actionSnapGrid->isChecked())
    {
      m_pivotOffset = m_pivotOffset.round(0.5f);
    }

    renderComponents()->repaint();

    return true;
  }
  return false;
}

bool SceneEditor::moveMousePress(QMouseEvent *mouseEvent)
{
  renderComponents()->grabMouse();
  m_mouseOffset = mouseEvent->pos();
  m_moveOffset = vec3();
  m_movePlane = movePlane();
  return true;
}

bool SceneEditor::moveMouseMove(QMouseEvent *mouseEvent)
{
  ray dir1 = m_cameraController->direction(m_mouseOffset);
  ray dir2 = m_cameraController->direction(mouseEvent->pos());
  vec3 p1 = dir1.hit(m_movePlane.collide(dir1));
  vec3 p2 = dir2.hit(m_movePlane.collide(dir2));
  vec3 offset = p2 - p1;

  if(actionSnapGrid->isChecked())
  {
    offset = offset.round(0.5f);

    if(qFuzzyIsNull(offset.X) && qFuzzyIsNull(offset.Y) && qFuzzyIsNull(offset.Z))
      return true;
  }

  offset -= m_moveOffset;
  m_moveOffset += offset;

  bool changed = false;

  foreach(const QModelIndex &index, sceneTreeView->selectionModel()->selectedIndexes())
  {
    if(!index.isValid())
      continue;

    Actor* actor = m_sceneItemModel->toActor(index);
    if(!actor)
      continue;

    actor->setWorldOrigin(actor->worldOrigin() + offset);

    changed = true;
  }

  if(changed)
  {
    renderComponents()->repaint();
  }
  return true;
}

bool SceneEditor::moveMouseRelease(QMouseEvent *mouseEvent)
{
  renderComponents()->releaseMouse();
  return true;
}
