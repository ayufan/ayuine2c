#ifndef SCENEITEMMODEL_HPP
#define SCENEITEMMODEL_HPP

#include <QAbstractItemModel>
#include <QSharedPointer>

class Actor;
class ActorManager;
class Scene;

class SceneItemModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  explicit SceneItemModel(QObject *parent = 0);
  ~SceneItemModel();

signals:
  void selectedChanged(const QModelIndex& index);
  void hiddenChanged(const QModelIndex& index);
  void graphicsChanged(const QModelIndex& index);

public:
  QModelIndex index(int row, int column,
                            const QModelIndex &parent = QModelIndex()) const;
  QModelIndex parent(const QModelIndex &child) const;
  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
  Qt::ItemFlags flags(const QModelIndex &index) const;  
  Qt::DropActions supportedDropActions() const;
  QStringList mimeTypes() const;
  QMimeData *mimeData(const QModelIndexList &indexes) const;
  bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                            int row, int column, const QModelIndex &parent);

  QVector<Actor*> all() const;

  Actor* toActor(const QModelIndex& index);
  QSharedPointer<Actor> toActorRef(const QModelIndex& index);

public:
  const QSharedPointer<Scene>& scene() const;
  void setScene(const QSharedPointer<Scene>& scene);

public:
  QModelIndex index(Actor* actor) const;

private slots:
  void objectNameChanged(Actor* actor);
  void graphicsChanged(Actor* actor);
  void groupedChanged(ActorManager* manager);
  void actorAdded(ActorManager* manager, Actor* actor);
  void actorAboutToBeRemoved(ActorManager* manager, Actor* actor);
  void actorRemoved(ActorManager* manager, Actor* actor);
  void actorsAboutToReset(ActorManager* manager);
  void actorsReset(ActorManager* manager);
  void rootActorAboutToChanged(Scene* scene, ActorManager* manager);
  void rootActorChanged(Scene* scene, ActorManager* manager);

private:
  QSharedPointer<Scene> m_scene;
};

#endif // SCENEITEMMODEL_HPP
