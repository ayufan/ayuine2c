TEMPLATE = lib
TARGET = MaterialSystem

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender

HEADERS += \
    VertexShaderCompiler.hpp \
    MaterialSystem.hpp \
    MaterialShaderCompiler.hpp \
    MaterialShader.hpp \
    Material.hpp \
    LightShader.hpp \
    Shader.hpp \
    ShaderBlock.hpp \
    ShaderBlockFinal.hpp \
    ShaderBlockInput.hpp \
    ShaderBlockOutput.hpp \
    ShaderBlockVariable.hpp \
    ShaderState_p.hpp \
    ShaderState.hpp

SOURCES += \
    VertexShaderCompiler.cpp \
    MaterialSystem.cpp \
    MaterialShaderCompiler.cpp \
    MaterialShader.cpp \
    Material.cpp \
    LightShader.cpp \
    Helpers.cpp \
    Blocks/VariableVector.cpp \
    Blocks/VariableTexture.cpp \
    Blocks/VariableRenderTarget.cpp \
    Blocks/VariableLightPosition.cpp \
    Blocks/VariableFloat.cpp \
    Blocks/VariableColor.cpp \
    Blocks/TexturingScreen.cpp \
    Blocks/TexturingSampler.cpp \
    Blocks/TexturingNormalMap.cpp \
    Blocks/StreamVertexScreen.cpp \
    Blocks/StreamVertexOrigin.cpp \
    Blocks/StreamVertexNormal.cpp \
    Blocks/StreamVertexDepth.cpp \
    Blocks/StreamVertexColor.cpp \
    Blocks/StreamTexCoords.cpp \
    Blocks/StreamMapCoords.cpp \
    Blocks/OutputSolid.cpp \
    Blocks/OutputShaded.cpp \
    Blocks/MathSplit.cpp \
    Blocks/MathSign.cpp \
    Blocks/MathScaleBias.cpp \
    Blocks/MathSaturate.cpp \
    Blocks/MathNormalize.cpp \
    Blocks/MathJoin.cpp \
    Blocks/MathInterpolate.cpp \
    Blocks/MathFrac.cpp \
    Blocks/MathFloor.cpp \
    Blocks/MathComponentMask.cpp \
    Blocks/MathCombine.cpp \
    Blocks/MathClamp.cpp \
    Blocks/MathAbs.cpp \
    Blocks/InputSolid.cpp \
    Blocks/InputShaded.cpp \
    Blocks/FinalValue.cpp \
    Blocks/FinalColor.cpp \
    Blocks/EffectVarianceShadowing.cpp \
    Blocks/EffectQuality.cpp \
    Blocks/EffectPCF.cpp \
    Blocks/EffectParallax.cpp \
    Blocks/EffectLighting.cpp \
    Blocks/ConstantTime.cpp \
    Blocks/ConstantColor.cpp \
    Blocks/EffectPulse.cpp \
    Blocks/MathTransform.cpp \
    Blocks/VariableMatrix.cpp \
    Blocks/TexturingCube.cpp \
    Blocks/MathToSpheric.cpp \
    Blocks/MathFromSpheric.cpp \
    Blocks/StreamLocalOrigin.cpp \
    Blocks/EffectLayers.cpp \
    Blocks/EffectSpot.cpp \
    Shader.cpp \
    ShaderBlock.cpp \
    ShaderBlockFinal.cpp \
    ShaderBlockInput.cpp \
    ShaderBlockOutput.cpp \
    ShaderBlockVariable.cpp \
    ShaderState.cpp
