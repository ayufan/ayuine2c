#ifndef SHADERBLOCKOUTPUT_HPP
#define SHADERBLOCKOUTPUT_HPP

#include "ShaderBlock.hpp"

class MATERIALSYSTEM_EXPORT ShaderBlockOutput : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Output")

public:
  virtual const QMetaObject* blockType() const;
  virtual unsigned blockMaxCount() const;
};

#endif // SHADERBLOCKOUTPUT_HPP
