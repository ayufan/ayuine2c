#ifndef MATERIALSHADER_HPP
#define MATERIALSHADER_HPP

#include "Shader.hpp"
#include "ShaderBlockOutput.hpp"

class MATERIALSYSTEM_EXPORT GeneralShader : public Shader
{
  Q_OBJECT

public:
  explicit GeneralShader();

public:
  bool addBlock(const QSharedPointer<ShaderBlock>& block);
  QSharedPointer<ShaderBlockOutput> findOutput() const;

public:
  unsigned constOffset() const;
  unsigned samplerOffset() const;
  unsigned constMaxCount() const;
  unsigned samplerMaxCount() const;
};

class MATERIALSYSTEM_EXPORT MaterialShader : public GeneralShader
{
    Q_OBJECT
    Q_CLASSINFO("UniquePrefix", "ms")

public:
  Q_INVOKABLE explicit MaterialShader();

public:
  VertexBuffer::Types vertexType() const;
};

Q_DECLARE_OBJECTREF(GeneralShader)
Q_DECLARE_OBJECTREF(MaterialShader)

#endif // MATERIALSHADER_HPP
