#ifndef SHADERBLOCKINPUT_HPP
#define SHADERBLOCKINPUT_HPP

#include "ShaderBlock.hpp"

class MATERIALSYSTEM_EXPORT ShaderBlockInput : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Input")

public:
  virtual const QMetaObject* blockType() const;
  virtual unsigned blockMaxCount() const;

protected:
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned output, const QString& varName) const;
};

#endif // SHADERBLOCKINPUT_HPP
