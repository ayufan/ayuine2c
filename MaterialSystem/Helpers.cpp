#include "ShaderBlock.hpp"

#include <Math/Vec3.hpp>
#include <Math/Sphere.hpp>

static MaterialSystem::Quality fixedQuality;
static bool fixedQualitySet;

void MaterialSystem::setDefaultQuality()
{
  fixedQualitySet = false;
}

void MaterialSystem::setFixedQuality(Quality quality)
{
  fixedQualitySet = Low <= quality && quality <= High;
  fixedQuality = quality;
}

MaterialSystem::Quality MaterialSystem::qualityForSphere(const vec3& viewOrigin, const sphere& sphere)
{
  if(fixedQualitySet)
    return fixedQuality;

  static float highQualityDistance = getSettings().value("Render/HighQualityDistance", 5.0f).toFloat();
  static float mediumQualityDistance = getSettings().value("Render/MediumQualityDistance", 15.0f).toFloat();

  float distance = (viewOrigin - sphere.Center).length() - sphere.Radius;
  if(distance < highQualityDistance)
    return MaterialSystem::High;
  if(distance < mediumQualityDistance)
    return MaterialSystem::Medium;
  return MaterialSystem::Low;
}

const char* ShaderBlock::linkTypeTransform[ShaderBlock::MaxTypes][ShaderBlock::MaxTypes] =
{
  // mbltUnknown
  {},

  // mbltFloat
  {NULL, "%1", "%1.rr", "%1.rrr", "%1.rrr", "%1.rrrr", "%1.rrrr", NULL, NULL},

  // mbltFloat2
  {NULL, "%1.r", "%1", "float3(%1, 0)", "float3(%1, 0)", "float4(%1, 0, 0)", "float4(%1, 0, 0)", NULL, NULL},

  // mbltFloat3 & mbltEuler
  {NULL, "%1.r", "%1.rg", "%1", "%1", "float4(%1, 0)", "float4(%1, 0)", NULL, NULL},
  {NULL, "%1.r", "%1.rg", "%1", "%1", "float4(%1, 0)", "float4(%1, 0)", NULL, NULL},

  // mbltFloat4 & mbltColor
  {NULL, "%1.r", "%1.rg", "%1.rgb", "%1.rgb", "%1", "%1", NULL, NULL},
  {NULL, "%1.r", "%1.rg", "%1.rgb", "%1.rgb", "%1", "%1", NULL, NULL},

  // mbltMatrix
  {NULL, NULL, NULL, NULL, NULL, NULL, NULL, "%1", NULL},

  // mbltSampler
  {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "%1"}
};

const char* ShaderBlock::linkTypeName[ShaderBlock::MaxTypes] =
{
  NULL,
  "float",
  "float2",
  "float3", "float3",
  "float4", "float4",
  "float4x4",
  "sampler"
};

const char* ShaderBlock::linkTypeDefaults[ShaderBlock::MaxTypes] =
{
  "float(0)",
  "float(0)",
  "float2(0,0)",
  "float3(0,0,0)", "float3(0,0,0)",
  "float4(0,0,0,0)", "float4(0,0,0,0)",
  "float4x4(1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1)",
  "sampler()"
};
