#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/RenderShader.hpp>
#include <Render/Texture.hpp>
#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/Euler.hpp>
#include <Math/Matrix.hpp>
#include <QColor>
#include <QPixmap>

const QMetaObject * ShaderBlockVariable::blockType() const
{
  return &staticMetaObject;
}

unsigned ShaderBlockVariable::blockMaxCount() const
{
  return ~0;
}

QString ShaderBlockVariable::constName() const
{
  return QString("const_%1").arg(buildBlockName());
}

QString ShaderBlockVariable::constVarName(MaterialShaderCompiler& compiler) const
{
  QString varName = constName();

  if(compiler.addVariable(varName))
  {
    compileConstVar(compiler, varName);
  }
  return varName;
}

void ShaderBlockVariable::compileConstVar(MaterialShaderCompiler& compiler, const QString& varName) const
{
  if(constType() == Undefined)
  {
    compiler.vs("float %s = 0.0f;", varName);
    compiler.ps("float %s = 0.0f;", varName);
    return;
  }

  QLatin1String type(linkTypeName[constType()]);

  const ShaderConst* constDesc = NULL;

  if(!baseMaterialShader().isNull())
  {
    constDesc = baseMaterialShader().toStrongRef()->findConst(objectName().toLatin1(), constType());
  }

  if(constType() == Sampler)
  {
    unsigned index = 0;
    if(constDesc)
      index = baseMaterialShader().toStrongRef()->samplerOffset() + constDesc->index;

    compiler.vsconst("%1 %2 : register(s%3)", type, varName, QString::number(index));
    compiler.psconst("%1 %2 : register(s%3)", type, varName, QString::number(index));
  }
  else
  {
    unsigned index = 0;
    if(constDesc)
      index = baseMaterialShader().toStrongRef()->constOffset() + constDesc->index;

    if(objectName().isEmpty())
    {
      switch(constType())
      {
      case Float:
      {
        float value = constValue().value<float>();
        compiler.vs("%1 %2 = %3;", type, varName, QString::number(value));
        compiler.ps("%1 %2 = %3;", type, varName, QString::number(value));
      }
        break;

      case Float2:
      {
        vec2 value = constValue().value<vec2>();
        compiler.vs("%1 %2 = float2(%3, %4);", type, varName, QString::number(value.X), QString::number(value.Y));
        compiler.ps("%1 %2 = float2(%3, %4);", type, varName, QString::number(value.X), QString::number(value.Y));
      }
        break;

      case Float3:
      {
        vec3 value = constValue().value<vec3>();
        compiler.vs("%1 %2 = float3(%3, %4, %5);", type, varName, QString::number(value.X), QString::number(value.Y), QString::number(value.Z));
        compiler.ps("%1 %2 = float3(%3, %4, %5);", type, varName, QString::number(value.X), QString::number(value.Y), QString::number(value.Z));
      }
        break;

      case Euler:
      {
        euler value = constValue().value<euler>();
        compiler.vs("%1 %2 = float3(%3, %4, %5);", type, varName, QString::number(value.Yaw), QString::number(value.Pitch), QString::number(value.Roll));
        compiler.ps("%1 %2 = float3(%3, %4, %5);", type, varName, QString::number(value.Yaw), QString::number(value.Pitch), QString::number(value.Roll));
      }
        break;

      case Float4:
      {
        vec4 value = constValue().value<vec4>();
        compiler.vs("%1 %2 = float4(%3, %4, %5, %6);", type, varName, QString::number(value.X), QString::number(value.Y), QString::number(value.Z), QString::number(value.W));
        compiler.ps("%1 %2 = float4(%3, %4, %5, %6);", type, varName, QString::number(value.X), QString::number(value.Y), QString::number(value.Z), QString::number(value.W));
      }
        break;

      case Matrix:
      {
        compiler.vs("%1 %2 = float4x4();", type, varName);
        compiler.ps("%1 %2 = float4x4();", type, varName);
      }
        break;

      case Color:
      {
        QColor value = constValue().value<QColor>();
        compiler.vs("%1 %2 = float4(%3, %4, %5, %6);", type, varName, QString::number(value.redF()), QString::number(value.greenF()), QString::number(value.blueF()), QString::number(value.alphaF()));
        compiler.ps("%1 %2 = float4(%3, %4, %5, %6);", type, varName, QString::number(value.redF()), QString::number(value.greenF()), QString::number(value.blueF()), QString::number(value.alphaF()));
      }
        break;

      default:
        // TODO
        break;
      }
    }
    else
    {
      compiler.vsconst("%1 %2 : register(c%3)", type, varName, QString::number(index));
      compiler.psconst("%1 %2 : register(c%3)", type, varName, QString::number(index));
    }
  }
}

QString MaterialShaderBlockOutputVariable::buildOutputVarName(MaterialShaderCompiler& compiler, unsigned index) const
{
  switch(index)
  {
  case 0:
    return constName();

  default:
    return ShaderBlock::buildOutputVarName(compiler, index);
  }
}

void MaterialShaderBlockOutputVariable::compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
{
  switch(index)
  {
  case 0:
    compileConstVar(compiler, varName);
    break;

  default:
    ShaderBlock::compileVarName(compiler, index, varName);
    break;
  }
}

const char* MaterialShaderBlockOutputVariable::constLabel() const
{
  return "Out";
}

unsigned MaterialShaderBlockOutputVariable::outputCount() const
{
  return 1;
}

ShaderBlock::Link MaterialShaderBlockOutputVariable::outputLink(unsigned index) const
{
  switch(index)
  {
  case 0:		return Link(index, constType(), constLabel());
  default:		return Link();
  }
}

QVariant ShaderBlockVariable::constValue() const
{
  return QVariant();
}

void ShaderBlockVariable::setConstValue(const QVariant &value)
{
}

void ShaderBlockVariable::setObjectName(const QString &name)
{
  if(objectName() != name)
  {
    ShaderBlock::setObjectName(name);
    changed();
    emit updated(this);
  }
}

bool ShaderBlockVariable::isDesignable() const
{
  return true;
}
