#include "Shader.hpp"
#include "ShaderBlock.hpp"
#include "ShaderBlockVariable.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/Texture.hpp>
#include <Render/RenderShader.hpp>

#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/Color.hpp>
#include <Math/Euler.hpp>

#include <QSet>

Q_IMPLEMENT_OBJECTREF(Shader)
Q_IMPLEMENT_ALIASREF(BaseMaterialShaderRef, ShaderRef)

Shader::Shader()
{
  m_flushConsts = false;
  internalIndex = 0;
}

Shader::~Shader()
{
}

bool Shader::addBlock(const QSharedPointer<ShaderBlock>& block)
{
  if(m_blocks.contains(block))
    return false;

  if(!block->m_baseMaterialShader.isNull())
    return false;

  const QMetaObject* blockMetaObject = block->blockType();
  unsigned blockCount = 0;

  foreach(QSharedPointer<ShaderBlock> other, m_blocks)
  {
    if(other->blockType() == blockMetaObject)
      ++blockCount;
  }

  if(blockCount >= block->blockMaxCount())
    return false;

  block->m_baseMaterialShader = this;
  m_blocks.append(block);

  changed();

  emit blockAdded(block);
  return true;
}

bool Shader::removeBlock(unsigned index)
{
  if((unsigned)m_blocks.size() <= index)
    return false;

  QSharedPointer<ShaderBlock> block = m_blocks[index];
  block->m_baseMaterialShader.clear();
  m_blocks.remove(index);

  foreach(QSharedPointer<ShaderBlock> other, m_blocks)
    other->unlink(block);

  changed();

  emit blockRemoved(block);
  return true;
}

bool Shader::removeBlock(const QSharedPointer<ShaderBlock>& block)
{
  int index = m_blocks.indexOf(block);
  if(index < 0)
    return false;
  return removeBlock(index);
}

QSharedPointer<ShaderBlock> Shader::findBlock(const QByteArray& name) const
{
  if(name.isEmpty())
    return QSharedPointer<ShaderBlock>();

  foreach(QSharedPointer<ShaderBlock> block, m_blocks)
    if(block->objectName() == name)
      return block;
  return QSharedPointer<ShaderBlock>();
}

QSharedPointer<ShaderBlock> Shader::findBlockType(const QMetaObject *metaObject) const
{
  foreach(QSharedPointer<ShaderBlock> block, m_blocks)
  {
    for(const QMetaObject* blockMetaObject = block->metaObject(); blockMetaObject; blockMetaObject = blockMetaObject->superClass())
    {
      if(blockMetaObject == metaObject)
      {
        return block;
      }
    }
  }

  return QSharedPointer<ShaderBlock>();
}

void Shader::updateConsts()
{
  m_maxConsts = 0;
  m_maxSamplers = 0;
  m_flushConsts = false;

  foreach(QSharedPointer<ShaderBlock> block, m_blocks)
  {
    if(ShaderBlockVariable* variable = qobject_cast<ShaderBlockVariable*>(block.data()))
    {
      ShaderConst newConst;
      newConst.name = variable->objectName().toLatin1();
      newConst.type = variable->constType();
      newConst.index = ~0U;
      newConst.value = variable->constValue();
      newConst.variable = block.objectCast<ShaderBlockVariable>();

      switch(newConst.type)
      {
      case ShaderBlock::Float:
      case ShaderBlock::Float2:
      case ShaderBlock::Float3:
      case ShaderBlock::Euler:
      case ShaderBlock::Float4:
      case ShaderBlock::Color:
        newConst.index = m_maxConsts++;
        break;

      case ShaderBlock::Matrix:
        newConst.index = m_maxConsts;
        m_maxConsts += 4;
        break;

      case ShaderBlock::Sampler:
        newConst.index = m_maxSamplers++;
        break;

      default:
        break;
      }

      if(newConst.index == ~0U)
        continue;

      m_consts.append(newConst);
    }
  }

  qSort(m_consts);

  emit constsUpdated(this);
}

void Shader::setConsts(const ShaderValues &values, bool onlyUsed) const
{
  float constValue[4] = {0,0,0,0};

  if(m_flushConsts)
  {
    const_cast<Shader*>(this)->updateConsts();
  }

  foreach(const ShaderConst &desc, m_consts)
  {
    QVariant value = values[desc.name];

    if(!desc.variable)
      continue;

    if(value.isNull() || value.type() != desc.value.type() || value.userType() != desc.value.userType())
    {
      desc.variable->setRenderVariable(desc.index, desc.value, onlyUsed);
    }
    else
    {
      desc.variable->setRenderVariable(desc.index, value, onlyUsed);
    }
  }
}

const ShaderConst * Shader::findConst(const QByteArray &name, ShaderBlock::LinkType type) const
{
  if(m_flushConsts)
  {
    const_cast<Shader*>(this)->updateConsts();
  }

  ShaderConst finder;
  finder.name = name;

  QVector<ShaderConst>::const_iterator itor = qBinaryFind(m_consts, finder);
  if(itor == m_consts.end())
    return NULL;
  if(type != ShaderBlock::Undefined && itor->type != type)
    return NULL;
  return &*itor;
}

QString Shader::unique() const
{
    int unique = metaObject()->indexOfClassInfo("UniquePrefix");
    if(unique >= 0)
        return metaObject()->classInfo(unique).value();
    return "shader";
}

QVector<QSharedPointer<ShaderBlock> > Shader::blocks() const
{
  return m_blocks;
}

void Shader::setBlocks(const QVector<QSharedPointer<ShaderBlock> > &blocks)
{
  foreach(QSharedPointer<ShaderBlock> block, m_blocks)
  {
    block->m_baseMaterialShader.clear();
  }

  m_blocks = blocks;

  foreach(QSharedPointer<ShaderBlock> block, m_blocks)
  {
    block->m_baseMaterialShader = this;
  }

  changed();
}

bool Shader::contains(const QByteArray &name) const
{
  if(findConst(name) != NULL)
    return true;
  return false;
}

QVariant Shader::get(const QByteArray &name) const
{
  if(const ShaderConst* shaderConst = findConst(name))
    return shaderConst->value;
  return QVariant();
}

void Shader::changed()
{
  m_flushConsts = true;
  m_shaders.clear();
}

RenderShaderRef Shader::shader(unsigned index, Shader *link) const
{
  if(m_shaders.size() <= index)
    return RenderShaderRef();
  if(m_shaders[index].contains(link))
    return m_shaders[index][link];
  return RenderShaderRef();
}

void Shader::setShader(unsigned index, Shader *link, const RenderShaderRef &shader)
{
  if(m_shaders.size() <= index)
    m_shaders.resize((index + 32) & ~31);

  RenderShaderRef& ref = m_shaders[index][link];
  if(ref == shader)
    return;

  ref = shader;
  if(link)
    connect(link, SIGNAL(constsUpdated(Shader*)), SLOT(linkedShaderConstsUpdated(Shader*)));
}

void Shader::linkedShaderConstsUpdated(Shader *link)
{
  if(!link)
    return;

  for(int i = 0; i < m_shaders.size(); ++i)
    m_shaders[i].remove(link);
  link->disconnect(this);
}

ShaderValues Shader::values() const
{
  if(m_flushConsts)
  {
    const_cast<Shader*>(this)->updateConsts();
  }

  ShaderValues values;

  foreach(const ShaderConst &value, m_consts)
  {
    if(value.name.isEmpty())
      continue;

    if(value.variable && !value.variable->isDesignable())
      continue;

    values[value.name] = value.value;
  }

  return values;
}

void Shader::setValues(const ShaderValues &values)
{
  if(m_flushConsts)
  {
    const_cast<Shader*>(this)->updateConsts();
  }

  for(int i = 0; i < m_consts.size(); ++i)
  {
    ShaderConst& shaderConst = m_consts[i];

    if(!values.contains(shaderConst.name))
      continue;

    QVariant newValue = values[shaderConst.name];
    if(shaderConst.value.userType() != newValue.userType())
      continue;

    shaderConst.value = newValue;

    if(shaderConst.variable)
    {
      shaderConst.variable->setConstValue(newValue);
    }
  }
}

VertexBuffer::Types Shader::vertexType() const
{
  return VertexBuffer::Vertex;
}
