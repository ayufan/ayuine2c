#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockConstantColor : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Constant")
  Q_CLASSINFO("BlockName", "Color")
public:
  Q_INVOKABLE MaterialShaderBlockConstantColor()
  {
  }

public:
  unsigned outputCount() const
  {
    return 1;
  }
  Link outputLink(unsigned index) const
  {
    return Link(index, Float4, "Out");
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    return compiler.getp(MaterialShaderCompiler::pColor);
  }
};

#include "ConstantColor.moc"
