#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectPCF : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "PCF")
  Q_PROPERTY(int samples READ samples WRITE setSamples)

  enum Inputs
  {
    iLightDir,
    iLightDist,
    iShadows,
    iMax
  };

  enum Outputs
  {
    oShadows,
    oDepth,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectPCF()
  {
    m_samples = 13;
  }

public:
  int samples() const
  {
    return m_samples;
  }
  void setSamples(int samples)
  {
    m_samples = qBound(1, samples, 13);
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iLightDir:			return Link(index, Float3, "LightDir");
    case iLightDist:		return Link(index, Float, "LightDist");
    case iShadows:			return Link(index, Sampler, "Shadows");
    default:                return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oShadows:		return Link(index, Float, "Out");
    case oDepth:		return Link(index, Float, "Depth");
    default:            return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oDepth:
      if(inputHasLink(iLightDir) && inputHasLink(iShadows))
      {
        QString lightDir = inputVarName(compiler, iLightDir);
        QString shadows = inputVarName(compiler, iShadows);

        compiler.ps("float %1 = texCUBE(%2, %3).r;",
                    varName, shadows, lightDir);
      }
      else
      {
        compiler.ps("float %1 = 1.0f;", varName);
      }
      break;

    case oShadows:
      if(inputHasLink(iLightDir) && inputHasLink(iLightDist) && inputHasLink(iShadows))
      {
        QString lightDir = inputVarName(compiler, iLightDir);
        QString lightDist = inputVarName(compiler, iLightDist);
        QString shadows = inputVarName(compiler, iShadows);

        QString dist = buildTempVarName(compiler, "dist");

        if(compiler.addVariable("poisson"))
        {
          compiler.ps("float2 poisson[12] = {float2(-0.326212f, -0.40581f), float2(-0.840144f, -0.07358f), float2(-0.695914f, 0.457137f),");
          compiler.ps("float2(-0.203345f, 0.620716f), float2(0.96234f, -0.194983f), float2(0.473434f, -0.480026f),");
          compiler.ps("float2(0.519456f, 0.767022f), float2(0.185461f, -0.893124f), float2(0.507431f, 0.064425f),");
          compiler.ps("float2(0.89642f, 0.412458f), float2(-0.32194f, -0.932615f), float2(-0.791559f, -0.59771f)};");
        }

        compiler.ps("float %1 = %2 - 0.0005 - 0.01 * (1 - dot(%2, %3));",
                    dist, lightDist, compiler.getp(MaterialShaderCompiler::pNormal));

        compiler.ps("float %1 = texCUBE(%2, %3).r > %4 ? 1.0f : 0.0f;", varName, shadows, lightDir, dist);

        if(m_samples > 1)
        {
          compiler.ps("for(int i = 0; i < %1; i++)", QString::number(m_samples-1));
          compiler.ps("%1 += texCUBE(%2, %3 + float3(poisson[i].xy, poisson[11-i].x) * 0.003125f).r > %4 ? 1.0f : 0.0f;",
                    varName, shadows, lightDir, dist);
          compiler.ps("%1 /= %2.0;", varName, QString::number(m_samples));
        }
      }
      else
      {
        compiler.ps("float %1 = 1.0f;", varName);
      }
      break;
    }
  }

private:
  int m_samples;
};

#include "EffectPCF.moc"
