#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/RenderSystem.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableLightPosition : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_PROPERTY(vec3 lightPosition READ lightPosition WRITE setLightPosition)
  Q_PROPERTY(float lightRadius READ lightRadius WRITE setLightRadius)
  Q_CLASSINFO("BlockName", "LightPosition")

  enum Outputs
  {
    oLightPosition,
    oLightRadius,
    oLightDir,
    oLightDist,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockVariableLightPosition()
  {
  }

public:
  LinkType constType() const
  {
    return Float4;
  }
  vec3 lightPosition() const
  {
    return m_lightPosition;
  }
  void setLightPosition(const vec3 &value)
  {
    if(m_lightPosition != value)
    {
      m_lightPosition = value;
      changed();
    }
  }
  float lightRadius() const
  {
    return 1.0f / m_invLightRadius;
  }
  void setLightRadius(float radius)
  {
    if(qFuzzyCompare(radius, 0.0f) || radius < 0.0f)
      radius = 0.00001f;
    m_invLightRadius = 1.0f / radius;
    changed();
  }
  QVariant constValue() const
  {
    return QVariant::fromValue(vec4(m_lightPosition, m_invLightRadius));
  }
  void setConstValue(const QVariant& value)
  {
    vec4 v = value.value<vec4>();
    m_lightPosition = vec3(v.X, v.Y, v.Z);
    m_invLightRadius = v.W;
    changed();
  }
  bool isDesignable() const
  {
    return false;
  }
  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned constOffset = offset + rawBaseMaterialShader()->constOffset();

    if(!onlyUsed || getRenderSystem().isConstUsed(constOffset))
    {
      vec4 constValue = value.value<vec4>();

      getRenderSystem().setConst(constOffset, &constValue.X, 1);
    }
  }

public:
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oLightPosition:	return Link(index, Float3, "Position");
    case oLightRadius:    return Link(index, Float, "Radius");
    case oLightDir:		return Link(index, Float3, "Dir");
    case oLightDist:		return Link(index, Float, "Dist");
    default:              return Link();
    }
  }
  QString compileLightDir(MaterialShaderCompiler& compiler) const
  {
    QString varName = buildTempVarName(compiler, "lightDir");
    if(compiler.addVariable(varName))
    {
      compiler.ps("float3 %1 = (%2.xyz - %3) * %4.w;", varName,
                  constVarName(compiler), compiler.getp(MaterialShaderCompiler::pOrigin),
                  constVarName(compiler));
      // compiler.linkAsTexCoord(varName, Float3);
    }
    return varName;
  }
  void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index) {
    case oLightRadius:
      compiler.ps("float %1 = 1.0/%2.w;", varName, constVarName(compiler));
      break;

    case oLightPosition:
      compiler.ps("float3 %1 = %2.xyz;", varName, constVarName(compiler));
      break;

    case oLightDir:
      compiler.ps("float3 %1 = normalize(%2);", varName, compileLightDir(compiler));
      break;

    case oLightDist:
      compiler.ps("float %1 = saturate(length(%2));", varName, compileLightDir(compiler));
      break;

    default:
      MaterialShaderBlockOutputVariable::compileVarName(compiler, index, varName);
      break;
    }
  }

private:
  vec3 m_lightPosition;
  float m_invLightRadius;
};

#include "VariableLightPosition.moc"
