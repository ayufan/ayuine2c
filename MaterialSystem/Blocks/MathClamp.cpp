#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathClamp : public ShaderBlock
{
  Q_OBJECT
  Q_PROPERTY(float minValue READ minValue WRITE setMinValue)
  Q_PROPERTY(float maxValue READ maxValue WRITE setMaxValue)
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Clamp")

  enum Inputs
  {
    iIn,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathClamp()
  {
    m_minValue = 0.0f;
    m_maxValue = 1.0f;
  }

public:
  float minValue() const
  {
    return m_minValue;
  }
  void setMinValue(float value)
  {
    m_minValue = qMin(value, m_maxValue);
    changed();
  }
  float maxValue() const
  {
    return m_maxValue;
  }
  void setMaxValue(float value)
  {
    m_maxValue = qMax(value, m_minValue);
    changed();
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float4, "In");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
      compiler.ps("float4 %1 = clamp(%2, %3, %4);", varName,
                  inputVarName(compiler, 0),
                  QString::number(m_minValue), QString::number(m_maxValue));
      break;
    }
  }

private:
  float m_minValue;
  float m_maxValue;
};

#include "MathClamp.moc"
