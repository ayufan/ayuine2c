#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathSplit : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Split")

  enum Inputs
  {
      iIn,
      iMax
  };

  enum Outputs
  {
      oR,
      oG,
      oB,
      oA,
      oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathSplit()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float4, "In");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oR:				return Link(index, Float, "R");
    case oG:				return Link(index, Float, "G");
    case oB:				return Link(index, Float, "B");
    case oA:				return Link(index, Float, "A");
    default:                return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oR:
        compiler.ps("float %1 = %2.r;", varName, inputVarName(compiler, 0));
        break;

    case oG:
        compiler.ps("float %1 = %2.g;", varName, inputVarName(compiler, 0));
        break;

    case oB:
        compiler.ps("float %1 = %2.b;", varName, inputVarName(compiler, 0));
        break;

    case oA:
        compiler.ps("float %1 = %2.a;", varName, inputVarName(compiler, 0));
        break;
    }
  }
};

#include "MathSplit.moc"
