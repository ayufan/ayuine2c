#include "ShaderBlockOutput.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockOutputShaded : public ShaderBlockOutput
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "Shaded")

  enum Inputs
  {
    iEmissive,
    iAmbient,
    iDiffuse,
    iGloss,
    iSpecularity,
    iNormal,
    iOpacity,
    iDistortion,
    iMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockOutputShaded()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iAmbient:			return Link(index, Float3, "Ambient");
    case iEmissive:			return Link(index, Float3, "Emissive");
    case iDiffuse:			return Link(index, Float3, "Diffuse");
    case iGloss:            return Link(index, Float3, "Gloss");
    case iSpecularity:      return Link(index, Float, "Specularity");
    case iNormal:			return Link(index, Float3, "Normal");
    case iOpacity:			return Link(index, Float, "Opacity");
    case iDistortion:		return Link(index, Float3, "Distortion");
    default:                return Link();
    }
  }
};

#include "OutputShaded.moc"
