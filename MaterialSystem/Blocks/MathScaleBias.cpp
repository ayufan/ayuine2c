#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathScaleBias : public ShaderBlock
{
  Q_OBJECT
  Q_PROPERTY(float scale READ scale WRITE setScale)
  Q_PROPERTY(float bias READ bias WRITE setBias)
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "ScaleBias")

  enum Inputs
  {
    iIn,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathScaleBias()
  {
    m_scale = 2.0f;
    m_bias = 1.0f;
  }

public:
  float scale() const
  {
    return m_scale;
  }
  void setScale(float value)
  {
    m_scale = value;
    changed();
  }
  float bias() const
  {
    return m_bias;
  }
  void setBias(float value)
  {
    m_bias = value;
    changed();
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float4, "In");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
      compiler.ps("float4 %1 = %2 * %3 - %4;",
                  varName,
                  inputVarName(compiler, iIn),
                  QString::number(m_scale),
                  QString::number(m_bias));
      break;
    }
  }

private:
  float m_scale;
  float m_bias;
};

#include "MathScaleBias.moc"
