#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockMathInterpolate : public ShaderBlock
{
  Q_OBJECT
  Q_ENUMS(Size Other)
  Q_PROPERTY(Size size READ size WRITE setSize)
  Q_PROPERTY(Other other READ other WRITE setOther DESIGNABLE isOtherDesignable)
  Q_CLASSINFO("BlockGroup", "Math")
  Q_CLASSINFO("BlockName", "Interpolate")

public:
  enum Size
  {
    Vec2,
    Vec3,
    Vec4
  };

  enum Other
  {
    Set1,
    Set0
  };

  enum Inputs
  {
    iA,
    iB,
    iTime,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockMathInterpolate()
  {
    m_size = Vec4;
    m_other = Set0;
  }

public:
  Size size() const
  {
    return m_size;
  }
  void setSize(Size size)
  {
    m_size = size;
    changed();
  }
  Other other() const
  {
    return m_other;
  }
  void setOther(Other other)
  {
    m_other = other;
    changed();
  }
  bool isOtherDesignable() const
  {
    return size() != Vec4;
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iA:			return Link(index, Float4, "A");
    case iB:			return Link(index, Float4, "B");
    case iTime:         return Link(index, Float4, "Time");
    default:            return Link();
    }
    return Link();
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:          return Link(index, Float4, "Out");
    default:            return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
    {
      QString mask;

      compiler.ps("float4 %1 = lerp(%2, %3, %4);", varName, inputVarName(compiler, iA), inputVarName(compiler, iB), inputVarName(compiler, iTime));

      switch(m_size)
      {
      case Vec2:        
        mask = "ba";
        break;

      case Vec3:
        mask = "a";
        break;

      case Vec4:
        break;
      }

      if(mask.length() != 0)
      {
        switch(m_other)
        {
        case Set0:
          compiler.ps("%1.%2 = 0;", varName, mask);
          break;

        case Set1:
          compiler.ps("%1.%2 = 1;", varName, mask);
          break;
        }
      }
    }
    break;
    }
  }

private:
  Size m_size;
  Other m_other;
};

#include "MathInterpolate.moc"
