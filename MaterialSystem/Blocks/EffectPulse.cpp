#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"
#include <QPixmap>
#include <QPainter>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectPulse : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "Pulse")
  Q_PROPERTY(float interval READ interval WRITE setInterval)
  Q_PROPERTY(float amplitude READ amplitude WRITE setAmplitude)
  Q_PROPERTY(float phase READ phase WRITE setPhase)
  Q_PROPERTY(float low READ low WRITE setLow)
  Q_PROPERTY(float high READ high WRITE setHigh)

  enum Inputs
  {
    iIn,
    iTime,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectPulse()
  {
    m_interval = 1;
    m_amplitude = 1;
    m_phase = 0;
    m_low = -1;
    m_high = 1;
  }

public:
  float interval() const
  {
    return m_interval;
  }
  void setInterval(float interval)
  {
    m_interval = qMax(interval, 0.001f);
    changed();
  }
  float amplitude() const
  {
    return m_amplitude;
  }
  void setAmplitude(float amplitude)
  {
    m_amplitude = qMax(amplitude, 0.0f);
    changed();
  }
  float phase() const
  {
    return m_phase;
  }
  void setPhase(float phase)
  {
    m_phase = qBound<float>(-M_PI, phase, M_PI);
    changed();
    updated();
    m_pixmap = QPixmap();
  }
  float low() const
  {
    return m_low;
  }
  void setLow(float low)
  {
    m_low = qMin(qMax(low, -1.0f), m_high);
    changed();
    updated();
    m_pixmap = QPixmap();
  }
  float high() const
  {
    return m_high;
  }
  void setHigh(float high)
  {
    m_high = qMin(qMax(high, m_low), 1.0f);
    changed();
    updated();
    m_pixmap = QPixmap();
  }
  QPixmap constValuePixmap() const
  {
    if(m_pixmap.isNull())
    {
      const int Size = 31;
      m_pixmap = QPixmap(Size, Size);
      m_pixmap.fill(Qt::white);

      QPainter painter(&m_pixmap);
      painter.setBrush(Qt::SolidPattern);
      painter.setPen(Qt::black);

      float yy = -1;

      for(int i = 0; i < Size; ++i)
      {
        float x = i * 2 * M_PI / Size;
        float y = sin(x + m_phase);
        y = qBound(m_low, y, m_high);
        y = Size / 2 - y * Size / 2;
        if(i > 0)
          painter.drawLine(i-1, yy, i, y);
        yy = y;
      }

      painter.setPen(Qt::DotLine);
      painter.drawLine(0, Size / 2, Size, Size / 2);
    }

    return m_pixmap;
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iIn:           return Link(index, Float4, "In");
    case iTime:         return Link(index, Float, "Time");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:           return Link(index, Float4, "Out");
    default:             return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oOut:
    {
      QString time = inputHasLink(iTime) ? inputVarName(compiler, iTime, "0.0f") : compiler.getp(MaterialShaderCompiler::pTime);
      compiler.ps("float4 %1 = %2 + clamp(cos(%3 / %4 + %5), %7, %8) * %6;",
                  varName, inputVarName(compiler, iIn, "0.0f"), time,
                  QString::number(m_interval / (2 * M_PI)),
                  QString::number(m_phase), QString::number(m_amplitude),
                  QString::number(m_low), QString::number(m_high));
    }
      break;
    }
  }

private:
  float m_interval, m_amplitude, m_phase, m_low, m_high;
  mutable QPixmap m_pixmap;
};

#include "EffectPulse.moc"
