#include "ShaderBlockInput.hpp"
#include "ShaderBlockOutput.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockInputSolid : public ShaderBlockInput
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "Solid")

  enum Outputs
  {
    oColor,
    oAlpha,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockInputSolid()
  {
  }

public:
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oColor:      return Link(index, Float3, "Color");
    case oAlpha:      return Link(index, Float, "Alpha");
    default:          return Link();
    }
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    switch(output)
    {
    case oColor:
      if(MaterialShaderCompiler::ShaderLink colorLink = compiler.getLink("Color"))
        return colorLink;
      if(MaterialShaderCompiler::ShaderLink diffuseLink = compiler.getLink("Diffuse"))
        return diffuseLink;
      return "float3(0,0,0)";

    case oAlpha:
      if(MaterialShaderCompiler::ShaderLink alphaLink = compiler.getLink("Alpha"))
        return alphaLink;
      if(MaterialShaderCompiler::ShaderLink opacityLink = compiler.getLink("Opacity"))
        return opacityLink;
      return "float(1.0f)";
    }
    return ShaderBlockInput::outputVarName(compiler, output);
  }
};

#include "InputSolid.moc"
