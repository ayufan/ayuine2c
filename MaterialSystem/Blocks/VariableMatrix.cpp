#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include "Shader.hpp"
#include <Render/RenderSystem.hpp>
#include <Math/Matrix.hpp>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableMatrix : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_PROPERTY(matrix value READ value WRITE setValue)
  Q_CLASSINFO("BlockName", "Matrix")

public:
  Q_INVOKABLE MaterialShaderBlockVariableMatrix()
  {
  }

public:
  LinkType constType() const
  {
    return Matrix;
  }
  matrix value() const
  {
    return m_value;
  }
  void setValue(const matrix &value)
  {
    if(m_value != value)
    {
      m_value = value;
      changed();
    }
  }
  QVariant constValue() const
  {
    return QVariant::fromValue(m_value);
  }
  void setConstValue(const QVariant& value)
  {
    setValue(value.value<matrix>());
  }
  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned constOffset = offset + rawBaseMaterialShader()->constOffset();

    if(!onlyUsed || getRenderSystem().isConstUsed(constOffset))
    {
      matrix constValue = value.value<matrix>();

      getRenderSystem().setConst(constOffset, &constValue.m11, 4);
    }
  }

private:
  matrix m_value;
};

#include "VariableMatrix.moc"
