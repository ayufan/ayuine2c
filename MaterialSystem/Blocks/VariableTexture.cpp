#include "ShaderBlockVariable.hpp"
#include "MaterialShaderCompiler.hpp"
#include <Render/Texture.hpp>
#include "Shader.hpp"
#include <Render/RenderSystem.hpp>
#include <QPixmap>

class MATERIALSYSTEM_EXPORT MaterialShaderBlockVariableTexture : public MaterialShaderBlockOutputVariable
{
  Q_OBJECT
  Q_PROPERTY(TextureRef texture READ texture WRITE setTexture)
  Q_CLASSINFO("BlockName", "Texture")
  Q_PROPERTY(RenderSystem::SamplerState sampler READ sampler WRITE setSampler)

public:
  Q_INVOKABLE MaterialShaderBlockVariableTexture()
  {
    m_texture = Resource::load<Texture>(":/Render/DefaultTexture.dds");
    m_texture->setInternal(true);
    m_sampler = RenderSystem::LinearWrap;
  }

public:
  RenderSystem::SamplerState sampler() const
  {
    return m_sampler;
  }
  void setSampler(RenderSystem::SamplerState sampler)
  {
    if(m_sampler != sampler)
    {
      m_sampler = sampler;
      changed();
    }
  }
  LinkType constType() const
  {
    return Sampler;
  }
  TextureRef texture() const
  {
    return m_texture;
  }
  void setTexture(const TextureRef& texture)
  {
    if(m_texture != texture)
    {
      m_texture = texture;
      m_pixmap = QPixmap();
      changed();
      emit constValueChanged(QVariant::fromValue(texture));
    }
  }
  QVariant constValue() const
  {
    return QVariant::fromValue(texture());
  }
  void setConstValue(const QVariant& value)
  {
    setTexture(value.value<TextureRef>());
  }  
  const QMetaObject *blockType() const
  {
    return &staticMetaObject;
  }
  virtual QPixmap constValuePixmap() const
  {
    if(m_pixmap.isNull() && m_texture)
    {
      QImage image = m_texture->toImage();

      if(!image.isNull())
      {
        m_pixmap = QPixmap::fromImage(image.scaled(64, 64, Qt::KeepAspectRatio));
      }
    }

    return m_pixmap;
  }

  void setRenderVariable(unsigned offset, const QVariant &value, bool onlyUsed)
  {
    if(!rawBaseMaterialShader())
      return;

    unsigned samplerOffset = offset + rawBaseMaterialShader()->samplerOffset();

    if(!onlyUsed || getRenderSystem().isSamplerUser(samplerOffset))
    {
      if(TextureRef texture = value.value<TextureRef>())
      {
        getRenderSystem().setTexture(*texture, samplerOffset);
        getRenderSystem().setSamplerState(m_sampler, samplerOffset);
      }
      else
      {
        getRenderSystem().setTexture(NULL, samplerOffset);
      }
    }
  }

private:
  QSharedPointer<Texture> m_texture;
  RenderSystem::SamplerState m_sampler;
  mutable QPixmap m_pixmap;
};

#include "VariableTexture.moc"
