#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectLayers : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "Layers")

  enum Inputs
  {
    iBase,
    iA,
    iB,
    iC,
    iD,
    iMask,
    iMax
  };

  enum Outputs
  {
    oOut,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectLayers()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iBase:     return Link(index, Float4, "Base");
    case iA:		    return Link(index, Float4, "A");
    case iB:        return Link(index, Float4, "B");
    case iC:		    return Link(index, Float4, "C");
    case iD:		    return Link(index, Float4, "D");
    case iMask:     return Link(index, Float4, "Mask");
    default:        return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oOut:		return Link(index, Float4, "Out");
    default:        return Link();
    }
  }
  void compileVarName(MaterialShaderCompiler &compiler, unsigned output, const QString &varName) const
  {
    compiler.ps("float4 %1 = %2;", varName, inputVarName(compiler, iBase));
    if(inputHasLink(iMask))
    {
      if(inputHasLink(iA))
        compiler.ps("%1 = lerp(%1, %2, %3.r);", varName, inputVarName(compiler, iA), inputVarName(compiler, iMask));
      if(inputHasLink(iB))
        compiler.ps("%1 = lerp(%1, %2, %3.g);", varName, inputVarName(compiler, iB), inputVarName(compiler, iMask));
      if(inputHasLink(iC))
        compiler.ps("%1 = lerp(%1, %2, %3.b);", varName, inputVarName(compiler, iC), inputVarName(compiler, iMask));
      if(inputHasLink(iD))
        compiler.ps("%1 = lerp(%1, %2, %3.a);", varName, inputVarName(compiler, iD), inputVarName(compiler, iMask));
     }
  }
};

#include "EffectLayers.moc"
