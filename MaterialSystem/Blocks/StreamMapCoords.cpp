#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockStreamMapCoords : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Stream")
  Q_CLASSINFO("BlockName", "MapCoords")
public:
  Q_INVOKABLE MaterialShaderBlockStreamMapCoords()
  {
  }

public:
  unsigned outputCount() const
  {
    return 1;
  }
  Link outputLink(unsigned index) const
  {
    return Link(index, Float2, "Coords");
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    return compiler.getp(MaterialShaderCompiler::pMapCoords);
  }
};

#include "StreamMapCoords.moc"
