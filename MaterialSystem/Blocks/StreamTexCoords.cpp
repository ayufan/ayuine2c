#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockStreamTexCoords : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Stream")
  Q_CLASSINFO("BlockName", "TexCoords")
public:
  Q_INVOKABLE MaterialShaderBlockStreamTexCoords()
  {
  }

public:
  unsigned outputCount() const
  {
    return 1;
  }
  Link outputLink(unsigned index) const
  {
    return Link(index, Float2, "Coords");
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    return compiler.getp(MaterialShaderCompiler::pTexCoords);
  }
};

#include "StreamTexCoords.moc"
