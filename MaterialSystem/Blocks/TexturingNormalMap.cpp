#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockTexturingNormalMap : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Texturing")
  Q_CLASSINFO("BlockName", "NormalMap")

  enum Inputs
  {
    iSampler,
    iCoords,
    iMax
  };

  enum Outputs
  {
    oNormal,
    oAlpha,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockTexturingNormalMap()
  {
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iSampler:      return Link(index, Sampler, "Sampler");
    case iCoords:		return Link(index, Float2, "Coords");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oNormal:         return Link(index, Float3, "Normal");
    case oAlpha:          return Link(index, Float, "Alpha");
    default:              return Link();
    }
  }
  QString compileNormal(MaterialShaderCompiler& compiler) const
  {
    QString varName = buildTempVarName(compiler, "nrm");
    if(compiler.addVariable(varName))
    {
      if(inputHasLink(iSampler))
        compiler.ps("float4 %1 = tex2D(%2, %3);", varName, inputVarName(compiler, iSampler), inputHasLink(iCoords) ? inputVarName(compiler, iCoords) : compiler.getp(MaterialShaderCompiler::pTexCoords));
      else
        compiler.ps("float4 %1 = float4(0,0,1,1);", varName);
    }
    return varName;
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
    switch(index)
    {
    case oNormal:
      if(inputHasLink(iSampler))
      {
        compiler.ps("float3 %1 = mul(%2 * 2.0f - 1.0f, float3x3(%3, %4, %5));",
                    varName, compileNormal(compiler),
                    compiler.getp(MaterialShaderCompiler::pTangent),
                    compiler.getp(MaterialShaderCompiler::pBinormal),
                    compiler.getp(MaterialShaderCompiler::pNormal));
      }
      else
      {
        compiler.ps("float3 %1 = %2;", varName, compiler.getp(MaterialShaderCompiler::pNormal));
      }
      break;

    case oAlpha:
      if(inputHasLink(iSampler))
        compiler.ps("float %1 = %2.a;", varName, compileNormal(compiler));
      else
        compiler.ps("float %1 = 1.0;", varName);
      break;
    }
  }
};

#include "TexturingNormalMap.moc"
