#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectSpot : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "Spot")
  Q_PROPERTY(float low READ low WRITE setLow)
  Q_PROPERTY(float high READ high WRITE setHigh)

  enum Inputs
  {
      iDir,
      iMax
  };

  enum Outputs
  {
      oSpot,
      oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectSpot()
  {
      m_low = 0.0f;
      m_high = 1.0f;
  }

public:
  float low() const
  {
    return m_low;
  }
  void setLow(float low)
  {
    m_low = qMax(qMin(low, m_high), 0.0f);
    changed();
  }
  float high() const
  {
    return m_high;
  }
  void setHigh(float high)
  {
    m_high = qMin(qMax(high, m_low), 1.0f);
    changed();
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iDir:        return Link(index, Float3, "Dir");
    default:          return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oSpot:       return Link(index, Float, "Spot");
    default:          return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
      compiler.ps("float %1 = saturate(((normalize(%2).z - 0.707) / (1.0 - 0.707) - %3) / %4);",
                varName,
                inputVarName(compiler, iDir),
                QString::number(1.0f - m_high),
                QString::number(m_high - m_low));
  }

private:
  float m_low;
  float m_high;
};

#include "EffectSpot.moc"
