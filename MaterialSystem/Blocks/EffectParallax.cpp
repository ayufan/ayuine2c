#include "ShaderBlock.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockEffectParallax : public ShaderBlock
{
  Q_OBJECT
  Q_CLASSINFO("BlockGroup", "Effect")
  Q_CLASSINFO("BlockName", "Parallax")
  Q_PROPERTY(float scale READ scale WRITE setScale)

  enum Inputs
  {
      iHeight,
      iCoords,
      iMax
  };

  enum Outputs
  {
      oCoords,
      oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockEffectParallax()
  {
    m_scale = 0.02f;
  }

public:
  float scale() const
  {
    return m_scale;
  }
  void setScale(float scale)
  {
    m_scale = scale;
    changed();
  }

public:
  unsigned inputCount() const
  {
    return iMax;
  }
  Link inputLink(unsigned index) const
  {
    switch(index)
    {
    case iHeight:		return Link(index, Float, "Height");
    case iCoords:		return Link(index, Float2, "Coords");
    default:            return Link();
    }
  }
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oCoords:		return Link(index, Float2, "Coords");
    default:            return Link();
    }
  }
  virtual void compileVarName(MaterialShaderCompiler& compiler, unsigned index, const QString& varName) const
  {
      switch(index)
      {
      case 0:
          if(inputHasLink(iHeight))
              compiler.ps("float2 %1 = %2 + %3.xy * (%4 * %5 - %6);", varName, inputHasLink(iCoords) ? inputVarName(compiler, iCoords) : compiler.getp(MaterialShaderCompiler::pTexCoords), compiler.getp(MaterialShaderCompiler::pCameraTanDir), inputVarName(compiler, iHeight), QString::number(m_scale), QString::number(m_scale * 0.5f));
          else
              compiler.ps("float2 %1 = %2;", varName, inputHasLink(iCoords) ? inputVarName(compiler, iCoords) : compiler.getp(MaterialShaderCompiler::pTexCoords));
          break;
      }
  }

private:
  float m_scale;
};

#include "EffectParallax.moc"
