#include "ShaderBlockInput.hpp"
#include "ShaderBlockOutput.hpp"
#include "MaterialShaderCompiler.hpp"

class MATERIALSYSTEM_EXPORT MaterialShaderBlockInputShaded : public ShaderBlockInput
{
  Q_OBJECT
  Q_CLASSINFO("BlockName", "Shaded")

  enum Outputs
  {
    oEmissive,
    oAmbient,
    oDiffuse,
    oGloss,
    oSpecularity,
    oNormal,
    oOpacity,
    oDistortion,
    oMax
  };

public:
  Q_INVOKABLE MaterialShaderBlockInputShaded()
  {
  }

public:
  unsigned outputCount() const
  {
    return oMax;
  }
  Link outputLink(unsigned index) const
  {
    switch(index)
    {
    case oAmbient:			return Link(index, Float3, "Ambient");
    case oEmissive:			return Link(index, Float3, "Emissive");
    case oDiffuse:			return Link(index, Float3, "Diffuse");
    case oGloss:            return Link(index, Float3, "Gloss");
    case oSpecularity:      return Link(index, Float, "Specularity");
    case oNormal:			return Link(index, Float3, "Normal");
    case oOpacity:			return Link(index, Float, "Opacity");
    case oDistortion:		return Link(index, Float3, "Distortion");
    default:                return Link();
    }
  }
  QString outputVarName(MaterialShaderCompiler &compiler, unsigned output) const
  {
    switch(output)
    {
    case oAmbient:
      if(!compiler.lit)
      {
        if(MaterialShaderCompiler::ShaderLink ambientLink = compiler.getLink("Ambient"))
          return ambientLink;
        if(MaterialShaderCompiler::ShaderLink colorLink = compiler.getLink("Color"))
          return colorLink;
      }
      return "float3(0,0,0)";

    case oEmissive:
      if(!compiler.lit)
      {
        if(MaterialShaderCompiler::ShaderLink emissiveLink = compiler.getLink("Emissive"))
          return emissiveLink;
      }
      return "float3(0,0,0)";

    case oDiffuse:
      if(!compiler.lit)
      {
        if(MaterialShaderCompiler::ShaderLink colorLink = compiler.getLink("Color"))
          return colorLink;
        if(MaterialShaderCompiler::ShaderLink diffuseLink = compiler.getLink("Diffuse"))
          return diffuseLink;
      }
      return "float3(1,1,1)";

    case oGloss:
      if(!compiler.lit)
      {
        if(MaterialShaderCompiler::ShaderLink glossLink = compiler.getLink("Gloss"))
          return glossLink;
      }
      return "float3(1,1,1)";

    case oNormal:
      if(MaterialShaderCompiler::ShaderLink normalLink = compiler.getLink("Normal"))
        return normalLink;
      return compiler.getp(MaterialShaderCompiler::pNormal);

    case oSpecularity:
      if(MaterialShaderCompiler::ShaderLink specularityLink = compiler.getLink("Specularity"))
        return specularityLink;
      return "float(16)";

    case oOpacity:
      if(MaterialShaderCompiler::ShaderLink alphaLink = compiler.getLink("Alpha"))
        return alphaLink;
      if(MaterialShaderCompiler::ShaderLink opacityLink = compiler.getLink("Opacity"))
        return opacityLink;
      return "float(1)";

    case oDistortion:
      if(MaterialShaderCompiler::ShaderLink distortionLink = compiler.getLink("Distortion"))
        return distortionLink;
      return "float2(0,0)";

    default:
      return "float4(0,0,0,0)";
    }
    return ShaderBlockInput::outputVarName(compiler, output);
  }
};

#include "InputShaded.moc"
