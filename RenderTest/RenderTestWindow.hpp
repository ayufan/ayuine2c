#ifndef RENDERTESTWINDOW_HPP
#define RENDERTESTWINDOW_HPP

#include <QMainWindow>
#include <Render/Texture.hpp>

namespace Ui {
class RenderTestWindow;
}

class RenderTestWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit RenderTestWindow(QWidget *parent = 0);
  ~RenderTestWindow();

public slots:
  void loadFile();
  void saveToFile();

private:
  Ui::RenderTestWindow *ui;

  QSharedPointer<Texture> texture;
};

#endif // RENDERTESTWINDOW_HPP
