sampler TextureSampler : register(s0);
sampler LuminanceSampler : register(s2);

float4 frameParams : register(c0);

static const float3 LUM_CONVERT = float3(0.299f, 0.587f, 0.114f);

float3 ToneMap(float3 color)
{
  const float g_fMiddleGrey = 0.6f;
  const float g_fMaxLuminance = 16.0f;

  // Get the calculated average luminance
  float lumAvg = exp(tex2D(LuminanceSampler, float2(0.5f, 0.5f)).r);

  // Calculate the luminance of the current pixel
  float lumPixel = dot(color, LUM_CONVERT);

  // Apply the modified operator (Eq. 4)
  float lumScaled = (lumPixel * g_fMiddleGrey) / lumAvg;
  // return 1.0f - exp(-lumScaled * color);
  float lumCompressed = (lumScaled * (1 + (lumScaled / (g_fMaxLuminance * g_fMaxLuminance)))) / (1 + lumScaled);
  return lumCompressed * color;
}

float4 VertexShader(
  in float3 inPos : POSITION0,
  in float2 inTexCoord : TEXCOORD0,
  out float2 outTexCoord : TEXCOORD0) : POSITION0
{
  outTexCoord = inTexCoord;
  return float4(inPos, 1.0f);
}

float4 LumPixelShader(
  float2 texCoord : TEXCOORD0,
  uniform sampler SceneSampler : register(s0)) : COLOR0
{
  float3 color = 0;
  for (int x = 0; x < 4; x++)
    for (int y = 0; y < 4; y++)
      color += tex2D(SceneSampler, texCoord + float2(x, y) * frameParams.xy * 0.25f).rgb;
  color /= 16.0f;
  float lum = dot(color, LUM_CONVERT);
  float logLum = log(1e-5 + lum);
  return float4(logLum, 0, 0, 1.0f);
}

float4 LumDownsamplePixelShader(
  uniform sampler LuminanceSampler : register(s0),
  float2 texCoord : TEXCOORD0) : COLOR0
{
  float color = 0;
  for (int x = 0; x < 4; x++)
    for (int y = 0; y < 4; y++)
      color += tex2D(LuminanceSampler, texCoord + float2(x, y) * frameParams.xy * 0.25f).r;
  color /= 16.0f;
  return float4(color, 0, 0, 1.0f);
}

float4 LumAdaptPixelShader(
  uniform sampler OldLuminanceSampler : register(s0),
  float2 texCoord : TEXCOORD0) : COLOR0
{
  float lastLum = exp(tex2D(OldLuminanceSampler, float2(0.5f, 0.5f)).r);
  float currentLum = exp(tex2D(LuminanceSampler, float2(0.5f, 0.5f)).r);

  // Adapt the luminance using Pattanaik's technique
  const float tau = 0.5f;
  float adaptedLum = lastLum + (currentLum - lastLum) * (1 - exp(-frameParams.z * tau));

  return float4(log(adaptedLum), 0, 0, 1.0f);
}

float4 ExtractPixelShader(
  float2 texCoord : TEXCOORD0,
  uniform sampler SceneSampler : register(s0),
  uniform float BloomThreshold : register(c10)) : COLOR0
{
  float3 color = tex2D(SceneSampler, texCoord).rgb;
  color = ToneMap(color);
  color -= BloomThreshold;
  color = max(color, 0.0f);
  return float4(color, 1.0f);
}

float4 CombinePixelShader(
  in float2 texCoord : TEXCOORD0,
  uniform float4 BloomParams : register(c10),
  uniform sampler BloomSampler : register(s0),
  uniform sampler BaseSampler : register(s1)) : COLOR0
{
  float3 color = tex2D(BaseSampler, texCoord - frameParams.xy * 0.5f).rgb;
  color = ToneMap(color) + tex2D(BloomSampler, texCoord).rgb * BloomParams.x;
  return float4(color, 1.0f);
}

static float2 delta[8] =
{
  float2(-1,1), float2(1,-1), float2(-1,1), float2(1,1), 
  float2(-1,0), float2(1,0), float2(0,-1), float2(0,1) 
};

float antiAliasFactor(uniform sampler normalSampler, float2 texCoord)
{
  float3 normal = tex2D(normalSampler, texCoord).xyz;

  float factor = 0.0f; 
  for(int i = 0; i < 4; i++) 
  { 
    float normal2 = tex2D(normalSampler, texCoord + delta[i] * frameParams.xy) - normal;
    factor += dot(normal2, normal2); 
  }
  factor = min(0.5f, factor) * 4.0f;

  return factor;
}

float3 antiAliasColor(uniform sampler baseSampler, float2 texCoord, float factor)
{  
  float3 color = 2.0f * tex2D(baseSampler, texCoord).rgb;
 
  for(int i = 0; i < 8; i++) 
  { 
    color += tex2D(baseSampler, texCoord + delta[i] * frameParams.xy * factor); 
  } 

  return color * 0.1f;
}

float4 CombineEdgesPixelShader(
  in float2 texCoord : TEXCOORD0,
  uniform float4 BloomParams : register(c10),
  uniform sampler BloomSampler : register(s0),
  uniform sampler BaseSampler : register(s1),
  uniform sampler NormalSampler : register(s3)) : COLOR0
{
  float2 texCoordOffset = texCoord - frameParams.xy * 0.5f;
  float factor = antiAliasFactor(NormalSampler, texCoordOffset);
  float3 color = antiAliasColor(BaseSampler, texCoordOffset, factor);
  color = ToneMap(color) + tex2D(BloomSampler, texCoord).rgb * BloomParams.x;
  return float4(color, 1.0f);
}
