#include "SpotLight.hpp"
#include "Frame.hpp"
#include <Math/Sphere.hpp>
#include <Render/RenderQuery.hpp>
#include <Render/RenderSystem.hpp>

SpotLight::SpotLight()
{
}

bool SpotLight::test(const box &, const sphere &sphereBounds) const
{
  if(m_camera.culler.test(sphereBounds))
    return true;
  return false;
}

bool SpotLight::set(bool transparent) const
{
  if(!lightState())
    return false;

  switch(m_visibilityState)
  {
  case Undefined:
    m_visibilityState = Visible;

  case Visible:
    getRenderSystem().setDepthStencilState(RenderSystem::DepthRead);
    break;

  case NotVisible:
    return false;
  }

  if(transparent)
    getRenderSystem().setBlendState(RenderSystem::Overlay);
  else
    getRenderSystem().setBlendState(RenderSystem::Additive);

  getRenderSystem().setRasterizerState(RenderSystem::CullClockwise);

  m_lightState.setConsts(false);

  return true;
}

void SpotLight::draw(RenderShader* shader, Frame& frame) const
{
  if(!lightState())
    return;

  // clear stencil
  getRenderSystem().setShader(RenderSystem::FixedConst);
  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setBlendState(RenderSystem::DisableColorWrite);
  getRenderSystem().setDepthStencilState(RenderSystem::DepthClearStencil);
  getRenderSystem().setRasterizerState(RenderSystem::CullCounterClockwise);
  getRenderSystem().drawCone(m_camera.culler.invObject());

  // fill stencil
  getRenderSystem().setDepthStencilState(RenderSystem::DepthFillEarlyCull);
  getRenderSystem().setRasterizerState(RenderSystem::CullNone);
  getRenderSystem().drawCone(m_camera.culler.invObject());

  // draw light
  getRenderSystem().setDepthStencilState(RenderSystem::DepthRenderEarlyCull);
  getRenderSystem().setBlendState(RenderSystem::Additive);
  getRenderSystem().setRasterizerState(RenderSystem::CullCounterClockwise);
  getRenderSystem().setShader(shader);
  m_lightState.setConsts();
  getRenderSystem().drawCone(m_camera.culler.invObject());
}

const Camera & SpotLight::camera() const
{
  return m_camera;
}

void SpotLight::setCamera(const Camera &camera)
{
  m_camera = camera;
}
