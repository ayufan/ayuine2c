#include "AmbientLight.hpp"
#include "Frame.hpp"

#include <Render/RenderSystem.hpp>
#include <MaterialSystem/ShaderState.hpp>

AmbientLight::AmbientLight()
{
  lightState().setMaterialShader("SimpleAmbientModel.grcz");
}

bool AmbientLight::test(const box &, const sphere &) const
{
  return true;
}

bool AmbientLight::set(bool transparent) const
{
  if(!lightState())
    return false;

  if(transparent)
  {
    getRenderSystem().setDepthStencilState(RenderSystem::DepthDefault);
    getRenderSystem().setBlendState(RenderSystem::AlphaBlend);
  }
  else
  {
    getRenderSystem().setDepthStencilState(RenderSystem::DepthDefault);
    getRenderSystem().setBlendState(RenderSystem::Opaque);
  }

  getRenderSystem().setRasterizerState(RenderSystem::CullClockwise);

  m_lightState.setConsts(false);

  return true;
}

void AmbientLight::draw(RenderShader *shader, Frame &frame) const
{
  if(!lightState())
    return;

  getRenderSystem().setShader(shader);
  getRenderSystem().setFillMode(RenderSystem::Solid);
  // getRenderSystem().setBlendState(RenderSystem::Opaque);
  getRenderSystem().setBlendState(RenderSystem::Additive);
  getRenderSystem().setDepthStencilState(RenderSystem::DepthNone);
  getRenderSystem().setRasterizerState(RenderSystem::CullNone);
  m_lightState.setConsts();
  // getRenderSystem().setProjView(mat4Identity, mat4Identity);
  // getRenderSystem().drawScreenQuad();
  getRenderSystem().drawSphere(sphere(frame.camera().origin, 10.0f));
  // getRenderSystem().setCamera(frame.camera());
}
