#ifndef FRAGMENT_HPP
#define FRAGMENT_HPP

#include <Render/VertexBuffer.hpp>
#include <Render/RenderSystem.hpp>
#include <MaterialSystem/ShaderState.hpp>
#include <MaterialSystem/Material.hpp>
#include "Pipeline.hpp"

#include <QScopedPointer>

class Frame;
class FragmentData;
class GeneralShader;
class Light;
class RenderQuery;

class PIPELINE_EXPORT Fragment
{
public:
  Fragment();
  virtual ~Fragment();

public:
  int order() const;
  void setOrder(int order);

public:
  virtual VertexBuffer::Types vertexType() const = 0;
  virtual bool usesInstancing() const;
  virtual float distance(const vec3& cameraOrigin, const vec4& cameraPlane) const;
  virtual Material::OpaqueMode opaque() const;

public:
  Fragment* next() const;
  Frame* frame() const;
  bool unlink();

public:
  virtual bool prepare();
  virtual bool bind(const Light* lights);
  virtual void draw() = 0;
  virtual void unbind();
  virtual void render(const Light* lights);

public:
  virtual ShaderState *materialShaderState() const = 0;
  GeneralShader* materialShader() const;
  Shader* rawMaterialShader() const;
  virtual MaterialSystem::Quality materialQuality() const;

private:
  int m_order;
  Fragment* m_next;
  Frame* m_frame;

public:
  Fragment* nextMaterial;
  Fragment* nextDistortion;
  Fragment* internalNext;
  float internalDistance;
  QScopedPointer<RenderQuery> occlusion;

  friend class Frame;
};

#endif // FRAGMENT_HPP

