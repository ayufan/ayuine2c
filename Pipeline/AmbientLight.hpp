#ifndef AMBIENTLIGHT_HPP
#define AMBIENTLIGHT_HPP

#include "Light.hpp"

#include <MaterialSystem/ShaderState.hpp>

class PIPELINE_EXPORT AmbientLight : public Light
{
public:
  AmbientLight();

public:
  bool set(bool transparent) const;
  void draw(RenderShader* shader, Frame& frame) const;

  bool test(const box& boxBounds, const sphere& sphereBounds) const;
};

#endif // AMBIENTLIGHT_HPP
