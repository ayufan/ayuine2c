#include "SimpleFrame.hpp"
#include "Fragment.hpp"
#include "Light.hpp"
#include <Render/RenderSystem.hpp>
#include <Render/RenderShader.hpp>
#include <MaterialSystem/ShaderState.hpp>
#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/VertexShaderCompiler.hpp>
#include <QDebug>

SimpleFrame::SimpleFrame()
{
}

void SimpleFrame::draw()
{
  prepareFrame(true);

  getRenderSystem().setCamera(camera());
  getRenderSystem().setFrame(deltaTime(), frameTime());

  renderOrderedFrags(OrderNegative);

  if(isWireframe())
    getRenderSystem().setFillMode(RenderSystem::Wire);
  else
    getRenderSystem().setFillMode(RenderSystem::Solid);

  getRenderSystem().setBlendState(RenderSystem::Opaque);

  renderSolidFrags(NULL);

  renderTransparentFrags(false);

  renderOrderedFrags(OrderPositive);
}

bool SimpleFrame::bindShader(GeneralShader *materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  if(!materialShader)
    return false;

  unsigned materialIndex = vertexToIndex(vertexType, quality);

  RenderShaderRef shader = materialShader->shader(materialIndex, NULL);

  if(!shader)
  {
    shader = readShader(NULL, materialShader, quality, vertexType);
    if(shader)
      materialShader->setShader(materialIndex, NULL, shader);
  }

  if(!shader)
  {
    VertexShaderCompiler compiler(vertexType);
    compiler.quality = quality;
    compile(compiler, materialShader->findOutput());
    shader = compiler.shader();
    materialShader->setShader(materialIndex, NULL, shader);

    writeShader(compiler.code(), NULL, materialShader, quality, vertexType);
  }

  if(!shader)
  {
    return false;
  }

  getRenderSystem().setShader(shader.data());
  return true;
}
