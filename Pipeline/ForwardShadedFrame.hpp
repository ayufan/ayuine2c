#ifndef FORWARDSHADEDFRAME_HPP
#define FORWARDSHADEDFRAME_HPP

#include "Frame.hpp"

class MaterialShaderCompiler;

class ShaderBlock;

class PIPELINE_EXPORT ForwardShadedFrame : public Frame
{
  Q_OBJECT

public:
  Q_INVOKABLE ForwardShadedFrame();

public:
  void draw();
  FrameType type() const;
  bool hasLighting() const;

protected:
  bool bindShader(GeneralShader* materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);
  bool bindLightShader(GeneralShader* materialShader, LightShader* lightShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);

  virtual void compileLightColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);
  virtual void compileSolidColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);
};

#endif // FORWARDSHADEDFRAME_HPP
