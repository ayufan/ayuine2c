#include "Fragment.hpp"
#include "Fragment_p.hpp"
#include "Frame.hpp"
#include <Render/RenderQuery.hpp>
#include <MaterialSystem/MaterialShader.hpp>

Fragment::Fragment()
{
  m_order = 0;
  m_next = NULL;
  m_frame = NULL;
  internalDistance = 0.0f;
}

Fragment::~Fragment()
{
}

int Fragment::order() const
{
  return m_order;
}

void Fragment::setOrder(int order)
{
  m_order = order;
}

Fragment * Fragment::next() const
{
  return m_next;
}

Frame * Fragment::frame() const
{
  return m_frame;
}

bool Fragment::unlink()
{
  if(m_frame)
  {
    for(Fragment** fragment = &m_frame->m_fragments; *fragment; fragment = &(*fragment)->m_next)
    {
      if(*fragment == this)
      {
        *fragment = m_next;
        m_next = NULL;
        m_frame = NULL;
        return true;
      }
    }

    m_next = NULL;
    m_frame = NULL;
    return false;
  }
  return true;
}

bool Fragment::prepare()
{
  return true;
}

bool Fragment::bind(const Light* lights)
{
  return true;
}

void Fragment::unbind()
{
}

void Fragment::render(const Light* lights)
{
  if(bind(lights))
  {
    draw();
    unbind();
  }
}

GeneralShader * Fragment::materialShader() const
{
  ShaderState* state = materialShaderState();
  if(state)
  {
    return qobject_cast<GeneralShader*>(state->materialShader().data());
  }
  return NULL;
}

float Fragment::distance(const vec3 &cameraOrigin, const vec4& cameraPlane) const
{
  return FLT_MAX;
}

MaterialSystem::Quality Fragment::materialQuality() const
{
  return MaterialSystem::High;
}

Material::OpaqueMode Fragment::opaque() const
{
  return Material::Opaque;
}

bool Fragment::usesInstancing() const
{
  return false;
}

Shader * Fragment::rawMaterialShader() const
{
  ShaderState* state = materialShaderState();
  if(state)
    return state->rawMaterialShader();
  return NULL;
}
