#ifndef DEFERREDSHADEDFRAME_HPP
#define DEFERREDSHADEDFRAME_HPP

#include "Frame.hpp"

#include <Render/RenderTarget.hpp>
#include <Render/RenderQuery.hpp>

class PIPELINE_EXPORT DeferredShadedFrame : public Frame
{
public:
  Q_INVOKABLE explicit DeferredShadedFrame();

public:
  bool begin();
  void draw();
  FrameType type() const;
  bool hasLighting() const;

public:
  RenderTarget* depthTarget() const;
  RenderTarget* normalTarget() const;

protected:
  void resizeEvent(const QSize& newSize);

protected:
  bool bindShader(GeneralShader* materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);
  void renderLight(Light* light);

private:
  QScopedPointer<RenderTarget> m_target0, m_target1, m_target2;
};

#endif // DEFERREDSHADEDFRAME_HPP
