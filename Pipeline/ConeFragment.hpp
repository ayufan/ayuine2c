#ifndef CONEFRAGMENT_HPP
#define CONEFRAGMENT_HPP

#include "TexturedFragment.hpp"
#include <Math/Box.hpp>

class PIPELINE_EXPORT ConeFragment : public TexturedFragment
{
public:
  ConeFragment();
  ~ConeFragment();

public:
  VertexBuffer::Types vertexType() const;

public:
  void draw();
};

#endif // CONEFRAGMENT_HPP
