#include "SkyFragment.hpp"
#include "Frame.hpp"

#include <Math/Camera.hpp>
#include <Math/Vec3.hpp>
#include <Math/Matrix.hpp>

SkyFragment::SkyFragment()
{
  setOrder(100000);
}

ShaderState * SkyFragment::materialShaderState() const
{
  return &m_state;
}

ShaderState & SkyFragment::state()
{
  return m_state;
}

const ShaderState & SkyFragment::state() const
{
  return m_state;
}

void SkyFragment::setState(const ShaderState &lightState)
{
  m_state = lightState;
}

VertexBuffer::Types SkyFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}

bool SkyFragment::bind(const Light *lights)
{
  if(!m_state.rawMaterialShader())
    return false;

  if(!frame())
    return false;

  getRenderSystem().setTransform(mat4Identity);
  getRenderSystem().setColor(Qt::white);
  getRenderSystem().setRasterizerState(RenderSystem::CullNone);
  getRenderSystem().setBlendState(RenderSystem::Opaque);
  getRenderSystem().setDepthStencilState(RenderSystem::DepthRead);
  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setAlphaTest(RenderSystem::AlphaTestDisabled);
  return true;
}

void SkyFragment::draw()
{
  if(frame())
  {
    float size = 10000.0f;

    if(!qFuzzyIsNull(frame()->camera().distance))
    {
      size = frame()->camera().distance * 0.5f;
    }

    getRenderSystem().drawSphere(sphere(frame()->camera().origin - vec3(0, 0, size * 0.5f), size));
  }
}

void SkyFragment::unbind()
{
}
