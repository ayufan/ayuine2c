#include "SphereFragment.hpp"

SphereFragment::SphereFragment()
{
}

SphereFragment::~SphereFragment()
{
}

const sphere & SphereFragment::bounds() const
{
  return m_bounds;
}

void SphereFragment::setBounds(const sphere &bounds)
{
  m_bounds = bounds;
}

void SphereFragment::draw()
{
#if 0
  // TODO
  getRender().drawSphere(m_bounds.Center, m_bounds.Radius);
#endif
}

VertexBuffer::Types SphereFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}
