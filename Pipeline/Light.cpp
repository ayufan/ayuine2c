#include "Light.hpp"
#include "Frame.hpp"

#include <MaterialSystem/LightShader.hpp>

Light::Light()
{
  m_frame = NULL;
  m_next = NULL;
  m_visibilityState = Undefined;
  m_quality = MaterialSystem::High;
}

Light::~Light()
{
}

Frame * Light::frame() const
{
  return m_frame;
}

Light * Light::next() const
{
  return m_next;
}

bool Light::unlink()
{
  if(m_frame)
  {
    for(Light** light = &m_frame->m_lights; *light; light = &(*light)->m_next)
    {
      if(*light == this)
      {
        *light = m_next;
        m_next = NULL;
        m_frame = NULL;
        return true;
      }
    }

    m_next = NULL;
    m_frame = NULL;
    return false;
  }
  return true;
}

ShaderState& Light::lightState() const
{
  return m_lightState;
}

LightShader * Light::lightShader() const
{
  return qobject_cast<LightShader*>(m_lightState.materialShader().data());
}

Light::VisibilityState Light::visibilityState() const
{
  return m_visibilityState;
}

void Light::draw(RenderShader* shader, Frame& frame) const
{
}

MaterialSystem::Quality Light::lightQuality() const
{
  return m_quality;
}

void Light::setLightState(const ShaderState &lightState)
{
  m_lightState = lightState;
}

void Light::setLightQuality(MaterialSystem::Quality quality)
{
  m_quality = quality;
}
