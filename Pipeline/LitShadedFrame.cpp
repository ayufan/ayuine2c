#include "LitShadedFrame.hpp"

#include <MaterialSystem/MaterialShaderCompiler.hpp>

LitShadedFrame::LitShadedFrame()
{
}

void LitShadedFrame::compileLightColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output)
{
  compiler.lit = true;
  ForwardShadedFrame::compileLightColor(compiler, output);
}

Frame::FrameType LitShadedFrame::type() const
{
  return LitShaded;
}
