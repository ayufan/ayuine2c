#ifndef BASESHADOWFRAME_P_HPP
#define BASESHADOWFRAME_P_HPP

#include <QSharedPointer>
#include <QObject>
#include <QElapsedTimer>

#include <Render/RenderTarget.hpp>

#include <Math/Vec3.hpp>

struct ShadowTexture
{
  QSharedPointer<BaseRenderTarget> renderTarget;
  QObject* owner;
  float frameTime;

  ShadowTexture()
  {
    owner = NULL;
    frameTime = 0;
  }
};

#endif // BASESHADOWFRAME_P_HPP
