#include "PointLight.hpp"
#include "Frame.hpp"
#include <Math/Sphere.hpp>
#include <Render/RenderQuery.hpp>
#include <Render/RenderSystem.hpp>

PointLight::PointLight()
{
  m_radius = 0.0f;
}

const vec3 &PointLight::origin() const
{
  return m_origin;
}

void PointLight::setOrigin(const vec3 &origin)
{
  m_origin = origin;
}

float PointLight::radius() const
{
  return m_radius;
}

void PointLight::setRadius(float radius)
{
  m_radius = radius;
}

bool PointLight::test(const box &, const sphere &sphereBounds) const
{
  if(sphere(m_origin, m_radius).test(sphereBounds))
    return true;
  return false;
}

bool PointLight::set(bool transparent) const
{
  if(!lightState())
    return false;

  switch(m_visibilityState)
  {
  case Undefined:
#if 1
    if(frame() && !sphere(m_origin, m_radius).test(frame()->camera().origin))
    {
      getRenderSystem().setDepthStencilState(RenderSystem::DepthRead);
      getRenderSystem().setBlendState(RenderSystem::DisableColorWrite);
      getRenderSystem().setFillMode(RenderSystem::Solid);
      getRenderSystem().setRasterizerState(RenderSystem::CullNone);
      getRenderSystem().setShader(RenderSystem::FixedConst);

      // Perfom occlusion query
      QScopedPointer<RenderQuery> query(new RenderQuery(RenderQuery::Occlussion));

      if(query->begin())
      {
        getRenderSystem().drawSphere(sphere(origin(), radius()));
        query->end();
        if(query->result() < 50)
        {
          m_visibilityState = NotVisible;
          return false;
        }
      }
    }
#endif
    m_visibilityState = Visible;

  case Visible:
    getRenderSystem().setDepthStencilState(RenderSystem::DepthRead);
    break;

  case NotVisible:
    return false;
  }

  if(transparent)
    getRenderSystem().setBlendState(RenderSystem::Overlay);
  else
    getRenderSystem().setBlendState(RenderSystem::Additive);

  getRenderSystem().setRasterizerState(RenderSystem::CullClockwise);

  m_lightState.setConsts(false);

  return true;
}

void PointLight::draw(RenderShader* shader, Frame& frame) const
{
  if(!lightState())
    return;

  sphere lightSphere(origin(), radius() * 1.1f);

  // clear stencil
  getRenderSystem().setShader(RenderSystem::FixedConst);
  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setBlendState(RenderSystem::DisableColorWrite);
  getRenderSystem().setDepthStencilState(RenderSystem::DepthClearStencil);
  getRenderSystem().setRasterizerState(RenderSystem::CullCounterClockwise);
  getRenderSystem().drawSphere(lightSphere, 2);

#if 0
  // Perfom occlusion query
  QScopedPointer<Query> query(new Query(Query::Occlussion));
  bool hasQuery = query->begin();
#endif

  // fill stencil
  getRenderSystem().setDepthStencilState(RenderSystem::DepthFillEarlyCull);
  getRenderSystem().setRasterizerState(RenderSystem::CullNone);
  getRenderSystem().drawSphere(lightSphere, 2);

#if 0
  if(hasQuery)
  {
    query->end();

    if(query->result() < 50)
    {
      m_visibilityState = NotVisible;
      return;
    }
    else
    {
      m_visibilityState = Visible;
    }
  }
#endif

  // draw light
  getRenderSystem().setDepthStencilState(RenderSystem::DepthRenderEarlyCull);
  getRenderSystem().setBlendState(RenderSystem::Additive);
  getRenderSystem().setRasterizerState(RenderSystem::CullCounterClockwise);
  getRenderSystem().setShader(shader);
  m_lightState.setConsts();
  getRenderSystem().drawSphere(lightSphere, 2);
}
