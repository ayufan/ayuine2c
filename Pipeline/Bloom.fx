sampler TextureSampler : register(s0);

float4 frameParams : register(c0);

float BloomThreshold : register(c6);
float BloomIntensity : register(c7);
#define SAMPLE_COUNT 15

float4 VertexShader(
  in float3 inPos : POSITION0,
  in float2 inTexCoord : TEXCOORD0,
  out float2 outTexCoord : TEXCOORD0) : POSITION0
{
  outTexCoord = inTexCoord;
  return float4(inPos, 1.0f);
}

float4 ExtractPixelShader(
  float2 texCoord : TEXCOORD0,
  uniform float BloomThreshold : register(c10)) : COLOR0
{
  float4 c = tex2D(TextureSampler, texCoord);
  return saturate((c - BloomThreshold) / (1 - BloomThreshold));
}

float4 BlurPixelShader(
  in float2 texCoord : TEXCOORD0,
  uniform float3 Samples[SAMPLE_COUNT] : register(c10)) : COLOR0
{
  float4 c = 0;
  for(int i = 0; i < SAMPLE_COUNT; i++)
    c += tex2D(TextureSampler, texCoord + Samples[i].xy) * Samples[i].z;
  return c;
}

float4 AdjustSaturation(float4 color, float saturation)
{
  // The constants 0.3, 0.59, and 0.11 are chosen because the
  // human eye is more sensitive to green light, and less to blue.
  float grey = dot(color.rgb, float3(0.3, 0.59, 0.11));
  return lerp(grey, color, saturation);
}

float4 CombinePixelShader(
  in float2 texCoord : TEXCOORD0,
  uniform float4 BloomParams : register(c10),
  uniform sampler BloomSampler : register(s0),
  uniform sampler BaseSampler : register(s1)) : COLOR0
{
  // Look up the bloom and original base image colors.
  float4 bloom = tex2D(BloomSampler, texCoord);
  float4 base = tex2D(BaseSampler, texCoord - frameParams.xy * 0.5f);

  // Adjust color saturation and intensity.
  bloom = AdjustSaturation(bloom, BloomParams.z) * BloomParams.x;
  base = AdjustSaturation(base, BloomParams.w) * BloomParams.y;

  // Darken down the base image in areas where there is a lot of bloom,
  // to prevent things looking excessively burned-out.
  base *= (1 - saturate(bloom));

  // Combine the two images.
  return base + bloom;
}
