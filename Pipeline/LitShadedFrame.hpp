#ifndef LITSHADEDFRAME_HPP
#define LITSHADEDFRAME_HPP

#include "ForwardShadedFrame.hpp"

class PIPELINE_EXPORT LitShadedFrame : public ForwardShadedFrame
{
  Q_OBJECT

public:
  Q_INVOKABLE LitShadedFrame();

public:
  FrameType type() const;

protected:
  void compileLightColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);
};

#endif // LITSHADEDFRAME_HPP
