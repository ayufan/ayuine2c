#ifndef SIMPLEFRAME_HPP
#define SIMPLEFRAME_HPP

#include "Pipeline.hpp"
#include "Frame.hpp"

#include <Render/VertexBuffer.hpp>

class RenderShader;
class ShaderState;
class GeneralShader;
class ShaderBlock;
class MaterialShaderCompiler;

class PIPELINE_EXPORT SimpleFrame : public Frame
{
  Q_OBJECT

public:
  SimpleFrame();

public:
  void draw();

protected:
  bool bindShader(GeneralShader *materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);

protected:
  virtual void compile(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output) = 0;
};

#endif // SIMPLEFRAME_HPP
