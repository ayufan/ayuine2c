#ifndef BASESHADOWFRAME_HPP
#define BASESHADOWFRAME_HPP

#include "Pipeline.hpp"
#include "DepthFrame.hpp"

#include <Render/RenderTarget.hpp>

typedef QSharedPointer<BaseRenderTarget> ShadowTextureRef;

struct ShadowTexture;

class PIPELINE_EXPORT BaseShadowFrame : public DepthFrame
{
  Q_OBJECT

public:
  enum UpdateMode
  {
    Ignore,
    Full,
    Partial
  };

public:
  BaseShadowFrame();
  ~BaseShadowFrame();

public:
  UpdateMode update(QObject* owner, ShadowTextureRef& renderTarget);
  ShadowTextureRef find(QObject* owner);
  bool free(QObject* owner);
  void nextFrame(float dt);

public:
  unsigned shadowCount() const;
  unsigned shadowUsedCount() const;
  unsigned estimatedMemUsage() const;

private slots:
  void deviceLost();

public:
  virtual QSharedPointer<BaseRenderTarget> allocRenderTarget(unsigned size) = 0;  
  virtual QSharedPointer<BaseRenderTarget> cleanRenderTarget() = 0;

private:
  QVector<ShadowTexture> m_shadows;
  float m_frameTime;

  friend struct ShadowTexture;
};

#endif // BASESHADOWFRAME_HPP
