#include "HdrComponent.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/RenderComponentWidget.hpp>
#include <Render/RenderShader.hpp>
#include <Render/RenderTarget.hpp>

#include <Math/Vec2.hpp>
#include <Math/Vec4.hpp>
#include <Math/Matrix.hpp>

HdrComponent::HdrComponent(bool antiEdges) : m_antiEdges(antiEdges)
{
  m_shaderLum.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Hdr.fx", "Lum"));
  m_shaderLumDownsample.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Hdr.fx", "LumDownsample"));
  m_shaderLumAdapt.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Hdr.fx", "LumAdapt"));
  m_shaderExtract.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Hdr.fx", "Extract"));
  m_shaderCombine.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Hdr.fx", "Combine"));
  m_shaderCombineEdges.reset(new RenderShader(RenderShader::File, ":/PipelineShaders/Hdr.fx", "CombineEdges"));
}

HdrComponent::~HdrComponent()
{
}

void HdrComponent::resizeEvent(const QSize &newSize)
{
  m_renderLuminance.clear();
  m_sceneRenderTarget.reset(new RenderTarget(newSize.width(), newSize.height(), RenderTarget::RgbaFloat));
  m_renderTarget1.reset(new RenderTarget(newSize.width() / 2, newSize.height() / 2, RenderTarget::Rgba));
  m_renderTarget2.reset(new RenderTarget(newSize.width() / 2, newSize.height() / 2, RenderTarget::Rgba));

  int newWidth = qMax(newSize.width(), 4);
  int newHeight = qMax(newSize.height(), 4);

  do
  {
    newWidth = qMax(newWidth / 4, 1);
    newHeight = qMax(newHeight / 4, 1);

    m_renderLuminance.append(QSharedPointer<RenderTarget>(new RenderTarget(newWidth, newHeight, RenderTarget::RFloat)));
  }
  while(newWidth > 2 || newHeight > 2);

  m_renderAdaptLuminance[0].reset(new RenderTarget(1, 1, RenderTarget::RFloat));
  m_renderAdaptLuminance[1].reset(new RenderTarget(1, 1, RenderTarget::RFloat));
}

void HdrComponent::draw()
{
  prepare();

  if(m_renderLuminance.size())
  {
    lumScene(m_sceneRenderTarget.data(), m_renderLuminance.front().data());

    for(int i = 1; i < m_renderLuminance.size(); ++i)
    {
      lumDownsample(m_renderLuminance[i-1].data(), m_renderLuminance[i].data());
    }

    lumAdapt(m_renderLuminance.back().data(), m_renderAdaptLuminance[0].data(), m_renderAdaptLuminance[1].data());

    m_renderAdaptLuminance[0].swap(m_renderAdaptLuminance[1]);
  }

  extractBloom(m_sceneRenderTarget.data(), m_renderAdaptLuminance[0].data(), m_renderTarget1.data());
  blurTarget(m_renderTarget1.data(), m_renderTarget2.data(), 1.0f / (float)m_renderTarget1->width(), 0.0f);
  blurTarget(m_renderTarget2.data(), m_renderTarget1.data(), 0.0f, 1.0f / (float)m_renderTarget1->height());
  blendTargets(m_sceneRenderTarget.data(), m_renderTarget1.data(), m_renderAdaptLuminance[0].data());
}

void HdrComponent::lumScene(RenderTarget *scene, RenderTarget *out)
{
  getRenderSystem().setShader(m_shaderLum.data());
  getRenderSystem().setRenderTarget(out);
  getRenderSystem().setTexture(scene, 0);
  getRenderSystem().drawScreenQuad();
}

void HdrComponent::lumDownsample(RenderTarget *lum, RenderTarget *out)
{
  getRenderSystem().setShader(m_shaderLumDownsample.data());
  getRenderSystem().setRenderTarget(out);
  getRenderSystem().setTexture(lum, 0);
  getRenderSystem().drawScreenQuad();
}

void HdrComponent::lumAdapt(RenderTarget *lum, RenderTarget *oldLum, RenderTarget *out)
{
  getRenderSystem().setShader(m_shaderLumAdapt.data());
  getRenderSystem().setRenderTarget(out);
  getRenderSystem().setTexture(oldLum, 0);
  getRenderSystem().setTexture(lum, 2);
  getRenderSystem().drawScreenQuad();
}

void HdrComponent::extractBloom(RenderTarget *in, RenderTarget *lum, RenderTarget *out)
{
  getRenderSystem().setShader(m_shaderExtract.data());
  getRenderSystem().setConst(10, &m_bloomThreshold, 1);
  getRenderSystem().setRenderTarget(out);
  getRenderSystem().setTexture(in, 0);
  getRenderSystem().setTexture(lum, 2);
  getRenderSystem().drawScreenQuad();
}

void HdrComponent::blendTargets(RenderTarget *base, RenderTarget *blur, RenderTarget *lum)
{
  if(!setWidgetRenderTarget())
    return;

  if(RenderTarget* normalTarget = m_antiEdges ? renderWidget()->normalTarget() : NULL)
  {
    getRenderSystem().setShader(m_shaderCombineEdges.data());
    getRenderSystem().setTexture(normalTarget, 3);
  }
  else
  {
    getRenderSystem().setShader(m_shaderCombine.data());
  }

  vec4 bloomParams(m_bloomIntensity, m_baseIntensity, m_bloomSaturation, m_baseSaturation);
  getRenderSystem().setConst(10, &bloomParams.X, 1.0f);
  getRenderSystem().setTexture(base, 1);
  getRenderSystem().setTexture(blur, 0);
  getRenderSystem().setTexture(lum, 2);
  getRenderSystem().drawScreenQuad();
}
