#ifndef BLOOMCOMPONENT_HPP
#define BLOOMCOMPONENT_HPP

#include <Render/RenderComponent.hpp>
#include <Render/RenderShader.hpp>
#include <Render/RenderTarget.hpp>

class PIPELINE_EXPORT BloomComponent : public RenderComponent
{
  Q_OBJECT
  Q_ENUMS(Preset)

public:
  enum Preset
  {
    Default,
    Soft,
    Desaturated,
    Saturated,
    Blurry,
    Subtle
  };

public:
  explicit BloomComponent();
  ~BloomComponent();

protected:
  void resizeEvent(const QSize& newSize);

public:
  void setPreset(Preset preset);

public:
  int beginOrder();
  int drawOrder();

public:
  bool begin();
  void draw();

private:
  void setBlurParameters(float dx, float dy);

protected:
  virtual void prepare();
  virtual void extractBloom(RenderTarget* in, RenderTarget* out);
  virtual void blurTarget(RenderTarget* in, RenderTarget* out, float dx, float dy);
  virtual void blendTargets(RenderTarget* base, RenderTarget* blur);

protected:
  QScopedPointer<RenderTarget> m_sceneRenderTarget, m_renderTarget1, m_renderTarget2;
  QScopedPointer<RenderShader> m_shaderExtract, m_shaderBlur, m_shaderCombine, m_shaderCombineEdges;

protected:
  float m_bloomThreshold;
  float m_blurAmount;
  float m_bloomIntensity;
  float m_baseIntensity;
  float m_bloomSaturation;
  float m_baseSaturation;
};

#endif // BLOOMCOMPONENT_HPP
