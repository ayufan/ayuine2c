#include "ConeFragment.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/VertexBuffer.hpp>
#include <Render/PrimitiveDrawer.hpp>

ConeFragment::ConeFragment()
{
}

ConeFragment::~ConeFragment()
{
}

void ConeFragment::draw()
{
  // if(m_fillMode == RenderSystem::Wire)
  //  getRenderSystem().drawLineCone(m_transform);
  // else
    getRenderSystem().drawCone(m_transform);
}

VertexBuffer::Types ConeFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}
