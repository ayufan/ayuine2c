#ifndef TEXTUREDFRAGMENT_HPP
#define TEXTUREDFRAGMENT_HPP

#include "Pipeline.hpp"
#include "Fragment.hpp"

#include <Math/Matrix.hpp>
#include <Render/RenderSystem.hpp>
#include <QColor>
#include <QPointer>

class RenderTexture;
class Texture;

class PIPELINE_EXPORT TexturedFragment : public Fragment
{
public:
  TexturedFragment();
  ~TexturedFragment();

public:
  void setTexture(RenderTexture* texture);
  void setTexture(const QSharedPointer<Texture>& texture);
  void setTexture(const QByteArray& name);
  void setColor(const QColor &color);

  void setTransform(const matrix& transform);

  void setFixedState(RenderSystem::FixedState state);
  void setAlphaTest(RenderSystem::AlphaTest state);
  void setBlendState(RenderSystem::BlendState state);
  void setDepthStencilState(RenderSystem::DepthStencilState state);
  void setRasterizerState(RenderSystem::RasterizerState state);
  void setFillMode(RenderSystem::FillMode mode);
  void setDepthBias(bool bias);

public:
  ShaderState *materialShaderState() const;

public:
  bool prepare();
  bool bind(const Light* lights);

protected:
  QPointer<RenderTexture> m_texture;
  QColor m_color;
  RenderSystem::FixedState m_fixedState;
  RenderSystem::BlendState m_blendState;
  RenderSystem::DepthStencilState m_depthStencilState;
  RenderSystem::RasterizerState m_rasterizerState;
  RenderSystem::AlphaTest m_alphaTest;
  RenderSystem::FillMode m_fillMode;
  bool m_depthBias;
  matrix m_transform;
};

#endif // TEXTUREDFRAGMENT_HPP

