#ifndef SHEETFRAGMENT_HPP
#define SHEETFRAGMENT_HPP

#include "TexturedFragment.hpp"

#include <Math/MathLib.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec2.hpp>
#include <Math/Camera.hpp>

class PIPELINE_EXPORT SheetFragment : public TexturedFragment
{
public:
  SheetFragment();
  ~SheetFragment();

public:
  vec3 axis() const;
  void setCamera(const Camera& camera);
  void setAxis(const vec3& axis);
  void setAxis(Axis::Enum axis);
  void setUp(const vec3& up);
  void setRight(const vec3& right);

  const vec3& origin() const;
  void setOrigin(const vec3& origin);

  const vec2& size() const;
  void setSize(const vec2& size);

public:
  bool bind(const Light* lights);
  void draw();
  VertexBuffer::Types vertexType() const;

private:
  vec3 m_axis, m_up, m_right;
  vec3 m_origin;
  vec2 m_size;
};

#endif // SHEETFRAGMENT_HPP
