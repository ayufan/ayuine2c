#ifndef PIPELINE_HPP
#define PIPELINE_HPP

#include <QtGlobal>

#ifdef PIPELINE_EXPORTS
#define PIPELINE_EXPORT Q_DECL_EXPORT
#else
#define PIPELINE_EXPORT Q_DECL_IMPORT
#endif

#endif // PIPELINE_HPP
