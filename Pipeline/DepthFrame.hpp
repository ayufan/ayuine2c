#ifndef DEPTHFRAME_HPP
#define DEPTHFRAME_HPP

#include "SimpleFrame.hpp"

class RenderTarget;

class PIPELINE_EXPORT DepthFrame : public SimpleFrame
{
  Q_OBJECT

public:
  Q_INVOKABLE DepthFrame();

public:
  FrameType type() const;
  void draw();

protected:
  void compile(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);
};

#endif // DEPTHFRAME_HPP
