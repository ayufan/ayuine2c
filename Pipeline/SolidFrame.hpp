#ifndef SOLIDFRAME_HPP
#define SOLIDFRAME_HPP

#include "SimpleFrame.hpp"

class PIPELINE_EXPORT SolidFrame : public SimpleFrame
{
  Q_OBJECT

public:
  Q_INVOKABLE SolidFrame();

public:
  FrameType type() const;

protected:
  void compile(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);
};

#endif // SOLIDFRAME_HPP
