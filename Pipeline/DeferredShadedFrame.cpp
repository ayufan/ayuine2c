#include "DeferredShadedFrame.hpp"
#include "Light.hpp"
#include "Fragment.hpp"

#include <Render/RenderSystem.hpp>
#include <Render/RenderShaderConst.hpp>
#include <Render/RenderShader.hpp>

#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/LightShader.hpp>
#include <MaterialSystem/VertexShaderCompiler.hpp>

DeferredShadedFrame::DeferredShadedFrame()
{
}

Frame::FrameType DeferredShadedFrame::type() const
{
  return DeferredShaded;
}

bool DeferredShadedFrame::hasLighting() const
{
  return true;
}

void DeferredShadedFrame::resizeEvent(const QSize &newSize)
{
  Frame::resizeEvent(newSize);
  m_target0.reset(new RenderTarget(newSize.width(), newSize.height(), RenderTarget::Rgba));
  m_target0->setDepthFormat(RenderTarget::Depth24Stencil8);
  m_target1.reset(new RenderTarget(newSize.width(), newSize.height(), RenderTarget::Rgba));
  m_target2.reset(new RenderTarget(newSize.width(), newSize.height(), RenderTarget::Rgba));
}

RenderTarget* DeferredShadedFrame::depthTarget() const
{
  return m_target1.data();
}

bool DeferredShadedFrame::begin()
{
  RenderTargetBindings bindings[] =
  {
    {m_target0.data()},
    {m_target1.data()},
    {m_target2.data()}
  };

  getRenderSystem().setRenderTargets(bindings, 3);
  getRenderSystem().clear(Qt::black);

  getRenderSystem().setCamera(camera());
  getRenderSystem().setFrame(deltaTime(), frameTime());

  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setRasterizerState(RenderSystem::CullClockwise);
  getRenderSystem().setBlendState(RenderSystem::Opaque);

  prepareFrame(false);
  renderSolidFrags(NULL, false);

  getRenderSystem().setRenderTargets(NULL, 0);

#if 0
  m_target0->saveToFile("E:/hdr_diffuse.dds");
  m_target1->saveToFile("E:/hdr_depth.dds");
  m_target2->saveToFile("E:/hdr_normal.dds");
#endif

  return true;
}

void DeferredShadedFrame::draw()
{
  prepareFrame(false);

  getRenderSystem().setDepthTarget(m_target0.data());

  getRenderSystem().setCamera(camera());
  getRenderSystem().setFrame(deltaTime(), frameTime());

  getRenderSystem().setFillMode(RenderSystem::Solid);
  getRenderSystem().setRasterizerState(RenderSystem::CullClockwise);
  getRenderSystem().setBlendState(RenderSystem::Additive);

  getRenderSystem().setTexture(m_target0.data(), 0);
  getRenderSystem().setTexture(m_target1.data(), 1);
  getRenderSystem().setTexture(m_target2.data(), 2);

  getRenderSystem().setSamplerState(RenderSystem::PointClamp, 0);
  getRenderSystem().setSamplerState(RenderSystem::PointClamp, 1);
  getRenderSystem().setSamplerState(RenderSystem::PointClamp, 2);

  for(Light* light = lights(); light; light = light->next())
  {
    renderLight(light);
  }

  getRenderSystem().setTexture((RenderTexture*)NULL, 0);
  getRenderSystem().setTexture((RenderTexture*)NULL, 1);
  getRenderSystem().setTexture((RenderTexture*)NULL, 2);

  renderOrderedFrags(OrderAll);
}

// target0: dx, dy, dz, spec
// target1: depthx3, gloss
// target2: nx, ny

static void compileMaterialShader(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& block)
{
  compiler.psout("float4 outColor2 : COLOR1");
  compiler.psout("float4 outColor3 : COLOR2");
  compiler.ps("outColor2 = 0;");
  compiler.ps("outColor3 = 0;");

  // save vertex normal
#if 0
  compiler.ps("outColor2.xyz = %1;", compiler.getp(MaterialShaderCompiler::pOrigin));
#else
#if 0
  compiler.ps("outColor2.x = %1;", compiler.getp(MaterialShaderCompiler::pScreenDepth));
#else
  compiler.ps("float depth2 = %1 * 256.0f;", compiler.getp(MaterialShaderCompiler::pScreenDepth));
  compiler.ps("outColor2.x = floor(depth2);", compiler.getp(MaterialShaderCompiler::pScreenDepth));
  compiler.ps("depth2 = (depth2 - outColor2.x) * 256.0f;");
  compiler.ps("outColor2.y = floor(depth2);");
  compiler.ps("outColor2.z = depth2 - outColor2.y;");
  compiler.ps("outColor2.xy *= 1.0f/256.0f;");
#endif
#endif

  // special case when no material output is defined
  if(block.isNull())
  {
    compiler.ps("outColor = %1;", compiler.getp(MaterialShaderCompiler::pColor));
    compiler.ps("outColor3.xyz = %1 * 0.5f + 0.5f;", compiler.getp(MaterialShaderCompiler::pNormal));
    return;
  }

  // save color
  if(ShaderBlock::InputLink colorLink = block->input("Color"))
  {
    compiler.ps("outColor.rgb = %1;", colorLink.varName(compiler));
    if(ShaderBlock::InputLink alphaLink = block->input("Alpha"))
      compiler.ps("clip(%1 - 0.5f);", alphaLink.varName(compiler));
  }
  else if(ShaderBlock::InputLink colorLink = block->input("Diffuse"))
  {
    compiler.ps("outColor.rgb = %1;", colorLink.varName(compiler));
    if(ShaderBlock::InputLink alphaLink = block->input("Opacity"))
      compiler.ps("clip(%1 - 0.5f);", alphaLink.varName(compiler));
  }
  else
  {
    compiler.ps("outColor = %1;", compiler.getp(MaterialShaderCompiler::pColor));
  }

  // save normal
  if(ShaderBlock::InputLink normalLink = block->input("Normal"))
  {
    compiler.ps("outColor3.xyz = %1 * 0.5f + 0.5f;", normalLink.varName(compiler));
  }
  else
  {
    compiler.ps("outColor3.xyz = %1 * 0.5f + 0.5f;", compiler.getp(MaterialShaderCompiler::pNormal));
  }

  // save material emissivness
  if(ShaderBlock::InputLink emissiveLink = block->input("Emissive"))
  {
    compiler.ps("outColor3.a = dot(%1.rgb, float3(0.3,0.4,0.3)) / 4.0f;", emissiveLink.varName(compiler));
  }

  // save material gloss
  if(ShaderBlock::InputLink glossLink = block->input("Gloss"))
  {
    compiler.ps("outColor2.a = dot(%1.rgb, float3(0.3,0.4,0.3));", glossLink.varName(compiler));
  }

  // save material specularity
  if(ShaderBlock::InputLink specularityLink = block->input("Specularity"))
  {
    compiler.ps("outColor.a = %1 / 64.0f;", specularityLink.varName(compiler));
  }
}

class DeferredShaderCompiler : public VertexShaderCompiler
{
public:
  DeferredShaderCompiler() : VertexShaderCompiler(VertexBuffer::Vertex)
  {
    qMemSet(m_targets, 0, sizeof(m_targets));
  }

public:
  QString target(unsigned index)
  {
    if(index >= 4)
      return "float4(0,0,0,0)";

    QString valueName = "inColor" + QString::number(index);

    if(!hasVariable(valueName))
    {
      psconst("sampler2D inTarget%1 : register(s%1)", QString::number(index));
      ps("float4 %1 = tex2D(inTarget%2, %3);", valueName, QString::number(index), getp(pScreen));
      addVariable(valueName);
    }
    return valueName;
  }

  QString getp(VertexShaderValue value)
  {
    return VertexShaderCompiler::getp(value);
  }

  QString getp(PixelShaderValue value)
  {
    QString valueName = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("PixelShaderValue")).valueToKey(value);

    if(m_pixelValues[value])
      return valueName;
    m_pixelValues[value] = true;

    switch(value)
    {
    case pFrame:
      psuniform("float4 %1 : register(c%2)", valueName, QString::number(PixelShaderConst::Frame));
      break;

    case pTime:
      ps("float %1 = %2.w;", valueName, getp(pFrame));
      break;

    case pPixelSize:
      ps("float2 %1 = %2.xy;", valueName, getp(pFrame));
      break;

    case pObjectColor:
      psuniform("float4 %1 : register(c%2)", valueName, QString::number(PixelShaderConst::Color));
      break;

    case pColor:
      ps("float4 %1 = %2;", valueName, target(0));
      break;

    case pScreenDepth:
      ps("float2 %1 = %2.xy / %2.w;", valueName, getp(vOutScreen));
      break;

    case pScreen:
      ps("float2 %1 = %2.xy / %2.z * 0.5f + 0.5f;", valueName, getp(vScreen));
      ps("%1 += %2 * 0.5f;", valueName, getp(pPixelSize));
      break;

    case pDepth2:
      ps("float t_%1 = %2 - %3;", valueName, getp(pOrigin), getp(vCameraDir));
      ps("float %1 = dot(t_%1, t_%1);", valueName);
      break;

    case pDepth:
      ps("float %1 = sqrt(%2);", valueName, getp(pDepth2));
      break;

    case pOrigin:
#if 0
      ps("float3 %1 = %2.xyz;", valueName, target(1));
#else
      ps("float depth = dot(%1.xyz, float3(1.0f, 1.0f/256.0f, 1.0f/65536.0f));", target(1));
      ps("float4 t_%1 = float4(%2.xy / %2.w, depth, 1.0f);", valueName, getp(vOutScreen), target(1));
      psuniform("float4x4 invProjView : register(c%1)", QString::number(PixelShaderConst::InvProjView));
      ps("t_%1 = mul(t_%1, invProjView);", valueName);
      ps("float3 %1 = t_%1.xyz / t_%1.w;", valueName);
#endif
      break;

    case pTexCoords:
      ps("float2 %1 = float2(0,0);", valueName);
      break;

    case pMapCoords:
      ps("float2 %1 = float2(0,0);", valueName);
      break;

    case pNormal:
#if 1
      ps("float3 %1 = %2.xyz * 2.0f - 1.0f;", valueName, target(2));
#else
      ps("float3 %1 = float3(%2.xy, sqrt(1.0f - dot(%2.xy, %2.xy)));", valueName, target(2));
#endif
      break;

    case pTangent:
      ps("float3 %1 = %2.gbr;", valueName, target(2));
      break;

    case pBinormal:
      ps("float3 %1 = cross(%2, %3);", valueName, getp(pTangent), getp(pBinormal));
      break;

    case pCameraDir:
      ps("float3 %1 = normalize(%2);",  valueName, getp(vCameraDir));
      break;

    case pCameraTanDir:
      ps("float3 %1 = normalize(%2);",  valueName, getp(vCameraDir));
      break;

    default:
      break;
    }

    return valueName;
  }

  ShaderLink getLink(const QString& name)
  {
    if(name == "Diffuse" || name == "Color")
      return target(0) + ".rgb";
    if(name == "Opacity" || name == "Alpha")
      return QString("1.0f"); // return target(0) + ".a";
    if(name == "Normal")
      return getp(pNormal);
    if(name == "Specularity")
      return target(0) + ".a * 64.0f";
    if(name == "Gloss")
      return target(1) + ".aaa";
    if(name == "Emissive")
      return target(2) + ".aaa * 4.0f";
    return ShaderLink();
  }

private:
  bool m_targets[4];
};

void compileLightColor(MaterialShaderCompiler& compiler, const QSharedPointer<ShaderBlock>& output);

void DeferredShadedFrame::renderLight(Light *light)
{
  if(LightShader* lightShader = light->lightShader())
  {
    unsigned lightIndex = vertexToIndex(VertexBuffer::Vertex, light->lightQuality());

    RenderShaderRef shader = lightShader->shader(lightIndex, NULL);

    if(!shader)
    {
      shader = readShader(NULL, lightShader, light->lightQuality(), VertexBuffer::Vertex);
      if(shader)
        lightShader->setShader(lightIndex, NULL, shader);
    }

    if(!shader)
    {
      DeferredShaderCompiler compiler;
      compiler.quality = light->lightQuality();
      compileLightColor(compiler, lightShader->findFinal());
      shader = compiler.shader();
      lightShader->setShader(lightIndex, NULL, shader);

      writeShader(compiler.code(), NULL, lightShader, light->lightQuality(), VertexBuffer::Vertex);
  }

    if(!shader)
    {
      return;
    }

    light->draw(shader.data(), *this);
  }
}

bool DeferredShadedFrame::bindShader(GeneralShader *materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType)
{
  if(!materialShader)
    return false;

  unsigned materialIndex = vertexToIndex(vertexType, quality);

  RenderShaderRef shader = materialShader->shader(materialIndex, NULL);

  if(!shader)
  {
    shader = readShader(NULL, materialShader, quality, vertexType);
    if(shader)
      materialShader->setShader(materialIndex, NULL, shader);
  }

  if(!shader)
  {
    VertexShaderCompiler compiler(vertexType);
    compiler.quality = quality;
    compileMaterialShader(compiler, materialShader->findOutput());
    shader = compiler.shader();
    materialShader->setShader(materialIndex, NULL, shader);

    writeShader(compiler.code(), NULL, materialShader, quality, vertexType);
  }

  if(!shader)
  {
    return false;
  }

  getRenderSystem().setShader(shader.data());
  return true;
}

RenderTarget * DeferredShadedFrame::normalTarget() const
{
  return m_target2.data();
}
