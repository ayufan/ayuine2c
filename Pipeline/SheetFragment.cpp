#include "SheetFragment.hpp"
#include <Math/Poly.hpp>
#include <Render/PrimitiveDrawer.hpp>

SheetFragment::SheetFragment()
{
  setAxis(Axis::X);
}

SheetFragment::~SheetFragment()
{
}

vec3 SheetFragment::axis() const
{
  return m_axis;
}

void SheetFragment::setAxis(const vec3& axis)
{
  m_axis = axis;
  m_axis.normalVectors(m_right, m_up);
}

void SheetFragment::setAxis(Axis::Enum axis)
{
  m_axis = vec3();
  m_axis[axis] = 1.0f;
  m_axis.normalVectors(m_right, m_up);
}

const vec3 & SheetFragment::origin() const
{
  return m_origin;
}

void SheetFragment::setOrigin(const vec3 &origin)
{
  m_origin = origin;
}

const vec2 & SheetFragment::size() const
{
  return m_size;
}

void SheetFragment::setSize(const vec2 &size)
{
  m_size = size;
}

struct SheetVertex
{
  vec3 origin;
  vec2 coords;
};

bool SheetFragment::bind(const Light* lights)
{
  if(!TexturedFragment::bind(lights))
    return false;

  // Przeskaluj wierzchołki
  vec3 right = m_right * m_size.X;
  vec3 up = m_up * m_size.Y;

  // Oblicz portal
  SheetVertex verts[4] =
  {
    {m_origin - right - up, vec2(0, 1)},
    {m_origin + right - up, vec2(1, 1)},
    {m_origin + right + up, vec2(1, 0)},
    {m_origin - right + up, vec2(0, 0)}
  };

  return getRenderSystem().setVertexData(vertexType(), verts, sizeof(verts));
}

void SheetFragment::draw()
{
  if(m_fillMode == RenderSystem::Wire)
    getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::LineStrip, 0, 3));
  else
    getRenderSystem().draw(PrimitiveDrawer(PrimitiveDrawer::TriFan, 0, 2));
}

VertexBuffer::Types SheetFragment::vertexType() const
{
  return VertexBuffer::Types(VertexBuffer::Vertex | VertexBuffer::Coords);
}

void SheetFragment::setUp(const vec3 &up)
{
  m_up = up;
}

void SheetFragment::setRight(const vec3 &right)
{
  m_right = right;
}

void SheetFragment::setCamera(const Camera &camera)
{
  m_axis = -camera.at;
  m_up = camera.up;
  m_right = camera.right;
}
