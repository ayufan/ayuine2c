#ifndef BOXFRAGMENT_HPP
#define BOXFRAGMENT_HPP

#include "TexturedFragment.hpp"
#include <Math/Box.hpp>

class PIPELINE_EXPORT BoxFragment : public TexturedFragment
{
public:
  BoxFragment();
  ~BoxFragment();

public:
  const box& bounds() const;
  void setBounds(const box& bounds);

public:
  VertexBuffer::Types vertexType() const;

public:
  void draw();

private:
  box m_bounds;
};

#endif // BOXFRAGMENT_HPP
