#ifndef FRAME_HPP
#define FRAME_HPP

#include "Pipeline.hpp"

#include <Math/Camera.hpp>
#include <Render/RenderComponent.hpp>
#include <Render/VertexBuffer.hpp>
#include <MaterialSystem/ShaderState.hpp>

class Fragment;
class Light;
class Light;

struct vec3;
struct box;
class QColor;

class DebugView
{
public:
  virtual bool isSelected(QObject* object) const = 0;
  virtual bool isHidden(QObject* object) const = 0;

  inline bool isSelected(const QObject* object) const
  {
    return isSelected(const_cast<QObject*>(object));
  }

  inline bool isHidden(const QObject* object) const
  {
    return isHidden(const_cast<QObject*>(object));
  }
};

class GeneralShader;
class LightShader;
class RenderShader;

class PIPELINE_EXPORT Frame : public RenderComponent
{
  Q_OBJECT
  Q_ENUMS(FrameType)

public:
  enum FrameType
  {
    Solid,
    Wire,
    Depth,
    ForwardShaded,
    LitShaded,
    ColorPick,
    DeferredShaded
  };

public:
  Frame();
  ~Frame();

public:
  virtual void clear();

  Fragment* fragments() const;
  bool addFragment(Fragment* fragment);
  bool addFragment(QScopedPointer<Fragment>& fragment);

  Light* lights() const;
  bool addLight(Light* light);
  bool addLight(QScopedPointer<Light>& light);

  float frameTime() const;
  void setFrameTime(float frameTime);

  float deltaTime() const;
  void setDeltaTime(float deltaTime);

  const Camera& camera() const;
  void setCamera(const Camera& camera);

  const QRect& viewport() const;
  void setViewport(const QRect& viewport);

  const DebugView* debugView() const;
  void setDebugView(const DebugView* debugView);

  QImage toImage();
  QImage toImage(const QRect& area);

  virtual bool isWireframe() const;
  virtual bool hasLighting() const;
  virtual FrameType type() const = 0;

  int beginOrder();
  int drawOrder();

  QStringList toStringList() const;

  int lightsCount(int *visibleCount) const;
  int fragmentsCount(float *instancedRatio = NULL) const;

public:
  bool isSelected(QObject* object, bool def = false) const;
  bool isHidden(QObject* object, bool def = false) const;

public:
  void addIconFragment(const vec3& iconOrigin, float iconSize, const QColor& iconColor, const QByteArray& iconTexture, bool selected = false);
  void addBoxFragment(const box& bbox, const QColor& fillColor, const QColor& outlineColor, bool selected = false);
  void addSheetFragment(const vec3& origin, Axis::Enum axis, const vec2& size, const QColor& fillColor, const QColor& outlineColor, bool selected = false);


protected:
  enum OrderedRender
  {
    OrderAll,
    OrderNegative,
    OrderPositive
  };

  virtual void prepareFrame(bool useTranslucent = true);
  virtual void renderSolidFrags(const Light* light, bool useAlphaTest = true);
  virtual void renderOrderedFrags(OrderedRender render = OrderAll);
  virtual void renderTransparentFrags(bool lights);
  virtual void renderSortedFrags(Fragment* frags, const Light* light, MaterialSystem::Quality quality, VertexBuffer::Types extraType);
  virtual bool bindShader(GeneralShader* materialShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);
  virtual bool bindLightShader(GeneralShader* materialShader, LightShader* lightShader, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);

  unsigned vertexToIndex(VertexBuffer::Types type, MaterialSystem::Quality quality = MaterialSystem::High);

protected:
  QString shaderFileName(QObject* objA, QObject* objB, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);
  QSharedPointer<RenderShader> readShader(QObject* objA, QObject* objB, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);
  void writeShader(QString shaderCode, QObject* objA, QObject* objB, MaterialSystem::Quality quality, VertexBuffer::Types vertexType);

private:
  Fragment* m_fragments;
  Light* m_lights;
  float m_frameTime, m_deltaTime;
  Camera m_camera;
  QRect m_viewport;
  const DebugView* m_debugView;
  friend class Fragment;
  friend class Light;
};

#endif // FRAME_HPP
