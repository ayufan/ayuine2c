#ifndef MATERIALSHADERVALUESPROPERTY_HPP
#define MATERIALSHADERVALUESPROPERTY_HPP

#include <CoreTypes/EditableProperty.hpp>

#include <MaterialSystem/ShaderState.hpp>

class MaterialShaderValuesProperty : public Property
{
  Q_OBJECT

public:
  Q_INVOKABLE explicit MaterialShaderValuesProperty(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  ShaderValues values() const;
  ShaderValues refValues() const;
  void setValues(const ShaderValues& values);

  bool isReadOnly();

  bool event(QEvent *event);

  void populateChildren();

  bool hasChildren() const;

private:
  QVector<Property*> m_properties;
  bool m_propertiesInitialized;
};

#endif // MATERIALSHADERVALUESPROPERTY_HPP
