#ifndef MATERIALSHADEREDITOR_HPP
#define MATERIALSHADEREDITOR_HPP

#include <QMainWindow>
#include <CoreFrames/FrameEditor.hpp>

class Shader;
class ShaderBlock;

namespace Ui {
class MaterialShaderEditor;
}

class Mesh;

class MaterialShaderEditor : public FrameEditor
{
  Q_OBJECT

public:
  Q_INVOKABLE explicit MaterialShaderEditor(QWidget *parent = 0);
  ~MaterialShaderEditor();

public:
  void updateUi();
  const QMetaObject* resourceType() const;
  QSharedPointer<Resource> resource() const;
  QSharedPointer<Shader> materialShader() const;
  bool setResource(const QSharedPointer<Resource> &newResource);

protected:
  void populateBlocks();
  void prepareComponents();
  void finalizeComponents();

private slots:
  void deleteBlocks();
  void deselectBlocks();
  void on_action_AutoUpdate_toggled(bool arg1);
  void on_actionHDR_toggled(bool arg1);

private:
  Ui::MaterialShaderEditor *ui;
  QSharedPointer<Shader> m_materialShader;
  QSharedPointer<Mesh> m_mesh;
  QTimer m_autoUpdateTimer;
  QScopedPointer<BloomComponent> m_bloomComponent;
  QElapsedTimer m_elapsedTimer;
};

#endif // MATERIALSHADEREDITOR_HPP
