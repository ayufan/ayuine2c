#ifndef BLOCKAREA_HPP
#define BLOCKAREA_HPP

#include <QFrame>

#include "BlockWidget.hpp"

class ShaderBlock;
class ResourceEditor;
class QRubberBand;

class BlockArea : public QFrame
{
  Q_OBJECT

public:
  explicit BlockArea(QWidget *parent = 0);
  ~BlockArea();

protected:
  void dragEnterEvent(QDragEnterEvent *dragEvent);
  void dropEvent(QDropEvent *dragEvent);
  void focusInEvent(QFocusEvent *);
  void mousePressEvent(QMouseEvent *mouseEvent);
  void mouseReleaseEvent(QMouseEvent *mouseEvent);
  void mouseMoveEvent(QMouseEvent *mouseEvent);

public slots:
  ResourceEditor* resourceEditor() const;
  void clear();
  void addBlock(const QSharedPointer<ShaderBlock>& block);
  void setBlocks(const QVector<QSharedPointer<ShaderBlock> >& blocks);
  void removeBlock(const QSharedPointer<ShaderBlock>& block);
  BlockWidget* blockWidget(const QSharedPointer<ShaderBlock>& block);
  void updateUi();
  QList<BlockWidget*> selected() const;
  void selectSelf();

private:
  QHash<QSharedPointer<ShaderBlock>, BlockWidget*> m_blocks;
  QRubberBand* m_rubberBand;
  QPoint m_rubberOrigin;
  QPoint m_scrollOrigin;
};

#endif // BLOCKAREA_HPP
