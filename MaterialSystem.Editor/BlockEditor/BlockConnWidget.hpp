#ifndef BLOCKCONNWIDGET_HPP
#define BLOCKCONNWIDGET_HPP

#include <QWidget>
#include <QPointer>

class BlockInputWidget;
class BlockOutputWidget;

class BlockConnWidget : public QWidget
{
  Q_OBJECT

public:
  explicit BlockConnWidget(BlockInputWidget* input, BlockOutputWidget* output, QWidget *parent = 0);

public slots:
  void updateUi();
  BlockInputWidget* input() const;
  QPoint inputPos() const;
  BlockOutputWidget* output() const;
  QPoint outputPos();

private slots:
  void destroyed(QObject *);

protected:
  void paintEvent(QPaintEvent *paintEvent);
  bool eventFilter(QObject *object, QEvent *event);

public:
  QPointer<BlockInputWidget> m_input;
  QPointer<BlockOutputWidget> m_output;
};

#endif // BLOCKCONNWIDGET_HPP
