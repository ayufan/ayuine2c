#ifndef BLOCKINPUTWIDGET_HPP
#define BLOCKINPUTWIDGET_HPP

#include <QCheckBox>

#include <MaterialSystem/ShaderBlock.hpp>

class BlockOutputWidget;
class BlockWidget;
class BlockConnWidget;

class BlockInputWidget : public QCheckBox
{
  Q_OBJECT

public:
  explicit BlockInputWidget(QWidget *parent = 0);
  ~BlockInputWidget();

public:
  ShaderBlock::Link link() const;
  void setLink(ShaderBlock::InputLink link);
  BlockWidget* blockWidget() const;
  QSharedPointer<ShaderBlock> block() const;

protected:
  void dragEnterEvent(QDragEnterEvent *dragEvent);
  void dropEvent(QDropEvent *dragEvent);
  void mousePressEvent(QMouseEvent *event);

private:
  ShaderBlock::Link m_link;
  QPointer<BlockConnWidget> m_conn;
};

#endif // BLOCKINPUTWIDGET_HPP
