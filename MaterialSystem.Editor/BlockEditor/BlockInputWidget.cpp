#include "BlockInputWidget.hpp"
#include "BlockOutputWidget.hpp"
#include "BlockWidget.hpp"
#include "BlockArea.hpp"
#include "BlockConnWidget.hpp"

#include <QDragEnterEvent>
#include <QPainter>

BlockInputWidget::BlockInputWidget(QWidget *parent) :
  QCheckBox(parent)
{
  QSizePolicy policy;
  policy.setHeightForWidth(sizePolicy().hasHeightForWidth());
  setSizePolicy(policy);
  setStyleSheet("QCheckBox::indicator {\n"
                "image: url(:/Editor/images/BlockOff.png);\n"
                "}\n"
                "QCheckBox::indicator::checked {\n"
                "image: url(:/Editor/images/BlockOn.png);\n"
                "}");
  setMinimumWidth(5 * 18);
  setAcceptDrops(true);
}

BlockInputWidget::~BlockInputWidget()
{
}

void BlockInputWidget::dragEnterEvent(QDragEnterEvent *dragEvent)
{
  if(!block())
    return;

  if(BlockOutputWidget* output = qobject_cast<BlockOutputWidget*>(dragEvent->source()))
  {
    if(!output->block())
      return;
    ShaderBlock::OutputLink outputLink;
    outputLink = output->link();
    outputLink.owner = output->block();
    if(!outputLink)
      return;
    if(block()->canConnect(outputLink, link()))
      dragEvent->acceptProposedAction();
  }
}

void BlockInputWidget::dropEvent(QDropEvent *dragEvent)
{
  if(!block())
    return;

  if(BlockOutputWidget* output = qobject_cast<BlockOutputWidget*>(dragEvent->source()))
  {
    if(!output->block())
      return;
    ShaderBlock::OutputLink outputLink;
    outputLink = output->link();
    outputLink.owner = output->block();
    if(!outputLink)
      return;
    block()->connect(outputLink, link());
  }
}

ShaderBlock::Link BlockInputWidget::link() const
{
  return m_link;
}

void BlockInputWidget::setLink(ShaderBlock::InputLink link)
{
  m_link = link;
  setText(link.name);
  setChecked(link.connection);

  if(!link)
  {
    delete m_conn;
  }
  else
  {
    BlockWidget* widget = blockWidget();

    if(!widget)
    {
      delete m_conn;
      return;
    }

    BlockArea* area = widget->blockArea();

    if(!area)
    {
      delete m_conn;
      return;
    }

    BlockWidget* otherWidget = area->blockWidget(link.connection.owner);

    if(!otherWidget)
    {
      delete m_conn;
      return;
    }

    BlockOutputWidget* output = otherWidget->output(link.connection.index);

    if(!output)
    {
      delete m_conn;
      return;
    }

    if(m_conn)
    {
      if(m_conn->output() == output)
        return;
      delete m_conn;
    }

    m_conn = new BlockConnWidget(this, output, area);
    m_conn->lower();
  }
}

QSharedPointer<ShaderBlock> BlockInputWidget::block() const
{
  BlockWidget* blckWidget = blockWidget();
  if(blckWidget)
    return blckWidget->block();
  return QSharedPointer<ShaderBlock>();
}

BlockWidget * BlockInputWidget::blockWidget() const
{
  for(QWidget* parent = parentWidget(); parent; parent = parent->parentWidget())
  {
    BlockWidget* blockWidget = qobject_cast<BlockWidget*>(parent);
    if(!blockWidget)
      continue;
    return blockWidget;
  }
  return NULL;
}

void BlockInputWidget::mousePressEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton)
  {
    event->accept();

    QSharedPointer<ShaderBlock> materialShaderBlock(block());

    if(materialShaderBlock->inputHasLink(m_link.index))
    {
      materialShaderBlock->connect(ShaderBlock::OutputLink(), m_link);
    }
  }
}
