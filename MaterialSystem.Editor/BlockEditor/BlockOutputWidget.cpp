#include "BlockOutputWidget.hpp"
#include "BlockWidget.hpp"

#include <QMouseEvent>
#include <QDrag>
#include <QPainter>

BlockOutputWidget::BlockOutputWidget(QWidget *parent) :
    QCheckBox(parent)
{
  QSizePolicy policy;
  policy.setHeightForWidth(sizePolicy().hasHeightForWidth());
  setSizePolicy(policy);
  setStyleSheet("QCheckBox::indicator {\n"
"image: url(:/Editor/images/BlockOut.png);\n"
"}");
  setLayoutDirection(Qt::RightToLeft);
  setMinimumWidth(5 * 18);
}

BlockOutputWidget::~BlockOutputWidget()
{
}

void BlockOutputWidget::mousePressEvent(QMouseEvent *event)
{
  if(event->button() == Qt::LeftButton)
  {
    event->accept();

    QDrag *drag = new QDrag(this);
    drag->setPixmap(QPixmap::grabWidget(this));
    drag->setMimeData(new QMimeData());
    drag->setHotSpot(event->pos());
    drag->start();
  }
}

QSharedPointer<ShaderBlock> BlockOutputWidget::block() const
{
  BlockWidget* blckWidget = blockWidget();
  if(blckWidget)
    return blckWidget->block();
  return QSharedPointer<ShaderBlock>();
}

BlockWidget * BlockOutputWidget::blockWidget() const
{
  for(QWidget* parent = parentWidget(); parent; parent = parent->parentWidget())
  {
    BlockWidget* blockWidget = qobject_cast<BlockWidget*>(parent);
    if(!blockWidget)
      continue;
    return blockWidget;
  }
  return NULL;
}

ShaderBlock::Link BlockOutputWidget::link() const
{
  return m_link;
}

void BlockOutputWidget::setLink(ShaderBlock::OutputLink link)
{
  m_link = link;
  setText(link.name);
}
