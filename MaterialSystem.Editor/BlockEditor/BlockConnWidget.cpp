#include "BlockConnWidget.hpp"
#include "BlockWidget.hpp"
#include "BlockInputWidget.hpp"
#include "BlockOutputWidget.hpp"

#include <QPainter>
#include <QPaintEvent>

#include <MaterialSystem/ShaderBlock.hpp>

BlockConnWidget::BlockConnWidget(BlockInputWidget* input, BlockOutputWidget* output, QWidget *parent) :
  QWidget(parent), m_input(input), m_output(output)
{
  if(m_input->blockWidget())
  {
    m_input->installEventFilter(this);
    m_input->blockWidget()->installEventFilter(this);
    connect(m_input->blockWidget(), SIGNAL(destroyed(QObject*)), SLOT(destroyed(QObject*)));
  }

  if(m_output->block())
  {
    m_output->installEventFilter(this);
    m_output->blockWidget()->installEventFilter(this);
    connect(m_output->blockWidget(), SIGNAL(destroyed(QObject*)), SLOT(destroyed(QObject*)));
  }

  updateUi();
  show();
  setFocus();
}

BlockInputWidget * BlockConnWidget::input() const
{
  return m_input;
}

BlockOutputWidget * BlockConnWidget::output() const
{
  return m_output;
}

void BlockConnWidget::updateUi()
{
  setGeometry(QRect(inputPos(), outputPos()).normalized());
}

QPoint BlockConnWidget::inputPos() const
{
  if(m_input)
  {
    return QPoint(m_input->mapTo(parentWidget(), QPoint(0, m_input->height() / 2)));
  }
  return QPoint();
}

QPoint BlockConnWidget::outputPos()
{
  if(m_output)
  {
    return QPoint(m_output->mapTo(parentWidget(), QPoint(m_output->width(), m_output->height() / 2)));
  }
  return QPoint();
}

void BlockConnWidget::paintEvent(QPaintEvent *paintEvent)
{
  QPainter painter(this);

  painter.setClipRect(paintEvent->rect());
  painter.setRenderHint(QPainter::Antialiasing);

  QPoint input(mapFromParent(inputPos()));
  QPoint output(mapFromParent(outputPos()));

  painter.drawLine(input, output);
}

bool BlockConnWidget::eventFilter(QObject *object, QEvent *event)
{
  if(event->type() == QEvent::Move)
  {
    if(object == m_input->blockWidget() || object == m_output->blockWidget() ||
       object == m_input || object == m_output)
    {
      updateUi();
    }
  }
  return false;
}

void BlockConnWidget::destroyed(QObject *)
{
  delete this;
}
