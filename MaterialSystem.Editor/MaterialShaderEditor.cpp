#include "MaterialShaderEditor.hpp"
#include "ui_MaterialShaderEditor.h"
#include "BlockEditor/BlockWidget.hpp"

#include <Render/RenderComponentWidget.hpp>

#include <Core/ClassInfo.hpp>
#include <Core/ResourceImporter.hpp>

#include <MaterialSystem/Shader.hpp>
#include <MaterialSystem/LightShader.hpp>
#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/ShaderBlock.hpp>

#include <CameraController/CameraController.hpp>
#include <CameraController/ModelViewController.hpp>
#include <CameraController/FirstPersonController.hpp>

#include <CoreFrames/LogView.hpp>

// #include <Pipeline/WireFrame.hpp>
// #include <Pipeline/SolidFrame.hpp>
// #include <Pipeline/BoxFragment.hpp>
#include <Pipeline/Frame.hpp>
#include <Pipeline/AmbientLight.hpp>
#include <Pipeline/PointLight.hpp>
#include <Pipeline/BloomComponent.hpp>
#include <Pipeline/HdrComponent.hpp>

#include <MeshSystem/Mesh.hpp>
#include <MeshSystem/MeshSystem.hpp>

Q_REGISTER_RESOURCE_EDITOR(Shader, MaterialShaderEditor)

MaterialShaderEditor::MaterialShaderEditor(QWidget *parent) :
  FrameEditor(parent),
  ui(new Ui::MaterialShaderEditor)
{
  ui->setupUi(this);
  ui->previewWidget->setWidget(renderComponents());
  ui->logsWidget->setWidget(new LogView);

  renderComponents()->connect(&m_autoUpdateTimer, SIGNAL(timeout()), SLOT(update()));

  // tabifyDockWidget(ui->blocksWidget, ui->previewWidget);

  on_actionShaded_triggered();
  on_action_AutoUpdate_toggled(true);

  // m_cameraController.reset(new ModelViewController(renderComponents()));
  m_cameraController.reset(new FirstPersonController(renderComponents()));

  if(m_mesh = ResourceImporter::loadFromFile("alien.grcz").objectCast<Mesh>())
  {
      m_cameraController->modelOnSphere(m_mesh->sphereBounds());

#if 0
      foreach(const QSharedPointer<MeshSubset>& meshSubset, m_mesh->subsets())
      {
          if(meshSubset->material())
          {
              meshSubset->material()->setOpaque(Material::AlphaTest);
          }
      }
#endif
  }

  populateBlocks();
  updateUi();
}

MaterialShaderEditor::~MaterialShaderEditor()
{
  delete ui;
}

void MaterialShaderEditor::populateBlocks()
{
  QMap<QByteArray, QList<const QMetaObject*> > blockGroups;

  foreach(const QMetaObject* metaObject, ClassInfo::metaObjects(&ShaderBlock::staticMetaObject))
  {
    if(!metaObject->constructorCount())
      continue;

    QByteArray blockGroup = "Other";

    int blockGroupIndex = metaObject->indexOfClassInfo("BlockGroup");
    if(blockGroupIndex >= 0)
      blockGroup = metaObject->classInfo(blockGroupIndex).value();

    blockGroups[blockGroup].append(metaObject);
  }

  foreach(QByteArray blockGroup, blockGroups.keys())
  {
    QTreeWidgetItem* groupItem = new QTreeWidgetItem(ui->blocksTree);
    groupItem->setText(0, blockGroup);

    foreach(const QMetaObject* blockMetaObject, blockGroups[blockGroup])
    {
      QByteArray blockName;

      int blockNameIndex = blockMetaObject->indexOfClassInfo("BlockName");
      if(blockNameIndex >= 0)
        blockName = blockMetaObject->classInfo(blockNameIndex).value();
      else
      {
        blockName = blockMetaObject->className();
        if(blockName.startsWith(ShaderBlock::staticMetaObject.className()))
          blockName.remove(0, QByteArray(ShaderBlock::staticMetaObject.className()).length());
      }

      QTreeWidgetItem* blockItem = new QTreeWidgetItem();
      blockItem->setText(0, blockName);
      blockItem->setData(0, Qt::UserRole, blockMetaObject->className());
      groupItem->addChild(blockItem);
    }
  }
}

const QMetaObject * MaterialShaderEditor::resourceType() const
{
  return &Shader::staticMetaObject;
}

QSharedPointer<Resource> MaterialShaderEditor::resource() const
{
  return m_materialShader;
}

bool MaterialShaderEditor::setResource(const QSharedPointer<Resource> &newResource)
{
  if(newResource.isNull())
  {
    if(m_materialShader)
    {
      m_materialShader->disconnect(ui->blockArea);
      m_materialShader->disconnect(renderComponents());
    }
    m_materialShader.clear();
    setPropertyObject(NULL);
    ui->blockArea->clear();
    updateUi();
    return true;
  }

  if(newResource.objectCast<Shader>().isNull())
    return false;

  if(m_materialShader)
  {
    m_materialShader->disconnect(ui->blockArea);    
    m_materialShader->disconnect(renderComponents());
  }

  if(m_materialShader = newResource.objectCast<Shader>())
  {
    if(m_materialShader)
    {
      connect(m_materialShader.data(), SIGNAL(blockAdded(QSharedPointer<ShaderBlock>)), ui->blockArea, SLOT(addBlock(QSharedPointer<ShaderBlock>)));
      connect(m_materialShader.data(), SIGNAL(blockRemoved(QSharedPointer<ShaderBlock>)), ui->blockArea, SLOT(removeBlock(QSharedPointer<ShaderBlock>)));
      connect(m_materialShader.data(), SIGNAL(objectNameChanged()), SLOT(updateUi()));
      connect(m_materialShader.data(), SIGNAL(changed()), renderComponents(), SLOT(update()));
    }

    ui->blockArea->setBlocks(m_materialShader->blocks());
    setPropertyObject(m_materialShader.data());
    updateUi();
    return true;
  }
  return false;
}

QSharedPointer<Shader> MaterialShaderEditor::materialShader() const
{
  return m_materialShader;
}

void MaterialShaderEditor::deleteBlocks()
{
  foreach(BlockWidget* widget, ui->blockArea->selected())
  {
    m_materialShader->removeBlock(widget->block());
  }

  ui->blockArea->selectSelf();
}

void MaterialShaderEditor::deselectBlocks()
{
  ui->blockArea->selectSelf();
}

void MaterialShaderEditor::updateUi()
{
  ResourceEditor::updateUi();

  if(m_materialShader)
  {
    ui->action_Delete->setEnabled(true);
    ui->action_Deselect->setEnabled(true);
  }
  else
  {
    ui->action_Delete->setEnabled(false);
    ui->action_Deselect->setEnabled(false);
  }

  ui->blockArea->updateUi();
}

void MaterialShaderEditor::prepareComponents()
{
  FrameEditor::prepareComponents();

  m_frame->clear();

  if(!m_mesh)
    return;

  float frameTime = m_elapsedTimer.restart() / 1000.0;
  m_frame->setFrameTime(m_frame->frameTime() + frameTime);
  m_frame->setDeltaTime(frameTime);

  if(QSharedPointer<GeneralShader> generalShader = m_materialShader.objectCast<GeneralShader>())
  {
      foreach(const QSharedPointer<MeshSubset>& subset, m_mesh->subsets())
      {
          if(QSharedPointer<Material> material = subset->material())
              material->setMaterialShader(generalShader);
      }
  }

  m_mesh->generateFrame(*m_frame, mat4Identity);

  if(m_mesh && m_cameraController)
  {
    QScopedPointer<PointLight> pointLight(new PointLight());
    // pointLight->setOrigin(m_cameraController->origin());
    pointLight->setOrigin(m_frame->camera().origin);
    pointLight->setRadius(m_mesh->sphereBounds().Radius * 6.0f);
    if(QSharedPointer<LightShader> lightShader = m_materialShader.objectCast<LightShader>())
        pointLight->lightState().setMaterialShader(lightShader);
    else
        pointLight->lightState().setMaterialShader("SimpleLightingModel.grcz");
    pointLight->lightState().set("lightPosition", vec4(pointLight->origin(), 1.0f / pointLight->radius()));

    if(pointLight->lightState().contains("shadowProjection"))
    {
      Camera lightCamera;
      Camera::fppCamera(lightCamera, pointLight->origin(), euler(0,0,0));
      pointLight->lightState().set("shadowProjection", m_frame->camera().culler.object());
    }

    m_frame->addLight(pointLight.take());

    AmbientLight* ambientLight = new AmbientLight();
    if(ui->actionAmbient->isChecked())
        ambientLight->lightState().setMaterialShader("SimpleAmbientModel.grcz");
    else
        ambientLight->lightState().setMaterialShader("NoAmbientModel.grcz");
    m_frame->addLight(ambientLight);
  }
}

void MaterialShaderEditor::finalizeComponents()
{
  QStringList lines;

  lines.append("");

  getRenderSystem().drawText(vec2(0,0), lines.join("\n"));
}

void MaterialShaderEditor::on_action_AutoUpdate_toggled(bool checked)
{
    if(checked)
        m_autoUpdateTimer.start(50);
    else
        m_autoUpdateTimer.stop();
    ui->action_AutoUpdate->setChecked(checked);
}

void MaterialShaderEditor::on_actionHDR_toggled(bool checked)
{
    if(checked)
    {
        m_bloomComponent.reset(new HdrComponent(false));
        m_bloomComponent->setPreset(BloomComponent::Subtle);
        renderComponents()->addComponent(m_bloomComponent.data());
    }
    else
    {
        m_bloomComponent.reset();
    }
    ui->actionHDR->setChecked(checked);
}
