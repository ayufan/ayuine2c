#include "PointLightFragment_p.hpp"

#include <Render/VertexBuffer.hpp>
#include <Render/RenderSystem.hpp>
#include <Render/PrimitiveDrawer.hpp>

#include <Math/Vec3.hpp>

PointLightFragment::PointLightFragment()
{
}

PointLightFragment::~PointLightFragment()
{
}

VertexBuffer::Types PointLightFragment::vertexType() const
{
  return VertexBuffer::Vertex;
}

void PointLightFragment::draw()
{
  const int MaxPoints = 16;
  static vec3 points[MaxPoints * 3];
  static unsigned short indices[MaxPoints * 3 * 2 + 3 * 2];
  static bool initialized = false;

  if(!initialized)
  {
    initialized = true;
    for(int i = 0; i < MaxPoints; ++i)
    {
      float s = qSin((float)i / MaxPoints * 2.0f * M_PI);
      float c = qCos((float)i / MaxPoints * 2.0f * M_PI);

      points[i + 0] = vec3(s, c, 0);
      indices[i * 2 + 0] = (i + 0) % MaxPoints;
      indices[i * 2 + 1] = (i + 1) % MaxPoints;

      points[i + MaxPoints] = vec3(c, 0, s);
      indices[i * 2 + 2 * MaxPoints + 0] = MaxPoints + (i + 0) % MaxPoints;
      indices[i * 2 + 2 * MaxPoints + 1] = MaxPoints + (i + 1) % MaxPoints;

      points[i + 2 * MaxPoints] = vec3(0, s, c);
      indices[i * 2 + 4 * MaxPoints + 0] = 2 * MaxPoints + (i + 0) % MaxPoints;
      indices[i * 2 + 4 * MaxPoints + 1] = 2 * MaxPoints + (i + 1) % MaxPoints;
    }

    indices[MaxPoints * 3 * 2 + 0] = 0;
    indices[MaxPoints * 3 * 2 + 1] = MaxPoints / 2;

    indices[MaxPoints * 3 * 2 + 2] = MaxPoints;
    indices[MaxPoints * 3 * 2 + 3] = MaxPoints + MaxPoints / 2;

    indices[MaxPoints * 3 * 2 + 4] = MaxPoints * 2;
    indices[MaxPoints * 3 * 2 + 5] = MaxPoints * 2 + MaxPoints / 2;
  }

  if(getRenderSystem().setVertexData(vertexType(), points, sizeof(points)))
  {
    int index = getRenderSystem().setIndexData(indices, sizeof(indices)/sizeof(indices[0]));
    if(index >= 0)
    {
      getRenderSystem().draw(IndexedPrimitiveDrawer(PrimitiveDrawer::LineList, 0, MaxPoints * 3,
                                                    0, MaxPoints * 3 + 3, index));
    }
  }
}
