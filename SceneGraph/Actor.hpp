#ifndef ACTOR_HPP
#define ACTOR_HPP

#include <QObject>

#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/Euler.hpp>
#include <Math/Box.hpp>
#include <Math/Sphere.hpp>
#include <Math/Matrix.hpp>

#include <Pipeline/Frame.hpp>

#include "SceneGraph.hpp"

class Frame;
class ColorPickFrame;
class DebugView;

class Scene;
class ActorManager;

class RigidBody;
class CollideInfo;
class QImage;

struct GameInputData;

class SCENEGRAPH_EXPORT Actor : public QObject
{
  Q_OBJECT  
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconActor.png")
  Q_PROPERTY(QString objectName READ objectName WRITE setObjectName)
  Q_PROPERTY(bool enabled READ enabled WRITE setEnabled)
  Q_PROPERTY(matrix transform READ transform WRITE setTransform DESIGNABLE false)
  Q_PROPERTY(vec3 origin READ origin WRITE setOrigin STORED false)
  Q_PROPERTY(euler angles READ angles WRITE setAngles STORED false)
  Q_PROPERTY(vec3 scale READ scale WRITE setScale STORED false)
  Q_PROPERTY(vec3 worldOrigin READ worldOrigin STORED false)
  Q_PROPERTY(euler worldAngles READ worldAngles STORED false)
  Q_PROPERTY(vec3 worldScale READ worldScale STORED false)
  Q_PROPERTY(box boxBounds READ boxBounds STORED false)
  Q_PROPERTY(sphere sphereBounds READ sphereBounds STORED false)

public:
  explicit Actor(QObject *parent = 0);
  ~Actor();

signals:

public:
  virtual void setObjectName(const QString &name);

  bool enabled() const;
  virtual void setEnabled(bool enabled);

  virtual bool autoSector() const;

  virtual void setBoundsDirty();

  virtual bool isSelectable() const;

  virtual bool supportsZones() const;

  virtual bool zonesFromPoint() const;

  virtual bool extendBounds() const;

  bool inGame() const;

public:
  vec3 origin() const;
  void setOrigin(const vec3& origin, bool update = true);

  euler angles() const;
  void setAngles(const euler& angles, bool update = true);

  vec3 scale() const;
  void setScale(const vec3& scale, bool update = true);

  const matrix& transform() const;
  const matrix& transform1() const;
  void setTransform(const matrix& transform, bool update = true);
  void setTransform(const vec3& origin, const euler& angles, const vec3& scale, bool update = true);

  const box& boxBounds() const;
  const sphere& sphereBounds() const;

public:
  vec3 worldOrigin() const;
  void setWorldOrigin(const vec3& origin, bool update = true);

  euler worldAngles() const;
  void setWorldAngles(const euler& angles, bool update = true);

  vec3 worldScale() const;
  void setWorldScale(const vec3& scale, bool update = true);

  matrix worldTransform() const;
  matrix worldTransform1() const;
  void setWorldTransform(const matrix& transform, bool update = true);
  void setWorldTransform(const vec3& origin, const euler& angles, const vec3& scale, bool update = true);

  box worldBoxBounds() const;
  sphere worldSphereBounds() const;

public:
  matrix parentTransform() const;
  matrix parentTransform1() const;

public:
  QSharedPointer<ActorManager> parentActor() const;
  bool setParentActor(const QSharedPointer<ActorManager>& nodeActor);

public:
  QSharedPointer<Scene> scene() const;
protected:
  virtual void setScene(const QSharedPointer<Scene>& scene);

public:
  virtual void removeFromScene();
  bool removedFromScene() const;

public:
  virtual const char* iconFileName() const;

public:
  virtual void generateFrame(Frame& frame, const LocalCamera& localCamera);
  virtual void generateDebugFrame(Frame& frame, const LocalCamera& localCamera);
  virtual void generateColorPickFrame(ColorPickFrame& frame, const LocalCamera& localCamera);

public:
  virtual bool collide(const ray& dir, CollideInfo& collide, DebugView* debugView = NULL) const;

public:
  virtual void compile();
  virtual void prepare();
  virtual void update(float dt);
  virtual void invalidate(const box& boxBounds, Actor* owner);
  virtual bool beginGame();
  virtual void endGame();

public:
  virtual bool input(const GameInputData& input);
  virtual bool camera(Camera& camera, float aspect);
  const QVector<unsigned>& inZones() const;

public:
  virtual QSharedPointer<Actor> clone() const;
  virtual const QMetaObject* type() const;

public:
  virtual void removeActors();

signals:
  void transformChanged(Actor* actor = NULL);
  void enabledChanged(Actor* actor = NULL);
  void parentChanged(Actor* actor = NULL);
  void objectNameAboutToBeChanged(Actor* actor = NULL);
  void objectNameChanged(Actor* actor = NULL);
  void graphicsChanged(Actor* actor = NULL);

protected slots:
  virtual void updateTransform();
  virtual void updateBounds();
  virtual void updateBspZone();
  virtual void updatePhysics();
  virtual void updateZone(bool force = false);

protected:
  bool m_enabled;
  bool m_inGame;

  matrix m_transform, m_transform1;
  box m_boxBounds;
  sphere m_sphereBounds;

  QWeakPointer<ActorManager> m_parentActor;

private:
  QWeakPointer<Scene> m_scene;

public:
  box m_zoneBox;
  QVector<unsigned> m_zoneList;
  unsigned m_zoneTick;

  bool m_removedFromScene;

  friend class Scene;
  friend class ActorManager;
};

Q_DECLARE_OBJECTREF(Actor)

#endif // ACTOR_HPP
