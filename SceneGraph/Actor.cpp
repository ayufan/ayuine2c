#include "Actor.hpp"
#include "ActorManager.hpp"
#include "Scene.hpp"

#include <Physics/CollideInfo.hpp>

#include <Pipeline/SheetFragment.hpp>

#include <Pipeline/Frame.hpp>
#include <Pipeline/ColorPickFrame.hpp>

#include <Render/RenderSystem.hpp>

#include <QIcon>

Q_IMPLEMENT_OBJECTREF(Actor)

Actor::Actor(QObject *parent) :
  QObject(parent)
{
  m_enabled = true;
  m_inGame = false;
  m_removedFromScene = false;
  m_zoneTick = 0;
  m_transform = m_transform1 = mat4Identity;
}

bool Actor::autoSector() const
{
  return false;
}

bool Actor::enabled() const
{
  return m_enabled;
}

void Actor::setEnabled(bool enabled)
{
  if(m_enabled != enabled)
  {
    m_enabled = enabled;
    emit enabledChanged(this);
  }
}

bool Actor::inGame() const
{
  return m_inGame;
}

const matrix & Actor::transform() const
{
  return m_transform;
}

const matrix & Actor::transform1() const
{
  return m_transform1;
}

void Actor::setTransform(const matrix &transform, bool update)
{
  m_transform = transform;
  emit transformChanged(this);
  emit graphicsChanged(this);

  if(update)
  {
    updateTransform();
    updatePhysics();
    updateBounds();
    updateZone(true);

    setBoundsDirty();
  }
}

void Actor::setWorldTransform(const matrix &transform, bool update)
{
  if(parentActor())
  {
    matrix local;
    matrix::multiply(local, parentActor()->worldTransform1(), transform);
    setTransform(local, update);
  }
  else
  {
    setTransform(transform, update);
  }
}

void Actor::setTransform(const vec3& origin, const euler& angles, const vec3& scale, bool update)
{
  matrix temp;
  matrix::transformation(temp, origin, angles, scale);
  setTransform(temp, update);
}

void Actor::setWorldTransform(const vec3& origin, const euler& angles, const vec3& scale, bool update)
{
  matrix temp;
  matrix::transformation(temp, origin, angles, scale);
  setWorldTransform(temp, update);
}

vec3 Actor::origin() const
{
  return m_transform.origin();
}

void Actor::setOrigin(const vec3 &origin, bool update)
{
  setTransform(origin, angles(), scale(), update);
}

euler Actor::angles() const
{
  return m_transform.angles();
}

void Actor::setAngles(const euler &angles, bool update)
{
  setTransform(origin(), angles, scale(), update);
}

vec3 Actor::scale() const
{
  return m_transform.scale();
}

void Actor::setScale(const vec3 &scale, bool update)
{
  setTransform(origin(), angles(), scale, update);
}

void Actor::update(float dt)
{
}

void Actor::invalidate(const box &boxBounds, Actor* owner)
{
}

bool Actor::collide(const ray& dir, CollideInfo &info, DebugView* debugView) const
{
  if(debugView && debugView->isHidden(this))
    return false;

  if(info.flags & CollideInfo::Picking)
  {
    float time = sphere(origin(), 0.2f).collide(dir);
    if(time < 0 || time > info.hitTime)
      return false;

    info.owner = QWeakPointer<QObject>(const_cast<Actor*>(this)).toStrongRef();
    info.hit = dir.hit(time);
    info.hitTime = time;
    return true;
  }
  return false;
}

bool Actor::beginGame()
{
  m_inGame = true;
  return true;
}

void Actor::endGame()
{
  m_inGame = false;
}

bool Actor::camera(Camera &camera, float aspect)
{
  return false;
}

void Actor::removeFromScene()
{
  if(m_removedFromScene)
    return;

  m_removedFromScene = true;

  if(supportsZones())
  {
    if(!m_zoneBox.empty() && scene())
      scene()->invalidate(m_zoneBox, this);

    if(parentActor())
      parentActor()->removeFromZones(this);
  }

  for(ActorManager* parent = m_parentActor.data(); parent; parent = parent->m_parentActor.data())
    parent->m_actorsDirty = true;
}

bool Actor::supportsZones() const
{
  return false;
}

bool Actor::zonesFromPoint() const
{
  return false;
}

const QVector<unsigned> &Actor::inZones() const
{
  return m_zoneList;
}

const box &Actor::boxBounds() const
{
  return m_boxBounds;
}

const sphere &Actor::sphereBounds() const
{
  return m_sphereBounds;
}

void Actor::updateBspZone()
{
  if(parentActor() && supportsZones())
  {
    parentActor()->removeFromZones(this);
    parentActor()->addToZones(this);
  }
}

void Actor::updateTransform()
{
  matrix::invert(m_transform1, m_transform);
}

void Actor::updateBounds()
{
  m_boxBounds = box(origin(), 0.2f);
  m_sphereBounds = sphere(origin(), 0.2f);
}

void Actor::updatePhysics()
{
}

void Actor::updateZone(bool force)
{
  if(!scene() || !supportsZones())
    return;

  box newZoneBox = worldBoxBounds();

  if(force == false && m_zoneBox == newZoneBox)
    return;

  if(m_zoneBox.test(newZoneBox))
  {
    box mergedBounds = box::merge(m_zoneBox, newZoneBox);
    scene()->invalidate(mergedBounds, this);
  }
  else
  {
    if(!m_zoneBox.empty())
      scene()->invalidate(m_zoneBox, this);

    if(!m_boxBounds.empty())
      scene()->invalidate(newZoneBox, this);
  }

  m_zoneBox = newZoneBox;
}

bool Actor::removedFromScene() const
{
  return m_removedFromScene;
}

bool Actor::input(const GameInputData &input)
{
  return false;
}

QSharedPointer<ActorManager> Actor::parentActor() const
{
  return m_parentActor.toStrongRef();
}

bool Actor::setParentActor(const QSharedPointer<ActorManager> &nodeActor)
{
  if(parentActor() == nodeActor)
    return true;

  QSharedPointer<Actor> selfActor(QWeakPointer<Actor>(this).toStrongRef());

  if(parentActor())
  {
    parentActor()->removeActor(selfActor);
  }

  bool result = false;

  if(nodeActor)
  {
    result = nodeActor->addActor(selfActor);
  }

  emit parentChanged(this);
  emit graphicsChanged(this);
  return result;
}

matrix Actor::worldTransform() const
{
  matrix trans = m_transform;
  for(Actor* parent = m_parentActor.data(); parent; parent = parent->m_parentActor.data())
    matrix::multiply(trans, parent->transform(), trans);
  return trans;
}

box Actor::worldBoxBounds() const
{
  if(parentActor())
    return m_boxBounds.transform(parentActor()->worldTransform());
  return m_boxBounds;
}

sphere Actor::worldSphereBounds() const
{
  if(parentActor())
    return m_sphereBounds.transform(parentActor()->worldTransform());
  return m_sphereBounds;
}

matrix Actor::worldTransform1() const
{
  matrix trans = m_transform1;
  for(Actor* parent = m_parentActor.data(); parent; parent = parent->m_parentActor.data())
    matrix::multiply(trans, trans, parent->transform1());
  return trans;
}

void Actor::removeActors()
{
}

vec3 Actor::worldOrigin() const
{
  return worldTransform().origin();
}

euler Actor::worldAngles() const
{
  return worldTransform().angles();
}

QSharedPointer<Scene> Actor::scene() const
{
  return m_scene.toStrongRef();
}

void Actor::setWorldOrigin(const vec3 &origin, bool update)
{
  setWorldTransform(origin, worldAngles(), worldScale(), update);
}

void Actor::setWorldAngles(const euler &angles, bool update)
{
  setWorldTransform(worldOrigin(), angles, worldScale(), update);
}

vec3 Actor::worldScale() const
{
  return worldTransform().scale();
}

void Actor::setWorldScale(const vec3 &scale, bool update)
{
  setWorldTransform(worldOrigin(), worldAngles(), scale, update);
}

void Actor::setObjectName(const QString &name)
{
  if(objectName() != name)
  {
    emit objectNameAboutToBeChanged(this);
    QObject::setObjectName(name);
    emit objectNameChanged(this);
  }
}

bool Actor::isSelectable() const
{
  return true;
}

void Actor::setScene(const QSharedPointer<Scene> &scene)
{
  if(m_scene)
  {
    m_scene.toStrongRef()->disconnect(this);
  }

  m_scene = scene;

  if(scene)
  {
    connect(this, SIGNAL(transformChanged(Actor*)), scene.data(), SIGNAL(transformChanged(Actor*)));
    connect(this, SIGNAL(enabledChanged(Actor*)), scene.data(), SIGNAL(enabledChanged(Actor*)));
    connect(this, SIGNAL(parentChanged(Actor*)), scene.data(), SIGNAL(parentChanged(Actor*)));
    connect(this, SIGNAL(objectNameAboutToBeChanged(Actor*)), scene.data(), SIGNAL(objectNameAboutToBeChanged(Actor*)));
    connect(this, SIGNAL(objectNameChanged(Actor*)), scene.data(), SIGNAL(objectNameChanged(Actor*)));
    connect(this, SIGNAL(graphicsChanged(Actor*)), scene.data(), SIGNAL(graphicsChanged(Actor*)));
  }
}

QSharedPointer<Actor> Actor::clone() const
{
  const QMetaObject* actorMetaObject = metaObject();

  // alloc new object
  QSharedPointer<Actor> newActor((Actor*)actorMetaObject->newInstance());
  if(!newActor)
    return QSharedPointer<Actor>();

  // copy static properties
  for(int i = 0; i < actorMetaObject->propertyCount(); ++i)
  {
    QMetaProperty actorProperty = actorMetaObject->property(i);

    if(!actorProperty.isStored(this))
      continue;

    if(actorProperty.userType() == qMetaTypeId<ActorRef>())
      continue;

    QVariant propertyValue = property(actorProperty.name());

    if(propertyValue.userType() == qMetaTypeId<ActorRefList>())
    {
      ActorRefList newList;

      foreach(const ActorRef& actor, propertyValue.value<ActorRefList>())
      {
        if(!actor)
          continue;
        ActorRef nextActor = actor->clone();
        if(!nextActor)
          continue;
        newList.append(nextActor);
      }

      propertyValue = QVariant::fromValue(newList);
    }

    newActor->setProperty(actorProperty.name(), propertyValue);
  }

  return newActor;
}

matrix Actor::parentTransform() const
{
  if(!m_parentActor.isNull())
    return m_parentActor.toStrongRef()->worldTransform();
  return mat4Identity;
}

matrix Actor::parentTransform1() const
{
  if(!m_parentActor.isNull())
    return m_parentActor.toStrongRef()->worldTransform1();
  return mat4Identity;
}

bool Actor::extendBounds() const
{
  return false;
}

void Actor::setBoundsDirty()
{
  if(!extendBounds())
    return;

  for(ActorManager* parentActor = m_parentActor.data(); parentActor; parentActor = parentActor->m_parentActor.data())
  {
    if(!parentActor->extendBounds())
      return;

    parentActor->m_boundsDirty = true;
  }
}

const QMetaObject * Actor::type() const
{
  return metaObject();
}

const char* Actor::iconFileName() const
{
  int index = metaObject()->indexOfClassInfo("IconFileName");
  if(index >= 0)
    return metaObject()->classInfo(index).value();
  return ":/SceneGraph/IconActor.png";
}

void Actor::generateFrame(Frame &frame, const LocalCamera& localCamera)
{
}

void Actor::generateDebugFrame(Frame &frame, const LocalCamera& localCamera)
{
  if(frame.isHidden(this))
    return;

  QScopedPointer<SheetFragment> iconFragment(new SheetFragment());
  iconFragment->setTexture(iconFileName());
  iconFragment->setFixedState(RenderSystem::FixedTextureConst);
  if(!frame.isSelected(this))
    iconFragment->setColor(QColor::fromRgbF(0.66f, 0.66f, 0.66f));
  iconFragment->setFillMode(RenderSystem::Solid);
  iconFragment->setDepthStencilState(RenderSystem::DepthRead);
  iconFragment->setBlendState(RenderSystem::NonPremultipled);
  iconFragment->setOrder(1002);
  iconFragment->setOrigin(worldOrigin());
  iconFragment->setSize(0.2f);
  iconFragment->setCamera(frame.camera());
  frame.addFragment(iconFragment.take());

  generateFrame(frame, localCamera);
}

void Actor::generateColorPickFrame(ColorPickFrame &frame, const LocalCamera& localCamera)
{
  if(frame.isHidden(this))
    return;

  QScopedPointer<SheetFragment> iconFragment(new SheetFragment());
  iconFragment->setTexture(iconFileName());
  iconFragment->setFixedState(RenderSystem::FixedMaskedConst);
  iconFragment->setColor(frame.objectToColor(this));
  iconFragment->setFillMode(RenderSystem::Solid);
  iconFragment->setDepthStencilState(RenderSystem::DepthRead);
  iconFragment->setAlphaTest(RenderSystem::AlphaTestEnabled);
  iconFragment->setOrder(1002);
  iconFragment->setOrigin(worldOrigin());
  iconFragment->setSize(0.2f);
  iconFragment->setCamera(frame.camera());
  frame.addFragment(iconFragment.take());
}

Actor::~Actor()
{
  if(m_scene)
  {
    m_scene.toStrongRef()->disconnect(this);
  }
}

void Actor::compile()
{
}

void Actor::prepare()
{
}
