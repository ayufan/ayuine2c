#include "ActorManager.hpp"
#include "Scene.hpp"

#include <Physics/CollideInfo.hpp>
#include <Pipeline/BoxFragment.hpp>
#include <Pipeline/Frame.hpp>
#include <Pipeline/ColorPickFrame.hpp>

#include <Math/Ray.hpp>

#include <QIcon>

Q_IMPLEMENT_OBJECTREF(ActorManager)

ActorManager::ActorManager()
{
  m_boundsDirty = false;
  m_grouped = false;
  m_autoSector = true;
}

ActorManager::~ActorManager()
{
}

void ActorManager::setBoundsDirty()
{
  Actor::setBoundsDirty();
  m_boundsDirty = true;
}

void ActorManager::updateBounds()
{
  if(!m_boundsDirty)
    return;

  m_boundsDirty = false;

  box boxBounds = bboxEmpty;

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;

    m_actors[i]->updateBounds();

    if(m_actors[i]->extendBounds())
      boxBounds = box::merge(boxBounds, m_actors[i]->boxBounds());
  }

  m_boxBounds = boxBounds.transform(m_transform);
  m_sphereBounds = boxBounds.range();
  m_sphereBounds.Center += origin();
}

bool ActorManager::addActor(const QSharedPointer<Actor> &actor)
{
  if(!actor)
    return false;

  int index = m_actors.indexOf(actor);
  if(index >= 0)
    return true;

  if(actor->parentActor())
    return false;

  if(actor->scene())
    return false;

  actor->setScene(scene());
  actor->m_parentActor = QWeakPointer<ActorManager>(this).toStrongRef();
  setBoundsDirty();
  m_actors.append(actor);
  emit actorAdded(this, actor.data());
  return true;
}

bool ActorManager::removeActor(const QSharedPointer<Actor> &actor)
{
  if(!actor)
    return true;

  int index = m_actors.indexOf(actor);
  if(index < 0)
    return false;

  emit actorAboutToBeRemoved(this, actor.data());
  actor->setScene(QSharedPointer<Scene>());
  actor->m_parentActor.clear();
  m_actors.remove(index);
  setBoundsDirty();
  removeFromZones(actor.data());
  emit actorRemoved(this, actor.data());
  return true;
}

void ActorManager::removeActors()
{
  if(!m_actorsDirty)
    return;

  m_actorsDirty = false;

  for(int i = m_actors.size(); i-- > 0; )
  {
    if(m_actors[i]->removedFromScene())
    {
      QSharedPointer<Actor> actor(m_actors[i]);
      emit actorAboutToBeRemoved(this, actor.data());
      actor->setScene(QSharedPointer<Scene>());
      actor->m_parentActor.clear();
      m_actors.remove(i);
      setBoundsDirty();
      emit actorRemoved(this, actor.data());
    }
    else
    {
      m_actors[i]->removeActors();
    }
  }
}

void ActorManager::update(float dt)
{
  Actor::update(dt);

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;

    m_actors[i]->update(dt);
  }

  if(m_boundsDirty)
  {
    updateBounds();
  }
}

bool ActorManager::beginGame()
{
  if(!Actor::beginGame())
    return false;

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;

    m_actors[i]->beginGame();
  }
  return true;
}

void ActorManager::endGame()
{
  Actor::endGame();

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;

    m_actors[i]->endGame();
  }
}

QVector<QSharedPointer<Actor> > ActorManager::actors() const
{
  return m_actors;
}

void ActorManager::setActors(const QVector<QSharedPointer<Actor> > &actors)
{
  emit actorsAboutToReset(this);

  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    actor->setScene(QSharedPointer<Scene>());
    actor->m_parentActor.clear();
  }

  m_actors = actors;

  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    actor->setScene(scene());
    actor->m_parentActor = QWeakPointer<ActorManager>(this).toStrongRef();
  }

  setBoundsDirty();

  emit actorsReset(this);
}

int ActorManager::indexOf(const QSharedPointer<Actor> &actor) const
{
  return m_actors.indexOf(actor);
}

int ActorManager::indexOf(Actor* actor) const
{
  for(int i = 0; i < m_actors.size(); ++i)
    if(m_actors[i].data() == actor)
      return i;
  return -1;
}

void ActorManager::invalidate(const box &boxBoundsParent, Actor *owner)
{
  box boxBounds = boxBoundsParent.transform(transform1());

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;
    if(m_actors[i]->boxBounds().test(boxBounds))
      m_actors[i]->invalidate(boxBounds, owner);
  }
}

bool ActorManager::collide(const ray &dirParent, CollideInfo &collide, DebugView* debugView) const
{
  if(debugView && debugView->isHidden(this))
    return false;

  if(collide.flags & CollideInfo::Picking)
  {
    if(!debugView)
      return false;

    if(m_grouped)
    {
      float hitTime = m_boxBounds.collide(dirParent);

      if(hitTime < 0.0f || hitTime > collide.hitTime)
        return false;

      collide.hitTime = hitTime;
      collide.hit = dirParent.hit(collide.hitTime);
      collide.owner = collide.collider = QWeakPointer<Actor>(const_cast<ActorManager*>(this)).toStrongRef();
      collide.material.clear();
      return true;
    }
  }

  bool hit = false;
  ray dir = dirParent.transform(transform1());

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;
    if(collide.flags & CollideInfo::Picking && debugView->isHidden(m_actors[i].data()))
      continue;

    if(m_actors[i]->collide(dir, collide, debugView))
    {
      collide.hit = dirParent.hit(collide.hitTime);
      if(collide.flags & CollideInfo::Fast)
        return true;
      hit = true;
    }
  }
  return hit;
}

void ActorManager::setScene(const QSharedPointer<Scene> &scene)
{
  Actor::setScene(scene);

  if(scene)
  {
    connect(this, SIGNAL(groupedChanged(ActorManager*)), scene.data(), SIGNAL(groupedChanged(ActorManager*)));
    connect(this, SIGNAL(actorAdded(ActorManager*,Actor*)), scene.data(), SIGNAL(actorAdded(ActorManager*,Actor*)));
    connect(this, SIGNAL(actorAboutToBeRemoved(ActorManager*,Actor*)), scene.data(), SIGNAL(actorAboutToBeRemoved(ActorManager*,Actor*)));
    connect(this, SIGNAL(actorRemoved(ActorManager*,Actor*)), scene.data(), SIGNAL(actorRemoved(ActorManager*,Actor*)));
    connect(this, SIGNAL(actorsAboutToReset(ActorManager*)), scene.data(), SIGNAL(actorsAboutToReset(ActorManager*)));
    connect(this, SIGNAL(actorsReset(ActorManager*)), scene.data(), SIGNAL(actorsReset(ActorManager*)));
  }

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;
    m_actors[i]->setScene(scene);
  }
}

bool ActorManager::grouped() const
{
  return m_grouped;
}

void ActorManager::setGrouped(bool grouped)
{
  if(m_grouped != grouped)
  {
    m_grouped = grouped;
    emit groupedChanged(this);
  }
}

void ActorManager::generateFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(m_boundsDirty)
  {
    updateBounds();
  }

  LocalCamera camera(localCamera, transform());

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;
    if(camera.culler.test(m_actors[i]->sphereBounds()))
      m_actors[i]->generateFrame(frame, camera);
  }
}

void ActorManager::generateColorPickFrame(ColorPickFrame &frame, const LocalCamera &localCamera)
{  
  LocalCamera camera(localCamera, transform());

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;
    if(camera.culler.test(m_actors[i]->sphereBounds()))
      m_actors[i]->generateColorPickFrame(frame, camera);
  }
}

void ActorManager::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(frame.isHidden(this))
    return;

  if(m_boundsDirty)
  {
    updateBounds();
  }

  if(frame.isSelected(this))
  {
    QScopedPointer<BoxFragment> outlineFragment(new BoxFragment());
    outlineFragment->setTransform(localCamera.object);
    outlineFragment->setBounds(boxBounds());
    outlineFragment->setColor(QColor::fromRgbF(0.55f, 0.25f, 0.63f, 0.20f));
    outlineFragment->setFixedState(RenderSystem::FixedConst);
    outlineFragment->setFillMode(RenderSystem::Solid);
    outlineFragment->setDepthStencilState(RenderSystem::DepthRead);
    outlineFragment->setBlendState(RenderSystem::Additive);
    outlineFragment->setOrder(999);
    frame.addFragment(outlineFragment.take());

    QScopedPointer<BoxFragment> wireFragment(new BoxFragment());
    wireFragment->setTransform(localCamera.object);
    wireFragment->setBounds(boxBounds());
    wireFragment->setFixedState(RenderSystem::FixedConst);
    wireFragment->setColor(QColor::fromRgbF(0.55f, 0.66f, 0.33f));
    wireFragment->setFillMode(RenderSystem::Wire);
    wireFragment->setOrder(1000);
    frame.addFragment(wireFragment.take());
  }

  LocalCamera camera(localCamera, transform());

  for(int i = 0; i < m_actors.size(); ++i)
  {
    if(m_actors[i]->removedFromScene())
      continue;
    if(camera.culler.test(m_actors[i]->sphereBounds()))
      m_actors[i]->generateDebugFrame(frame, camera);
  }
}

bool ActorManager::isSelectable() const
{
  if(!parentActor())
    return false;
  return true;
}

const QMetaObject * ActorManager::type() const
{
  return &staticMetaObject;
}

void ActorManager::compile()
{
  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    actor->compile();
  }
}

bool ActorManager::extendBounds() const
{
  return true;
}

bool ActorManager::supportsZones() const
{
  return true;
}

bool ActorManager::zonesFromPoint() const
{
  return false;
}

bool ActorManager::autoSector() const
{
  return m_autoSector;
}

void ActorManager::setAutoSector(bool autoSector)
{
  m_autoSector = autoSector;
}

void ActorManager::prepare()
{
  foreach(QSharedPointer<Actor> actor, m_actors)
  {
    actor->prepare();
  }
}
