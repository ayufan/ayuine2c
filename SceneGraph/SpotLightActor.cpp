#include "SpotLightActor.hpp"
#include "StaticMeshActor.hpp"
#include "SpotLightFragment_p.hpp"
#include "Scene.hpp"

#include <Pipeline/SpotLight.hpp>
#include <Pipeline/TexturedFragment.hpp>
#include <Pipeline/ConeFragment.hpp>

#include <MaterialSystem/ShaderState.hpp>

SpotLightActor::SpotLightActor()
{
  m_lightRadius = 16.0f;
  m_shadowSides = 0;
  m_shadowFovy = 90;
}

float SpotLightActor::lightRadius() const
{
  return m_lightRadius;
}

void SpotLightActor::setLightRadius(float lightRadius)
{
  if(m_lightRadius != lightRadius)
  {
    m_lightRadius = lightRadius;
    m_shadowSides = 0;
    updateBounds();
    updateZone();
    setBoundsDirty();
    emit graphicsChanged(this);
  }
}

void SpotLightActor::updateBounds()
{
  m_sphereBounds = sphere(origin(), m_lightRadius);
  m_boxBounds = box(origin(), m_lightRadius);
  m_lightCameraIsValid = false;
}

void SpotLightActor::invalidate(const box &boxBounds, Actor *owner)
{
  if(!qobject_cast<StaticMeshActor*>(owner))
    return;

  if(boxBounds.test(origin()))
  {
    m_shadowSides = 0;
    return;
  }

  if(castShadows())
  {
    if(lightCamera().culler.test(boxBounds.transform(transform1())))
    {
      m_shadowSides = 0;
    }
  }
}

void SpotLightActor::renderShadowMap(ShadowFrame* shadowFrame, const QSharedPointer<BaseRenderTarget> &shadowTexture)
{
  if(!shadowFrame->begin(shadowTexture))
    return;

  shadowFrame->setCamera(lightCamera());

  if(scene())
    scene()->generateFrame(*shadowFrame);

  shadowFrame->draw();
}

bool SpotLightActor::zonesFromPoint() const
{
  return true;
}

void SpotLightActor::updateTransform()
{
  LightActor::updateTransform();

  if(scene())
  {
    if(scene()->shadowFrame())
      scene()->shadowFrame()->free(this);
  }
}

void SpotLightActor::generateFrame(Frame &frame, const LocalCamera& localCamera)
{
  if(!enabled())
    return;

  if(frame.hasLighting())
  {
    m_lightState.set("lightColor", lightColor());
    m_lightState.set("lightPosition", vec4(worldOrigin(), 1.0f / qMax(lightRadius(), 0.0001f)));

    QScopedPointer<SpotLight> light(new SpotLight());
    light->setCamera(lightCamera());
    light->setLightState(m_lightState);
    light->setLightQuality(MaterialSystem::qualityForSphere(frame.camera().origin, sphere(worldOrigin(), lightRadius())));

    if(scene())
    {
      if(scene()->shadowFrame() && castShadows())
      {
#if 0
        light->lightState().set("shadowTexture", QVariant::fromValue<BaseTextureRef>(scene()->cubeShadowFrame()->cleanRenderTarget()));
#else
        ShadowTextureRef shadowTexture;

        switch(scene()->shadowFrame()->update(this, shadowTexture))
        {
        case CubeShadowFrame::Ignore:
          break;
        case CubeShadowFrame::Full:
          m_shadowSides = 0;
        case CubeShadowFrame::Partial:
          if(m_shadowSides == 0)
          {
            renderShadowMap(scene()->shadowFrame(), shadowTexture);
            m_shadowSides = 1;
          }
          break;
        }

        light->lightState().set("shadowTexture", QVariant::fromValue<RenderTextureRef>(shadowTexture));
#endif
      }
    }

    if(light->lightState().contains("shadowProjection"))
    {
      light->lightState().set("shadowProjection", lightCamera().culler.object());
    }

    frame.addLight(light.take());
  }
}

void SpotLightActor::generateDebugFrame(Frame &frame, const LocalCamera& localCamera)
{
  LightActor::generateDebugFrame(frame, localCamera);

  if(frame.isSelected(this))
  {
    QScopedPointer<SpotLightFragment> coneFragment(new SpotLightFragment());
    coneFragment->setTransform(lightCamera().culler.invObject());
    coneFragment->setRasterizerState(RenderSystem::CullNone);
    coneFragment->setFixedState(RenderSystem::FixedConst);
    coneFragment->setColor(QColor::fromRgbF(0.35f, 0.46f, 0.13f));
    coneFragment->setFillMode(RenderSystem::Wire);
    coneFragment->setOrder(1001);
    frame.addFragment(coneFragment.take());
  }
}

bool SpotLightActor::castShadows() const
{
  return m_lightState.contains("shadowTexture");
}

void SpotLightActor::compile()
{
  m_shadowSides = 0;
}

void SpotLightActor::prepare()
{
  m_shadowSides = 0;
}

float SpotLightActor::shadowFovy() const
{
  return m_shadowFovy;
}

void SpotLightActor::setShadowFovy(float shadowFovy)
{
  m_shadowFovy = qBound(0.0f, shadowFovy, 180.0f);
  m_shadowSides = 0;
  m_lightCameraIsValid = false;
}

const Camera& SpotLightActor::lightCamera()
{
  if(!m_lightCameraIsValid)
  {
    Camera::fppCamera(m_lightCamera, worldOrigin(), worldAngles(), m_shadowFovy * Deg2Rad, 1.0f, m_lightRadius);
    m_lightCameraIsValid = true;
  }
  return m_lightCamera;
}
