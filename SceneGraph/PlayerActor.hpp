#ifndef PLAYERACTOR_HPP
#define PLAYERACTOR_HPP

#include "Actor.hpp"

struct ControllerBodyHit;

class PhysicsController;

class SCENEGRAPH_EXPORT PlayerActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconPlayer.png")
  Q_PROPERTY(float radius READ radius WRITE setRadius)
  Q_PROPERTY(float eyeAt READ eyeAt WRITE setEyeAt)
  Q_PROPERTY(float speed READ speed WRITE setSpeed)
  Q_PROPERTY(float height READ height WRITE setHeight)

public:
  Q_INVOKABLE PlayerActor();
  ~PlayerActor();

public:
  float radius() const;
  void setRadius(float radius);

  float eyeAt() const;
  void setEyeAt(float eyeAt);

  float speed() const;
  void setSpeed(float speed);

  float height() const;
  void setHeight(float height);

public:
  bool beginGame();

  void endGame();

  bool input(const GameInputData &input);

  bool camera(Camera &camera, float aspect);

  void update(float dt);

protected:
  void updateBounds();
  void updatePhysics();

private slots:
  void bodyHit(const ControllerBodyHit& hit);

private:
  QScopedPointer<PhysicsController> m_controller;
  float m_jumping;
  float m_radius;
  float m_eyeAt;
  float m_speed;
  float m_height;
};

#endif // PLAYERACTOR_HPP
