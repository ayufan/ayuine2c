#include "Scene.hpp"
#include "Actor.hpp"
#include "ActorManager.hpp"
#include "NaiveActorManager.hpp"

#include <Physics/PhysicsScene.hpp>
#include <Physics/RigidBody.hpp>

#include <Pipeline/CubeShadowFrame.hpp>
#include <Pipeline/ShadowFrame.hpp>
#include <Pipeline/ColorPickFrame.hpp>
#include <Pipeline/AmbientLight.hpp>
#include <Pipeline/SkyFragment.hpp>

Scene::Scene()
{
  m_isGame = false;
  m_gameTime = 0.0f;
  m_gravity = vec3(0, 0, -9.81f);
  m_rootActor = QSharedPointer<ActorManager>(new NaiveActorManager());
  m_cubeShadowFrame.reset(new CubeShadowFrame());
  m_shadowFrame.reset(new ShadowFrame());
  m_ambientState.setMaterialShader("SimpleAmbientModel.grcz");
}

Scene::~Scene()
{
  if(isGame())
  {
    endGame();
  }

  m_rootActor.clear();
}

bool Scene::isGame() const
{
  return m_isGame;
}

void Scene::setIsGame(bool isGame)
{
  m_isGame = isGame;
}

float Scene::gameTime() const
{
  return m_gameTime;
}

void Scene::setGameTime(float gameTime)
{
  m_gameTime = gameTime;
}

const box & Scene::boxBounds() const
{
  return m_rootActor->boxBounds();
}

const vec3 & Scene::gravity() const
{
  return m_gravity;
}

void Scene::setGravity(const vec3 &gravity)
{
  if(m_gravity != gravity)
  {
    m_gravity = gravity;
#ifndef NO_PHYSICS
    if(m_physicsScene)
    {
      m_physicsScene->setGravity(gravity);
    }
#endif
    emit gravityChanged(this);
  }
}

QSharedPointer<Actor> Scene::cameraActor() const
{
  if(QSharedPointer<Actor> actor = m_cameraActor.toStrongRef())
  {
    if(actor->scene() == this)
      return actor;
  }
  return QSharedPointer<Actor>();
}

void Scene::setCameraActor(const QSharedPointer<Actor> &cameraActor)
{
#if 0
  if(cameraActor)
  {
    if(cameraActor->scene() != this)
      return;
  }
#endif

  if(m_cameraActor != cameraActor)
  {
    m_cameraActor = cameraActor;
    emit cameraActorChanged(this);
  }
}

QSharedPointer<Actor> Scene::inputActor() const
{
  if(QSharedPointer<Actor> actor = m_inputActor.toStrongRef())
  {
    if(actor->scene() == this)
      return actor;
  }
  return QSharedPointer<Actor>();
}

void Scene::setInputActor(const QSharedPointer<Actor> &inputActor)
{
#if 0
  if(inputActor)
  {
    if(inputActor->scene() != this)
      return;
  }
#endif

  if(m_inputActor != inputActor)
  {
    m_inputActor = inputActor;
    emit inputActorChanged(this);
  }
}

void Scene::update(float dt)
{
  m_gameTime += dt;

#ifndef NO_PHYSICS
  if(m_physicsScene && dt > 0.0f)
  {
    m_physicsScene->simulate(dt);
  }
#endif

  if(m_cubeShadowFrame)
  {
    m_cubeShadowFrame->nextFrame(dt);
  }

  if(m_shadowFrame)
  {
    m_shadowFrame->nextFrame(dt);
  }

  if(m_rootActor)
  {
    m_rootActor->update(dt);
    m_rootActor->removeActors();
  }
}

bool Scene::beginGame()
{
  m_isGame = true;
  m_gameTime = 0;

#ifndef NO_PHYSICS
  m_physicsScene.reset(new PhysicsScene());
  m_physicsScene->setGravity(m_gravity);
#endif

  if(m_rootActor)
  {
    return m_rootActor->beginGame();
  }
  return false;
}

void Scene::endGame()
{
  m_isGame = false;

  if(m_rootActor)
  {
    m_rootActor->endGame();
    m_rootActor->removeActors();
  }

#ifndef NO_PHYSICS
  m_physicsScene.reset();
#endif
}

void Scene::invalidate(const box &boxBounds, Actor *owner)
{
  if(m_rootActor)
  {
    m_rootActor->invalidate(boxBounds, owner);
  }
}

bool Scene::input(const GameInputData &input)
{
  if(m_inputActor)
  {
    return m_inputActor.toStrongRef()->input(input);
  }
  return false;
}

bool Scene::camera(Camera &camera, float aspect)
{
  if(m_cameraActor)
  {
    return m_cameraActor.toStrongRef()->camera(camera, aspect);
  }
  return false;
}

PhysicsScene * Scene::physicsScene() const
{
  return m_physicsScene.data();
}

bool Scene::collide(const ray &dir, CollideInfo &collide, DebugView* debugView)
{
  if(m_rootActor)
  {
    return m_rootActor->collide(dir, collide, debugView);
  }
  return false;
}

const QSharedPointer<ActorManager> & Scene::rootActor() const
{
  if(m_rootActor->m_scene.isNull())
    m_rootActor->m_scene = const_cast<Scene*>(this);
  return m_rootActor;
}

void Scene::setRootActor(const QSharedPointer<ActorManager> &rootActor)
{
  if(m_rootActor != rootActor)
  {
    emit rootActorAboutToBeChanged(this, m_rootActor.data());

    if(m_rootActor)
      m_rootActor->setScene(QSharedPointer<Scene>());

    m_rootActor = rootActor;

    if(m_rootActor)
      m_rootActor->setScene(QWeakPointer<Scene>(this).toStrongRef());

    emit rootActorChanged(this, m_rootActor.data());
  }
}

CubeShadowFrame * Scene::cubeShadowFrame() const
{
  return m_cubeShadowFrame.data();
}

ShadowFrame * Scene::shadowFrame() const
{
  return m_shadowFrame.data();
}

void Scene::generateFrame(Frame &frame)
{
  if(m_rootActor)
  {
    m_rootActor->generateFrame(frame, LocalCamera(frame.camera()));
  }

  if(frame.hasLighting() && m_ambientState.rawMaterialShader())
  {
    QScopedPointer<AmbientLight> ambientLight(new AmbientLight());
    ambientLight->setLightState(m_ambientState);
    frame.addLight(ambientLight.take());
  }

  if(m_skyState.rawMaterialShader())
  {
    QScopedPointer<SkyFragment> skyFragment(new SkyFragment());
    skyFragment->setState(m_skyState);
    frame.addFragment(skyFragment.take());
  }
}

void Scene::generateDebugFrame(Frame &frame)
{
  if(m_rootActor)
  {
    m_rootActor->generateDebugFrame(frame, LocalCamera(frame.camera()));
  }

  if(frame.hasLighting() && m_ambientState.rawMaterialShader())
  {
    QScopedPointer<AmbientLight> ambientLight(new AmbientLight());
    ambientLight->setLightState(m_ambientState);
    frame.addLight(ambientLight.take());
  }
}

void Scene::generateColorPickFrame(ColorPickFrame &frame)
{
  if(m_rootActor)
  {
    m_rootActor->generateColorPickFrame(frame, LocalCamera(frame.camera()));
  }
}

void Scene::compile()
{
  QElapsedTimer compileTimer;
  compileTimer.start();

  if(m_rootActor)
  {
    m_rootActor->compile();
  }

  qDebug() << "[SCENE] Compile took " << compileTimer.elapsed() << "msec";
}

void Scene::prepare()
{
  QElapsedTimer prepareTimer;
  prepareTimer.start();

  m_shadowFrame.reset(new ShadowFrame());
  m_cubeShadowFrame.reset(new CubeShadowFrame());

  if(m_rootActor)
  {
    m_rootActor->prepare();
  }

  qDebug() << "[SCENE] Prepare took " << prepareTimer.elapsed() << "msec";
}

LightShaderRef Scene::ambientShader() const
{
  return m_ambientState.materialShader().objectCast<LightShader>();
}

void Scene::setAmbientShader(const LightShaderRef &lightShader)
{
  m_ambientState.setMaterialShader(lightShader);
}

void Scene::setAmbientShader(const QString& lightShader)
{
  m_ambientState.setMaterialShader(lightShader);
}

ShaderValues Scene::ambientShaderValues() const
{
  return m_ambientState.values();
}

void Scene::setAmbientShaderValues(const ShaderValues &values)
{
  m_ambientState.setValues(values);
}

ShaderValues Scene::refAmbientShaderValues() const
{
  return m_ambientState.refValues();
}

MaterialShaderRef Scene::skyShader() const
{
  return m_skyState.materialShader().objectCast<MaterialShader>();
}

void Scene::setSkyShader(const MaterialShaderRef &lightShader)
{
  m_skyState.setMaterialShader(lightShader);
}

void Scene::setSkyShader(const QString& lightShader)
{
  m_skyState.setMaterialShader(lightShader);
}

ShaderValues Scene::skyShaderValues() const
{
  return m_skyState.values();
}

void Scene::setSkyShaderValues(const ShaderValues &values)
{
  m_skyState.setValues(values);
}

ShaderValues Scene::refSkyShaderValues() const
{
  return m_skyState.refValues();
}
