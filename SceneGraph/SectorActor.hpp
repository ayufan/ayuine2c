#ifndef SECTORACTOR_HPP
#define SECTORACTOR_HPP

#include "Actor.hpp"

class SCENEGRAPH_EXPORT SectorActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconSector.png")
  Q_PROPERTY(vec3 extent READ scale WRITE setScale)
  Q_PROPERTY(bool autoSector READ autoSector WRITE setAutoSector)

public:
  Q_INVOKABLE SectorActor();

public:
  void generateDebugFrame(Frame &frame, const LocalCamera& localCamera);
  void generateColorPickFrame(ColorPickFrame &frame, const LocalCamera& localCamera);

  bool collide(const ray &dir, CollideInfo &collide, DebugView* debugView = NULL) const;

  bool supportsZones() const;

  bool autoSector() const;
  void setAutoSector(bool autoSector);

protected:
  void updateBounds();

private:
  bool m_autoSector;
};

#endif // SECTORACTOR_HPP
