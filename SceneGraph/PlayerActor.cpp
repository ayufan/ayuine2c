#include "PlayerActor.hpp"
#include "Scene.hpp"

#include <Physics/PhysicsController.hpp>
#include <Physics/MovableRigidBody.hpp>

#include <QColor>
#include <QIcon>
#include <QDebug>

const float JumpTime = 0.2f;

PlayerActor::PlayerActor()
{
  m_height = 1.6f;
  m_radius = 0.3f;
  m_eyeAt = 0.9f;
  m_speed = 0.5f;
  m_jumping = 0;
}

PlayerActor::~PlayerActor()
{
}

float PlayerActor::radius() const
{
  return m_radius;
}

void PlayerActor::setRadius(float radius)
{
  m_radius = qMax(radius, 0.01f);
  updateBounds();
  updateZone();
}

float PlayerActor::eyeAt() const
{
  return m_eyeAt;
}

void PlayerActor::setEyeAt(float eyeAt)
{
  m_eyeAt = qBound(0.0f, eyeAt, 1.0f);
}

float PlayerActor::speed() const
{
  return m_speed;
}

void PlayerActor::setSpeed(float speed)
{
  m_speed = speed;
}

float PlayerActor::height() const
{
  return m_height;
}

void PlayerActor::setHeight(float height)
{
  m_height = qMax(height, 0.01f);
  updateBounds();
  updateZone();
}

bool PlayerActor::beginGame()
{
  if(!Actor::beginGame())
    return false;

  if(!scene() || !scene()->physicsScene())
    return false;

#ifndef NO_PHYSICS
  m_controller.reset(new PhysicsController(scene()->physicsScene()));
  m_controller->setOrigin(worldOrigin());
  m_controller->setHeight(height());
  m_controller->setStepOffset(0.4f * height());
  m_controller->setRadius(radius());

  connect(m_controller.data(), SIGNAL(bodyHit(ControllerBodyHit)), SLOT(bodyHit(ControllerBodyHit)));
#endif
  return true;
}

void PlayerActor::endGame()
{
#ifndef NO_PHYSICS
  m_controller.reset();
#endif
}

bool PlayerActor::input(const GameInputData &input)
{
#ifndef NO_PHYSICS
  if(!m_controller)
    return false;

  vec3 o(worldOrigin());
  vec3 d;
  euler a(worldAngles());

  vec3 at(a.at());
  at.Z = 0.0f;
  at = at.normalize() * input.Delta * 6.0f;

  vec3 right(a.right());
  right.Z = 0.0f;
  right = right.normalize() * input.Delta * 6.0f;

  if(input.Up)
    d += at;
  if(input.Down)
    d -= at;
  if(input.Left)
    d -= right;
  if(input.Right)
    d += right;

  a.Yaw += input.dX;
  a.Pitch = clamp(a.Pitch - input.dY, - Pi / 2.0f, Pi / 2.0f);

  if(m_jumping > 0.0f || input.Jump && m_controller->isOnFloor())
  {
    if(m_jumping < 0.0f)
      m_jumping = JumpTime;
    d -= scene()->gravity() * input.Delta;
  }

  else
  {
    d += scene()->gravity() * input.Delta;
  }

  m_controller->move(d);

  setWorldTransform(worldOrigin(), a, worldScale(), false);

  updateTransform();
#endif
  return true;
}

bool PlayerActor::camera(Camera &camera, float aspect)
{
  vec3 offset(vec3(0.0f, 0.0f, 0.5f) * m_eyeAt * m_height);
  Camera::fppCamera(camera, worldOrigin() + offset, worldAngles(), Deg2Rad * 60.0f, aspect);
  return true;
}

void PlayerActor::update(float dt)
{
#ifndef NO_PHYSICS
  if(!m_controller)
    return;

  setWorldTransform(m_controller->origin(), worldAngles(), worldScale(), false);

  updateTransform();
  updateBounds();
  updateZone();

  m_jumping -= dt;

  if(m_controller->isOnFloor() && m_controller->isHittingCeil())
    m_jumping = 0.0f;
#endif
}

void PlayerActor::updateBounds()
{
  vec3 org(origin());
  vec3 extent(m_radius, m_radius, m_height / 2.0f);

  m_boxBounds = box(org - extent, org + extent);
  m_sphereBounds = m_boxBounds.range();
}

void PlayerActor::updatePhysics()
{
  Actor::updatePhysics();

#ifndef NO_PHYSICS
  if(m_controller)
    m_controller->setOrigin(worldOrigin());
#endif
}

void PlayerActor::bodyHit(const ControllerBodyHit &hit)
{
  if(!hit.body)
    return;

  if(MovableRigidBody* body = qobject_cast<MovableRigidBody*>(hit.body))
  {
    float coeff = body->mass() * hit.length * 10.0f;
    body->addForce(hit.localNormal * coeff, MovableRigidBody::Impulse);
  }
}
