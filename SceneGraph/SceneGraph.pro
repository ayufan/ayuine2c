TEMPLATE = lib
TARGET = SceneGraph

include(../ayuine2c.pri)

# DEFINES += NO_PHYSICS

HEADERS += \
    SceneGraph.hpp \
    Actor.hpp \
    Scene.hpp \
    SectorActor.hpp \
    PortalActor.hpp \
    PlayerActor.hpp \
    StaticMeshActor.hpp \
    ActorManager.hpp \
    NaiveActorManager.hpp \
    BspActorManager.hpp \
    BspActorManager_p.hpp \
    LightActor.hpp \
    PointLightActor.hpp \
    MeshActor.hpp \
    TerrainActor.hpp \
    PointLightFragment_p.hpp \
    TerrainFragment_p.hpp \
    TerrainShader.hpp \
    SpotLightActor.hpp \
    SpotLightFragment_p.hpp \
    PathPointActor.hpp

SOURCES += \
    SceneGraph.cpp \
    Actor.cpp \
    Scene.cpp \
    SectorActor.cpp \
    PortalActor.cpp \
    PlayerActor.cpp \
    StaticMeshActor.cpp \
    ActorManager.cpp \
    NaiveActorManager.cpp \
    BspActorManager.cpp \
    LightActor.cpp \
    PointLightActor.cpp \
    MeshActor.cpp \
    TerrainActor.cpp \
    PointLightFragment.cpp \
    TerrainFragment.cpp \
    TerrainShader.cpp \
    SpotLightActor.cpp \
    SpotLightFragment.cpp \
    PathPointActor.cpp

LIBS += -lMath -lCore -lRender -lPipeline -lMeshSystem -lMaterialSystem -lPhysics

RESOURCES += \
    SceneGraph.qrc
