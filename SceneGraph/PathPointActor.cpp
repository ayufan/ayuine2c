#include "PathPointActor.hpp"
#include "Scene.hpp"

#include <QColor>
#include <QIcon>
#include <QDebug>

#include <Pipeline/PointFragment.hpp>

// #define CONST_TIME

PathPointActor::PathPointActor()
{
}

PathPointActor::~PathPointActor()
{
}

bool PathPointActor::beginGame()
{
    if(!Actor::beginGame())
        return false;

    m_pathTime = 0;
    m_cameraPosition = originAtTime(m_pathTime, true);
    return true;
}

void PathPointActor::endGame()
{
    m_pathTime = 0;
}

bool PathPointActor::camera(Camera &camera, float aspect)
{

  // qDebug() << "[PATH] Origin:" << origin.X << origin.Y << origin.Z;

  vec3 cameraEye = originAtTime(m_pathTime + 4.0f, true);
  if(m_cameraPosition == cameraEye)
    cameraEye += vec3(1, 0, 0);

  Camera::lookAt(camera, m_cameraPosition, cameraEye, Deg2Rad * 60.0f, aspect);
  return true;
}

void PathPointActor::update(float dt)
{
    m_pathTime += dt * 3.0f;

    const float tau = 0.5f;
    float timef = 1.0f - expf(-dt * tau);
    vec3 origin = originAtTime(m_pathTime, true);
    m_cameraPosition = vec3::lerp(m_cameraPosition, origin, timef);
}

void PathPointActor::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
    Actor::generateDebugFrame(frame, localCamera);

    if(!m_nextPoint)
        return;

    vec3 origin = worldOrigin();
    vec3 otherOrigin = m_nextPoint->worldOrigin();

    QScopedPointer<LineFragment> fragment(new LineFragment());
    fragment->setFixedState(RenderSystem::FixedConst);
    fragment->setColor(QColor::fromRgbF(0.75f, 0.45f, 0.4f));
    fragment->setDrawMode(LineFragment::List);
    fragment->setOrder(1000);
    fragment->addVerts(PointVertex(origin));
    fragment->addVerts(PointVertex(otherOrigin));
    frame.addFragment(fragment.take());
}

QSharedPointer<Actor> PathPointActor::nextPoint() const
{
  return m_nextPoint;
}

void PathPointActor::setNextPoint(const QSharedPointer<Actor> &nextPoint)
{
  m_nextPoint = nextPoint.objectCast<PathPointActor>();
}

float PathPointActor::nextTime() const
{
  if(!m_nextPoint)
    return 0;
#ifdef CONST_TIME
  return 1;
#else
  return (m_nextPoint->worldOrigin() - origin()).length();
#endif
}

float PathPointActor::totalTime() const
{
  float totalTime = 0;
#ifndef CONST_TIME
  vec3 origin = worldOrigin();
#endif
  QSharedPointer<PathPointActor> point = m_nextPoint;

  while(!point.isNull() && point != this)
  {
#ifdef CONST_TIME
    totalTime += 1;
#else
    vec3 newOrigin = point->worldOrigin();
    totalTime += (newOrigin - origin).length();
#endif
    point = point->m_nextPoint;
  }

  return totalTime;
}

QSharedPointer<PathPointActor> PathPointActor::pointAtTime(float time, bool loop, float* outTime) const
{
  if(time < 0)
  {
    return QSharedPointer<PathPointActor>();
  }

  if(loop)
  {
    float total = totalTime();
    time -= int(time / total) * total;
  }

#ifndef CONST_TIME
  vec3 origin = worldOrigin();
#endif
  QSharedPointer<PathPointActor> point = m_nextPoint;

  while(!point.isNull() && point != this)
  {
#ifdef CONST_TIME
    if(time < 1.0f)
    {
      if(outTime)
      {
        *outTime = time;
      }
      return point;
    }
    time -= 1.0f;
#else
    vec3 newOrigin = point->worldOrigin();
    float currentTime = (newOrigin - origin).length();
    if(time < currentTime)
    {
      if(outTime)
      {
        *outTime = time / currentTime;
      }
      return point;
    }
    time -= currentTime;
    origin = newOrigin;
#endif
    point = point->m_nextPoint;
  }

  return QSharedPointer<PathPointActor>();
}

vec3 PathPointActor::origin2(float time)
{
  if(!m_nextPoint)
    return worldOrigin();

  vec3 a = worldOrigin();
  vec3 b = m_nextPoint->worldOrigin();
  return vec3::lerp(a, b, time);
}

vec3 PathPointActor::origin3(float time)
{
  if(!m_nextPoint)
    return worldOrigin();
  return vec3::lerp(origin2(time), m_nextPoint->origin2(time), time);
}

vec3 PathPointActor::origin4(float time)
{
  if(!m_nextPoint)
    return worldOrigin();

  vec3 abc = m_nextPoint->origin3(time);

  if(time < 0.5)
  {
    return vec3::lerp(origin3(time+0.5f), abc, time+0.5f);
  }
  else if(m_nextPoint->m_nextPoint)
  {
    return vec3::lerp(abc, origin3(time-0.5f), time-0.5f);
  }
  else
  {
    return abc;
  }
}

vec3 PathPointActor::originAtTime(float time, bool loop) const
{
  float currentTime = 0;
  QSharedPointer<PathPointActor> point = pointAtTime(time, loop, &currentTime);
  if(!point)
    return worldOrigin();
  return point->origin2(currentTime);
}
