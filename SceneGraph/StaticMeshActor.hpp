#ifndef STATICMESHACTOR_HPP
#define STATICMESHACTOR_HPP

#include "MeshActor.hpp"

#include <MeshSystem/Mesh.hpp>

class SCENEGRAPH_EXPORT StaticMeshActor : public MeshActor
{
  Q_OBJECT
  Q_PROPERTY(float mass READ mass WRITE setMass)
  Q_PROPERTY(MeshRef mesh READ mesh WRITE setMesh)

public:
  Q_INVOKABLE StaticMeshActor();
  ~StaticMeshActor();

public:
  const QSharedPointer<Mesh> &mesh() const;
  void setMesh(const QSharedPointer<Mesh>& mesh);

  float mass() const;
  void setMass(float mass);

public:
  void generateFrame(Frame &frame, const LocalCamera& localCamera);
  void generateDebugFrame(Frame &frame, const LocalCamera& localCamera);
  void generateColorPickFrame(ColorPickFrame &frame, const LocalCamera& localCamera);

  bool collide(const ray& dir, CollideInfo &collide, DebugView* debugView = NULL) const;

  bool beginGame();

  void endGame();

protected:
  void updateBounds();

private slots:
  void transformUpdated();

private:
  QSharedPointer<Mesh> m_mesh;
  float m_mass;
  QScopedPointer<RigidBody> m_rigidBody;
};

#endif // STATICMESHACTOR_HPP
