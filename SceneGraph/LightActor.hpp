#ifndef LIGHTACTOR_HPP
#define LIGHTACTOR_HPP

#include "Actor.hpp"

#include <MaterialSystem/ShaderState.hpp>
#include <MaterialSystem/LightShader.hpp>

#include <QColor>

class SCENEGRAPH_EXPORT LightActor : public Actor
{
  Q_OBJECT
  Q_CLASSINFO("IconFileName", ":/SceneGraph/IconLight.png")
  Q_PROPERTY(bool castShadows READ castShadows)
  Q_PROPERTY(QColor lightColor READ lightColor WRITE setLightColor)
  Q_PROPERTY(LightShaderRef lightShader READ lightShader WRITE setLightShader)
  Q_PROPERTY(ShaderValues lightShaderValues READ lightShaderValues WRITE setLightShaderValues)
  Q_PROPERTY(ShaderValues __reflightShaderValues READ refLightShaderValues DESIGNABLE false)

public:
  LightActor();
  ~LightActor();

public:
  virtual bool castShadows() const;

public:
  bool extendBounds() const;
  bool supportsZones() const;
  bool zonesFromPoint() const;

  void removeFromScene();

  QColor lightColor() const;
  void setLightColor(QColor lightColor);

  LightShaderRef lightShader() const;
  void setLightShader(const LightShaderRef& lightShader);
  void setLightShader(const QString& lightShader);

  ShaderValues lightShaderValues() const;
  void setLightShaderValues(const ShaderValues& values);

  ShaderValues refLightShaderValues() const;

  const QMetaObject* type() const;

protected:


protected:
  QColor m_lightColor;
  ShaderState m_lightState;
};

#endif // LIGHTACTOR_HPP
