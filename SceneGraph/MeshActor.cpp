#include "MeshActor.hpp"

MeshActor::MeshActor()
{
  m_movable = false;
  m_collidable = true;
  m_gravity = true;
  m_castShadows = true;
  m_autoSector = false;
}

MeshActor::~MeshActor()
{
}

bool MeshActor::supportsZones() const
{
  return true;
}

bool MeshActor::zonesFromPoint() const
{
  return false;
}

bool MeshActor::extendBounds() const
{
  if(movable())
    return false;
  return true;
}

bool MeshActor::movable() const
{
  return m_movable;
}

void MeshActor::setMovable(bool movable)
{
  if(m_movable != movable)
  {
    m_movable = movable;
    emit movableChanged(this);
  }
}

bool MeshActor::collidable() const
{
  return m_collidable;
}

void MeshActor::setCollidable(bool collidable)
{
  if(m_collidable != collidable)
  {
    m_collidable = collidable;
    emit collidableChanged(this);
  }
}

bool MeshActor::gravity() const
{
  return m_gravity;
}

void MeshActor::setGravity(bool gravity)
{
  if(m_gravity != gravity)
  {
    m_gravity = gravity;
    emit gravityChanged(this);
  }
}

bool MeshActor::castShadows() const
{
  return m_castShadows;
}

void MeshActor::setCastShadows(bool shadows)
{
  if(m_castShadows != shadows)
  {
    m_castShadows = shadows;
    emit castShadowsChanged(this);
    emit graphicsChanged(this);
  }
}

bool MeshActor::autoSector() const
{
  return !movable() && m_autoSector;
}

void MeshActor::setAutoSector(bool autoSector)
{
  m_autoSector = autoSector;
}

const QMetaObject * MeshActor::type() const
{
  return &MeshActor::staticMetaObject;
}
