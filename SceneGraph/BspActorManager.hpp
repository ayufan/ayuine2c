#ifndef BSPACTORMANAGER_HPP
#define BSPACTORMANAGER_HPP

#include "ActorManager.hpp"

struct BspZone;
struct BspNode;
struct BspPlaneEx;
struct vec3;
struct frustum;

class SCENEGRAPH_EXPORT BspActorManager : public ActorManager
{
  Q_OBJECT

public:
  Q_INVOKABLE BspActorManager();
  ~BspActorManager();

private:
  const BspNode& bspNode(unsigned node) const;
  unsigned bspNodeCount() const;
  const BspZone& bspZone(unsigned zone) const;
  unsigned bspZoneCount() const;
  unsigned findNode(const vec3& origin);
  void buildBsp_r(unsigned node, QVector<BspPlaneEx>& planes);
  void addZoneToBspTree(BspZone& zone, unsigned zoneIndex);
  BspZone* findZones(const box& bounds);
  BspZone* findZones(const vec3& origin, float distance);
  void findVisibleZones_r(unsigned zoneIndex, const frustum& f, const QRectF& s,
                          QVector<unsigned> &depth, BspZone*& zoneList);
  BspZone* findVisibleZones(const vec3& origin, const frustum& f);
  void findVisibleActors(const vec3& origin, const frustum& f, QVector<Actor*> &actorList);
  void findActors(const box& bounds, QVector<Actor*> &actorList);

public:
  void addToZones(Actor* actor);
  void removeFromZones(Actor* actor);
  void generateFrame(Frame& frame, const LocalCamera& localCamera);
  void generateDebugFrame(Frame& frame, const LocalCamera& localCamera);
  void invalidate(const box& boxBounds, Actor* owner);

public:
  void compile();

private:
  QVector<BspNode> m_bspNodes;
  QVector<BspZone> m_bspZones;
  unsigned m_bspTick;
};

#endif // BSPACTORMANAGER_HPP
