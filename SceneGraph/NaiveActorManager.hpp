#ifndef NAIVEACTORMANAGER_HPP
#define NAIVEACTORMANAGER_HPP

#include "ActorManager.hpp"

class SCENEGRAPH_EXPORT NaiveActorManager : public ActorManager
{
  Q_OBJECT

public:
  Q_INVOKABLE NaiveActorManager();
  ~NaiveActorManager();

public:
  void addToZones(Actor* actor);
  void removeFromZones(Actor* actor);
};

#endif // NAIVEACTORMANAGER_HPP
