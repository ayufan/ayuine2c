#include "StaticMeshActor.hpp"
#include "Scene.hpp"

#include <Math/Ray.hpp>

#include <Physics/CollideInfo.hpp>
#include <Physics/StaticRigidBody.hpp>
#include <Physics/MovableRigidBody.hpp>

#include <Pipeline/Frame.hpp>
#include <Pipeline/ColorPickFrame.hpp>

#include <QIcon>

StaticMeshActor::StaticMeshActor()
{
  m_mass = 0.0f;
}

StaticMeshActor::~StaticMeshActor()
{
}

void StaticMeshActor::generateFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(!m_mesh)
    return;

  if(frame.type() == Frame::Depth)
  {
    if(!castShadows())
      return;
  }

  m_mesh->generateFrame(frame, localCamera.object * transform());
}

void StaticMeshActor::generateDebugFrame(Frame &frame, const LocalCamera &localCamera)
{
  if(!m_mesh || frame.isHidden(this, false))
    return;

  m_mesh->generateDebugFrame(frame, localCamera.object * transform(), this);
}

void StaticMeshActor::generateColorPickFrame(ColorPickFrame &frame, const LocalCamera &localCamera)
{
  if(!m_mesh || frame.isHidden(this, false))
    return;

  m_mesh->generateColorPickFrame(frame, localCamera.object * transform(), this);
}

void StaticMeshActor::transformUpdated()
{
  if(!m_rigidBody)
    return;

#ifndef NO_PHYSICS
  setWorldTransform(m_rigidBody->transform(), false);
#endif

  updateTransform();
  updateBounds();
  updateZone();
}

bool StaticMeshActor::collide(const ray& dir, CollideInfo &collide, DebugView* debugView) const
{
  if(debugView && debugView->isHidden(this))
    return false;

  if(m_mesh)
  {
    ray dirLocal = dir.transform(transform1());

    if(m_mesh->sphereBounds().collide(dirLocal) < 0)
      return false;

    if(!m_mesh->collide(dirLocal, collide))
      return false;
    collide.hit = dir.hit(collide.hitTime);
    collide.owner = QWeakPointer<QObject>(const_cast<StaticMeshActor*>(this)).toStrongRef();
    return true;
  }
  else
  {
    return Actor::collide(dir, collide);
  }
}

bool StaticMeshActor::beginGame()
{
  if(!Actor::beginGame())
    return false;

  if(!m_mesh)
    return false;

  if(!scene() || !scene()->physicsScene())
    return false;

#ifndef NO_PHYSICS
  if(movable())
  {
    if(mesh()->colliderObject())
    {
      QScopedPointer<MovableRigidBody> rigidBody(new MovableRigidBody(mesh()->colliderObject().data(), worldTransform(), scene()->physicsScene()));

      rigidBody->setGravity(gravity());
      rigidBody->setCollidable(collidable());
      rigidBody->setMass(mass() < 0.0f ? mesh()->colliderMass() : mass());

      connect(rigidBody.data(), SIGNAL(bodyMoved(matrix)), SLOT(transformUpdated()));

      rigidBody->wakeUp();

      m_rigidBody.reset(rigidBody.take());
    }
  }
  else
  {
    if(!mesh()->colliderObject())
      return true;

    if(!collidable())
      return true;

    m_rigidBody.reset(new StaticRigidBody(mesh()->colliderObject().data(), worldTransform(), scene()->physicsScene()));
  }
#endif
  return true;
}

void StaticMeshActor::endGame()
{
  Actor::endGame();
#ifndef NO_PHYSICS
  m_rigidBody.reset();
#endif
}

void StaticMeshActor::updateBounds()
{
  if(m_mesh)
  {
    m_boxBounds = m_mesh->boxBounds().transform(m_transform);
    m_sphereBounds = m_mesh->sphereBounds().transform(m_transform);
  }
  else
  {
    m_boxBounds = bboxEmpty;
    m_sphereBounds = sphere();
  }
}

const QSharedPointer<Mesh> &StaticMeshActor::mesh() const
{
  return m_mesh;
}

void StaticMeshActor::setMesh(const QSharedPointer<Mesh> &mesh)
{
  m_mesh = mesh;
  updateBounds();
  updateZone();
}

float StaticMeshActor::mass() const
{
  return m_mass;
}

void StaticMeshActor::setMass(float mass)
{
  m_mass = qMax(mass, 0.0f);
}
