#include <MeshSystem/MeshVertex.hpp>
#include <MeshSystem/Mesh.hpp>
#include <MeshSystem/MeshSubset.hpp>
#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>
#include <Core/Logger.hpp>
#include <Core/ProgressStatus.hpp>
#include <Render/Texture.hpp>
#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/Matrix.hpp>
#include <Math/TangentVectors.hpp>
#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/Material.hpp>
#include <QColor>
#include <QDebug>

#include "niflib/include/obj/NiObject.h"
#include "niflib/include/obj/NiNode.h"
#include "niflib/include/obj/NiTriStrips.h"
#include "niflib/include/obj/NiTriStripsData.h"
#include "niflib/include/obj/NiMaterialProperty.h"
#include "niflib/include/obj/NiImage.h"
#include "niflib/include/obj/NiRawImageData.h"
#include "niflib/include/obj/NiTexturingProperty.h"
#include "niflib/include/obj/NiTextureProperty.h"
#include "niflib/include/obj/NiSourceTexture.h"
#include "niflib/include/obj/NiSkinInstance.h"
#include "niflib/include/obj/BSShaderNoLightingProperty.h"
#include "niflib/include/obj/BSShaderTextureSet.h"
#include "niflib/include/obj/NiMaterialProperty.h"
#include "niflib/include/obj/SkyShaderProperty.h"
#include "niflib/include/obj/TileShaderProperty.h"
#include "niflib/include/obj/BSShaderPPLightingProperty.h"
#include "niflib/include/obj/Lighting30ShaderProperty.h"
#include "niflib/include/obj/bhkCollisionObject.h"
#include "niflib/include/obj/bhkRigidBody.h"
#include "niflib/include/niflib.h"
#include "qstdistream.hpp"

using namespace Niflib;

class NifImporter : public ResourceImporter
{
public:
    NifImporter()
    {
        registerImporter("nif", &Mesh::staticMetaObject, "NIF Importer");
    }

private:
    static QColor toColor(Color3 color, float alpha)
    {
        return QColor::fromRgbF(color.r, color.g, color.b, alpha);
    }

    QSharedPointer<Material> parseMaterial(NiAVObjectRef object)
    {
        QSharedPointer<Material> material(new Material);
        material->setMaterialShader("StandardMaterial.grcz");
        material->setObjectName(object->GetName().c_str());
        material->setCollidable(true);

        enum TextureTypes
        {
            DiffuseTexture,
            NormalTexture,
            GlowTexture,
            HeighTexture,
            EnvTexture,
            EnvMaskTexture,
            MaxTextures
        };

        string textures[MaxTextures];

        if(NiMaterialPropertyRef property = DynamicCast<NiMaterialProperty>(object->GetPropertyByType(NiMaterialProperty::TYPE)))
        {
            float alpha = property->GetTransparency();
            if(alpha < 0.9f)
            {
                material->setMaterialShader("TranslucentMaterial.grcz");
                material->setOpaque(Material::AlphaTest);
            }

            float power = property->GetGlossiness();
            Color3 ambient = property->GetAmbientColor();
            Color3 diffuse = property->GetDiffuseColor();
            Color3 emissive = property->GetEmissiveColor();
            Color3 specular = property->GetSpecularColor();
            material->set("ambientColor", toColor(ambient, alpha));
            material->set("diffuseColor", toColor(diffuse, alpha));
            material->set("emissiveColor", toColor(emissive, alpha));
            material->set("specularColor", toColor(specular, alpha));
            material->set("specularPower", power);
        }

        if(NiTexturePropertyRef property = DynamicCast<NiTextureProperty>(object->GetPropertyByType(NiTextureProperty::TYPE)))
        {
            if(NiImageRef image = property->GetImage())
            {
                if(image->IsTextureExternal())
                {
                    textures[DiffuseTexture] = image->GetTextureFileName();
                }
                else if(NiRawImageDataRef imageData = image->GetRawImageData())
                {
                    // not supported
                }
            }
        }

        if(BSShaderNoLightingPropertyRef property = DynamicCast<BSShaderNoLightingProperty>(object->GetPropertyByType(BSShaderNoLightingProperty::TYPE)))
        {
            textures[DiffuseTexture] = property->GetFileName();
        }

        if(SkyShaderPropertyRef property = DynamicCast<SkyShaderProperty>(object->GetPropertyByType(SkyShaderProperty::TYPE)))
        {
            textures[DiffuseTexture] = property->GetFileName();
        }

        if(TileShaderPropertyRef property = DynamicCast<TileShaderProperty>(object->GetPropertyByType(TileShaderProperty::TYPE)))
        {
            textures[DiffuseTexture] = property->GetFileName();
        }

        if(BSShaderPPLightingPropertyRef property = DynamicCast<BSShaderPPLightingProperty>(object->GetPropertyByType(BSShaderPPLightingProperty::TYPE)))
        {
            if(BSShaderTextureSetRef textureSet = property->GetTextureSet())
            {
                /*!
                 * Textures.
                 *             0: Diffuse
                 *             1: Normal/Gloss
                 *             2: Glow/Skin/Hair
                 *             3: Height/Parallax
                 *             4: Environment
                 *             5: Environment Mask
                 */

                for(int i = min<int>(textureSet->GetTextures().size(), MaxTextures); i-- > 0; )
                {
                    textures[i] = textureSet->GetTexture(i);
                }
            }
        }

        if(NiTexturingPropertyRef property = DynamicCast<NiTexturingProperty>(object->GetPropertyByType(NiTexturingProperty::TYPE)))
        {
            for(int i = min<int>(property->GetTextureCount(), MaxTextures); i-- > 0; )
            {
                TexDesc& desc = property->GetTexture(i);
                if(!desc.source)
                    continue;

                if(desc.source->IsTextureExternal())
                {
                    textures[i] = desc.source->GetTextureFileName();
                }
                else
                {
                    // not supported
                }
            }
        }

        if(textures[DiffuseTexture].size())
        {
            TextureRef textureRef = Resource::load<Texture>(textures[DiffuseTexture].c_str(), meshContext);
            material->setMainTexture(textureRef);
            material->set("diffuseTexture", textureRef);
        }
        else
        {
            return QSharedPointer<Material>();
        }

        if(textures[NormalTexture].size())
        {
            TextureRef textureRef = Resource::load<Texture>(textures[NormalTexture].c_str(), meshContext);
            material->set("normalTexture", textureRef);
        }

        if(textures[GlowTexture].size())
        {
            TextureRef textureRef = Resource::load<Texture>(textures[GlowTexture].c_str(), meshContext);
            material->set("glowTexture", textureRef);
        }

        if(textures[HeighTexture].size())
        {
            TextureRef textureRef = Resource::load<Texture>(textures[HeighTexture].c_str(), meshContext);
            material->set("heighTexture", textureRef);
        }

        if(textures[EnvTexture].size())
        {
            TextureRef textureRef = Resource::load<Texture>(textures[EnvTexture].c_str(), meshContext);
            material->set("envTexture", textureRef);
        }

        if(textures[EnvMaskTexture].size())
        {
            TextureRef textureRef = Resource::load<Texture>(textures[EnvMaskTexture].c_str(), meshContext);
            material->set("envMaskTexture", textureRef);
        }

        return material;
    }

    bool parseTriStrip(NiTriStripsRef triStrips, const Matrix44& transform = Matrix44::IDENTITY)
    {
        if(!triStrips)
            return false;

        NiTriBasedGeomDataRef geomData = DynamicCast<NiTriBasedGeomData>(triStrips->GetData());
        if(!geomData)
            return false;

        QSharedPointer<MeshSubset> meshSubset(new MeshSubset());
        meshSubset->setObjectName(triStrips->GetName().c_str());

        if(QSharedPointer<Material> material = parseMaterial(NiAVObjectRef(triStrips)))
        {
            meshSubset->setMaterial(material);
        }
        else
        {
            return false;
        }

        unsigned offset = meshVertices.size();
        meshVertices.resize(meshVertices.size() + geomData->GetVertexCount());

        vector<Vector3> vertices = geomData->GetVertices();
        vector<Vector3> normals = geomData->GetNormals();
        vector<Vector3> tangents = geomData->GetTangents();
        vector<Vector3> binormals = geomData->GetBinormals();
        vector<TexCoord> texCoords = geomData->GetUVSet(0);

        for(unsigned i = 0; i < geomData->GetVertexCount(); ++i)
        {
            MeshVertex& vertex = meshVertices[offset];
            vertex.origin = i < vertices.size() ? &vertices[i].x : vec3(0,0,0);
            vertex.normal = i < normals.size() ? &normals[i].x : vec3(0,0,1);
            vertex.tangent = i < tangents.size() ? vec4(&tangents[i].x, 1) : vec4(1,0,0,1);
            if(i < binormals.size())
                vertex.tangent.W = (vertex.tangent ^ vec3(&binormals[i].x)) >= 0 ? 1 : -1;
            vertex.coords = i < texCoords.size() ? vec2(texCoords[i].u, texCoords[i].v) : vec2(0,0);
        }

        if(NiTriStripsDataRef triData = DynamicCast<NiTriStripsData>(geomData))
        {
            unsigned indexOffset = meshIndices.size();

            for(unsigned i = 0; i < triData->GetStripCount(); ++i)
            {
                vector<unsigned short> indices = triData->GetStrip(i);
                if(indices.empty())
                    continue;
                if(meshIndices.size() != indexOffset)
                {
                    meshIndices.append(meshIndices.back());
                    meshIndices.append(indices.front());
                }
                foreach(unsigned short index, indices)
                {
                    meshIndices.append(index);
                }
            }

            IndexedPrimitiveDrawer drawer;
            drawer.BaseIndex = offset;
            drawer.MinIndex = 0;
            drawer.NumVertices = meshVertices.size() - offset;
            drawer.StartIndex = indexOffset;
            drawer.PrimitiveCount = meshIndices.size() - indexOffset - 2;
            meshSubset->setDrawer(drawer);
        }
        else
        {
            return false;
        }

        meshSubsets.append(meshSubset);
        return true;
    }
    bool parseCollider(bhkCollisionObjectRef collisionObject, const Matrix44& transform = Matrix44::IDENTITY)
    {
        if(!collisionObject || !collisionObject->GetBody())
            return true;

        if(bhkRigidBodyRef rigidBody = DynamicCast<bhkRigidBody>(collisionObject->GetBody()))
        {
            return true;
        }
        else
        {
            qDebug() << "[NIF] Unsupported collider:" << collisionObject->GetBody()->GetType().GetTypeName().c_str();
            return false;
        }
    }

    void parseNif(NiNodeRef node, const Matrix44& transform)
    {
        if(!node)
            return;

        foreach(NiAVObjectRef object, node->GetChildren())
        {
            if(object->IsDerivedType(bhkCollisionObject::TYPE))
            {
                bhkCollisionObjectRef collisionObject = DynamicCast<bhkCollisionObject>(object);
                if(!collisionObject)
                    continue;
                parseCollider(collisionObject, transform);
            }
            else if(object->IsDerivedType(NiTriStrips::TYPE))
            {
                NiTriStripsRef triStrips = DynamicCast<NiTriStrips>(object);
                if(!triStrips)
                    continue;
                parseTriStrip(triStrips, triStrips->GetLocalTransform() * transform);
            }
            else if(object->IsDerivedType(NiNode::TYPE))
            {
                NiNodeRef node = DynamicCast<NiNode>(object);
                if(!node)
                    continue;
                parseNif(node, node->GetLocalTransform() * transform);
            }
            else
            {
                qDebug() << "[NIF] Unsupported nodeObject:" << object->GetType().GetTypeName().c_str();
            }
        }
    }

    QSharedPointer<Resource> parseNif(NiNodeRef root)
    {
        meshSubsets.clear();
        meshVertices.clear();
        meshIndices.clear();

        parseNif(root, Matrix44::IDENTITY);

        if(meshSubsets.size())
        {
            QSharedPointer<Mesh> mesh(new Mesh());
            mesh->setVertices(meshVertices);
            mesh->setIndices(meshIndices);
            mesh->setSubsets(meshSubsets);
            return mesh;
        }

        return QSharedPointer<Resource>();
    }

public:
    QSharedPointer<Resource> load(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
    {
        if(!device)
            return QSharedPointer<Resource>();

        try
        {
            NiObjectRef nif = ReadNifTree(device->objectName().toAscii().data());
            if(!nif)
                return QSharedPointer<Resource>();

            NiNodeRef node = Niflib::DynamicCast<NiNode>(nif);
            if(node)
                return parseNif(node);
        }
        catch(std::exception& e)
        {
            qWarning() << "[NIF]" << e.what();
        }

        return QSharedPointer<Resource>();
    }

    const QMetaObject* metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
    {
        return &Mesh::staticMetaObject;
    }

private:
    QVector<QSharedPointer<MeshSubset> > meshSubsets;
    QVector<MeshVertex> meshVertices;
    QVector<unsigned> meshIndices;
    QString meshContext;
};

static NifImporter nifImporter;
