TEMPLATE = lib
TARGET = NifReader

include(../ayuine2c.pri)

LIBS += -lMath -lCore -lRender -lMaterialSystem -lPipeline -lPhysics -lSceneGraph -lMeshSystem

# DEFINES += NO_PHYSICS

HEADERS += \
    EsmFormat.hpp \
    NifReader.hpp \
    qstdistream.hpp

SOURCES += \
    obj.cpp \
    frc.cpp \
    EsmFormat.cpp \
    esm.cpp \
    NifReader.cpp \
    nif.cpp

DEFINES += NIFLIB_STATIC_LINK

include(niflib/NifLib.pri)
