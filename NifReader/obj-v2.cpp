#include "MeshVertex.hpp"
#include "Mesh.hpp"
#include "MeshSubset.hpp"

#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>
#include <Core/Logger.hpp>
#include <Core/ProgressStatus.hpp>
#include <Render/Texture.hpp>
#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Math/TangentVectors.hpp>
#include <MaterialSystem/MaterialShader.hpp>
#include <MaterialSystem/Material.hpp>
#include <QColor>
#include <QDebug>

struct ObjIndex
{
  int Vertex, Coords, Normal;

  ObjIndex()
  {
    Vertex = 0;
    Coords = 0;
    Normal = 0;
  }

  bool operator < (const ObjIndex& index) const
  {
    return Vertex < index.Vertex ||
      Coords < index.Coords ||
      Normal < index.Normal;
  }

  bool operator == (const ObjIndex& index) const
  {
    return Vertex == index.Vertex &&
      Coords == index.Coords &&
      Normal == index.Normal;
  }
};

struct ObjFace
{
  unsigned Indices[3];
  unsigned Group;

  ObjFace()
  {
    Group = 0;
    Indices[0] = 0;
    Indices[1] = 0;
    Indices[2] = 0;
  }
};

class MeshObjImporter : public ResourceImporter
{
  typedef QVector<ObjFace>                              ObjSurface;
  typedef QHash<QString, ObjSurface>                    ObjObject;
  typedef QHash<QString, QSharedPointer<Material> >     ObjMaterials;
  typedef QHash<ObjIndex, unsigned>                     ObjIndices;

  QString objName;
  QString groupName;
  QString materialName;
  ObjObject object;
  ObjSurface* surface;
  QVector<vec3> vertices, normals, tangents, binormals;
  QVector<vec2> coords;
  ObjMaterials materials;
  QPair<QColor, bool> ambientColor, diffuseColor, specularColor;
  QPair<float, bool> opacity, shininess;
  QPair<QString, bool> diffuseTexture, normalTexture;
  Material::OpaqueMode opaque;
  ObjIndices indices;

  QVector<MeshVertex> meshVertices;
  QVector<unsigned> meshIndices;
  QVector<QSharedPointer<MeshSubset> > meshSubsets;
  QVector<vec3> meshColliderVertices;
  QVector<unsigned> meshColliderIndices;

private:
  void clearCache()
  {
    object.clear();
    surface = NULL;
    groupName.clear();
    materialName.clear();
    ambientColor.second = diffuseColor.second = specularColor.second = false;
    opacity.second = shininess.second = false;
    diffuseTexture.second = normalTexture.second = false;
    opaque = Material::Opaque;
    indices.clear();
  }

  static void fixVertex(vec3 &origin, vec3 &normal)
  {
#if 1
    origin /= 64.0f;
#else
#define swapYZ(v) { vec3 tmp = v; v.Y = -tmp.Z; v.Z = tmp.Y; }
    swapYZ(origin);
    swapYZ(normal);
#endif
  }
  void finishGroupCache()
  {
    if(groupName.isEmpty())
      return;

    MeshVertex meshVertex;
    unsigned meshVerticesStart = meshVertices.size();
    unsigned meshIndicesStart = meshIndices.size();

    for(ObjObject::Iterator surface = object.begin(); surface != object.end(); ++surface)
    {
      if(surface.value().isEmpty())
        continue;
      if(surface.key().isEmpty())
        continue;

      QVector<ObjIndex> objIndices(indices.size());

      for(ObjIndices::Iterator index = indices.begin(); index != indices.end(); ++index)
      {
        objIndices[index.value()] = index.key();
      }

      if(surface.key() == "collision")
      {
        foreach(const ObjFace &face, surface.value())
        {
          meshColliderIndices.append(meshVertices.size());
          meshColliderVertices.append(vertices.at(indices[face.Indices[0]]));

          meshColliderIndices.append(meshVertices.size());
          meshColliderVertices.append(vertices.at(indices[face.Indices[1]]));

          meshColliderIndices.append(meshVertices.size());
          meshColliderVertices.append(vertices.at(indices[face.Indices[2]]));
        }
        continue;
      }

      QSharedPointer<MeshSubset> meshSubset(new MeshSubset);;
      meshSubset->setObjectName(groupName);
      meshSubset->setMaterial(materials[surface.key()]);

      // normals.clear();
      // tangents.clear();
      // binormals.clear();

      if(meshSubset->material().isNull())
        continue;

      unsigned baseMeshVertex = meshVertices.size();

      foreach(ObjIndex index, objIndices)
      {
        vec3 origin = vertices.at(index.Vertex - 1); // 64.0f;
        vec2 coord = index.Coords && coords.size() ? coords.at(index.Coords - 1) : vec2();
        vec3 normal = index.Normal > 0 && index.Normal <= normals.size() ? normals.at(index.Normal - 1).normalize() : ts.Normal;
        // if((normal ^ ts.Normal) < 0)
        //  normal = -normal;

        fixVertex(origin, normal);

        vec3 tangent = index.Normal > 0 && index.Normal <= tangents.size() ? tangents.at(index.Normal - 1).normalize() : ts.calculateTangent(normal);
        vec3 binormal = index.Normal > 0 && index.Normal <= binormals.size() ? binormals.at(index.Normal - 1).normalize() : ts.calculateBinormal(normal);
        float coherent = binormal ^ (normal % tangent);
        float tangentFlag = coherent >= 0.0f ? 1.0f : -1.0f;

        meshVertex.origin = origin;
        meshVertex.coords = coord;
        // meshVertex.coords.Y = -meshVertex.coords.Y;
        meshVertex.normal = normal;
        meshVertex.tangent = vec4(tangent, tangentFlag);
        meshVertices.append(meshVertex);
      }

      foreach(ObjFace face, surface.value())
      {
#if 1
        meshIndices.append(baseMeshVertex + face.Indices[2]);
        meshIndices.append(baseMeshVertex + face.Indices[1]);
        meshIndices.append(baseMeshVertex + face.Indices[0]);
#else
        if(face.Indices.size() < 3)
          continue;

        TangentVectors ts(
              vertices[face.Indices[0].Vertex - 1],
              vertices[face.Indices[1].Vertex - 1],
              vertices[face.Indices[2].Vertex - 1],
              face.Indices[0].Coords ? coords[face.Indices[0].Coords - 1] : vec2(),
              face.Indices[1].Coords ? coords[face.Indices[1].Coords - 1] : vec2(),
              face.Indices[2].Coords ? coords[face.Indices[2].Coords - 1] : vec2());

        unsigned faceStartVertex = meshVertices.size() - meshVerticesStart;

        foreach(ObjIndex index, face.Indices)
        {
          vec3 origin = vertices.at(index.Vertex - 1); // 64.0f;
          vec2 coord = index.Coords && coords.size() ? coords.at(index.Coords - 1) : vec2();
          vec3 normal = index.Normal > 0 && index.Normal <= normals.size() ? normals.at(index.Normal - 1).normalize() : ts.Normal;
          // if((normal ^ ts.Normal) < 0)
          //  normal = -normal;

          fixVertex(origin, normal);

          vec3 tangent = index.Normal > 0 && index.Normal <= tangents.size() ? tangents.at(index.Normal - 1).normalize() : ts.calculateTangent(normal);
          vec3 binormal = index.Normal > 0 && index.Normal <= binormals.size() ? binormals.at(index.Normal - 1).normalize() : ts.calculateBinormal(normal);
          float coherent = binormal ^ (normal % tangent);
          float tangentFlag = coherent >= 0.0f ? 1.0f : -1.0f;

          meshVertex.origin = origin;
          meshVertex.coords = coord;
          // meshVertex.coords.Y = -meshVertex.coords.Y;
          meshVertex.normal = normal;
          meshVertex.tangent = vec4(tangent, tangentFlag);
          meshVertices.append(meshVertex);
        }

        for(int i = 2; i < face.Indices.size(); ++i)
        {
          meshIndices.append(faceStartVertex + i - 1);
          meshIndices.append(faceStartVertex + i);
          meshIndices.append(faceStartVertex);
        }
#endif
      }

      meshSubset->setDrawer(IndexedPrimitiveDrawer(PrimitiveDrawer::TriList, 0, meshVertices.size() - meshVerticesStart, meshIndicesStart, (meshIndices.size() - meshIndicesStart) / 3, meshVerticesStart));
      meshSubset->setBoxBounds(box(&meshVertices[meshVerticesStart].origin, meshVertices.size() - meshVerticesStart, sizeof(MeshVertex)));
      meshSubset->setSphereBounds(sphere(&meshVertices[meshVerticesStart].origin, meshVertices.size() - meshVerticesStart, sizeof(MeshVertex)));

      if(!meshSubset->drawer().PrimitiveCount || !meshSubset->drawer().NumVertices)
        return;

      meshSubsets.append(meshSubset);
    }
  }

  void finishMaterialCache()
  {
    if(materialName.isEmpty())
      return;

    QSharedPointer<Material> material(new Material);
    if(opaque != Material::Opaque)
      material->setMaterialShader("TranslucentMaterial.grcz");
    else
      material->setMaterialShader("StandardMaterial.grcz");
    material->setObjectName(materialName);
    material->setCollidable(true);

    if(specularColor.second)
    {
      QColor newColor = specularColor.first;
      newColor.setAlphaF(opacity.second ? opacity.first : 1.0f);
      material->set("specularColor", newColor);
    }

    if(shininess.second)
    {
      material->set("specularPower", shininess.first);
    }

    material->setOpaque(opaque);

    if(TextureRef diffuseTextureRef = Resource::load<Texture>(diffuseTexture.first, objName))
    {
      material->setMainTexture(diffuseTextureRef);
      material->set("diffuseTexture", diffuseTextureRef);
    }

    if(TextureRef normalTextureRef = Resource::load<Texture>(normalTexture.first, objName))
    {
      material->set("normalTexture", normalTextureRef);
    }

    materials[materialName] = material;

    // qDebug() << "[OBJ] Material: " << materialName << material->values();
  }

  void finishCache()
  {
    finishGroupCache();
    finishMaterialCache();
  }

public:
  void clear()
  {
    meshVertices.resize(0);
    meshIndices.resize(0);
    meshSubsets.resize(0);
    meshColliderVertices.resize(0);
    meshColliderIndices.resize(0);
    materials.clear();
    vertices.resize(0);
    normals.resize(0);
    coords.resize(0);
    tangents.resize(0);
    binormals.resize(0);
    indices.resize(0);
    clearCache();
  }

  bool loadObj(QIODevice& stream)
  {
    clear();

    objName = stream.objectName();
    if(parseObj(stream))
    {
      finishCache();
      clearCache();
      return true;
    }
    return false;
  }

  bool parseObj(const QString& fileName)
  {
    QFile file(fileName);
    if(file.open(QFile::ReadOnly))
    {
      return parseObj(file);
    }
    return false;
  }

  bool parseObj(QIODevice& stream)
  {
    unsigned sg = 0;

    ProgressStatus status(&stream);

    QTextStream textStream(&stream);

    // Czytaj ze strumienia dopuki nie osi�gni�to ko�ca pliku
    while(!textStream.atEnd())
    {
      QString command;
      textStream >> command;
      if(textStream.atEnd() || command.isEmpty())
        break;

      status.setValue(&stream);

      QString lineBuf = textStream.readLine();
      lineBuf = lineBuf.trimmed();

      if(command.length() > 0 && command[0] == '#')
        continue;

      if(command == "mtllib")
      {
        debugf("Including mttlib: %s", qPrintable(lineBuf));

        // wxString currentFileName = wxFileName(objName).GetPath() + wxFileName::GetPathSeparator() + mtllib;
        parseObj(lineBuf);
      }

      // Wierzcho�ek
      else if(command == "v")
      {
        vec3 vertex;
        sscanf(qPrintable(lineBuf), "%f %f %f", &vertex.X, &vertex.Y, &vertex.Z);
        vertices.append(vertex);
      }

      // Koordynanty tekstur
      else if(command == "vt")
      {
        vec2 coord;
        sscanf(qPrintable(lineBuf), "%f %f", &coord.X, &coord.Y);
        //coord.Y *= -1.0f;
        coords.append(coord);
      }

      // Wektory styczne
      else if(command == "vtan")
      {
        vec3 tangent;
        sscanf(qPrintable(lineBuf), "%f %f %f", &tangent.X, &tangent.Y, &tangent.Z);
        tangents.append(tangent);
      }
      else if(command == "vb")
      {
        vec3 binormal;
        sscanf(qPrintable(lineBuf), "%f %f %f", &binormal.X, &binormal.Y, &binormal.Z);
        binormals.append(binormal);
      }

      // Wektor normalny
      else if(command == "vn")
      {
        vec3 normal;
        sscanf(qPrintable(lineBuf), "%f %f %f", &normal.X, &normal.Y, &normal.Z);
        normals.append(normal);
      }

      // Kolor wierzcho�ka
      else if(command == "vc")
      {
      }

      // Nazwa grupy
      else if(command == "g")
      {
        finishCache();
        clearCache();
        groupName = lineBuf;
      }

      // Ustaw materia�
      else if(command == "usemtl")
      {
        surface = &object[lineBuf];
      }

      // Nowy materia�
      else if(command == "newmtl")
      {
        finishCache();
        clearCache();
        materialName = lineBuf;
      }

      // Ambient color
      else if(command == "Ka")
      {
        float red, green, blue;
        sscanf(qPrintable(lineBuf), "%f %f %f", &red, &green, &blue);
        ambientColor.first = QColor::fromRgbF(red, green, blue);
        ambientColor.second = true;
      }

      // Diffuse color
      else if(command == "Kd") {
        float red, green, blue;
        sscanf(qPrintable(lineBuf), "%f %f %f", &red, &green, &blue);
        diffuseColor.first = QColor::fromRgbF(red, green, blue);
        diffuseColor.second = true;
      }

      // Specular color
      else if(command == "Ks") {
        float red, green, blue;
        sscanf(qPrintable(lineBuf), "%f %f %f", &red, &green, &blue);
        specularColor.first = QColor::fromRgbF(red, green, blue);
        specularColor.second = true;
      }

      // Opacity
      else if(command == "d" || command == "Tr")
      {
        sscanf(qPrintable(lineBuf), "%f", &opacity.first);
        opacity.second = true;
      }

      // Shininess
      else if(command == "Ns")
      {
        sscanf(qPrintable(lineBuf), "%f", &shininess.first);
        shininess.second = true;
      }

      // Diffuse map
      else if(command == "map_Kd")
      {
        diffuseTexture.first = lineBuf;
        diffuseTexture.second = true;
      }

      // Normal map
      else if(command == "normalMap")
      {
        normalTexture.first = lineBuf;
        normalTexture.second = true;
      }

      else if(command == "glowMap")
      {
      }

      else if(command == "decal")
      {
      }

      else if(command == "bump")
      {
      }

      // AlphaBlend
      else if(command == "alphaBlend")
      {
        // if(lineBuf.startsWith("true"))
        //  opaque = Material::Sort;
        // else
          opaque = Material::AlphaTest;
      }

      // Smoothing object
      else if(command == "s")
      {
        sscanf(qPrintable(lineBuf), "%i", &sg);
      }

      // Scianka
      else if(command == "f")
      {
        if(!surface)
          continue;
        if(vertices.isEmpty())
          continue;

        ObjFace face;
        unsigned indexCount = 0;
        QString indexDesc;
        bool result = true;

        face.Group = sg;

        QTextStream lineStream(&lineBuf);

        // Pobierz wszystkie indeksy
        while(!lineStream.atEnd())
        {
          lineStream >> indexDesc;

          ObjIndex index;

          // Przeanalizuj dane
          if(coords.size())
          {
            if(normals.size())
              result = sscanf(qPrintable(indexDesc), "%i/%i/%i", &index.Vertex, &index.Coords, &index.Normal) == 3;
            else
              result = sscanf(qPrintable(indexDesc), "%i/%i", &index.Vertex, &index.Coords) == 2;
          }
          else
          {
            if(normals.size())
              result = sscanf(qPrintable(indexDesc), "%i//%i", &index.Vertex, &index.Normal) == 2;
            else
              result = sscanf(qPrintable(indexDesc), "%i", &index.Vertex) == 1;
          }
          if(!result)
            break;

          if(index.Vertex == 0)
            continue;

          // Popraw indeksy
          if(index.Vertex < 0)
            index.Vertex += vertices.size() + 1;
          if(index.Normal < 0)
            index.Normal += normals.size() + 1;
          if(index.Coords < 0)
            index.Coords += coords.size() + 1;

          if(indices.contains(index))
            face.Indices[indexCount++] = indices[index];
          else
            face.Indices[indexCount++] = indices[index] = indices.size();

          if(indexCount == 3)
          {
            surface->append(face);
            face.Indices[1] = face.Indices[2];
            indexCount = 2;
          }
        }
      }

      else
      {
        warningf("OBJ: Unknown command: %s", qPrintable(command));
        //throw NotSupportedException();
      }
    }
    return true;
  }

public:
  MeshObjImporter()
  {
    vertices.reserve(10000);
    normals.reserve(10000);
    tangents.reserve(10000);
    binormals.reserve(10000);
    coords.reserve(10000);
    indices.reserve(10000);
    meshVertices.reserve(10000);
    meshIndices.reserve(10000);
    meshSubsets.reserve(10000);
    meshColliderVertices.reserve(10000);
    meshColliderIndices.reserve(10000);

    registerImporter("obj", &Mesh::staticMetaObject);
  }

public:
  QSharedPointer<Resource> load(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    if(!device)
      return QSharedPointer<Resource>();

    if(device->objectName().isEmpty())
      return QSharedPointer<Resource>();

    if(loadObj(*device))
    {
      if(meshVertices.isEmpty() || meshIndices.isEmpty() || meshSubsets.isEmpty())
        return QSharedPointer<Resource>();

      QSharedPointer<Mesh> mesh(new Mesh());
      mesh->setBoxBounds(box(&meshVertices.constData()->origin, meshVertices.size(), sizeof(MeshVertex)));
      mesh->setSphereBounds(sphere(&meshVertices.constData()->origin, meshVertices.size(), sizeof(MeshVertex)));
      mesh->setVertices(meshVertices);
      mesh->setIndices(meshIndices);
      mesh->setSubsets(meshSubsets);

      if(meshColliderIndices.size())
        mesh->setColliderType(Mesh::ColliderExact);
      else if(meshColliderVertices.size())
        mesh->setColliderType(Mesh::ColliderConvex);
      else
        mesh->setColliderType(Mesh::ColliderDisabled);
      mesh->setColliderMass(0);

      return mesh;
    }
    return QSharedPointer<Resource>();
  }

  const QMetaObject* metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
  {
    return &Mesh::staticMetaObject;
  }
};

static MeshObjImporter meshObjImporter;

