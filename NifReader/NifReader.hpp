#ifndef NIFREADER_HPP
#define NIFREADER_HPP

#include <QtGlobal>

#ifdef NIFREADER_EXPORTS
#define NIFREADER_EXPORT Q_DECL_EXPORT
#else
#define NIFREADER_EXPORT Q_DECL_IMPORT
#endif

#endif // NIFREADER_HPP
