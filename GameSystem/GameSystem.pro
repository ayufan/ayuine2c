TEMPLATE = lib
TARGET = GameSystem

include(../ayuine2c.pri)

HEADERS += \
    GameSystem.hpp \
    Game.hpp \
    TimeDemoGame.hpp

SOURCES += \
    GameSystem.cpp \
    Game.cpp \
    TimeDemoGame.cpp

LIBS += -lMath -lCore -lRender -lPipeline -lMeshSystem -lMaterialSystem -lPhysics -lSceneGraph -lUser32
