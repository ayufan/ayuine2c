#ifndef GAME_HPP
#define GAME_HPP

#include <QMainWindow>

#include "GameSystem.hpp"

#include <QElapsedTimer>

#include <Render/RenderComponentWidget.hpp>
#include <Pipeline/Frame.hpp>
#include <SceneGraph/Scene.hpp>

class BloomComponent;

class GAMESYSTEM_EXPORT Game : public RenderComponentWidget
{
  Q_OBJECT

public:
  explicit Game(QWidget *parent = 0);
  ~Game();

public:
  void run();

signals:

public slots:
  bool askForResolution();
  bool beginGame(const QSharedPointer<Scene>& scene);
  void endGame();
  void resetTimer();
  void update();
  bool isGame() const;
  bool isActive() const;

protected:
  void closeEvent(QCloseEvent *);

protected slots:
  virtual void updateInput();
  virtual void updateInput(const GameInputData& data);
  virtual void updateTime(float dt);
  virtual void prepareComponents();
  virtual void finalizeComponents();

public:
  QStringList toStringList() const;

protected:
  QElapsedTimer m_timer;
  float m_deltaTime;
  float m_generateTime;

  QElapsedTimer m_fpsTimer;
  unsigned m_fpsFrames;
  float m_fps;

  QSharedPointer<Scene> m_scene;
  Frame* m_frame;
  BloomComponent* m_bloomComponent;

  float m_inputMouseSensitivity;

  bool m_mouseInvalid;
};

#endif // GAME_HPP
