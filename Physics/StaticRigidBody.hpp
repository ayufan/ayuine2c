#ifndef STATICRIGIDBODY_HPP
#define STATICRIGIDBODY_HPP

#include "RigidBody.hpp"

class PHYSICS_EXPORT StaticRigidBody : public RigidBody
{
  Q_OBJECT

public:
  explicit StaticRigidBody(Collider* collider, const matrix& transform, PhysicsScene* scene);
};

#endif // STATICRIGIDBODY_HPP
