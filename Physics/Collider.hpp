//------------------------------------//
// 
// Collider.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-03-08
// 
//------------------------------------//

#pragma once

#include <QObject>
#include "Physics.hpp"

struct vec3;
class NxShapeDesc;

class PHYSICS_EXPORT Collider : public QObject
{
  Q_OBJECT

public:
  Collider();

  // Methods
public:
  virtual NxShapeDesc* shapeDesc() = 0;

  vec3 origin() const;
  void setOrigin(const vec3& origin);

  float density() const;
  void setDensity(float density);

  float mass() const;
  void setMass(float mass);
};
