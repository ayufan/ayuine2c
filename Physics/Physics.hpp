#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#include <QtGlobal>

#ifdef PHYSICS_EXPORTS
#define PHYSICS_EXPORT Q_DECL_EXPORT
#else
#define PHYSICS_EXPORT Q_DECL_IMPORT
#endif

#endif // PHYSICS_HPP
