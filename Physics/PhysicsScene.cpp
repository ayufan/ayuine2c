#include "PhysicsScene.hpp"
#include "RigidBody.hpp"
#include "PhysicsEngine.hpp"
#include "NxControllerManager.h"
#include <NxPhysicsSDK.h>
#include <NxMaterial.h>
#include <NxSceneDesc.h>
#include <Math/Vec3.hpp>
#include <NxScene.h>

class ControllerManagerAllocator : public NxUserAllocator
{
public:
  void*	mallocDEBUG(size_t size, const char* fileName, int line)	{ return ::malloc(size); }
  void*	malloc(size_t size)											{ return ::malloc(size); }
  void*	realloc(void* memory, size_t size)							{ return ::realloc(memory, size); }
  void	free(void* memory)											{ ::free(memory); }
};

PhysicsScene::PhysicsScene() :
  QObject(&getPhysicsEngine())
{
  // create scene
  NxSceneDesc desc;
  desc.gravity = NxVec3(0, -9.81f, 0);
  desc.upAxis = NX_Y;
  desc.userData = this;
  desc.flags |= NX_SF_ENABLE_ACTIVETRANSFORMS;
  m_scene = getPhysicsEngine()->createScene(desc);

  // create controller
  static ControllerManagerAllocator allocator;
  m_controller = NxCreateControllerManager(&allocator);

  // default material
  NxMaterial* nxMaterial = m_scene->getMaterialFromIndex(0);
  nxMaterial->setRestitution(0.0f);
  nxMaterial->setStaticFriction(0.5f);
  nxMaterial->setDynamicFriction(0.5f);
}

PhysicsScene::~PhysicsScene()
{
  if(m_scene)
  {
	  getPhysicsEngine()->releaseScene(*m_scene);
	  m_scene = NULL;
  }

  if(m_controller)
  {
	  NxReleaseControllerManager(m_controller);
	  m_controller = NULL;
  }
}

void PhysicsScene::simulate(float dt)
{
  if(m_scene && dt > 0.0f)
  {
    m_scene->simulate(dt);
    m_scene->flushStream();
    m_scene->fetchResults(NX_ALL_FINISHED, true);

    // Pobierz transformacje
    unsigned transformCount;
    NxActiveTransform* transforms = m_scene->getActiveTransforms(transformCount);

    // Uaktualnij transformacje
    for( ; transformCount-- > 0; ++transforms)
    {
      if(transforms->userData)
        static_cast<RigidBody*>(transforms->userData)->updateTransform(transforms->actor2World);
    }
  }

  if(m_controller)
  {
    m_controller->updateControllers();
  }
}

vec3 PhysicsScene::gravity() const
{
  if(m_scene)
  {
    NxVec3 gravity;
    m_scene->getGravity(gravity);
    return vec3(gravity.x, gravity.y, gravity.z);
  }
  return vec3Zero;
}

void PhysicsScene::setGravity(const vec3 &value) const
{
  if(m_scene)
  {
    m_scene->setGravity(NxVec3(value.X, value.Y, value.Z));
  }
}
