//------------------------------------//
// 
// Collider.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Physics
// Date: 2008-08-28
// 
//------------------------------------//

#include "Collider.hpp"
#include <NxShapeDesc.h>
#include <Math/Vec3.hpp>

Collider::Collider()
{
}

vec3 Collider::origin() const {
  NxVec3 origin = const_cast<Collider*>(this)->shapeDesc()->localPose.t;
  return vec3(origin.x, origin.y, origin.z);
}

void Collider::setOrigin(const vec3& origin) {
  const_cast<Collider*>(this)->shapeDesc()->localPose.t.set(&origin.X);
}

float Collider::density() const {
  return const_cast<Collider*>(this)->shapeDesc()->density;
}

void Collider::setDensity(float density) {
  const_cast<Collider*>(this)->shapeDesc()->density = density;
}

float Collider::mass() const {
  return const_cast<Collider*>(this)->shapeDesc()->mass;
}

void Collider::setMass(float mass) {
  const_cast<Collider*>(this)->shapeDesc()->mass = mass;
}

