#include "StaticRigidBody.hpp"
#include "PhysicsScene.hpp"
#include "PhysicsEngine_p.hpp"
#include <NxScene.h>
#include <NxActorDesc.h>

StaticRigidBody::StaticRigidBody(Collider* collider, const matrix& transform, PhysicsScene* scene) :
    RigidBody(scene)
{
  Q_ASSERT(collider);

  if(scene && collider)
  {
    NxActorDesc actorDesc;
    actorDesc.userData = this;
    actorDesc.globalPose = fromMatrix(transform);

    // dodaj collidera
    actorDesc.shapes.push_back(collider->shapeDesc());

    // Utw�rz aktora
    m_actor = scene->m_scene->createActor(actorDesc);
    m_transform = transform;
    Q_ASSERT(m_actor);
  }
}
