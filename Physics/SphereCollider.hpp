#ifndef SPHERECOLLIDER_HPP
#define SPHERECOLLIDER_HPP

#include "Collider.hpp"

class NxSphereShapeDesc;
struct sphere;
struct vec3;

class PHYSICS_EXPORT SphereCollider : public Collider
{
  Q_OBJECT

private:
  NxSphereShapeDesc* m_shapeDesc;

  // Constructor
public:
  SphereCollider();
  SphereCollider(const sphere& sph);
  SphereCollider(const vec3& origin, float radius);
  SphereCollider(float radius);

  // Destructor
public:
  ~SphereCollider();

  // Methods
public:
  NxShapeDesc* shapeDesc();
  float radius() const;
  void setRadius(float radius);
};


#endif // SPHERECOLLIDER_HPP
