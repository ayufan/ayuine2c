#ifndef TRICOLLIDER_HPP
#define TRICOLLIDER_HPP

#include "Collider.hpp"
#include <QVector>

class NxTriangleMeshShapeDesc;

class PHYSICS_EXPORT TriCollider : public Collider
{
  Q_OBJECT

private:
  NxTriangleMeshShapeDesc* m_shapeDesc;

  // Constructor
public:
  TriCollider();
  TriCollider(const QByteArray& data);

  // Destructor
public:
  ~TriCollider();

  // Methods
public:
  NxShapeDesc* shapeDesc();

  // Static Methods
public:
  //! Tworzy triangle mesha
  static QByteArray cookTriangleMesh(
    const void* vertices, unsigned vertexCount, unsigned vertexStride,
    const void* indices, unsigned indexCount, unsigned indexStride,
    bool use16indices = false);

  //! Tworzy triangle mesha z 16 bitowych indeks�w
  template<typename VertexType>
  static QByteArray cookTriangleMesh(
    const QVector<VertexType>& vertices,
    const QVector<ushort>& indices)
  {
    return cookTriangleMesh(vertices.constData(), vertices.size(), sizeof(VertexType),
                            indices.constData(), indices.size(), sizeof(ushort),
                            true);
  }

  //! Tworzy triangle mesha z 32 bitowych indeks�w
  template<typename VertexType>
  static QByteArray cookTriangleMesh(
    const QVector<VertexType>& vertices,
    const QVector<unsigned>& indices)
  {
    return cookTriangleMesh(vertices.constData(), vertices.size(), sizeof(VertexType),
                            indices.constData(), indices.size(), sizeof(unsigned),
                            false);
  }
};

#endif // TRICOLLIDER_HPP
