SOURCES = PropertyEditor/ColorCombo.cpp \
          PropertyEditor/Property.cpp \
          PropertyEditor/QPropertyEditorWidget.cpp \
          PropertyEditor/QPropertyModel.cpp \
          PropertyEditor/QVariantDelegate.cpp \
          PropertyEditor/EnumProperty.cpp \
    PropertyEditor/PropertyCategory.cpp

HEADERS=PropertyEditor/ColorCombo.h \
        PropertyEditor/Property.h \
        PropertyEditor/QPropertyEditorWidget.h \
        PropertyEditor/QPropertyModel.h \
        PropertyEditor/QVariantDelegate.h \
        PropertyEditor/EnumProperty.h \
    PropertyEditor/PropertyCategory.hpp
		
INCLUDEPATH+=PropertyEditor
