// *************************************************************************************************
//
// QPropertyEditor v 0.3
//   
// --------------------------------------
// Copyright (C) 2007 Volker Wiendl
// Acknowledgements to Roman alias banal from qt-apps.org for the Enum enhancement
//
//
// The QPropertyEditor Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation version 3 of the License 
//
// The Horde3D Scene Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// *************************************************************************************************

#include "EnumProperty.h"

EnumProperty::EnumProperty(const QString &name /* = QString()*/, 
						   QObject *propertyObject /* = 0*/, QObject *parent /* = 0*/)
  : Property(name, propertyObject, parent)
{
  const QMetaObject* meta = propertyObject->metaObject();
  QMetaProperty prop = meta->property(meta->indexOfProperty(qPrintable(name)));

  if(prop.isEnumType())
  {
    m_menum = prop.enumerator();

    for(int i=0; i < m_menum.keyCount(); i++)
    {
      m_enum << m_menum.key(i);
    }
  }
}

EnumProperty::EnumProperty(QMetaEnum menum, const QString &name, QObject *propertyObject, QObject *parent)
  : Property(name, propertyObject, parent)
{
  for(int i = 0; i < menum.keyCount(); i++)
  {
    m_enum << menum.key(i);
  }

  m_menum = menum;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// value
/////////////////////////////////////////////////////////////////////////////////////////////
QVariant EnumProperty::value(int role /* = Qt::UserRole */) const
{
  if(role == Qt::DisplayRole)
  {
    if(m_propertyObject)
    {
      int index = m_propertyObject->property(qPrintable(objectName())).toInt();
      return QVariant(m_menum.valueToKey(index));
    }
    else
    {
      return QVariant();
    }
  }
  else
  {
    return Property::value(role);
  }
}

QWidget* EnumProperty::createEditor(QWidget* parent, const QStyleOptionViewItem& option)
{
  QComboBox* editor = new QComboBox(parent);
  editor->addItems(m_enum);
  connect(editor, SIGNAL(currentIndexChanged(const QString)),
          this, SLOT(valueChanged(const QString)));
  return editor;
}

bool EnumProperty::setEditorData(QWidget *editor, const QVariant &data)
{
  if(QComboBox * combo = qobject_cast<QComboBox*>(editor))
  {
    if(m_propertyObject)
    {
      int index = combo->findText(m_menum.valueToKey(data.toInt()));

      if(index == -1)
        return false;

      combo->setCurrentIndex(index);
      return true;
    }
  }

  return false;
}

QVariant EnumProperty::editorData(QWidget *editor)
{
  QComboBox* combo = 0;
  if(combo = qobject_cast<QComboBox*>(editor))
    return QVariant(combo->currentText());
  else
    return QVariant();
}

void EnumProperty::valueChanged(const QString item)
{
  setValue(QVariant(item));
}
