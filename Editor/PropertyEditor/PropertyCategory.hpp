#ifndef PROPERTYCATEGORY_HPP
#define PROPERTYCATEGORY_HPP

#include "Property.h"

class PropertyCategory : public Property
{
  Q_OBJECT

public:
  explicit PropertyCategory(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  bool isRoot();
};

#endif // PROPERTYCATEGORY_HPP
