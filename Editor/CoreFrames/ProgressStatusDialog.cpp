#include "ProgressStatusDialog.hpp"

#include <QApplication>
#include <QDebug>

ProgressStatusDialog::ProgressStatusDialog(QWidget *parent) :
    QProgressDialog(parent)
{
  connect(&m_progressStatus, SIGNAL(valueChanged(ProgressStatus*)), SLOT(valueChanged(ProgressStatus*)));
  connect(&m_progressStatus, SIGNAL(textChanged(ProgressStatus*)), SLOT(textChanged(ProgressStatus*)));
}

ProgressStatusDialog::ProgressStatusDialog(const QString & labelText, const QString & cancelButtonText, QWidget *parent) :
    QProgressDialog(labelText, cancelButtonText, 0, 100, parent)
{
  setWindowTitle(labelText);
  connect(&m_progressStatus, SIGNAL(valueChanged(ProgressStatus*)), SLOT(valueChanged(ProgressStatus*)));
  connect(&m_progressStatus, SIGNAL(textChanged(ProgressStatus*)), SLOT(textChanged(ProgressStatus*)));
}

void ProgressStatusDialog::valueChanged(ProgressStatus *progressStatus)
{
  int newValue = progressStatus->progress() * 100;
  if(newValue != value())
  {
    setValue(newValue);
    QApplication::flush();
  }
}

void ProgressStatusDialog::textChanged(ProgressStatus *progressStatus)
{
  setLabelText(progressStatus->text());
  QApplication::flush();
}
