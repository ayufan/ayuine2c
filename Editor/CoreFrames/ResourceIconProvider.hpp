#ifndef RESOURCEICONPROVIDER_HPP
#define RESOURCEICONPROVIDER_HPP

#include <QObject>
#include <QFileIconProvider>
#include <QHash>
#include <QList>

#include "Editor.hpp"

class EDITOR_EXPORT ResourceIconProvider : public QObject, public QFileIconProvider
{
  Q_OBJECT

public:
  explicit ResourceIconProvider(QObject *parent = 0);
  ~ResourceIconProvider();

public:
  void addToPendingList(const QFileInfo& fileInfo);
  void clearPendingList();
  QString cacheFileName(const QFileInfo& info) const;

private slots:
  void run();

signals:
  void iconLoaded(const QString& filePath);

public:
  QIcon icon(const QFileInfo &info) const;
  QString type(const QFileInfo &info) const;

private:
  static QHash<QFileInfo, QIcon> iconList;
  static QHash<QFileInfo, QString> typeList;
  mutable QList<QFileInfo> fileList;
  mutable bool waiting;
};

#endif // RESOURCEICONPROVIDER_HPP
