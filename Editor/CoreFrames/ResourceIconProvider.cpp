#include "ResourceIconProvider.hpp"
#include "ResourcePreviewGenerator.hpp"

#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>
#include <Render/Texture.hpp>

#include <QIcon>
#include <QPixmap>
#include <QApplication>

inline uint qHash(const QFileInfo &info)
{
  return qHash(info.filePath()) ^ qHash(info.lastModified().toTime_t());
}

QHash<QFileInfo, QIcon> ResourceIconProvider::iconList;

QHash<QFileInfo, QString> ResourceIconProvider::typeList;

ResourceIconProvider::ResourceIconProvider(QObject *parent) :
  QObject(parent)
{
  waiting = false;
}

ResourceIconProvider::~ResourceIconProvider()
{
}

void ResourceIconProvider::addToPendingList(const QFileInfo& fileInfo)
{
  int index = fileList.indexOf(fileInfo);
  if(index >= 0)
    return;

  fileList.append(fileInfo);

  if(!waiting)
  {
    waiting = true;
    metaObject()->invokeMethod(this, "run", Qt::QueuedConnection);
  }
}

void ResourceIconProvider::clearPendingList()
{
  fileList.clear();
  waiting = false;
}

QString ResourceIconProvider::cacheFileName(const QFileInfo& info) const
{
  return QString("E:/cache/%1.png").arg(qHash(info));
}

static QIcon defaultIcon()
{
  static QPixmap newFile(":/Editor/General/page_white.png");
  static int x = (64 - newFile.width()) / 2;
  static int y = (64 - newFile.height()) / 2;
  static QIcon newFileIcon = newFile.transformed(QPixmap::trueMatrix(QTransform::fromTranslate(x, y), 64, 64));
  return newFileIcon;
}

static QIcon folderIcon()
{
  static QPixmap newFile(":/Editor/General/open.png");
  static int x = (64 - newFile.width()) / 2;
  static int y = (64 - newFile.height()) / 2;
  static QIcon newFileIcon = newFile.transformed(QPixmap::trueMatrix(QTransform::fromTranslate(x, y), 64, 64));
  return newFileIcon;
}

void ResourceIconProvider::run()
{  
  waiting = false;

  while(fileList.size() > 0)
  {
    QFileInfo info = fileList.first();
    fileList.pop_front();

    if(iconList.contains(info))
    {
      continue;
    }

    QIcon icon = defaultIcon();

    if(QFile::exists(cacheFileName(info)))
    {
      icon = QIcon(cacheFileName(info));

      if(icon.isNull())
        icon = defaultIcon();
    }
    else
    {
      QPixmap pixmap = ResourcePreviewGenerator::generate(info.filePath());

      if(!pixmap.isNull())
      {
        if(pixmap.width() < 64 && pixmap.height() < 64)
          icon = pixmap;
        else
          icon = pixmap = pixmap.scaled(64, 64, Qt::KeepAspectRatio);
      }

      if(!pixmap.isNull())
        pixmap.save(cacheFileName(info));
    }

    iconList[info] = icon;

    if(icon.data_ptr() != defaultIcon().data_ptr())
    {
      emit iconLoaded(info.filePath());
      break;
    }
  }

  if(fileList.size() != 0 && !waiting)
  {
    waiting = true;
    metaObject()->invokeMethod(this, "run", Qt::QueuedConnection);
  }
}

QIcon ResourceIconProvider::icon(const QFileInfo &info) const
{
  if(info.isDir())
    return folderIcon(); // QFileIconProvider::icon(info);

  if(iconList.contains(info))
  {
    QIcon icon = iconList[info];
    if(icon.isNull())
      return defaultIcon();
    return iconList[info];
  }

  const_cast<ResourceIconProvider*>(this)->addToPendingList(info);

#if 1
  return defaultIcon();
#else
  return QFileIconProvider::icon(info);
#endif
}

QString ResourceIconProvider::type(const QFileInfo &info) const
{
  if(info.isDir())
    return QFileIconProvider::type(info);

  if(typeList.contains(info))
    return typeList[info];

  const QMetaObject* metaObject = ResourceImporter::metaObjectFromFile(info.filePath());

  if(metaObject)
    typeList[info] = metaObject->className();
  else
    typeList[info] = "Undefined";

  return typeList[info];
}
