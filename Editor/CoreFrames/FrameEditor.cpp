#include "FrameEditor.hpp"
#include "ui_FrameEditor.h"

#include "CameraController/CameraController.hpp"

#include <Render/RenderComponentWidget.hpp>
#include <Render/RenderSystem.hpp>

#include <Pipeline/Frame.hpp>
#include <Pipeline/SolidFrame.hpp>
#include <Pipeline/WireFrame.hpp>
#include <Pipeline/DepthFrame.hpp>
#include <Pipeline/LitShadedFrame.hpp>
#include <Pipeline/ForwardShadedFrame.hpp>
#include <Pipeline/ColorPickFrame.hpp>
#include <Pipeline/DeferredShadedFrame.hpp>
#include <Pipeline/BloomComponent.hpp>
#include <Pipeline/HdrComponent.hpp>

FrameEditor::FrameEditor(QWidget *parent) :
    ResourceEditor(parent), ui(new Ui::FrameEditor())
{
  ui->setupUi(this);

  if(QActionGroup* lightingGroup = new QActionGroup(this))
  {
    lightingGroup->addAction(ui->actionWire);
    lightingGroup->addAction(ui->actionSolid);
    lightingGroup->addAction(ui->actionShaded);
    lightingGroup->addAction(ui->actionLit);
    lightingGroup->addAction(ui->actionDeferred);
  }

  on_actionSolid_triggered();
}

FrameEditor::~FrameEditor()
{
}

void FrameEditor::prepareComponents()
{
  getRenderSystem().resetStats();

  if(m_cameraController && m_frame)
    m_cameraController->updateFrame(*m_frame);

  if(m_frame)
    m_frame->setDeltaTime(ui->renderComponents->drawTime());
}

void FrameEditor::finalizeComponents()
{
}

void FrameEditor::updateUi()
{
  ResourceEditor::updateUi();
  ui->renderComponents->update();
}

RenderComponentWidget *FrameEditor::renderComponents()
{
  return ui->renderComponents;
}

void FrameEditor::on_actionWire_triggered()
{
  m_frame.reset(new WireFrame());
  ui->renderComponents->addComponent(m_frame.data());
  ui->renderComponents->update();
  ui->actionWire->setChecked(true);
}

void FrameEditor::on_actionSolid_triggered()
{
  m_frame.reset(new SolidFrame());
  ui->renderComponents->addComponent(m_frame.data());
  ui->renderComponents->update();
  ui->actionSolid->setChecked(true);
}

void FrameEditor::on_actionShaded_triggered()
{
  m_frame.reset(new ForwardShadedFrame());
  ui->renderComponents->addComponent(m_frame.data());
  ui->renderComponents->update();
  ui->actionShaded->setChecked(true);
}

void FrameEditor::on_actionLit_triggered()
{
  m_frame.reset(new LitShadedFrame());
  ui->renderComponents->addComponent(m_frame.data());
  ui->renderComponents->update();
  ui->actionLit->setChecked(true);
}

void FrameEditor::on_actionDeferred_triggered()
{
  m_frame.reset(new DeferredShadedFrame());
  ui->renderComponents->addComponent(m_frame.data());
  ui->renderComponents->update();
  ui->actionDeferred->setChecked(true);
}
