#include "ResourcePreviewGenerator.hpp"

#include <Core/ResourceImporter.hpp>

ResourcePreviewGenerator::ResourcePreviewGenerator(QObject *parent) :
    QObject(parent)
{
  previewGenerators().append(this);
}

ResourcePreviewGenerator::~ResourcePreviewGenerator()
{
  previewGenerators().removeOne(this);
}

QList<ResourcePreviewGenerator *> & ResourcePreviewGenerator::previewGenerators()
{
  static QList<ResourcePreviewGenerator *> previewGenerators;
  return previewGenerators;
}

QPixmap ResourcePreviewGenerator::generate(const QString &fileName)
{
  const QMetaObject* metaObject = ResourceImporter::metaObjectFromFile(fileName);
  if(!metaObject)
    return QPixmap();

  foreach(ResourcePreviewGenerator* previewGenerator, previewGenerators())
  {
    QPixmap pixmap;
    if(previewGenerator->generate(fileName, metaObject, pixmap))
      return pixmap;
  }

  if(const char* iconFileName = metaObject->classInfo(metaObject->indexOfClassInfo("IconFileName")).value())
  {
    return QPixmap(iconFileName);
  }

  return QPixmap();
}
