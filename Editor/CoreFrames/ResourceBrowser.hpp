#ifndef RESOURCEBROWSER_HPP
#define RESOURCEBROWSER_HPP

#include "Editor.hpp"

#include <QMainWindow>

namespace Ui {
class ResourceBrowser;
}

class QFileSystemModel;
class QModelIndex;
class ResourceIconProvider;

class FileFilterModel;
class DirFilterModel;

class EDITOR_EXPORT ResourceBrowser : public QMainWindow
{
  Q_OBJECT

public:
  static ResourceBrowser *resourceBrowser;

public:
  explicit ResourceBrowser(QWidget *parent = 0);
  ~ResourceBrowser();

public slots:
  void updateUi();

private slots:
  void iconLoaded(const QString& filePath);
  void on_dirList_doubleClicked(const QModelIndex &index);
  void on_fileList_doubleClicked(const QModelIndex &index);
  void on_dirList_clicked(const QModelIndex &index);
  void on_actionNew_triggered();
  void on_actionOpen_triggered();
  void on_actionDelete_triggered();
  void on_actionRename_triggered();
  void on_actionListView_toggled(bool arg1);
  void on_actionNewFolder_triggered();
  void on_actionDeleteFolder_triggered();
  void on_actionFolderRename_triggered();
  void on_fileList_clicked(const QModelIndex &index);
  void on_actionShowExplorer_triggered();
  void on_actionSelectFolder_triggered();

private:
  void setPath(const QString& path);

private:
  Ui::ResourceBrowser* ui;
  ResourceIconProvider* iconProvider;
  QFileSystemModel* fsModel;
  FileFilterModel* fileFilterModel;
  DirFilterModel* dirFilterModel;
};

#endif // RESOURCEBROWSER_HPP
