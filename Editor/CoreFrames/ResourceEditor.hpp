#ifndef RESOURCEEDITOR_HPP
#define RESOURCEEDITOR_HPP

#include <QMainWindow>

#include "Editor.hpp"

class Resource;

namespace Ui {
class ResourceEditor;
}

class EDITOR_EXPORT ResourceEditor : public QMainWindow
{
  Q_OBJECT

public:
  static QHash<const QMetaObject*, const QMetaObject*>& resourceEditors();
  static bool registerEditor(const QMetaObject* resourceMetaObject, const QMetaObject* editorMetaObject);
  static void unregisterEditor(const QMetaObject* editorMetaObject);
  static const QMetaObject* resourceEditor(const QMetaObject* resourceMetaObject);
  static ResourceEditor* openEditor(const QSharedPointer<Resource>& resource);
  static ResourceEditor* openEditor(const QString& fileName);
  static ResourceEditor* findEditor(const QString& fileName);
  static ResourceEditor* findOrOpenEditor(const QString& fileName);

public:
  explicit ResourceEditor(QWidget *parent = 0);
  ~ResourceEditor();

protected:
  void closeEvent(QCloseEvent *);
  void showEvent(QShowEvent *);

public:
  virtual const QMetaObject* resourceType() const = 0;
  virtual QSharedPointer<Resource> resource() const = 0;
  virtual bool setResource(const QSharedPointer<Resource> &newResource) = 0;
  static ResourceEditor* openFile(QWidget* parent);

public slots:
  static void newFile();
  virtual void openFile();
  virtual void saveFile();
  virtual void saveAsFile();
  virtual void updateUi();
  void setPropertyObject(QObject* object);

private:
  Ui::ResourceEditor* ui;
};

#define Q_REGISTER_RESOURCE_EDITOR(ResourceType, EditorType)  \
  static int registerEditor_##ResourceType##_##EditorType() { return ResourceEditor::registerEditor(&ResourceType::staticMetaObject, &EditorType::staticMetaObject); } \
  static int unregisterEditor_##ResourceType##_##EditorType() { ResourceEditor::unregisterEditor(&EditorType::staticMetaObject); return 0; } \
  Q_CONSTRUCTOR_FUNCTION(registerEditor_##ResourceType##_##EditorType) \
  Q_DESTRUCTOR_FUNCTION(unregisterEditor_##ResourceType##_##EditorType) \

#endif // RESOURCEEDITOR_HPP
