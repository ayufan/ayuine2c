#include "ObjectProperty.hpp"

#include <QMenu>
#include <QToolButton>
#include <QMetaProperty>
#include <QMimeData>

ObjectProperty::ObjectProperty(const QMetaObject* metaObject, const QMetaObject* objectType, int userType, const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(metaObject, name, propertyObject, parent)
{
  m_userType = userType;
  m_objectType = objectType;
  m_childrenPopulated = false;
}

ObjectProperty::ObjectProperty(const QMetaObject* objectType, int userType, const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&ObjectProperty::staticMetaObject, name, propertyObject, parent)
{
  m_userType = userType;
  m_objectType = objectType;
  m_childrenPopulated = false;
}

ObjectProperty::~ObjectProperty()
{
}

QSharedPointer<QObject> ObjectProperty::object() const
{
  return object(value());
}

QSharedPointer<QObject> ObjectProperty::object(const QVariant& value) const
{
  if(value.userType() == m_userType)
  {
    return *(QSharedPointer<QObject>*)value.constData();
  }
  return QSharedPointer<QObject>();
}

void ObjectProperty::setObject(const QSharedPointer<QObject> &object)
{
  Property::setValue(QVariant(m_userType, &object));
  update();
}

QStringList ObjectProperty::valueList() const
{
  QStringList strings;
  if(QSharedPointer<QObject> ptr = object())
  {
    if(ptr->objectName().size())
      strings.append(ptr->objectName());
    strings.append(ptr->metaObject()->className());
  }
  else
  {
    strings.append(QMetaType::typeName(m_userType));
    strings.append("(null)");
  }
  return strings;
}

QWidget * ObjectProperty::createEditor(QWidget *parent, const QStyleOptionViewItem &option)
{
  if(QToolButton* button = new QToolButton(parent))
  {
    if(!m_menu)
    {
      m_menu.reset(createMenu());
    }
    button->setAutoRaise(true);
    button->setPopupMode(QToolButton::MenuButtonPopup);
    button->setMenu(m_menu.data());
    return button;
  }
  return NULL;
}

bool ObjectProperty::setEditorData(QWidget *editor, const QVariant &data)
{
  QToolButton* button = (QToolButton*)editor;

  if(QSharedPointer<QObject> ptr = object(data))
  {
    button->setText(ptr->objectName());
  }
  return false;
}

void ObjectProperty::unlink()
{
  setObject(QSharedPointer<QObject>());
}

void ObjectProperty::setValue(const QVariant &value)
{
}

void ObjectProperty::update()
{
  QSharedPointer<QObject> ptr = object();

  foreach(Property* property, m_properties)
  {
    property->setPropertyObject(ptr.data());
  }
}

bool ObjectProperty::hasChildren() const
{
  return true;
}

void ObjectProperty::populateChildren()
{
  if(m_childrenPopulated)
    return;

  m_childrenPopulated = true;

  if(!m_objectType)
    return;

  QSharedPointer<QObject> ptr = object();

  for(int i = 0; i < m_objectType->propertyCount(); ++i)
  {
    QMetaProperty objectProperty = m_objectType->property(i);

    if(metaObject()->indexOfProperty(objectProperty.name()) >= 0)
      continue;

    if(!objectProperty.isDesignable(ptr.data()))
      continue;

    Property* newProperty = createPropertyType(objectProperty, ptr.data(), this);

    if(newProperty)
    {
      m_properties.append(newProperty);
    }
  }
}

QMenu * ObjectProperty::createMenu() const
{
  QMenu* menu = new QMenu;
  menu->addAction(QIcon(":/Editor/General/delete.png"), "Unlink", this, SLOT(unlink()));
  return menu;
}

bool ObjectProperty::mimeData(QMimeData *mimeData)
{
  QSharedPointer<QObject> ptr = object();

  if(!ptr)
    return false;

  if(mimeData->hasFormat("ObjectRef"))
    return false;

  QByteArray encodedData;
  QDataStream ds(&encodedData, QIODevice::WriteOnly);
  ds << reinterpret_cast<qint64>(ptr.data());
  mimeData->setData("ObjectRef", encodedData);
  return true;
}

bool ObjectProperty::dropMimeData(const QMimeData *mimeData, Qt::DropAction dropAction)
{
  if(!mimeData->hasFormat("ObjectRef"))
    return false;

  qint64 index = 0;
  QDataStream ds(mimeData->data("ObjectRef"));
  ds >> index;
  if(!index)
    return false;

  QObject* object = (QObject*)index;

  for(const QMetaObject* metaObject = object->metaObject(); metaObject; metaObject = metaObject->superClass())
  {
    if(metaObject == m_objectType)
    {
      setObject(QWeakPointer<QObject>(object).toStrongRef());
      return true;
    }
  }

  return false;
}

bool ObjectProperty::supportsDrop() const
{
  return true;
}

bool ObjectProperty::supportsDrag() const
{
  return !object().isNull();
}
