#include "ResourceProperty.hpp"
#include "CoreFrames/ResourceEditor.hpp"
#include "CoreFrames/ProgressStatusDialog.hpp"

#include <Core/Resource.hpp>
#include <Core/ResourceImporter.hpp>

#include <QMenu>
#include <QFileDialog>

ResourceProperty::ResourceProperty(const QMetaObject* resourceType, int userType, const QString& name, QObject* propertyObject, QObject* parent)
    : ObjectProperty(&ResourceProperty::staticMetaObject, resourceType, userType, name, propertyObject, parent)
{
}

QString ResourceProperty::resourceName() const
{
  if(QSharedPointer<Resource> ptr = resource())
  {
    return ptr->objectName();
  }
  return QString();
}

void ResourceProperty::setResourceName(const QString &name)
{
  if(QSharedPointer<Resource> ptr = resource())
  {
    ptr->setObjectName(name);
  }
}

bool ResourceProperty::isLocal() const
{
  if(QSharedPointer<Resource> ptr = resource())
  {
    return ptr->isLocal();
  }
  return false;
}

void ResourceProperty::setLocal(bool isLocal)
{
  if(QSharedPointer<Resource> ptr = resource())
  {
    ptr->setLocal(isLocal);
  }
}

QString ResourceProperty::fileName() const
{
  if(QSharedPointer<Resource> ptr = resource())
  {
    return ptr->fileName();
  }
  return QString();
}

QSharedPointer<Resource> ResourceProperty::resource() const
{
  return resource(value());
}

QSharedPointer<Resource> ResourceProperty::resource(const QVariant& value) const
{
  if(value.userType() == m_userType)
  {
    return *(QSharedPointer<Resource>*)value.constData();
  }
  return QSharedPointer<Resource>();
}

void ResourceProperty::setResource(const QSharedPointer<Resource> &resource)
{
  Property::setValue(QVariant(m_userType, &resource));
  update();
}

void ResourceProperty::pick()
{
  QString openFile = QFileDialog::getOpenFileName(NULL, "Open File", Resource::getResourceDir().absolutePath(), ResourceImporter::importFilters(m_objectType));
  if(openFile.isEmpty())
    return;

  ProgressStatusDialog progressDialog("Loading...", QString(), NULL);
  progressDialog.show();

  QSharedPointer<Resource> newResource = Resource::load(openFile);

  if(newResource)
  {
    setResource(newResource);
  }
}

void ResourceProperty::edit()
{
  ResourceEditor::openEditor(resource());
}

void ResourceProperty::unlink()
{
  setResource(QSharedPointer<Resource>());
}

void ResourceProperty::setValue(const QVariant &value)
{
}

QMenu * ResourceProperty::createMenu() const
{
  QMenu* menu = new QMenu;
  menu->addAction(QIcon(":/Editor/General/open.png"), "Pick", this, SLOT(pick()));
  menu->addAction(QIcon(":/Editor/General/edit.png"), "Edit", this, SLOT(edit()));
  menu->addAction(QIcon(":/Editor/General/delete.png"), "Unlink", this, SLOT(unlink()));
  return menu;
}

