HEADERS += \
    CoreTypes/Vec2Property.hpp \
    CoreTypes/EditableProperty.hpp \
    CoreTypes/Vec3Property.hpp \
    CoreTypes/Vec4Property.hpp \
    CoreTypes/BoxProperty.hpp \
    CoreTypes/SphereProperty.hpp \
    CoreTypes/EulerProperty.hpp \
    CoreTypes/ResourceProperty.hpp \
    CoreTypes/MaterialShaderValuesProperty.hpp \
    CoreTypes/ObjectProperty.hpp

SOURCES += \
    CoreTypes/Vec2Property.cpp \
    CoreTypes/EditableProperty.cpp \
    CoreTypes/Vec3Property.cpp \
    CoreTypes/Vec4Property.cpp \
    CoreTypes/BoxProperty.cpp \
    CoreTypes/SphereProperty.cpp \
    CoreTypes/EulerProperty.cpp \
    CoreTypes/ResourceProperty.cpp \
    CoreTypes/MaterialShaderValuesProperty.cpp \
    CoreTypes/ObjectProperty.cpp
