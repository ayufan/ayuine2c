#ifndef VEC2PROPERTY_HPP
#define VEC2PROPERTY_HPP

#include "EditableProperty.hpp"

class Vec2Property : public EditableProperty
{
  Q_OBJECT
  Q_PROPERTY(float x READ x WRITE setX DESIGNABLE true)
  Q_PROPERTY(float y READ y WRITE setY DESIGNABLE true)

public:
  Q_INVOKABLE explicit Vec2Property(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  float x() const;
  void setX(float x);
  float y() const;
  void setY(float y);
};

#endif // VEC2PROPERTY_HPP
