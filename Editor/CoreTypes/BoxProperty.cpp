#include "BoxProperty.hpp"

#include <Math/Box.hpp>

Q_IMPLEMENT_PROPERTYTYPE(box, BoxProperty)

BoxProperty::BoxProperty(const QString& name, QObject* propertyObject, QObject* parent)
    : EditableProperty(&BoxProperty::staticMetaObject, name, propertyObject, parent)
{
}

vec3 BoxProperty::mins() const
{
  return value().value<box>().Mins;
}

void BoxProperty::setMins(vec3 mins)
{
  setValue(QVariant::fromValue(box(mins, maxs())));
}

vec3 BoxProperty::maxs() const
{
  return value().value<box>().Maxs;
}

void BoxProperty::setMaxs(vec3 maxs)
{
  setValue(QVariant::fromValue(box(mins(), maxs)));
}
