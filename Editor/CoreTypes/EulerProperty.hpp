#ifndef EULERPROPERTY_HPP
#define EULERPROPERTY_HPP

#include "EditableProperty.hpp"

class EulerProperty : public EditableProperty
{
  Q_OBJECT
  Q_PROPERTY(float yaw READ yaw WRITE setYaw DESIGNABLE true)
  Q_PROPERTY(float pitch READ pitch WRITE setPitch DESIGNABLE true)
  Q_PROPERTY(float roll READ roll WRITE setRoll DESIGNABLE true)

public:
  Q_INVOKABLE explicit EulerProperty(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  float yaw() const;
  void setYaw(float yaw);
  float pitch() const;
  void setPitch(float pitch);
  float roll() const;
  void setRoll(float roll);
};

#endif // EULERPROPERTY_HPP
