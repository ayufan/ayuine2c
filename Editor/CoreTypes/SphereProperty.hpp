#ifndef SPHEREPROPERTY_HPP
#define SPHEREPROPERTY_HPP

#include "EditableProperty.hpp"

#include <Math/Vec3.hpp>

class SphereProperty : public EditableProperty
{
  Q_OBJECT
  Q_PROPERTY(vec3 center READ center WRITE setCenter DESIGNABLE true)
  Q_PROPERTY(float radius READ radius WRITE setRadius DESIGNABLE true)

public:
  Q_INVOKABLE explicit SphereProperty(const QString& name = QString(), QObject* propertyObject = 0, QObject* parent = 0);

public:
  vec3 center() const;
  void setCenter(vec3 center);
  float radius() const;
  void setRadius(float radius);
};

#endif // SPHEREPROPERTY_HPP
