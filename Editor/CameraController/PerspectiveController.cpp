#include "PerspectiveController.hpp"
#include "CameraController.hpp"

#include <Core/Logger.hpp>
#include <Math/Rect.hpp>
#include <Math/Frustum.hpp>

PerspectiveController::PerspectiveController(QWidget *parent) :
  CameraController(parent)
{
  m_fovy = 65.0f;
  m_angularVelocity = Pi / 2.0f;
  m_linearVelocity = 4.0f;
}

QString PerspectiveController::section() const
{
  return "perspectiveCamera";
}

const euler & PerspectiveController::angles() const
{
  return m_angles;
}

void PerspectiveController::setAngles(const euler &angles)
{
  m_angles = angles;
}

float PerspectiveController::fovy() const
{
  return m_fovy;
}

void PerspectiveController::setFovy(float fovy)
{
  m_fovy = fovy;
}

vec3 PerspectiveController::screenToWorld(QPoint point)
{
  warningf("PerspectiveController::screenToWorld not supported!");
  return vec3();
}

QPoint PerspectiveController::worldToScreen(const vec3 &point)
{
  warningf("PerspectiveController::screenToWorld not supported!");
  return QPoint();
}

ray PerspectiveController::direction(QPoint point)
{
  QRect viewport;
  Camera camera;
  if(!updateCamera(viewport, camera))
    return ray();

  // Wyznacz po�o�enie promienia
  float s = (float)point.x() / viewport.width();
  float t = (float)point.y() / viewport.height();

  // Pobierz rozmiar frustuma
  rect bounds = camera.projection.frustumBounds();

  // Oblicz promie� w przestrzenii ekranu
  vec3 dir(bounds.at(s, t), 1.0f);

  // Oblicz promie� w przestrzenii �wiata
  dir = dir.X * camera.right + dir.Y * camera.up + dir.Z * camera.at;

  // Oblicz promie� w przestrzenii swiata
  return ray(camera.origin, dir.normalize());
}

float PerspectiveController::angularVelocity() const
{
  return m_angularVelocity;
}

void PerspectiveController::setAngularVelocity(float angularVelocity)
{
  m_angularVelocity = angularVelocity;
}

float PerspectiveController::linearVelocity() const
{
  return m_linearVelocity;
}

void PerspectiveController::setLinearVelocity(float linearVelocity)
{
  m_linearVelocity = linearVelocity;
}

QStringList PerspectiveController::toStringList() const
{
  QStringList lines = CameraController::toStringList();
  lines.append(QString("Angles: (%1, %2, %3)").arg(QString::number(angles().Yaw * Rad2Deg), QString::number(angles().Pitch * Rad2Deg), QString::number(angles().Roll * Rad2Deg)));
  return lines;
}
