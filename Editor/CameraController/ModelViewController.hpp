#ifndef MODELVIEWCONTROLLER_HPP
#define MODELVIEWCONTROLLER_HPP

#include "PerspectiveController.hpp"

class EDITOR_EXPORT ModelViewController : public PerspectiveController
{
  Q_OBJECT

public:
  explicit ModelViewController(QWidget *parent = 0);

protected:
  QString section() const;

protected:
  bool mousePress(QWidget* widget, QMouseEvent& event);
  bool mouseRelease(QWidget* widget, QMouseEvent& event);
  bool mouseMove(QWidget* widget, QMouseEvent& event);
  bool mouseLost(QWidget* widget, QMouseEvent& event);

public:
  bool updateCamera(QRect& viewport, Camera& camera);  
  void modelOnSphere(const sphere &center);

private:
  enum State
  {
      Idle,
      Zoom = 15000,
      Rotate,
      MoveForward,
      MoveUp
  }
  m_state;
};

#endif // MODELVIEWCONTROLLER_HPP
