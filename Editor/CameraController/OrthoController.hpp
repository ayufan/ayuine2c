#ifndef ORTHOCONTROLLER_HPP
#define ORTHOCONTROLLER_HPP

#include <Math/mathlib.hpp>

#include "CameraController.hpp"

class EDITOR_EXPORT OrthoController : public CameraController
{
  Q_OBJECT

public:
  explicit OrthoController(QWidget *parent = 0);

public:
  Axis::Enum axis() const;
  void setAxis(Axis::Enum axis);
  vec3 screenToWorld(QPoint point);
  QPoint worldToScreen(const vec3& point);
  ray direction(QPoint point);
  bool updateCamera(QRect& viewport, Camera& camera);

  float linearKeyVelocity() const;
  void setLinearKeyVelocity(float linearKeyVelocity);

  float zoomFactor() const;
  void setZoomFactor(float zoomFactor);

protected:
  bool mousePress(QWidget* widget, QMouseEvent& event);
  bool mouseRelease(QWidget* widget, QMouseEvent& event);
  bool mouseMove(QWidget* widget, QMouseEvent& event);
  bool mouseLost(QWidget* widget, QMouseEvent& event);
  bool keyPress(QWidget* widget, QKeyEvent& event);

protected:
  Axis::Enum m_axis;
  float m_linearKeyVelocity;
  float m_zoomFactor;
};

#endif // ORTHOCONTROLLER_HPP
