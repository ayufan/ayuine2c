#include "ModelViewController.hpp"
#include <QWidget>
#include <QMouseEvent>

ModelViewController::ModelViewController(QWidget *parent) :
  PerspectiveController(parent)
{
}

QString ModelViewController::section() const
{
  return "modelViewController";
}

bool ModelViewController::updateCamera(QRect &viewport, Camera &camera)
{
  if(QWidget* widget = qobject_cast<QWidget*>(parent()))
  {
    viewport = widget->geometry();

    // Wyznacz wsp�czynnik ekranu
    float aspect = (float)viewport.width() / viewport.height();

    // Wyznacz ustawienia kamery
    Camera::modelViewCamera(camera, m_origin + m_offset, m_angles, m_zoom, Deg2Rad * m_fovy, aspect);
    return true;
  }
  return false;
}

void ModelViewController::modelOnSphere(const sphere &center)
{
    setOrigin(center.Center + vec3(center.Radius * 0.5f,0,0));
    setZoom(1.0f);
}

bool ModelViewController::mousePress(QWidget *widget, QMouseEvent &event)
{
  if(event.button() == Qt::RightButton)
  {
#if 1
    widget->grabMouse();
    widget->setFocus();
    m_mouse = event.globalPos();
#else
    if(event.button() == Qt::LeftButton)
      m_state = Zoom;
    else if(event.modifiers() & Qt::ControlModifier)
      m_state = event.modifiers() & Qt::ShiftModifier ? MoveForward : MoveUp;
    else
      m_state = Rotate;
#endif
    return true;
  }

  return false;
}

bool ModelViewController::mouseRelease(QWidget *widget, QMouseEvent &event)
{
  if(QWidget::mouseGrabber() != widget)
    return false;
  if(event.buttons() & Qt::RightButton)
    return false;
  widget->releaseMouse();
  m_state = Idle;
  return true;
}

bool ModelViewController::mouseMove(QWidget *widget, QMouseEvent &event)
{
  // if(m_state == Idle)
  //  return false;
  if(QWidget::mouseGrabber() != widget)
    return false;
  if(~event.buttons() & Qt::RightButton)
    return false;

  QPoint delta = event.globalPos() - m_mouse;
  QCursor::setPos(m_mouse);

  float s = - (float)delta.x() / widget->width();
  float t = - (float)delta.y() / widget->height();

#if 1
  if(event.buttons() & Qt::LeftButton)
  {
    m_zoom = qBound<float>(m_zoom + 10.0f * t, m_minZoom, m_maxZoom);
  }
  else if(event.modifiers() & Qt::ControlModifier)
  {
    if(event.modifiers() & Qt::ShiftModifier)
    {
      m_origin += m_angles.right() * linearVelocity() * s;
      m_origin -= m_angles.at() * linearVelocity() * t;
    }
    else
    {
      m_origin += m_angles.right() * linearVelocity() * s;
      m_origin += m_angles.up() * linearVelocity() * t;
    }
  }
  else
  {
    m_angles.Yaw -= angularVelocity() * s;
    m_angles.Pitch += angularVelocity() * t;
  }
#else
  switch(m_state)
  {
  case Zoom:
    m_zoom = qBound<float>(m_zoom + 10.0f * t, m_minZoom, m_maxZoom);
    break;

  case Rotate:
    m_angles.Yaw += angularVelocity() * s;
    m_angles.Pitch += angularVelocity() * t;
    break;

  case MoveUp:
    m_origin += m_angles.right() * linearVelocity() * s;
    m_origin += m_angles.up() * linearVelocity() * t;
    break;

  case MoveForward:
    m_origin += m_angles.right() * linearVelocity() * s;
    m_origin += m_angles.at() * linearVelocity() * t;
    break;

  default:
    return false;
  }
#endif

  widget->update();
  return true;
}

bool ModelViewController::mouseLost(QWidget *widget, QMouseEvent &event)
{
  widget->releaseMouse();
  m_state = Idle;
  return true;
}
