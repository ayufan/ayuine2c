HEADERS += \
    CoreEditor/ResourceEditor.hpp \
    CoreEditor/FrameEditor.hpp \
    CoreEditor/MetaObjectItemModel.hpp \
    CoreEditor/ProgressStatusDialog.hpp \
    CoreEditor/ResourceBrowser.hpp \
    CoreEditor/ResourceIconProvider.hpp \
    CoreEditor/ResourcePreviewGenerator.hpp

SOURCES += \
    CoreEditor/ResourceEditor.cpp \
    CoreEditor/FrameEditor.cpp \
    CoreEditor/MetaObjectItemModel.cpp \
    CoreEditor/ProgressStatusDialog.cpp \
    CoreEditor/ResourceBrowser.cpp \
    CoreEditor/ResourceIconProvider.cpp \
    CoreEditor/ResourcePreviewGenerator.cpp

FORMS += \
    CoreEditor/ResourceEditor.ui \
    CoreEditor/FrameEditor.ui \
    CoreEditor/ResourceBrowser.ui

RESOURCES += \
    CoreEditor/Images.qrc
