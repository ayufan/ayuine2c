TARGET = Launcher
TEMPLATE = app
CONFIG -= precompile_header

include(../ayuine2c.pri)

SOURCES += Launcher.cpp

INCLUDEPATH += ../Editor

LIBS += -lCore -lEditor -lRender -lGameSystem -lSceneGraph -lMaterialSystem
