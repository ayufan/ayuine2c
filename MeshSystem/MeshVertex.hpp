#ifndef MESHVERTEX_HPP
#define MESHVERTEX_HPP

#include "MeshSystem.hpp"

#include <QDataStream>

#include <Math/Vec2.hpp>
#include <Math/Vec3.hpp>
#include <Math/Vec4.hpp>
#include <Render/VertexBuffer.hpp>
#include <Core/Serializer.hpp>

class MESHSYSTEM_EXPORT MeshVertex
{
public:
  static const VertexBuffer::Types vertexType;

public:
  vec3 origin;
  vec2 coords;
  vec2 coords2;
  vec3 normal;
  vec4 tangent;

public:
  MeshVertex();

public:
  bool operator < (const MeshVertex &mv) const;
  bool operator == (const MeshVertex &mv) const;

#if 0
  friend QDataStream& operator << (QDataStream& ds, const MeshVertex& vertex)
  {
    return ds << vertex.origin << vertex.coords << vertex.coords2 << vertex.normal << vertex.tangent;
  }
#endif
};

inline void swapOrder(MeshVertex& value)
{
  swapOrder(value.origin);
  swapOrder(value.coords);
  swapOrder(value.coords2);
  swapOrder(value.normal);
  swapOrder(value.tangent);
}

Q_DECLARE_RAW_SERIALIZER(MeshVertex)
Q_DECLARE_RAW_VECTOR_SERIALIZER(MeshVertex)

typedef QVector<MeshVertex> MeshVertexList;

Q_DECLARE_METATYPE(MeshVertex)
Q_DECLARE_METATYPE(MeshVertexList)

typedef QVector<unsigned> MeshIndicesList;

Q_DECLARE_RAW_VECTOR_SERIALIZER(unsigned)
Q_DECLARE_METATYPE(MeshIndicesList)

#endif // MESHVERTEX_HPP
