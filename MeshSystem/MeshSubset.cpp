#include "MeshSubset.hpp"
#include "MeshSubsetFragment_p.hpp"
#include "Mesh.hpp"

#include <Pipeline/Frame.hpp>
#include <Pipeline/ColorPickFrame.hpp>
#include <Render/Texture.hpp>

Q_IMPLEMENT_OBJECTREF(MeshSubset)

MeshSubset::MeshSubset()
{
  for(int i = 0; i < MaterialSystem::QualityCount; ++i)
    m_meshSubsetFragment[i] = NULL;
}

MeshSubset::~MeshSubset()
{
  for(int i = 0; i < MaterialSystem::QualityCount; ++i)
    if(m_meshSubsetFragment[i])
      m_meshSubsetFragment[i]->m_meshSubset = NULL;
}

const IndexedPrimitiveDrawer & MeshSubset::drawer() const
{
  return m_drawer;
}

void MeshSubset::setDrawer(const IndexedPrimitiveDrawer &drawer)
{
  m_drawer = drawer;
}

const QSharedPointer<Material> &MeshSubset::material() const
{
  return m_material;
}

void MeshSubset::setMaterial(const QSharedPointer<Material> &material)
{
  m_material = material;
}

const box & MeshSubset::boxBounds() const
{
  return m_boxBounds;
}

void MeshSubset::setBoxBounds(const box &bounds)
{
  m_boxBounds = bounds;
}

const sphere & MeshSubset::sphereBounds() const
{
  return m_sphereBounds;
}

void MeshSubset::setSphereBounds(const sphere &bounds)
{
  m_sphereBounds = bounds;
}

QSharedPointer<Mesh> MeshSubset::mesh() const
{
  return m_mesh.toStrongRef();
}

void MeshSubset::generateFrame(Frame &frame, const matrix &objectTransform)
{
  if(!material())
    return;

  MaterialSystem::Quality quality = MaterialSystem::qualityForSphere(frame.camera().origin, sphereBounds().transform(objectTransform));

  if(m_meshSubsetFragment[quality] && m_meshSubsetFragment[quality]->frame() == &frame)
  {
    m_meshSubsetFragment[quality]->addInstance(objectTransform);
    return;
  }

  QScopedPointer<MeshSubsetFragment> meshFragment(new MeshSubsetFragment(this));
  meshFragment->setBoxBounds(boxBounds());
  meshFragment->setSphereBounds(sphereBounds());
  meshFragment->addInstance(objectTransform);
  meshFragment->setMaterialQuality(quality);

  if(material()->isOpaque() && !m_meshSubsetFragment[quality])
  {
    m_meshSubsetFragment[quality] = meshFragment.data();
    m_meshSubsetFragment[quality]->m_meshSubset = this;
  }

  frame.addFragment(meshFragment.take());
}

void MeshSubset::generateDebugFrame(Frame &frame, const matrix &objectTransform, QObject *owner)
{
  if(frame.isSelected(owner))
  {
    if(!frame.isWireframe())
    {
      generateFrame(frame, objectTransform);

      QScopedPointer<SelectedMeshSubsetFragment> outlineFragment(new SelectedMeshSubsetFragment(this));
      outlineFragment->setTransform(objectTransform);
      outlineFragment->setColor(QColor::fromRgbF(0.35f, 0.45f, 0.23f, 0.20f));
      outlineFragment->setFixedState(RenderSystem::FixedConst);
      outlineFragment->setFillMode(RenderSystem::Solid);
      outlineFragment->setDepthStencilState(RenderSystem::DepthRead);
      outlineFragment->setBlendState(RenderSystem::Additive);
      outlineFragment->setOrder(999);
      frame.addFragment(outlineFragment.take());
    }

    QScopedPointer<SelectedMeshSubsetFragment> wireFragment(new SelectedMeshSubsetFragment(this));
    wireFragment->setTransform(objectTransform);
    wireFragment->setFixedState(RenderSystem::FixedConst);
    wireFragment->setColor(QColor::fromRgbF(0.55f, 0.66f, 0.33f));
    wireFragment->setFillMode(RenderSystem::Wire);
    wireFragment->setOrder(1000);
    frame.addFragment(wireFragment.take());
  }
  else
  {
    generateFrame(frame, objectTransform);
  }
}

void MeshSubset::generateColorPickFrame(ColorPickFrame &frame, const matrix &objectTransform, QObject *owner)
{
  QScopedPointer<MeshSubsetFragment> meshFragment(new MeshSubsetFragment(this));
  meshFragment->setBoxBounds(boxBounds());
  meshFragment->setSphereBounds(sphereBounds());
  meshFragment->addInstance(objectTransform, frame.objectToColor(owner));
  frame.addFragment(meshFragment.take());
}

void MeshSubset::setObjectName(const QString &name)
{
  if(objectName() != name)
  {
    QString oldName(objectName());
    QObject::setObjectName(name);
    emit objectNameChanged(this, oldName);
  }
}
