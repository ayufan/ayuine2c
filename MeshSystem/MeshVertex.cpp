#include "MeshVertex.hpp"

Q_IMPLEMENT_METATYPE(MeshVertex)
Q_IMPLEMENT_METATYPE(MeshVertexList)

const VertexBuffer::Types MeshVertex::vertexType = VertexBuffer::Types(VertexBuffer::Vertex | VertexBuffer::Coords | VertexBuffer::Coords2 | VertexBuffer::Normal | VertexBuffer::Tangent);

MeshVertex::MeshVertex()
{
}

bool MeshVertex::operator <(const MeshVertex &mv) const
{
  return origin < mv.origin && coords < mv.coords && coords2 < mv.coords2 &&
      normal < mv.normal && tangent < mv.tangent;
}

bool MeshVertex::operator ==(const MeshVertex &mv) const
{
  return origin == mv.origin && coords == mv.coords && coords2 == mv.coords2 &&
      normal == mv.normal && tangent == mv.tangent;
}
