#ifndef MESHSUBSETFRAGMENT_P_HPP
#define MESHSUBSETFRAGMENT_P_HPP

#include <Pipeline/InstancedFragment.hpp>
#include <Pipeline/TexturedFragment.hpp>
#include <MaterialSystem/Material.hpp>

#include <Render/IndexBuffer.hpp>
#include <Render/VertexBuffer.hpp>
#include <Render/PrimitiveDrawer.hpp>

class Mesh;
class MeshSubset;

class MeshSubsetFragment : public InstancedFragment
{
public:
  MeshSubsetFragment(MeshSubset* meshSubset);
  ~MeshSubsetFragment();

public:
  bool bind(const Light* lights);
  void draw();
  VertexBuffer::Types instanceType() const;
  ShaderState *materialShaderState() const;
  float distance(const vec3& cameraOrigin, const vec4& cameraPlane) const;
  Material::OpaqueMode opaque() const;
  MaterialSystem::Quality materialQuality() const;
  void setMaterialQuality(MaterialSystem::Quality quality);

private:
  MeshSubset* m_meshSubset;
  QSharedPointer<Material> m_material;
  QSharedPointer<VertexBuffer> m_vertexBuffer;
  QSharedPointer<IndexBuffer> m_indexBuffer;
  IndexedPrimitiveDrawer m_drawer;
  ShaderState m_materialShaderState;
  MaterialSystem::Quality m_materialQuality;

  friend class MeshSubset;
};

class SelectedMeshSubsetFragment : public TexturedFragment
{
public:
  SelectedMeshSubsetFragment(const MeshSubset* meshSubset);
  ~SelectedMeshSubsetFragment();

public:
  bool prepare();
  bool bind(const Light* lights);
  void draw();
  VertexBuffer::Types vertexType() const;

private:
  QSharedPointer<VertexBuffer> m_vertexBuffer;
  QSharedPointer<IndexBuffer> m_indexBuffer;
  IndexedPrimitiveDrawer m_drawer;
};

#endif // MESHSUBSETFRAGMENT_P_HPP
