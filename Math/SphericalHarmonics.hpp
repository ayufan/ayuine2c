//------------------------------------//
// 
// SphericalHarmonics.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include "Math.hpp"
#include "Vec2.hpp"
#include "Vec3.hpp"
#include "Matrix.hpp"

//------------------------------------//
// SphericalHarmonics class

class MATH_EXPORT SphericalHarmonics
{
	// Classes
public:
	struct Sample
	{
		vec3 Normal;
		vec2 sphere;
		double Coeffs[16];
	};

	// Static Fields
private:
	static const unsigned _quality;
	static bool _initialized;
	static float _weight;
	static Sample _samples[];

	// Functions
private:
	static void computeSamples();

	
public:
	static vec3 fromSphere(const vec2 &v);
	static vec2 toSphere(const vec3 &v);
	static void simpleLight(const vec3 &dir, float intensity, matrix& out);
	static void directionalLight(const vec3 &dir, float intensity, matrix& out);
	static void sphericalLight(const vec3 &origin, float radius, float intensity, matrix& out);
	static void coneLight(const vec3 &origin, const vec3 &dir, float radius, float intensity, matrix& out);
	static void clearSkyLight(const vec2 &sun, float zenithIntensity, matrix& out);
	static void overcastLight(float zenithIntensity, matrix& out);
};

