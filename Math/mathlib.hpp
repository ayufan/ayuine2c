//------------------------------------//
//
// MathLib.hpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-02-08
//
//------------------------------------//

#pragma once

//------------------------------------//
// includes

#include <QtGlobal>
#include "Math.hpp"
#include <qobjectdefs.h>
#include <cmath>
#include <cfloat>

//------------------------------------//

const float Pi = 3.14159265358979323846f;
const float Sqrt2 = 1.41421356237309504880f;
const float Deg2Rad = Pi / 180.0f;
const float Rad2Deg = 180.0f / Pi;
const float MaxWorldSize = 4096.f;
const float Epsf = 0.0001f;

//------------------------------------//

class MATH_EXPORT CubeFace
{
  Q_GADGET
  Q_ENUMS(Enum)

public:
  enum Enum {
      PX,
      NX,
      PY,
      NY,
      PZ,
      NZ,

      Front = NX,
      Back = PX,
      OtherSide = PY,
      Side = NY,
      Top = NZ,
      Bottom = PZ
    };
};

class MATH_EXPORT Side
{
  Q_GADGET
  Q_ENUMS(Enum)

public:
  enum Enum {
    On,
    Front,
    Back,
    Split
  };
};

class MATH_EXPORT Axis
{
  Q_GADGET
  Q_ENUMS(Enum)

public:
  enum Enum {
    X,
    Y,
    Z,
    NonAxial
  };
};

//------------------------------------//

struct vec3;
Q_DECL_EXPORT bool cubeFaceVectors(CubeFace::Enum face, vec3& at, vec3& up, vec3& right);
Q_DECL_EXPORT bool cubeFaceVectors(Axis::Enum axis, vec3& at, vec3& up, vec3& right);

//------------------------------------//

template<typename Type>
inline Type clamp(Type a, Type low, Type high) {
    return qBound(a, low, high);
}

template<typename Type>
inline Type saturate(Type a) {
    return qBound(a, 0, 1);
}

inline bool zerof(float v) {
    return qFuzzyIsNull(v);
}

inline bool nonzerof(float v) {
    return !qFuzzyIsNull(v);
}

inline float truncf(float x) {
	return x - (int)x;
}

inline bool eq(float x, float y) {
	return zerof(x - y);
}

inline float clamp(float a, float low, float high) {
	return clamp<float>(a, low, high);
}

inline float saturate(float a) {
	return clamp<float>(a, 0, 1);
}

inline int clamp(int a, int low, int high) {
	return clamp<int>(a, low, high);
}

inline unsigned clamp(unsigned a, unsigned low, unsigned high) {
	return clamp<unsigned>(a, low, high);
}

