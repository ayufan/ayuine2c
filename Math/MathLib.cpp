//------------------------------------//
//
// MathLib.cpp
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayuine2
// Date: 2006-7-24
//
//------------------------------------//

#include "MathLib.hpp"
#include "Vec3.hpp"

bool cubeFaceVectors(CubeFace::Enum face, vec3& at, vec3& up, vec3& right) {
	switch(face) {
        case CubeFace::PX:
			at = vec3(1, 0, 0);
			up = vec3(0, 0, 1);
			right = vec3(0, -1, 0);
			break;

        case CubeFace::NX:
			at = vec3(-1, 0, 0);
			up = vec3(0, 0, 1);
			right = vec3(0, 1, 0);
			break;

        case CubeFace::PY:
			at = vec3(0, 1, 0);
			right = vec3(1, 0, 0);
			up = vec3(0, 0, 1);
			break;

        case CubeFace::NY:
			at = vec3(0, -1, 0);
			right = vec3(-1, 0, 0);
			up = vec3(0, 0, 1);
			break;

        case CubeFace::PZ:
			at = vec3(0, 0, 1);
			up = vec3(-1, 0, 0);
			right = vec3(0, -1, 0);
			break;

        case CubeFace::NZ:
			at = vec3(0, 0, -1);
			up = vec3(1, 0, 0);
			right = vec3(0, -1, 0);
			break;

		default:
			return false;
	}
	return true;
}

//bool cubeFaceVectors(CubeFace face, vec3& at, vec3& up, vec3& right) {
//	euler angles;
//
//	switch(face) {
//		case CubeFace::PX:
//			break;
//
//		case CubeFace::NX:
//			angles.Yaw = Pi;
//			break;
//
//		case CubeFace::PY:
//			angles.Yaw = -Pi/2.0f;
//			break;
//
//		case CubeFace::NY:
//			angles.Yaw = Pi/2.0f;
//			break;
//
//		case CubeFace::PZ:
//			angles.Pitch = Pi/2.0f;
//			break;
//
//		case CubeFace::NZ:
//			angles.Pitch = -Pi/2.0f;
//			break;
//
//		default:
//			return false;
//	}
//
//	at = angles.at();
//	up = angles.up();
//	right = angles.right();
//	return true;
//}

bool cubeFaceVectors(Axis::Enum axis, vec3& at, vec3& up, vec3& right) {
	switch(axis) {
    case Axis::X:
        return cubeFaceVectors(CubeFace::Front, at, up, right);

    case Axis::Y:
        return cubeFaceVectors(CubeFace::Side, at, up, right);

    case Axis::Z:
        return cubeFaceVectors(CubeFace::Top, at, up, right);

	default:
		return false;
	}
}
