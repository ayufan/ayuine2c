//------------------------------------//
// 
// Rect.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QDataStream>
#include "Math.hpp"
#include "Vec2.hpp"

//------------------------------------//
// rect structure

struct MATH_EXPORT rect
{
	// Fields
	float Left, Top;
	float Right, Bottom;
	
	// Constructor
	rect() {
		Left = Top = FLT_MAX;
		Right = Bottom = -FLT_MAX;
	}
	rect(float width, float height) {
		Left = Top = 0;
		Right = width;
		Bottom = height;
	}
	rect(const vec2 &size) {
		min() = vec2();
		max() = size;
	}
	rect(float left, float top, float width, float height) {
		min() = vec2(left, top);
		max() = vec2(left + width, top + height);
	}
	rect(const vec2 &min_, const vec2 &max_) {
		min() = vec2::minimize(min_, max_);
		max() = vec2::maximize(min_, max_);
	}

	// Properties
	vec2 &min() {
		return (vec2&)Left;
	}
	const vec2 &min() const {
		return (vec2&)Left;
	}
	vec2 &max() {
		return (vec2&)Right;
	}
	const vec2 &max() const {
		return (vec2&)Right;
	}
	float width() const {
		return Right - Left;
	}
	float height() const {
		return Bottom - Top;
	}
	bool empty() const {
		return Right <= Left || Bottom <= Top;
	}
	vec2 at(float s, float t) const {
		return vec2(Left * (1 - s) + Right * s, Top * (1 - t) + Bottom * t);
	}

	// Operators

#if 0
	operator bool () const {
		return !empty();
	}
#endif
	bool operator ! () const {
		return empty();
	}
	

	// Methods
    rect intersect(const rect &r) const;
    rect merge(const rect &r) const;

	// Friends

	rect& operator += (const rect &b) {
		return *this = merge(b);
	}
	rect& operator *= (const rect &b) {
		return *this = intersect(b);
	}
	

	bool operator == (const rect &b) const {
		return Left == b.Left && Right == b.Right && Top == b.Top && Bottom == b.Bottom;
	}
	bool operator != (const rect &b) const {
		return Left != b.Left || Right != b.Right || Top != b.Top || Bottom != b.Bottom;
	}
	rect operator + (const rect &b) const {
		return merge(b);
	}
	rect operator * (const rect &b) const {
		return intersect(b);
	}
	rect operator + (const vec2 &b) const {
		return rect(min() + b, max() + b);
	}
	rect operator - (const vec2 &b) const {
		return rect(min() - b, max() - b);
    }

public:
    friend QDataStream &operator<<(QDataStream &ds, const rect &rhs)
    {
      ds.writeRawData((const char*)&rhs.Left, sizeof(float) * 4);
      return ds;
    }

    friend QDataStream &operator>>(QDataStream &ds, rect &rhs)
    {
      ds.readRawData((char*)&rhs.Left, sizeof(float) * 4);
      return ds;
    }
};

MATH_EXPORT extern const rect rectEmpty;
MATH_EXPORT extern const rect rectZero;
MATH_EXPORT extern const rect rectClip;


