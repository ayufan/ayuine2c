#ifndef COLOR_HPP
#define COLOR_HPP

#include <QDataStream>
#include "MathLib.hpp"
#include "Math.hpp"

//------------------------------------//
// color class

struct MATH_EXPORT color
{
    // Fields
public:
    float Red, Green, Blue, Alpha;

    // Constructor
public:
    color() : Red(1), Green(1), Blue(1), Alpha(1) {
    }
    color(float r, float g, float b, float a = 1.0f) : Red(r), Green(g), Blue(b), Alpha(a) {
    }
#ifndef _SWIG
    color(int r, int g, int b, int a) : Red(r / 255.0f), Green(g / 255.0f), Blue(b / 255.0f), Alpha(a / 255.0f / 255.0f) {
    }
#endif
    color(unsigned c) : Red(((c >> 16) & 0xFF) / 255.0f), Green(((c >> 8) & 0xFF) / 255.0f), Blue(((c >> 0) & 0xFF) / 255.0f), Alpha(((c >> 24) & 0xFF) / 255.0f) {
    }
    color(const color& c, float alpha) : Red(c.Red), Green(c.Green), Blue(c.Blue), Alpha(alpha) {
    }

    // Methods
public:
    int red() const {
        return int(Red * 255.0f);
    }
    int green() const {
        return int(Green * 255.0f);
    }
    int blue() const {
        return int(Blue * 255.0f);
    }
    int alpha() const {
        return int(Alpha * 255.0f);
    }
    unsigned toColor() const {
        return red() << 16 | green() << 8 | blue() << 0 | alpha() << 24;
    }

    bool isOpaque() const {
        return Alpha > 1.0f-Epsf;
    }
    bool isVisible() const {
        return Alpha > Epsf;
    }

public:
    friend QDataStream &operator<<(QDataStream &ds, const color &rhs)
    {
      return ds << rhs.Red << rhs.Green << rhs.Blue << rhs.Alpha;
    }

    friend QDataStream &operator>>(QDataStream &ds, color &rhs)
    {
      return ds >> rhs.Red >> rhs.Green >> rhs.Blue >> rhs.Alpha;
    }
};

Q_DECLARE_METATYPE(color)

#endif // COLOR_HPP
