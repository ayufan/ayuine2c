//------------------------------------//
// 
// TangentVectors.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QDataStream>
#include "Math.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"

//------------------------------------//
// TangentVectors structure

struct MATH_EXPORT TangentVectors
{
	// Fields
	vec3 Tangent;
	vec3 Binormal;
	vec3 Normal;

	// Constructor
	TangentVectors() {
	}
	TangentVectors(const vec3 &v1, const vec3 &v2, const vec3 &v3, const vec2 &st1, const vec2 &st2, const vec2 &st3);

	// Methods
	vec3 calculateTangent(const vec3 &normal);
	vec3 calculateBinormal(const vec3 &normal);
	vec4 calculateTangentFlag(const vec3 &normal);

	//// Serializer
	//aDEFINE_SERIALIZE(TangentVectors) {
	//	return ar & self.Tangent & self.Binormal & self.Normal;
	//}

public:
    friend QDataStream &operator<<(QDataStream &ds, const TangentVectors &rhs)
    {
      return ds << rhs.Tangent << rhs.Binormal << rhs.Normal;
    }

    friend QDataStream &operator>>(QDataStream &ds, TangentVectors &rhs)
    {
      return ds >> rhs.Tangent >> rhs.Binormal >> rhs.Normal;
    }
};


