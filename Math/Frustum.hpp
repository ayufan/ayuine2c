//------------------------------------//
// 
// Frustum.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QVector>
#include "Math.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"
#include "matrix.hpp"
#include "Sphere.hpp"
#include "Box.hpp"

//------------------------------------//
// frustum structure

struct MATH_EXPORT frustum
{
  Q_GADGET
  Q_ENUMS(Planes)

public:
  enum Planes
  {
    Left, Right, Top, Bottom, Back, Front,
    _Count
  };

public:
  frustum();
  frustum(const matrix &transform);
  frustum(const matrix &projection, const matrix &view);
  frustum(const frustum &culler, const matrix &object);

public slots:
  bool test(const vec3 &point) const;
  bool test(const vec4 &plane) const;
  bool test(const vec3 *verts, unsigned count, unsigned stride = sizeof(vec3)) const;
  bool test(const QVector<vec3> &verts) const;
  bool test(const box &v) const;
  bool test(const sphere &v) const;
  vec3 screen(const vec3 &v) const;
  vec3 world(const vec3 &v) const;
  QRectF screenRect(const vec3 *verts, unsigned count) const;
  QRectF screenRect(const box &v) const;

public:
  const matrix& object() const;
  const matrix& invObject() const;
  const vec4& plane(Planes plane) const;

private:
  // Fields
  matrix m_object;
  mutable matrix m_invObject;
  mutable bool m_hasInvObject;
  vec4 m_planes[_Count];
};

Q_DECLARE_METATYPE(frustum)
