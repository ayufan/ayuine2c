//------------------------------------//
// 
// Poly.hpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#pragma once

#include <QVector>
#include "Math.hpp"
#include "MathLib.hpp"
#include "Vec2.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"

//------------------------------------//
// Typedefs

struct vec2;
struct vec3;
struct vec4;
typedef QVector<vec3> PolyVerts;
typedef QVector<vec4> PolyEdges;

//------------------------------------//
// Poly class

class MATH_EXPORT Poly
{
	// Functions
public:
    static vec4 plane(const PolyVerts &verts);
    static vec4 plane(const vec3* verts, unsigned count);
    static PolyVerts build(const PolyVerts &points);
    static PolyVerts portal(const vec4 &plane, float size = 65536);
    static PolyVerts portal(const vec3 &at, const vec2& size, const vec3& origin = vec3());
    static PolyVerts portal(Axis::Enum axis, const vec2& size, const vec3& origin = vec3());
    static Side::Enum split(const PolyVerts &verts, const vec4 &plane, PolyVerts *front = NULL, PolyVerts *back = NULL);
    static PolyVerts add(unsigned count , ...);
    static PolyVerts merge(unsigned count, ...);
    static void removeCollinear(PolyVerts &verts);
    static float area(const PolyVerts &verts);
    static bool isTiny(const PolyVerts &verts, float size);
    static bool isHuge(const PolyVerts &verts, float size);
    static bool isPlanar(const PolyVerts &verts);
    static bool isConvex(const PolyVerts &verts);
    static PolyEdges edges(const PolyVerts &verts);
    static Side::Enum test(const PolyVerts &verts, const vec4 &plane);
    static bool test(const PolyVerts &verts, const vec3 &point);
    static bool test(const PolyVerts &a, const PolyVerts &b);
    static bool test(const vec3 *verts, unsigned count, const vec3 &point);
    static bool test(const vec4 *edges, unsigned count, const vec3 &point);
};

