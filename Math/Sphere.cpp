//------------------------------------//
// 
// Sphere.cpp
// 
// Author: ayufan (ayufan(at)o2.pl)
// Project: Core
// Date: 2007-08-14
// 
//------------------------------------//

#include "Sphere.hpp"
#include "Matrix.hpp"
#include "Ray.hpp"

//------------------------------------//
// sphere: Constructor

sphere::sphere(const vec3 &center, float radius) {
	Center = center;
	Radius = radius;
}

sphere::sphere(const vec3 *v, unsigned count, unsigned stride) {
	for(size_t i = count; i > 0; ) {
		Center += *(vec3*)(((char*)v) + --i * stride);
	}
	Center /= (float)count;
	Radius = 0;
	for(size_t i = count; i > 0; )
        Radius = qMax(Radius, (*(vec3*)(((char*)v) + --i * stride) - Center).length());
}

//------------------------------------//
// sphere: Methods

sphere sphere::transform(const matrix &m) const {
	return sphere(m.transformCoord(Center), m.transformNormal(Radius).length());
}

bool sphere::test(const vec3 &v) const {
	return (v - Center).lengthSq() <= Radius * Radius;
}

bool sphere::test(const sphere &s) const {
	return (s.Center - Center).lengthSq() <= (Radius + s.Radius) * (Radius + s.Radius);
}

float sphere::collide(const ray &ray) const {
	vec3 q = Center - ray.Origin;
	float c = q.lengthSq();
	float r2 = Radius * Radius;
	//if(c <= r2)
	//	return 0.0f;
	float v = ray.At ^ q;
	float d = r2 - (c - v * v);

	if (d < -Epsf)
		return -1.0f;
	if (d < Epsf)
		return 0.0f;

	return v - sqrt(d);
}

matrix sphere::toTransform() const
{
  return matrix(
      Radius, 0, 0, 0,
      0, Radius, 0, 0,
      0, 0, Radius, 0,
      Center.X, Center.Y, Center.Z, 1.0f);
}
