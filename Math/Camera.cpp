#include "Camera.hpp"
#include "Euler.hpp"

Camera::Camera()
{
  ortho = false;
  distance = 0.0f;
  scaleFactor = 1.0f;
}

void Camera::lookAt(Camera &info, const vec3 &origin, const vec3 &eye, float fovy, float aspect, float distance)
{
  // Ustaw parametry widoku
  info.origin = origin;
  info.at = (eye-origin).normalize();
  info.up = vec3(0,0,1);
  info.right = info.at % info.up;

  matrix::lookAt(info.view, origin, eye, vec3(0,0,1));

  // Wyznacz macierz projekcji
  if(zerof(distance))
      matrix::perspective(info.projection, fovy, aspect, 0.05f);
  else
      matrix::perspective(info.projection, fovy, aspect, 0.05f, fabsf(distance));
  info.distance = distance < 0 ? 0 : distance;

  // Wyznacz płaszczyzny widoku
  info.culler = frustum(info.projection, info.view);
  info.ortho = false;
}

void Camera::fppCamera(Camera& info, const vec3& origin, const euler& angles, float fovy, float aspect, float distance)
{
    // Ustaw parametry widoku
    info.origin = origin;
    info.at = angles.at();
    info.up = angles.up();
    info.right = angles.right();

    matrix::lookAt(info.view, origin, origin + info.at, info.up);

    // Wyznacz macierz projekcji
    if(zerof(distance))
        matrix::perspective(info.projection, fovy, aspect, 0.05f);
    else
        matrix::perspective(info.projection, fovy, aspect, 0.05f, fabsf(distance));
    info.distance = distance < 0 ? 0 : distance;

    // Wyznacz płaszczyzny widoku
    info.culler = frustum(info.projection, info.view);
    info.ortho = false;
}

void Camera::modelViewCamera(Camera& info, const vec3& origin, const euler& angles, float radius, float fovy, float aspect, float distance)
{
    // Ustaw parametry widoku
    info.origin = origin + angles.at() * radius;
    info.at = -angles.at();
    info.up = angles.up();
    info.right = angles.right();

    // Wyznacz macierz widoku
    matrix::lookAt(info.view, origin + radius * angles.at(), origin, angles.up());

    // Wyznacz macierz projekcji
    if(zerof(distance))
        matrix::perspective(info.projection, fovy, aspect, 0.05f);
    else
        matrix::perspective(info.projection, fovy, aspect, 0.05f, fabsf(distance));
    info.distance = distance < 0 ? 0 : distance;

    // Wyznacz płaszczyzny widoku
    info.culler = frustum(info.projection, info.view);
    info.ortho = false;
}

void Camera::cubeCamera(Camera& info, const vec3& origin, CubeFace::Enum face, float distance)
{
    static const matrix BaseView(0,0,1,0, -1,0,0,0, 0,1,0,0, 0,0,0,1);

    switch(face) {
    case CubeFace::PX:
        info.at = vec3(-1, 0, 0);
        info.up = vec3(0, -1, 0);
        break;

    case CubeFace::NX:
        info.at = vec3(1, 0, 0);
        info.up = vec3(0, -1, 0);
        break;

    case CubeFace::PY:
        info.at = vec3(0, -1, 0);
        info.up = vec3(0, 0, 1);
        break;

    case CubeFace::NY:
        info.at = vec3(0, 1, 0);
        info.up = vec3(0, 0, -1);
        break;

    case CubeFace::PZ:
        info.at = vec3(0, 0, -1);
        info.up = vec3(0, -1, 0);
        break;

    case CubeFace::NZ:
        info.at = vec3(0, 0, 1);
        info.up = vec3(0, -1, 0);
        break;

    default:
        return;
    }
    info.right = (info.up % info.at).normalize();
    info.origin = origin;
    info.distance = distance < 0 ? 0 : distance;

    // Wyznacz macierz widoku
    matrix temp;
    matrix::fromCols(temp, vec4(info.at, 0.0f), vec4(info.right, 0), vec4(info.up, 0), vec4(0, 0, 0, 1));
    matrix::multiply(info.view, BaseView, temp);
    matrix::translation(temp, -info.origin);
    matrix::multiply(info.view, info.view, temp);

    // Wyznacz macierz transformacji
    if(qFuzzyIsNull(distance))
        matrix::perspective(info.projection, 90.0f * Deg2Rad, 1.0f, 0.1f);
    else
        matrix::perspective(info.projection, 90.0f * Deg2Rad, 1.0f, 0.1f, fabsf(distance));

    // Oblicz płaszczyzny widoku
    info.culler = frustum(info.projection, info.view);
    info.ortho = false;
}

void Camera::orthoCamera(Camera& info, const vec3& origin, Axis::Enum axis, float zoom, unsigned width, unsigned height)
{
    // Zapisz parametry widoku
    info.origin = origin;
    cubeFaceVectors(axis, info.at, info.up, info.right);

    vec2 origin2 = vec2(origin ^ info.right, origin ^ info.up);
    vec2 bounds2 = vec2((width / 2.0f) / zoom, -(height / 2.0f) / zoom);

    // Oblicz macierz projekcji
    matrix::ortho(info.projection, origin2 - bounds2, origin2 + bounds2, -4096.f, 4096.f);

    // Oblicz macierz widoku
    matrix::fromCols(info.view, vec4(info.right, 0.0f), vec4(info.up, 0), vec4(info.at, 0.0f), vec4(0, 0, 0, 1));

    // Oblicz stożek widoczności
    info.culler = frustum(info.projection, info.view);
    info.ortho = true;
    info.scaleFactor = height * 0.5f / zoom;
}

LocalCamera::LocalCamera(const Camera &camera)
  : culler(camera.culler), view(camera.view), viewOrigin(camera.origin), object(mat4Identity)
{
}

LocalCamera::LocalCamera(const Camera& camera, const matrix& transform)
{
  view = camera.view * transform;
  viewOrigin = view.origin1();

  object = transform;
  objectOrigin = object.origin();

  culler = frustum(camera.projection, view);
}

LocalCamera::LocalCamera(const LocalCamera& camera, const matrix& transform)
{
  view = camera.view * transform;
  viewOrigin = view.origin1();

  object = camera.object * transform;
  objectOrigin = object.origin();

  culler = frustum(camera.culler, transform);
}
