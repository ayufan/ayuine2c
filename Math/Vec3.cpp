#include "Vec3.hpp"
#include <stdexcept>

//------------------------------------//
// vec3: Methods

vec3 vec3::normalize(float &l) const {
    if((l = length()) < Epsf)
        return vec3();

    float invLength = 1.0f / l;
    return vec3(X * invLength, Y * invLength, Z * invLength);
}

vec3 vec3::rotateX(float angle) const {
    vec3 value;
    float s, c;

    s = sinf(angle); c = cosf(angle);

    value.X = X;
    value.Y = Y * c - Z * s;
    value.Z = Y * s + Z * c;

    return value;
}

vec3 vec3::rotateY(float angle) const {
    vec3 value;
    float s, c;

    s = sinf(angle); c = cosf(angle);

    value.X = Z * c - X * s;
    value.Y = Y;
    value.Z = Z * s + X * c;

    return value;
}

vec3 vec3::rotateZ(float angle) const {
    vec3 value;
    float s, c;

    s = sinf(angle); c = cosf(angle);

    value.X = X * c - Y * s;
    value.Y = X * s + Y * c;
    value.Z = Z;

    return value;
}

vec3 vec3::orthogonalize(const vec3 &v) const {
    return (*this) - v * (v ^ (*this));
}

void vec3::normalVectors(vec3 &right) const {
    right = vec3(Z, -X, Y).orthogonalize(*this);
}

void vec3::normalVectors(vec3 &right, vec3 &up) const {
    right = vec3(Z, -X, Y).orthogonalize(*this);
    up = right % *this;
}

void vec3::axis(vec3 &xv, vec3 &yv) const {
    static vec3 baseAxis[][3] = {
        {vec3(0, 0, 1), vec3(1, 0, 0), vec3(0, -1, 0)},		// Floor
        {vec3(0, 0, -1), vec3(1, 0, 0), vec3(0, -1, 0)},	// Ceiling
        {vec3(1, 0, 0), vec3(0, 1, 0), vec3(0, 0, -1)},		// West wall
        {vec3(-1, 0, 0), vec3(0, 1, 0), vec3(0, 0, -1)},	// East wall
        {vec3(0, 1, 0), vec3(1, 0, 0), vec3(0, 0, -1)},		// South wall
        {vec3(0, -1, 0), vec3(1, 0, 0), vec3(0, 0, -1)}		// North wall
    };

    float best = 0;
    unsigned axis = 0;

    for (unsigned i = 0; i < 6; i++)
    {
        float dot = *this ^ baseAxis[i][0];
        if (dot > best)
        {
            best = dot;
            axis = i;
        }
    }

    xv = baseAxis[axis][1];
    yv = baseAxis[axis][2];
}

vec3 vec3::reflect(const vec3 &normal) const {
    return *this - (2 * (*this ^ normal)) * normal;
}

vec3 vec3::round(float ep) const {
    vec3 v;
    float iep = 1.0f / ep;

    v.X = floorf(X * iep + 0.5f) * ep;
    v.Y = floorf(Y * iep + 0.5f) * ep;
    v.Z = floorf(Z * iep + 0.5f) * ep;

    return v;
}

//------------------------------------//
// vec3: Functions

vec3 vec3::cubeVector(CubeFace::Enum face, float s, float t)
{
    float sc = s * 2.0f - 1.0f;
    float tc = t * 2.0f - 1.0f;

    switch (face)
    {
        case CubeFace::PX:
            return vec3(1.0f, -tc, -sc);

        case CubeFace::NX:
            return vec3(-1.0f, -tc, sc);

        case CubeFace::PY:
            return vec3(sc, 1.0f, tc);

        case CubeFace::NY:
            return vec3(sc, -1.0f, -tc);

        case CubeFace::PZ:
            return vec3(sc, -tc, 1.0f);

        case CubeFace::NZ:
            return vec3(-sc, -tc, -1.0f);

        default:
            throw std::runtime_error("invalid face");
    }
    return vec3();
}

vec3 vec3::lerp(const vec3 &v1, const vec3 &v2, float amount) {
    return vec3(
        v1.X * (1.0f - amount) + v2.X * amount,
        v1.Y * (1.0f - amount) + v2.Y * amount,
        v1.Z * (1.0f - amount) + v2.Z * amount);
}

vec3 vec3::lerp3(const vec3 &v1, const vec3 &v2, vec3 &v3, float amount) {
    return lerp(lerp(v1, v2, amount), lerp(v2, v3, amount), amount);
}
