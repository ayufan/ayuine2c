#ifndef MATH_HPP
#define MATH_HPP

#include <QtCore>

#ifdef MATH_EXPORTS
#define MATH_EXPORT Q_DECL_EXPORT
#else
#define MATH_EXPORT Q_DECL_IMPORT
#endif

#endif // MATH_HPP
