TEMPLATE = lib
QT = core
TARGET = Math

include(../ayuine2c.pri)
# include(../LuaBindings/Lua.pri)

HEADERS += Box.hpp \
           Frustum.hpp \
           MathLib.hpp \
           Matrix.hpp \
           Poly.hpp \
           Ray.hpp \
           Rect.hpp \
           Sphere.hpp \
           SphericalHarmonics.hpp \
           TangentVectors.hpp \
    Vec2.hpp \
    Vec3.hpp \
    Vec4.hpp \
    Euler.hpp \
    Color.hpp \
    Math.hpp \
    MathLua.hpp \
    Camera.hpp \
    MathLua.hpp

SOURCES += Box.cpp \
           Consts.cpp \
           Frustum.cpp \
           Math.cpp \
           MathLib.cpp \
           Matrix.cpp \
           Poly.cpp \
           Ray.cpp \
           Rect.cpp \
           Sphere.cpp \
           SphericalHarmonics.cpp \
           TangentVectors.cpp \
    Vec2.cpp \
    Vec3.cpp \
    Vec4.cpp \
    Euler.cpp \
    Color.cpp \
    Camera.cpp

LIBS += -lCore

# include(../LuaBindings/Lua.pri)
