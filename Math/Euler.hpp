#ifndef EULER_HPP
#define EULER_HPP

#include <QDataStream>
#include "MathLib.hpp"
#include "Math.hpp"
#include "Vec3.hpp"

//------------------------------------//
// euler class

struct MATH_EXPORT euler
{
    // Fields
public:
    float Yaw, Pitch, Roll;

    // Constructor
public:
    euler() : Yaw(0), Pitch(0), Roll(0) {
    }
    euler(float yaw, float pitch, float roll) : Yaw(yaw), Pitch(pitch), Roll(roll) {
    }
    euler(const float v[3]) {
      Yaw = v[0];
      Pitch = v[1];
      Roll = v[2];
    }
    euler(const vec3 &dir, float r);
    euler(const vec3 &dir, const vec3 &up);

    // Properties
public:
    vec3 at() const;
    vec3 right() const;
    vec3 up() const;
    vec3 rotate(const vec3 &in) const;
    vec3 rotate(const vec3 &in, const vec3 &around) const;

    euler toRadians() const {
        return euler(Yaw * Deg2Rad, Pitch * Deg2Rad, Roll * Deg2Rad);
    }
    euler toDegrees() const {
        return euler(Yaw * Rad2Deg, Pitch * Rad2Deg, Roll * Rad2Deg);
    }

public:
    friend QDataStream &operator<<(QDataStream &ds, const euler &rhs)
    {
      return ds << rhs.Yaw << rhs.Pitch << rhs.Roll;
    }

    friend QDataStream &operator>>(QDataStream &ds, euler &rhs)
    {
      return ds >> rhs.Yaw >> rhs.Pitch >> rhs.Roll;
    }
};

Q_DECL_EXPORT extern const euler angleZero;

inline euler operator + (const euler &v) {
    return euler(+v.Yaw, +v.Pitch, +v.Roll);
}
inline euler operator - (const euler &v) {
    return euler(-v.Yaw, -v.Pitch, -v.Roll);
}

Q_DECLARE_METATYPE(euler)

#endif // EULER_HPP
