#include "Vec4.hpp"
#include "Sphere.hpp"
#include "Ray.hpp"
#include "Box.hpp"

//------------------------------------//
// vec4: Methods

vec4 vec4::normalize(float &l) const {
    if((l = length()) < Epsf)
        return vec4();

    float invLength = 1.0f / l;
    return vec4(X * invLength, Y * invLength, Z * invLength, W * invLength);
}

vec4 vec4::normalizeNormal(float &l) const {
    if((l = normal().length()) < Epsf)
        return vec4();

    float invLength = 1.0f / l;
    return vec4(X * invLength, Y * invLength, Z * invLength, W * invLength);
}

Side::Enum vec4::test(const vec3 &v) const {
    float d;
    return test(v, d);
}

Side::Enum vec4::test(const vec3 &v, float &d) const {
    d = (normal() ^ v) + W;
    if(d < -Epsf)	return Side::Back;
    if(d >  Epsf)	return Side::Front;
    return Side::On;
}

Side::Enum vec4::test(const vec3 *v, unsigned count) const {
    unsigned result = 0;

    for(unsigned i = 0; i < count && result != Side::Split; i++, v++)
        result |= test(*v);
    return (Side::Enum)result;
}

Side::Enum vec4::test(const struct sphere &sphere) const {
    float d = *this ^ sphere.Center;

    if(d - sphere.Radius > Epsf)
        return Side::Front;
    if(d + sphere.Radius < Epsf)
        return Side::Back;
    if(sphere.Radius > 0)
        return Side::Split;
    return Side::On;
}

Side::Enum vec4::test(const struct box &box) const {
    vec3 mins, maxs;

    // Wyznacz wektory
    if(X < 0) {
        mins.X = box.Mins.X;
        maxs.X = box.Maxs.X;
    }
    else {
        maxs.X = box.Mins.X;
        mins.X = box.Maxs.X;
    }

    if(Y < 0) {
        mins.Y = box.Mins.Y;
        maxs.Y = box.Maxs.Y;
    }
    else {
        maxs.Y = box.Mins.Y;
        mins.Y = box.Maxs.Y;
    }

    if(Z < 0) {
        mins.Z = box.Mins.Z;
        maxs.Z = box.Maxs.Z;
    }
    else {
        maxs.Z = box.Mins.Z;
        mins.Z = box.Maxs.Z;
    }

    // Oblicz dystans
    float a = *this ^ mins;
    float b = *this ^ maxs;

    // Sprawd� wynik
    if(a >= Epsf && b < -Epsf)
        return Side::Split;
    if(a > Epsf)
        return Side::Front;
    if(b < -Epsf)
        return Side::Back;
    return Side::On;
}

float vec4::collide(const ray& r) const {
    float a = r.At ^ normal();
    if(zerof(a))
        return 0;
    return - (*this ^ r.Origin) / a;
}

vec3 vec4::reflect(const vec3 &v) const {
    return normal().reflect(v);
}

vec4 vec4::prependicular(const vec3 &a, const vec3 &b) {
    return vec4(((a - b) % normal()).normalize(), a);
}

//------------------------------------//
// vec4: Functions

vec4 vec4::lerp(const vec4 &v1, const vec4 &v2, float amount) {
    return vec4(
        v1.X * (1.0f - amount) + v2.X * amount,
        v1.Y * (1.0f - amount) + v2.Y * amount,
        v1.Z * (1.0f - amount) + v2.Z * amount,
        v1.W * (1.0f - amount) + v2.W * amount);
}

vec4 vec4::lerp3(const vec4 &v1, const vec4 &v2, const vec4 &v3, float amount) {
    return lerp(lerp(v1, v2, amount), lerp(v2, v3, amount), amount);
}

unsigned vec4::compress(const vec4 &value) {
    unsigned sum = 0;

    sum |= qBound(unsigned((value.X * 0.5f + 0.5f) * 255.0f), 0U, 255U) << 16;
    sum |= qBound(unsigned((value.Y * 0.5f + 0.5f) * 255.0f), 0U, 255U) <<  8;
    sum |= qBound(unsigned((value.Z * 0.5f + 0.5f) * 255.0f), 0U, 255U) <<  0;
    sum |= qBound(unsigned((value.W * 0.5f + 0.5f) * 255.0f), 0U, 255U) << 24;

    return sum;
}

vec4 vec4::uncompress(unsigned value) {
    float x, y, z, w;

    x = float(((value >> 16) & 0xFF) * 2 - 0xFF) / 255.0f;
    y = float(((value >>  8) & 0xFF) * 2 - 0xFF) / 255.0f;
    z = float(((value >>  0) & 0xFF) * 2 - 0xFF) / 255.0f;
    w = float(((value >> 24) & 0xFF) * 2 - 0xFF) / 255.0f;

    return vec4(x, y, z, w);
}

void vec4::copy(float dest[]) const
{
  dest[0] = X;
  dest[1] = Y;
  dest[2] = Z;
  dest[3] = W;
}
