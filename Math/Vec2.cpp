#include "Vec2.hpp"

//------------------------------------//
// vec2: Methods

vec2 vec2::normalize(float &l) const {
    if((l = length()) < Epsf)
        return vec2();

    float invLength = 1.0f / l;
    return vec2(X * invLength, Y * invLength);
}

vec2 vec2::rotate(float angle) const {
    vec2 value;
    float s, c;

    s = sinf(angle); c = cosf(angle);

    value.X = X * c - Y * s;
    value.Y = X * s + Y * c;

    return value;
}

//------------------------------------//
// vec2: Functions

vec2 vec2::lerp(const vec2 &v1, const vec2 &v2, float amount) {
    return vec2(
        v1.X * (1.0f - amount) + v2.X * amount,
        v1.Y * (1.0f - amount) + v2.Y * amount);
}

vec2 vec2::lerp3(const vec2 &v1, const vec2 &v2, const vec2 &v3, float amount) {
    return lerp(lerp(v1, v2, amount), lerp(v2, v3, amount), amount);
}

