#ifndef VEC2_HPP
#define VEC2_HPP

#include <QDataStream>
#include "Math.hpp"
#include "MathLib.hpp"

#include <Core/ClassInfo.hpp>
#include <Core/Serializer.hpp>

//------------------------------------//
// vec2 class

struct MATH_EXPORT vec2
{
    Q_GADGET
    Q_PROPERTY(float x READ x WRITE setX)
    Q_PROPERTY(float y READ y WRITE setY)
    Q_PROPERTY(float length READ length)
    Q_PROPERTY(float lengthSq READ lengthSq)
    Q_PROPERTY(bool empty READ empty)
    Q_PROPERTY(vec2 normalize READ normalize)

    // Fields
public:
    float X, Y;

    // Properties
public:
    float x() const { return X; }
    float y() const { return Y; }
    void setX(float x) { X = x; }
    void setY(float y) { Y = y; }

    // Constructors
public:
    Q_INVOKABLE vec2() {
        X = 0;
        Y = 0;
    }
    vec2(float v[]) {
        X = v[0];
        Y = v[1];
    }
    Q_INVOKABLE vec2(float _x, float _y) {
        X = _x;
        Y = _y;
    }
    vec2(float _y) {
        X = _y;
        Y = _y;
    }

    Q_INVOKABLE static void testme() {}

    // Operators
public:

    float &operator [] (unsigned index) {
        return (&X)[index];
    }
    float operator [] (unsigned index) const {
        return (&X)[index];
    }
#if 0
    operator bool () const {
        return !empty();
    }
#endif
    bool operator ! () const {
        return empty();
    }


    // Properties
public:
    float lengthSq() const {
        return X * X + Y * Y;
    }
    float length() const {
        return sqrt(X * X + Y * Y);
    }
    bool empty() const {
        return X == 0.0f && Y == 0.0f;
    }

    // Methods
public:
    Q_INVOKABLE vec2 rotate(float angle) const;
    vec2 normalize(float &l) const;
    vec2 normalize() const {
        float temp;
        return normalize(temp);
    }

    // Functions
public:
    Q_INVOKABLE static vec2 lerp(const vec2 &v1, const vec2 &v2, float amount = 0.5f);
    Q_INVOKABLE static vec2 lerp3(const vec2 &v1, const vec2 &v2, const vec2 &v3, float amount = 0.5f);
    Q_INVOKABLE static vec2 minimize(const vec2 &v1, const vec2 &v2) {
        return vec2(qMin(v1.X, v2.X), qMin(v1.Y, v2.Y));
    }
    Q_INVOKABLE static vec2 maximize(const vec2 &v1, const vec2 &v2) {
        return vec2(qMax(v1.X, v2.X), qMax(v1.Y, v2.Y));
    }

    // Friends

    vec2 &operator += (const vec2 &v) {
        X += v.X;
        Y += v.Y;
        return *this;
    }
    vec2 &operator += (const float v)	{
        X += v;
        Y += v;
        return *this;
    }
    vec2 &operator -= (const vec2 &v) {
        X -= v.X;
        Y -= v.Y;
        return *this;
    }
    vec2 &operator -= (const float v)	{
        X -= v;
        Y -= v;
        return *this;
    }
    vec2 &operator *= (const vec2 &v)	{
        X *= v.X;
        Y *= v.Y;
        return *this;
    }
    vec2 &operator *= (const float v)	{
        X *= v;
        Y *= v;
        return *this;
    }
    vec2 &operator /= (const vec2 &v) {
        X /= v.X;
        Y /= v.Y;
        return *this;
    }
    vec2 &operator /= (const float v)	{
        X /= v;
        Y /= v;
        return *this;
    }


    Q_INVOKABLE vec2 operator + (const vec2 &b) const {
        return vec2(X + b.X, Y + b.Y);
    }
    vec2 operator + (const float b) const {
        return vec2(X + b, Y + b);
    }
    vec2 operator - (const vec2 &b) const {
        return vec2(X - b.X, Y - b.Y);
    }
    vec2 operator - (const float b) const {
        return vec2(X - b, Y - b);
    }
    vec2 operator * (const vec2 &b) const {
        return vec2(X * b.X, Y * b.Y);
    }
    vec2 operator * (const float b) const {
        return vec2(X * b, Y * b);
    }
    vec2 operator / (const vec2 &b) const {
        return vec2(X / b.X, Y / b.Y);
    }
    vec2 operator / (const float b) const {
        return vec2(X / b, Y / b);
    }
    vec2 operator % (const vec2 &b) const {
        return vec2(Y - b.X, X - b.Y);
    }
    float operator ^ (const vec2 &b)	{
        return X * b.X + Y * b.Y;
    }
    float operator ^ (float b)	{
        return X * b + Y;
    }
    float operator % (float b) const {
        return X * b;
    }
    bool operator == (const vec2 &b) const {
        return fabs(X - b.X) < Epsf && fabs(Y - b.Y) < Epsf;
    }
    bool operator != (const vec2 &b) const {
        return X != b.X || Y != b.Y;
    }
    bool operator < (const vec2 &b) const {
        return X - b.X < -Epsf && Y - b.Y < -Epsf;
    }
    static vec2 cross(const vec2 &a, const vec2 &b) {
        return vec2(a.Y - b.X, a.X - b.Y);
    }
    static float dot (const vec2 &a, const vec2 &b)	{
        return a.X * b.X + a.Y * b.Y;
    }
    static float dot (const vec2 &a, float b)	{
        return a.X * b + a.Y;
    }

    //// Serializer
    //aDEFINE_SERIALIZE(vec2) {
    //	return ar & self.X & self.Y;
    //}

public:
    friend QDataStream &operator<<(QDataStream &ds, const vec2 &rhs)
    {
      return ds << rhs.X << rhs.Y;
    }

    friend QDataStream &operator>>(QDataStream &ds, vec2 &rhs)
    {
      return ds >> rhs.X >> rhs.Y;
    }
};

inline vec2 operator + (vec2 &v) {
    return vec2(+v.X, +v.Y);
}
inline vec2 operator - (vec2 &v) {
    return vec2(-v.X, -v.Y);
}

MATH_EXPORT extern const vec2 vec2Zero;

typedef QVector<vec2> Vec2List;

inline void swapOrder(vec2& value)
{
  swapOrder(value.X);
  swapOrder(value.Y);
}

Q_DECLARE_RAW_VECTOR_SERIALIZER(vec2)

Q_DECLARE_METATYPE(vec2)
Q_DECLARE_METATYPE(Vec2List)

#endif // VEC2_HPP
