TEMPLATE = lib
TARGET = Core

INCLUDEPATH += ../lib/include

include(../ayuine2c.pri)
include(qtiocompressor/qtiocompressor.pri)

HEADERS += \
           ClassInfo.hpp \
           Logger.hpp \
           Resource.hpp \
    Core.hpp \
    Serializer.hpp \
    ObjectRef.hpp \
    ResourceImporter.hpp \
    ProgressStatus.hpp \
    ObjectDesc_p.hpp \
    SerializerDevice_p.hpp \
    DeserializerDevice_p.hpp
		   
SOURCES += \
           ClassInfo.cpp \
           GadgetInfo.cpp \
    Resource.cpp \
    Core.cpp \
    Serializer.cpp \
    ObjectRef.cpp \
    ResourceImporter.cpp \
    Importers/grc.cpp \
    Importers/grcz.cpp \
    ProgressStatus.cpp \
    ObjectDesc.cpp \
    SerializerDevice.cpp \
    DeserializerDevice.cpp

LIBS += -lpsapi
