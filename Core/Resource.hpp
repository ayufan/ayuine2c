#pragma once

#include <QObject>
#include <QSharedPointer>

#include "Core.hpp"
#include "ObjectRef.hpp"

class CORE_EXPORT Resource : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString objectName READ objectName WRITE setObjectName)
  Q_PROPERTY(bool isLocal READ isLocal WRITE setLocal STORED false)
  Q_PROPERTY(QString fileName READ fileName)
  Q_PROPERTY(QString relativeName READ relativeName)

protected:
  Resource();

public:
  static QDir &getResourceDir();

  static QString absoluteFilePath(const QString& filePath);
  static QString relativeFilePath(const QString& filePath);

public:
  virtual ~Resource();

public:
  static QHash<QString, QWeakPointer<Resource> >& resources();

  static QSharedPointer<Resource> load(const QString& name);
  static QSharedPointer<Resource> load(const QString& name, const QString& otherFileName);

  template<typename T>
  static QSharedPointer<T> load(const QString& name)
  {
    return load(name).objectCast<T>();
  }

  template<typename T>
  static QSharedPointer<T> load(const QString& name, const QString& otherFileName)
  {
    return load(name, otherFileName).objectCast<T>();
  }

  void finish();

  bool isInternal() const;

  void setInternal(bool internal);

public:
  virtual void setObjectName(const QString &name);

signals:
  void objectNameChanged(QObject* object = NULL, const QString& oldName = QString());
  void updated(QObject* object = NULL);

public:
  QString fileName() const;
  QString relativeName() const;
  void setFileName(const QString& fileName);

  bool isLocal() const;
  void setLocal(bool local);

  bool saveFile();
  bool saveFile(const QString& fileName);

private:
  bool m_isLocal;
  QString m_fileName;
  QSharedPointer<Resource> m_internal;
};

CORE_EXPORT void internalLoadResource(QDataStream& ds, QSharedPointer<Resource>& object, const QMetaObject* metaObject);
CORE_EXPORT void internalSaveResource(QDataStream& ds, const QSharedPointer<Resource>& object, const QMetaObject* metaObject);

template<typename T>
inline void internalLoad(const Resource*, QDataStream& ds, QSharedPointer<T>& object)
{
  internalLoadResource(ds, (QSharedPointer<Resource>&)object, &T::staticMetaObject);
}

template<typename T>
inline void internalSave(const Resource*, QDataStream& ds, const QSharedPointer<T>& object)
{
  internalSaveResource(ds, (const QSharedPointer<Resource>&)object, &T::staticMetaObject);
}

// Q_DECLARE_OBJECTREF(Resource)

#if 0
typedef BaseObjectRef<Resource> ResourceRef;
// typedef QVector<ResourceRef> ResourceRefList;

Q_DECLARE_METATYPE(ResourceRef)
// Q_DECLARE_METATYPE(ResourceRefList)

CORE_EXPORT QDataStream &operator << (QDataStream &ds, const ResourceRef &objectRef);
CORE_EXPORT QDataStream &operator >> (QDataStream &ds, ResourceRef &objectRef);

#define Q_DECLARE_RESOURCEREF(Type) \
  Q_DECLARE_OBJECTREF(Type, Resource)

#define Q_IMPLEMENT_RESOURCEREF(Type) \
  Q_IMPLEMENT_OBJECTREF(Type)
#endif
