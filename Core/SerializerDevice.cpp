#include "SerializerDevice_p.hpp"
#include "ProgressStatus.hpp"
#include "Resource.hpp"
#include "Serializer.hpp"

SerializerDevice::SerializerDevice(QIODevice* device)
  : m_device(device)
{
  if(m_device)
  {
    setObjectName(m_device->objectName());
  }
  m_finished = false;
}

qint64 SerializerDevice::pos() const
{
  return m_written;
}

qint64 SerializerDevice::size() const
{
  return m_total;
}

qint64 SerializerDevice::readData(char *data, qint64 maxlen)
{
  return -1;
}

qint64 SerializerDevice::writeData(const char *data, qint64 len)
{
  if(m_device)
  {
    qint64 ret = m_device->write(data, len);
    m_written += len;
    return ret;
  }
  return -1;
}

unsigned SerializerDevice::count()
{
  return m_objects.size();
}

const ObjectDesc& SerializerDevice::operator [] (unsigned index) const
{
  return m_objects[index];
}

ObjectDesc& SerializerDevice::operator [] (unsigned index)
{
  return m_objects[index];
}

inline bool isBaseOf(const char* superObject, const QMetaObject* metaObject)
{
  while(metaObject)
  {
      if(!strcmp(metaObject->className(), superObject))
      return true;
    metaObject = metaObject->superClass();
  }
  return false;
}

unsigned SerializerDevice::getObject(const QSharedPointer<QObject>& object)
{
  if(!object)
    return 0;

  if(!m_refs.contains(object.data()))
  {
    if(m_finished)
      return 0;

    const QMetaObject* metaObject = object->metaObject();

    if(metaObject->constructorCount() == 0)
    {
      qDebug() << "[SERIALIZER] No constructor defined for " << metaObject->className();
      return 0;
    }

    if(object->objectName().length() && m_refsByName[metaObject][object->objectName()])
    {
        QObject* otherObject = m_refsByName[metaObject][object->objectName()];

        for(const QMetaObject* baseObject = metaObject; baseObject; baseObject = baseObject->superClass())
        {
            if(getSettings().value(QByteArray("MergeConflicts/") + baseObject->className(), false).toBool())
            {
                qWarning() << "[SERIALIZER] Merged conflict with other resource" << object.data() << otherObject;
                return m_refs[object.data()] = m_refs[otherObject];
            }
        }

        // qWarning() << "[SERIALIZER] Possible conflict with other resource" << object.data() << otherObject;
    }

    ObjectDesc newObject;
    newObject.className = object->metaObject()->className();
    newObject.refIndex = m_objects.size() + 1;
    newObject.sharedObject = object;
    m_objects.append(newObject);

    m_refs[object.data()] = newObject.refIndex;
    m_refsByName[metaObject][object->objectName()] = object.data();

#ifdef _DEBUG
    // qDebug() << "[SERIALIZER] NewRef:" << newObject.refIndex << object.data();
#endif
    return newObject.refIndex;
  }

  return m_refs[object.data()];
}

void SerializerDevice::serializeObject(QDataStream& ds, QSharedPointer<QObject> object)
{
  QHash<QByteArray, QVariant> values;

  if(object)
  {
    const QMetaObject* metaObject = object->metaObject();

    for(int i=0; i < metaObject->propertyCount(); ++i)
    {
      QMetaProperty property = metaObject->property(i);
      if(!property.isStored(object.data()))
        continue;
      if(!property.isWritable())
        continue;
      values[property.name()] = property.read(object.data());
    }
  }

  ds << values;
}

QVector<ObjectDesc> SerializerDevice::objects() const
{
  return m_objects;
}

NullSerializerDevice::NullSerializerDevice(BaseSerializerDevice *other)
  : m_other(other)
{
  m_written = 0;
}

qint64 NullSerializerDevice::readData(char *data, qint64 maxlen)
{
  return -1;
}

qint64 NullSerializerDevice::writeData(const char *data, qint64 len)
{
  m_written += len;
  return len;
}

qint64 NullSerializerDevice::written() const
{
  return m_written;
}

unsigned NullSerializerDevice::getObject(const QSharedPointer<QObject> &object)
{
  if(m_other)
    return m_other->getObject(object);
  return 0;
}

void internalSaveQObject(QDataStream& ds, const QSharedPointer<QObject>& object, const QMetaObject* metaObject)
{
  QHash<QByteArray, QVariant> values;

  if(BaseSerializerDevice* device = qobject_cast<BaseSerializerDevice*>(ds.device()))
  {
    if(object)
    {
      if(metaObject)
        values["metaObject"] = QByteArray(metaObject->className());
      values["refIndex"] = device->getObject(object);
    }
  }
  else
  {
    qDebug() << "[SERIALIZER] Warning no serializer defined for " << ds.device();
  }

  ds << values;
}

void internalSaveResource(QDataStream& ds, const QSharedPointer<Resource>& object, const QMetaObject* metaObject)
{
  QHash<QByteArray, QVariant> values;

  if(BaseSerializerDevice* device = qobject_cast<BaseSerializerDevice*>(ds.device()))
  {
    if(object)
    {
      if(metaObject)
        values["metaObject"] = QByteArray(metaObject->className());
      if(object->isLocal())
        values["refIndex"] = device->getObject(object);
      else
        values["fileName"] = object->relativeName();
    }
  }
  else
  {
    qDebug() << "[SERIALIZER] Warning no serializer defined for " << ds.device();
  }

  ds << values;
}

void SerializerDevice::walkObjects(const QSharedPointer<QObject> &object)
{
  m_finished = false;
  m_total = 0;

  getObject(object);

  for(unsigned i = 0; i < count(); ++i)
  {
    // prepare null device
    NullSerializerDevice nullDevice(this);
    QDataStream nullStream(&nullDevice);
    nullStream.setByteOrder((QDataStream::ByteOrder)QSysInfo::ByteOrder);    

    // perform null serialization
    if(nullDevice.open(QIODevice::WriteOnly))
    {
      serializeObject(nullStream, m_objects[i].sharedObject);
      m_objects[i].dataSize = nullDevice.written();
      m_total += m_objects[i].dataSize;
    }
  }

  m_finished = true;
}

void SerializerDevice::serializeObjects()
{
  // show progress
  ProgressStatus status(this);

  // write header
  QDataStream ds(this);
  ds.setByteOrder((QDataStream::ByteOrder)QSysInfo::ByteOrder);
  ds << MagicValue;

  // save object list
  ds << m_objects;

  m_written = 0;

  // serialize indiwidual objects
  for(unsigned i = 0; i < count(); ++i)
  {
    qint64 end = m_written + m_objects[i].dataSize;
    status.setValue(m_written);
    serializeObject(ds, m_objects[i].sharedObject);
    if(end != m_written)
    {
       qDebug() << "[SERIALIZER] Incosistency while serializing" << m_objects[i].className << "written" << m_written << "expected" << end;
    }
  }
}

void saveToDevice(const QSharedPointer<QObject>& object, QIODevice& device)
{
  SerializerDevice serializer(&device);
  if(serializer.open(QIODevice::WriteOnly))
  {
    serializer.walkObjects(object);
    serializer.serializeObjects();
  }
}
