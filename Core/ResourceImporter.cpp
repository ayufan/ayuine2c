#include "ResourceImporter.hpp"

ResourceImporter::ResourceImporter()
{
}

ResourceImporter::~ResourceImporter()
{
  foreach(QByteArray key, resourceImporters().keys(this))
  {
    resourceImporters().remove(key);
  }
  foreach(QByteArray key, resourceExporters().keys(this))
  {
    resourceExporters().remove(key);
  }
}

bool ResourceImporter::registerImporter(const QByteArray &extension, const QMetaObject *metaObject, const QByteArray& description)
{
  if(!extension.size())
    return true;
  if(resourceImporters().contains(extension))
    return false;
  ResourceExtension desc;
  desc.extension = extension;
  desc.importer = this;
  desc.metaObject = metaObject ? metaObject : &Resource::staticMetaObject;
  desc.description = description;
  resourceImporters().insert(extension, desc);
  return true;
}

bool ResourceImporter::registerNativeImporter(const QByteArray &extension, const QMetaObject *metaObject, const QByteArray& description)
{
  if(!extension.size())
    return true;
  if(resourceImporters().contains(extension))
    return false;
  ResourceExtension desc;
  desc.extension = extension;
  desc.importer = this;
  desc.metaObject = metaObject ? metaObject : &Resource::staticMetaObject;
  desc.description = description;
  desc.isNative = true;
  resourceImporters().insert(extension, desc);
  return true;
}

bool ResourceImporter::registerExporter(const QByteArray &extension, const QMetaObject *metaObject, const QByteArray& description)
{
  if(!extension.size())
    return true;
  if(resourceExporters().contains(extension))
    return false;
  ResourceExtension desc;
  desc.extension = extension;
  desc.importer = this;
  desc.metaObject = metaObject ? metaObject : &Resource::staticMetaObject;
  desc.description = description;
  resourceExporters().insert(extension, desc);
  return true;
}

bool ResourceImporter::unregisterImporter(const QByteArray &extension)
{
  if(!extension.size())
    return true;
  if(!resourceImporters().contains(extension))
    return false;
  if(resourceImporters()[extension].importer != this)
    return false;
  resourceImporters().remove(extension);
  return true;
}

bool ResourceImporter::unregisterExporter(const QByteArray &extension)
{
  if(!extension.size())
    return true;
  if(!resourceExporters().contains(extension))
    return false;
  if(resourceExporters()[extension].importer != this)
    return false;
  resourceExporters().remove(extension);
  return true;
}

ResourceImporter::ResourceExtensions& ResourceImporter::resourceImporters()
{
  static ResourceExtensions importers;
  return importers;
}

ResourceImporter::ResourceExtensions& ResourceImporter::resourceExporters()
{
  static ResourceExtensions exporters;
  return exporters;
}

QSharedPointer<Resource> ResourceImporter::loadFromFile(const QString &fileName)
{
  QByteArray extension = QFileInfo(fileName).suffix().toLower().toAscii();
  if(!extension.size())
    return QSharedPointer<Resource>();
  return loadFromFile(fileName, extension);
}

QSharedPointer<Resource> ResourceImporter::loadFromFile(const QString &fileName, const QByteArray &extension)
{
  ResourceExtension desc = resourceImporters()[extension];
  if(!desc)
    return QSharedPointer<Resource>();

  QSharedPointer<QFile> io(new QFile(fileName));

  if(io->open(QFile::ReadOnly))
  {
    io->setObjectName(fileName);
    return desc.importer->load(io, extension);
  }

  return QSharedPointer<Resource>();
}

QSharedPointer<Resource> ResourceImporter::loadFromStream(const QSharedPointer<QIODevice> &io, const QByteArray &extension)
{
  ResourceExtension desc = resourceImporters()[extension];
  if(!desc)
  {
    return QSharedPointer<Resource>();
  }
  return desc.importer->load(io, extension);
}

bool ResourceImporter::save(QSharedPointer<Resource> &resource, const QSharedPointer<QIODevice> &device, const QByteArray &extension)
{
  return false;
}

static QStringList extensionList(ResourceImporter::ResourceExtensions& extensions, const QMetaObject *metaObject)
{
  QStringList stringList;

  foreach(ResourceExtension desc, extensions)
  {
    if(!desc.metaObject)
      continue;

    if(desc.metaObject == &Resource::staticMetaObject || !metaObject)
    {
      stringList.append(desc.extension);
    }
    else
    {
      for(const QMetaObject* descMetaObject = desc.metaObject; descMetaObject; descMetaObject = descMetaObject->superClass())
      {
        if(descMetaObject == metaObject)
        {
          stringList.append(desc.extension);
          break;
        }
      }
    }
  }

  return stringList;
}

static QString filterList(ResourceImporter::ResourceExtensions& extensions, const QMetaObject *metaObject, bool includeAllFiles = true)
{
  QHash<QByteArray, QStringList> metaObjects;
  QStringList allExtensions;

  foreach(ResourceExtension desc, extensions)
  {
    if(!desc.metaObject)
      continue;

    if(desc.metaObject == &Resource::staticMetaObject || !metaObject)
    {
      metaObjects[desc.info()].append(desc.extension);
      allExtensions.append(desc.extension);
    }
    else
    {
      for(const QMetaObject* descMetaObject = desc.metaObject; descMetaObject; descMetaObject = descMetaObject->superClass())
      {
        if(descMetaObject == metaObject)
        {
          metaObjects[desc.info()].append(desc.extension);
          allExtensions.append(desc.extension);
          break;
        }
      }
    }
  }

  QStringList filters;

  if(includeAllFiles && allExtensions.size())
  {
    QString filterText;

    filterText.append("All Files (*.");
    filterText.append(allExtensions.join(" *."));
    filterText.append(")");

    filters.append(filterText);
  }

  foreach(const QByteArray &label, metaObjects.keys())
  {
    QString filterText;

    filterText.append(label);
    filterText.append(" (*.");
    filterText.append(metaObjects[label].join(" *."));
    filterText.append(")");

    filters.append(filterText);
  }

  return filters.join(";;");
}


QStringList ResourceImporter::importExtensions(const QMetaObject *metaObject)
{
  return extensionList(resourceImporters(), metaObject);
}

QString ResourceImporter::importFilters(const QMetaObject *metaObject)
{
  return filterList(resourceImporters(), metaObject, true);
}

QStringList ResourceImporter::exportExtensions(const QMetaObject *metaObject)
{
  return extensionList(resourceExporters(), metaObject);
}

QString ResourceImporter::exportFilters(const QMetaObject *metaObject)
{
  return filterList(resourceExporters(), metaObject, false);
}

bool ResourceImporter::saveToFile(QSharedPointer<Resource> &resource, const QString &fileName)
{
  QByteArray extension = QFileInfo(fileName).suffix().toLower().toAscii();
  if(!extension.size())
    return false;
  return saveToFile(resource, fileName, extension);
}

bool ResourceImporter::saveToFile(QSharedPointer<Resource> &resource, const QString &fileName, const QByteArray &extension)
{
  ResourceExtension desc = resourceExporters()[extension];
  if(!desc)
    return false;

  QSharedPointer<QFile> io(new QFile(fileName));

  QFileInfo info(fileName);

  if(info.path().size())
  {
    Resource::getResourceDir().mkpath(info.path());
  }

  if(io->open(QFile::WriteOnly))
  {
    io->setObjectName(fileName);
    return desc.importer->save(resource, io, extension);
  }
  return false;
}

bool ResourceImporter::saveToStream(QSharedPointer<Resource> &resource, const QSharedPointer<QIODevice> &io, const QByteArray &extension)
{
  ResourceExtension desc = resourceExporters()[extension];
  if(!desc)
    return false;
  return desc.importer->save(resource, io, extension);
}

const QMetaObject * ResourceImporter::metaObject(const QSharedPointer<QIODevice> &device, const QByteArray &extension)
{
  return NULL;
}

const QMetaObject * ResourceImporter::metaObjectFromFile(const QString &fileName)
{
  QByteArray extension = QFileInfo(fileName).suffix().toLower().toAscii();
  if(!extension.size())
    return NULL;
  return metaObjectFromFile(fileName, extension);
}

const QMetaObject * ResourceImporter::metaObjectFromFile(const QString &fileName, const QByteArray& extension)
{
  QSharedPointer<QFile> io(new QFile(fileName));

  if(io->open(QFile::ReadOnly))
  {
    io->setObjectName(fileName);
    return metaObjectFromStream(io, extension);
  }
  return NULL;
}

const QMetaObject * ResourceImporter::metaObjectFromStream(const QSharedPointer<QIODevice> &io, const QByteArray &extension)
{
  ResourceExtension desc = resourceImporters()[extension];
  if(!desc)
  {
    return NULL;
  }
  return desc.importer->metaObject(io, extension);
}

bool ResourceImporter::isNative(const QString &fileName)
{
  QByteArray extension = QFileInfo(fileName).suffix().toLower().toAscii();
  if(!extension.size())
    return false;
  return isNative(fileName, extension);
}

bool ResourceImporter::isNative(const QString& fileName, const QByteArray& extension)
{
  ResourceExtension desc = resourceImporters()[extension];
  if(!desc)
    return false;
  return desc.isNative;
}

bool ResourceImporter::isSaveable(const QString &fileName)
{
  QByteArray extension = QFileInfo(fileName).suffix().toLower().toAscii();
  if(!extension.size())
    return false;
  return isSaveable(fileName, extension);
}

bool ResourceImporter::isSaveable(const QString& fileName, const QByteArray& extension)
{
  ResourceExtension desc = resourceExporters()[extension];
  if(!desc)
    return false;
  return true;
}

bool ResourceImporter::isLoadable(const QString &fileName)
{
  QByteArray extension = QFileInfo(fileName).suffix().toLower().toAscii();
  if(!extension.size())
    return false;
  return isLoadable(fileName, extension);
}

bool ResourceImporter::isLoadable(const QString& fileName, const QByteArray& extension)
{
  ResourceExtension desc = resourceImporters()[extension];
  if(!desc)
    return false;
  return true;
}
