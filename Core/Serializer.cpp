#include "ClassInfo.hpp"
#include "Resource.hpp"
#include "Serializer.hpp"
#include "SerializerDevice_p.hpp"
#include "DeserializerDevice_p.hpp"

CORE_EXPORT QSharedPointer<QObject> shallowCopy(const QSharedPointer<QObject>& object)
{
  QBuffer device;
  SerializerDevice serializer(&device);
  DeserializerDevice deserializer(&device);

  if(!serializer.open(QIODevice::WriteOnly) || !deserializer.open(QIODevice::ReadOnly))
    return QSharedPointer<QObject>();

  // walk all objects
  serializer.getObject(object);

  for(unsigned i = 0; i < serializer.count(); ++i)
  {
    if(i != 0 && serializer[i].sharedObject.objectCast<Resource>())
      continue;

    // reset stream
    device.setBuffer(NULL);

    // serialize object to memory
    if(device.open(QBuffer::WriteOnly))
    {
      QDataStream ds(&serializer);
      ds.setByteOrder((QDataStream::ByteOrder)QSysInfo::ByteOrder);
      serializer.serializeObject(ds, serializer[i].sharedObject);
      device.close();
    }

    // create shallow values
    while(deserializer.count() < serializer.count())
    {
      ObjectDesc newDesc = serializer[deserializer.count()];

      if(deserializer.count() == 0 || !newDesc.sharedObject.objectCast<Resource>())
      {
        newDesc.sharedObject = QSharedPointer<QObject>((QObject*)newDesc.sharedObject->metaObject()->newInstance());
        if(!newDesc.sharedObject)
          return QSharedPointer<QObject>();
      }

      deserializer.add(newDesc);
    }

    // deserialize object from memory
    if(device.open(QBuffer::ReadOnly))
    {
      QDataStream ds(&deserializer);
      ds.setByteOrder((QDataStream::ByteOrder)QSysInfo::ByteOrder);
      deserializer.deserializeObject(ds, deserializer[i].sharedObject);
      device.close();
    }
  }

  // get first - is yours
  if(deserializer.count() > 0)
    return deserializer[0].sharedObject;
  return QSharedPointer<QObject>();
}
