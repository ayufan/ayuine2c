#ifndef PROGRESSSTATUS_HPP
#define PROGRESSSTATUS_HPP

#include <QObject>

#include "Core.hpp"

class CORE_EXPORT ProgressStatus : public QObject
{
  Q_OBJECT
public:
  static ProgressStatus* last();

public:
  explicit ProgressStatus(int count = 1);
  explicit ProgressStatus(QIODevice* device);
  ~ProgressStatus();

public:
  int count() const;

  int value() const;
  void setValue(QIODevice* device);
  void setValue(int value);

  QString text() const;
  void setText(const QString& text);

  float progress() const;

signals:
  void valueChanged(ProgressStatus*);
  void textChanged(ProgressStatus*);

private:
  QString m_text;
  int m_value, m_count;
};

#endif // PROGRESSSTATUS_HPP
