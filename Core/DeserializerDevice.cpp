#include "DeserializerDevice_p.hpp"
#include "Resource.hpp"
#include "ClassInfo.hpp"
#include "ProgressStatus.hpp"
#include "Serializer.hpp"

DeserializerDevice::DeserializerDevice(QIODevice* device)
  : m_device(device)
  , m_maxlen(INT_MAX)
{
  if(m_device)
  {
    setObjectName(m_device->objectName());
  }
}

qint64 DeserializerDevice::pos() const
{
  return m_device ? m_device->pos() : -1;
}

qint64 DeserializerDevice::size() const
{
  return m_device ? m_device->size() : -1;
}

qint64 DeserializerDevice::readData(char *data, qint64 maxlen)
{
    if(!maxlen)
        return 0;
    if(m_maxlen <= 0)
        return -1;
    if(m_maxlen < maxlen)
    {
        m_maxlen = -1;
        return -1;
    }
    if(m_device)
    {
        qint64 len = m_device->read(data, maxlen);
        if(len > 0)
            m_maxlen -= len;
        return len;
    }
    return -1;
}

qint64 DeserializerDevice::writeData(const char *data, qint64 len)
{
  return -1;
}

unsigned DeserializerDevice::count()
{
  return m_objects.size();
}

const ObjectDesc& DeserializerDevice::operator [] (unsigned index) const
{
  return m_objects[index];
}

ObjectDesc& DeserializerDevice::operator [] (unsigned index)
{
  return m_objects[index];
}

const QSharedPointer<QObject> &DeserializerDevice::getObject(unsigned refIndex)
{
  static QSharedPointer<QObject> nullObject;
  if(refIndex == 0)
    return nullObject;
  if(refIndex > (unsigned)m_objects.size())
    return nullObject;
  return m_objects[refIndex-1].sharedObject;
}

void DeserializerDevice::add(const ObjectDesc &desc)
{
  m_objects.append(desc);
}

#define DESERIALIZE_DEBUG

void DeserializerDevice::deserializeObject(QDataStream& ds, const QSharedPointer<QObject>& object)
{
  QHash<QByteArray, QVariant> values;
  ds >> values;

  if(ds.status() != QDataStream::Ok)
      return;

  if(object)
  {
    const QMetaObject* metaObject = object->metaObject();

    for(int i=0; i < metaObject->propertyCount(); ++i)
    {
      QMetaProperty property = metaObject->property(i);

      if(values.contains(property.name()))
      {
        if(property.isStored(object.data()) && property.isWritable() && !property.write(object.data(), values[property.name()]))
        {
#ifdef DESERIALIZE_DEBUG
            qDebug() << "[SERIALIZER] Failed to set an " << property.name() << " in " << object.data() << " of " << values[property.name()];
#endif
        }

#ifdef DESERIALIZE_DEBUG
        values.remove(property.name());
#endif
      }
    }
  }

#ifdef _DEBUG
    foreach(QByteArray name, values.keys())
    {
        qDebug() << "[SERIALIZER] No value for key " << name << " in " << object.data();
    }
#endif
}

void internalLoadQObject(QDataStream& ds, QSharedPointer<QObject>& object, const QMetaObject* metaObject)
{
  QHash<QByteArray, QVariant> values;
  ds >> values;

  if(DeserializerDevice* device = qobject_cast<DeserializerDevice*>(ds.device()))
  {
    if(values.contains("refIndex"))
      object = device->getObject(values["refIndex"].toUInt());
    // if(values.contains("metaObject"))
    //  objectRef.theMetaObject = ClassInfo::getMetaObject(values["metaObject"].toByteArray());
  }
  else
  {
    qDebug() << "[SERIALIZER] Warning no deserializer defined for " << ds.device();
  }
}

void internalLoadResource(QDataStream& ds, QSharedPointer<Resource>& object, const QMetaObject* metaObject)
{
  QHash<QByteArray, QVariant> values;
  ds >> values;

  if(DeserializerDevice* device = qobject_cast<DeserializerDevice*>(ds.device()))
  {
    if(values.contains("refIndex"))
      object = device->getObject(values["refIndex"].toUInt()).objectCast<Resource>();
    else if(values.contains("fileName"))
      object = Resource::load(values["fileName"].toString());
    // if(values.contains("metaObject"))
    //  resourceRef.theMetaObject = ClassInfo::getMetaObject(values["metaObject"].toByteArray());
  }
  else
  {
    qDebug() << "[SERIALIZER] Warning no deserializer defined for " << ds.device();
  }
}

inline bool readHeader(QDataStream& ds)
{
#if 0
  ds.setByteOrder(QDataStream::BigEndian);
#else
  ds.setByteOrder(QDataStream::LittleEndian);

  unsigned magicValue = 0;
  ds >> magicValue;

  if(magicValue != MagicValue && magicValue != MagicValueOther)
  {
    return false;
  }

  if(magicValue == MagicValueOther)
  {
    ds.setByteOrder(QDataStream::BigEndian);
  }
#endif
  return true;
}

bool DeserializerDevice::deserializeObjects(QDataStream &ds)
{
  ProgressStatus status(ds.device());

  // deserialize object list
  ds >> m_objects;

  // create object instances
  for(unsigned i = 0; i < count(); ++i)
  {
    ObjectDesc& desc = m_objects[i];
    desc.sharedObject = ClassInfo::createNewInstance(desc.className);
    if(desc.sharedObject.data() == NULL)
      return false;
  }

  // deserialize from stream
  for(unsigned i = 0; i < count(); ++i)
  {
    // m_maxlen = m_objects[i].dataSize;
    status.setValue(this);
    deserializeObject(ds, m_objects[i].sharedObject);
    if(ds.status() != QDataStream::Ok)
    {
        qDebug() << "[SERIALIZER] " << m_objects[i].className << " ds in wrong state:" << ds.status();
        return false;
    }
#if 0
    if(m_maxlen != 0)
    {
        qDebug() << "[SERIALIZER]" << m_objects[i].className << "ds read error" << m_maxlen;
        return false;
    }
#endif
  }

  return m_objects.size() > 0;
}

QSharedPointer<QObject> loadFromDevice(QIODevice& device)
{
  DeserializerDevice deserializer(&device);
  if(!deserializer.open(QIODevice::ReadOnly))
    return QSharedPointer<QObject>();

  // read header
  QDataStream ds(&deserializer);
  if(!readHeader(ds))
    return QSharedPointer<QObject>();

  // deserialize objects
  if(!deserializer.deserializeObjects(ds))
    return QSharedPointer<QObject>();

  // get first - is yours
  return deserializer[0].sharedObject;
}

QSharedPointer<QObject> loadFromFile(const QString& fileName)
{
  QFile file(fileName);

  if(file.open(QFile::ReadOnly))
  {
    file.setObjectName(fileName);
    return loadFromDevice(file);
  }
  return QSharedPointer<QObject>();
}

const QMetaObject* peakMetaObjectFromDevice(QIODevice &device)
{
  // read header
  QDataStream ds(&device);
  if(!readHeader(ds))
    return NULL;

  // get object count
  quint32 count = 0;
  ds >> count;

  if(count)
  {
    // get first one
    ObjectDesc firstObject;
    ds >> firstObject;
    return ClassInfo::getMetaObject(firstObject.className);
  }

  return NULL;
}

const QMetaObject* peakMetaObjectFromFile(const QString& fileName)
{
  QFile file(fileName);

  if(file.open(QFile::ReadOnly))
  {
    file.setObjectName(fileName);
    return peakMetaObjectFromDevice(file);
  }
  return NULL;
}

