#include "ClassInfo.hpp"
#include "Resource.hpp"
#include <QObject>
#include <QHash>
#include <QMetaType>
#include <QDebug>
#include <QIcon>
#include <windows.h>

static QHash<int, const GadgetInfo*>& staticGadgets()
{
  static QHash<int, const GadgetInfo*> array;
  return array;
}

GadgetInfo::GadgetInfo(const QMetaObject* metaObject, int typeId) : m_metaObject(metaObject), m_typeId(typeId)
{
  Q_ASSERT(metaObject);
  if(staticGadgets()[m_typeId] != NULL)
    return;
  staticGadgets()[m_typeId] = this;
}

GadgetInfo::~GadgetInfo()
{
  if(staticGadgets()[m_typeId] != this)
    return;
  staticGadgets().remove(m_typeId);
}

const QMetaObject *GadgetInfo::metaObject() const
{
  return m_metaObject;
}

int GadgetInfo::typeId() const
{
  return m_typeId;
}

QList<const GadgetInfo *> GadgetInfo::allGadgets()
{
  return staticGadgets().values();
}

bool GadgetInfo::staticIsGadget(const QVariant &value)
{
  return staticGadgetInfo(value) != NULL;
}

const GadgetInfo *GadgetInfo::staticGadgetInfo(const QVariant &value)
{
  if(value.type() != QVariant::UserType)
    return NULL;
  if(!staticGadgets().contains(value.userType()))
    return NULL;
  return staticGadgets()[value.userType()];
}

const QMetaObject* GadgetInfo::staticMetaObject(const QVariant &value)
{
  const GadgetInfo* gadget = staticGadgetInfo(value);
  if(!gadget)
    return NULL;
  return gadget->m_metaObject;
}

QVariant GadgetInfo::staticProperty(const QVariant &value, const char *name)
{
  const GadgetInfo* gadget = staticGadgetInfo(value);
  if(!gadget)
    return NULL;
  return gadget->property(value, name);
}

bool GadgetInfo::staticSetProperty(QVariant &value, const char *name, const QVariant &newValue)
{
  const GadgetInfo* gadget = staticGadgetInfo(value);
  if(!gadget)
    return NULL;
  return gadget->setProperty(value, name, newValue);
}

const GadgetInfo *GadgetInfo::staticGadgetInfo(const QByteArray &name)
{
  int type = QMetaType::type(name.constData());
  if(!staticGadgets().contains(type))
    return NULL;
  return staticGadgets()[type];
}

const QMetaObject *GadgetInfo::staticMetaObject(const QByteArray &name)
{
  const GadgetInfo* gadget = staticGadgetInfo(name);
  if(!gadget)
    return NULL;
  return gadget->m_metaObject;
}
